from datetime import datetime, timedelta
import pytest
from wowengine.stats import GenerationStats, BannerStats, SourceStats, ArticleStats, ArticleTopChart, ActiveItemsStats


# fixture for real redis, use only in extreme debugging cases
# @pytest.fixture
# def redisdb():
# return redis.StrictRedis()

# ## СТАТИСТИКА ПО ИСТОЧНИКАМ

# дано:
# today            10={show: 50, click=15}, 20={show: 40, click: 8}
# 05.03.2014 00:12 10={show: 40, click=5}, 20={show: 30, click: 3}

# должны получить
# today            10={show: 50, click=15}, 20={show: 40, click: 8}
# total            10={show: 90, click=20}, 20={show: 70, click: 11}

@pytest.fixture
def source_stats(redisdb):
    """
    Наполнить редис данными.
    """
    stats = SourceStats(redisdb)

    def set_stats(item_id, show_amount, click_amount, dt):
        stats.register_shown(item_id, amount=show_amount, ts=dt.timestamp())
        stats.register_clicked(item_id, amount=click_amount, ts=dt.timestamp())

    set_stats(10, 50, 15, datetime.now())
    set_stats(20, 40, 8, datetime.now())
    set_stats(10, 40, 5, datetime(2014, 3, 5, 0, 12))
    set_stats(20, 30, 3, datetime(2014, 3, 5, 0, 12))

    return stats


@pytest.mark.parametrize("single_method_name,all_method_name,expected_10,expected_20", [
    ('get_for_today', 'get_all_for_today', (50, 15, 15 / 50), (40, 8, 8 / 40)),
    ('get_total', 'get_all_total', (90, 20, 20 / 90), (70, 11, 11 / 70)),
])
def test_source_stats(source_stats, single_method_name, all_method_name, expected_10, expected_20):
    """
    Проверить результаты для конкретных баннеров.
    """

    def result(shown, clicked, ctr):
        return {'show': shown, 'click': clicked, 'ctr': ctr}

    single_method = getattr(source_stats, single_method_name)
    assert single_method(10) == result(*expected_10)
    assert single_method(20) == result(*expected_20)

    all_method = getattr(source_stats, all_method_name)
    assert all_method() == {10: result(*expected_10),
                            20: result(*expected_20)}


# ## СТАТИСТИКА ПОКАЗОВ/КЛИКОВ/CTR (BannerStats)

# дано: (id=count)

# 05.03.2014 00:12 10=40,10, 20=30,5
# 05.03.2014 00:37 10=50,10, 20=100,5
# 05.03.2014 00:46 10=10,10, 20=40,5
# 05.03.2014 00:58 10=50,10, 20=100,5

# 05.03.2014 01:50 10=70,10, 20=80,5

# 06.03.2014 12:16 10=10,10, 20=20,5
# 07.08.2014 14:08 10=60,10, 20=50,5

# должны получить
# на 05.03.2014 01:00
# 5min  10=50,10,  20=100,5
# 15min 10=60,20,  20=140,10
# 30min 10=110,30, 20=240,15
# 60min 10=150,40 20=270,20

# на 06.03.2014 15:48
# today      10=10,10, 20=20,5
# yesterday  10=220,50, 20=350,25
# this month 10=230,60, 20=370,30

# total 10=290,70, 20=420,35

@pytest.fixture
def banner_stats(redisdb):
    """
    Наполнить редис данными.
    """
    stats = BannerStats(redisdb)

    def set_stats(shown_10, clicked_10, shown_20, clicked_20, dt):
        stats.register_shown(10, amount=shown_10, ts=dt.timestamp())
        stats.register_clicked(10, amount=clicked_10, ts=dt.timestamp())
        stats.register_shown(20, amount=shown_20, ts=dt.timestamp())
        stats.register_clicked(20, amount=clicked_20, ts=dt.timestamp())

    set_stats(40, 10, 30, 5, datetime(2014, 3, 5, 0, 12))
    set_stats(50, 10, 100, 5, datetime(2014, 3, 5, 0, 37))
    set_stats(10, 10, 40, 5, datetime(2014, 3, 5, 0, 46))
    set_stats(50, 10, 100, 5, datetime(2014, 3, 5, 0, 58))
    set_stats(70, 10, 80, 5, datetime(2014, 3, 5, 1, 50))
    set_stats(10, 10, 20, 5, datetime(2014, 3, 6, 12, 16))
    set_stats(60, 10, 50, 5, datetime(2014, 8, 7, 14, 8))

    return stats


@pytest.mark.parametrize("method_name,dt_args,expected", [
    ('get_for_5min', (2014, 3, 5, 1, 0, 34), (50, 10, 100, 5)),
    ('get_for_15min', (2014, 3, 5, 1, 0, 34), (60, 20, 140, 10)),
    ('get_for_30min', (2014, 3, 5, 1, 0, 34), (110, 30, 240, 15)),
    ('get_for_60min', (2014, 3, 5, 1, 0, 34), (150, 40, 270, 20)),
    ('get_for_today', (2014, 3, 6, 15, 48), (10, 10, 20, 5)),
    ('get_for_yesterday', (2014, 3, 6, 15, 48), (220, 50, 350, 25)),
    ('get_for_month', (2014, 3, 6, 15, 48), (230, 60, 370, 30)),
    ('get_total', (2014, 9, 25), (290, 70, 420, 35)),
])
def test_banner_stats(banner_stats, method_name, dt_args, expected):
    """
    Проверить результаты.
    """

    def result(shown_10, clicked_10, shown_20, clicked_20):
        h = lambda s, c: {'in': 0, 'out': 0, 'gen': 0,
                          'show': s, 'click': c, 'ctr': 0 if s == 0 else c / s}
        return {10: h(shown_10, clicked_10),
                20: h(shown_20, clicked_20)}

    method = getattr(banner_stats, method_name)
    # can't test equality with Counter, must test each key separately
    d = method(ts=datetime(*dt_args).timestamp())
    for i, r in result(*expected).items():
        for k, v in r.items():
            assert d[i][k] == v


def test_single_banner_stats(banner_stats):
    h = lambda s, c: {'in': 0, 'out': 0, 'gen': 0,
                      'show': s, 'click': c, 'ctr': 0 if s == 0 else c / s}

    assert banner_stats.get_total_for_id(10) == h(290, 70)
    assert banner_stats.get_total_for_id(20) == h(420, 35)
    assert banner_stats.get_total_for_id(23) == h(0, 0)


@pytest.mark.parametrize("dt_args,expected", [
    ((2014, 3, 5), {10: 100 / 440, 20: 50 / 700}),
    ((2014, 3, 6), {10: 70 / 240, 20: 35 / 390}),
])
def test_average_ctrs(banner_stats, dt_args, expected):
    assert banner_stats.get_average_ctrs(min_show=10, ts=datetime(*dt_args).timestamp()) == expected


def test_get_for_each_5min_in_60min(banner_stats: BannerStats):
    val = banner_stats.get_for_each_5min_in_60min(
        10, ts=datetime(2014, 3, 5).timestamp())
    assert val == [(datetime(2014, 3, 5, 0, 0), {'show': 0, 'in': 0,
                                                 'out': 0, 'gen': 0,
                                                 'click': 0, 'ctr': 0}),
                   (datetime(2014, 3, 5, 0, 5), {'show': 0, 'in': 0,
                                                 'out': 0, 'gen': 0,
                                                 'click': 0, 'ctr': 0}),
                   (datetime(2014, 3, 5, 0, 10), {'show': 40, 'in': 0,
                                                  'out': 0, 'gen': 0,
                                                  'click': 10, 'ctr': 0.25}),
                   (datetime(2014, 3, 5, 0, 15), {'show': 0, 'in': 0,
                                                  'out': 0, 'gen': 0,
                                                  'click': 0, 'ctr': 0}),
                   (datetime(2014, 3, 5, 0, 20), {'show': 0, 'in': 0,
                                                  'out': 0, 'gen': 0,
                                                  'click': 0, 'ctr': 0}),
                   (datetime(2014, 3, 5, 0, 25), {'show': 0, 'in': 0,
                                                  'out': 0, 'gen': 0,
                                                  'click': 0, 'ctr': 0}),
                   (datetime(2014, 3, 5, 0, 30), {'show': 0, 'in': 0,
                                                  'out': 0, 'gen': 0,
                                                  'click': 0, 'ctr': 0}),
                   (datetime(2014, 3, 5, 0, 35), {'show': 50, 'in': 0,
                                                  'out': 0, 'gen': 0,
                                                  'click': 10, 'ctr': 0.2}),
                   (datetime(2014, 3, 5, 0, 40), {'show': 0, 'in': 0,
                                                  'out': 0, 'gen': 0,
                                                  'click': 0, 'ctr': 0}),
                   (datetime(2014, 3, 5, 0, 45), {'show': 10, 'in': 0,
                                                  'out': 0, 'gen': 0,
                                                  'click': 10, 'ctr': 1.0}),
                   (datetime(2014, 3, 5, 0, 50), {'show': 0, 'in': 0,
                                                  'out': 0, 'gen': 0,
                                                  'click': 0, 'ctr': 0}),
                   (datetime(2014, 3, 5, 0, 55), {'show': 50, 'in': 0,
                                                  'out': 0, 'gen': 0,
                                                  'click': 10, 'ctr': 0.2})]


def test_get_for_range(banner_stats: BannerStats):
    assert banner_stats.get_for_range(datetime(2014, 1, 1),
                                      datetime(2014, 10, 10)) \
        == {10: {"ctr": 0.2413793103448276,
                 "gen": 0,
                 "click": 70,
                 "in": 0,
                 "out": 0,
                 "show": 290},
            20: {"ctr": 0.08333333333333333,
                 "gen": 0,
                 "click": 35,
                 "in": 0,
                 "out": 0,
                 "show": 420}}
    assert banner_stats.get_for_range(datetime(2014, 5, 5),
                                      datetime(2014, 9, 9)) \
        == {10: {"in": 0,
                 "out": 0,
                 "ctr": 0.16666666666666666,
                 "click": 10,
                 "show": 60,
                 "gen": 0},
            20: {"in": 0,
                 "out": 0,
                 "ctr": 0.1,
                 "click": 5,
                 "show": 50,
                 "gen": 0}}


# ## СТАТИСТИКА ЗАХОДОВ/УХОДОВ/ГЕНЕРАЦИИ (GenerationStats)

# дано:
# 01.03.2014 00:16 in=10 out=5
# 01.03.2014 00:42 in=80 out=60
# 01.03.2014 07:44 in=25 out=10

# должны получить
# 5min 01.03.2014 00:15 in=10 out=5
# 5min 01.03.2014 00:40 in=80 out=60
# 5min 01.03.2014 07:40 in=25 out=10
# hour 01.03.2014 00 in=90 out=65
# hour 01.03.2014 07 in=25 out=10
# day 01.03.2014 total in=115 out=75

# любой другой день по нулям

@pytest.fixture
def gen_stats(redisdb):
    """
    Наполнить редис данными.
    """
    stats = GenerationStats(redisdb)

    def set_stats(in_amount, out_amount, dt):
        stats.register_visit(amount=in_amount, ts=dt.timestamp())
        stats.register_exit(amount=out_amount, ts=dt.timestamp())

    set_stats(10, 5, datetime(2014, 3, 1, 0, 16))
    set_stats(80, 60, datetime(2014, 3, 1, 0, 42))
    set_stats(25, 10, datetime(2014, 3, 1, 7, 44))

    return stats


@pytest.mark.parametrize("method_name,dt_args,expected", [
    ('get_for_5min', (2014, 3, 1, 0, 15), (10, 5, 0.5)),
    ('get_for_5min', (2014, 3, 1, 0, 40), (80, 60, 6 / 8)),
    ('get_for_5min', (2014, 3, 1, 7, 40), (25, 10, 10 / 25)),
    ('get_for_hour', (2014, 3, 1, 0), (90, 65, 65 / 90)),
    ('get_for_hour', (2014, 3, 1, 7), (25, 10, 10 / 25)),
    ('get_for_day', (2014, 3, 1), (115, 75, 75 / 115)),
    ('get_for_day', (2014, 4, 3), (0, 0, 0)),
])
def test_generation_stats(gen_stats, method_name, dt_args, expected):
    """
    Проверить результаты.
    """

    def result(_in, _out, _gen):
        return {'in': _in, 'out': _out, 'gen': _gen,
                'li_in': 0, 'li_out': 0, 'li_gen': 0}

    method = getattr(gen_stats, method_name)
    assert method(ts=datetime(*dt_args).timestamp()) == result(*expected)


def test_get_for_all_days(gen_stats):
    assert gen_stats.get_for_all_days() == [
        (datetime(2014, 3, 1).timestamp(), {
            'in': 115,
            'out': 75,
            'gen': 75 / 115,
            'li_in': 0, 'li_out': 0, 'li_gen': 0
        })
    ]
    assert gen_stats.get_for_all_days(ts_end=datetime(1975, 12, 3).timestamp()) == []


# СТАТИСТИКА ПОПУЛЯРНОСТИ СТАТЕЙ

# дано: {id: hits}
# 01.03.2014 07:40 {1: 7, 2: 6, 3: 5}
# 02.03.2014 19:50 {1: 2, 2: 3, 3: 4}

# должны получить
# на 01.03 = [1, 2, 3]
# на 02.03 = [3,2,1]

# любой другой день = []


@pytest.fixture
def article_top_chart(redisdb):
    """
    Наполнить редис данными.

    :rtype: ArticleStats
    """
    stats = ArticleTopChart(redisdb)

    given_data = {
        int(datetime(2014, 3, 1, 7, 40).timestamp()): {1: 7, 2: 6, 3: 5},
        int(datetime(2014, 3, 2, 19, 50).timestamp()): {1: 2, 2: 3, 3: 4}
    }

    for ts, data in given_data.items():
        for i, v in data.items():
            stats.register_shown(i, amount=v, ts=ts)

    return stats


@pytest.mark.parametrize("method_name, dt_args,expected", [
    ('get_top_popular', (2014, 3, 1, 0, 15), [1, 2, 3]),
    ('get_top_popular', (2014, 3, 2, 0, 15), [3, 2, 1]),
    ('get_top_popular_with_scores', (2014, 3, 1, 0, 15), [(1, 14), (2, 12), (3, 10)]),
    ('get_top_popular_with_scores', (2014, 3, 2, 0, 15), [(3, 13), (2, 12), (1, 11)])
])
def test_article_stats(article_top_chart, method_name, dt_args, expected):
    """
    Проверить результаты.

    :type article_stats: ArticleStats
    :type method_name: str
    :type dt_args: (int, int, int, int, int)
    :type expected: list[int]
    :return: None
    """
    method = getattr(article_top_chart, method_name)
    ts = datetime(*dt_args).timestamp()

    assert method(10, ts=ts) == expected


class TestActiveItemsStats(object):
    @pytest.fixture
    def stats(self, redisdb):
        return ActiveItemsStats(redisdb)

    def _hist_set_equal(self, stats, hist_name, ts, ids):
        hist = stats.t.get_histogram('5min', hist_name, ts=ts.timestamp())
        return set(hist.keys()) == set(ids)

    def _test_register_items(self, stats, method, item_type):
        now_date = datetime.now()
        start_date = now_date - timedelta(minutes=5)
        start_ids = ['1', '2', '3']
        method(start_ids, ts=start_date.timestamp())
        # When registered first time:
        assert self._hist_set_equal(stats, item_type, start_date, start_ids)
        now_ids = ['2', '4']
        method(now_ids, ts=now_date.timestamp())
        # When registered in next 5 minute:
        assert self._hist_set_equal(stats, item_type, now_date, now_ids)

    def test_register_banners(self, stats):
        self._test_register_items(stats, stats.register_banners, 'banners')

    def test_register_sources(self, stats):
        self._test_register_items(stats, stats.register_sources, 'sources')

    def _list_vals_to_sets(self, items):
        return [{key: set(val) if isinstance(val, list) else val
                 for key, val in dct.items()}
                for dct in items]

    def _test_get_items_for_day(self, register_method, get_method):
        start_ts = 1000000000
        register_method([1, 2, 3], ts=start_ts)
        register_method([2, 3, 4], ts=start_ts + 300)
        register_method([3, 4, 5], ts=start_ts + 600)
        register_method([], ts=start_ts + 900)
        assert self._list_vals_to_sets(get_method(start_ts)[:5])\
            == self._list_vals_to_sets([
                {'added': ['3', '1', '2'], 'ts': 1000000000,
                 'removed': [], 'count': 3},
                {'added': ['4'], 'ts': 1000000300,
                 'removed': ['1'], 'count': 3},
                {'added': ['5'], 'ts': 1000000600,
                 'removed': ['2'], 'count': 3},
                {'added': [], 'ts': 1000000900,
                 'removed': ['4', '3', '5'], 'count': 0},
                {'added': [], 'ts': 1000001200,
                 'removed': [], 'count': 0}])

    def test_get_banners_for_day(self, stats):
        self._test_get_items_for_day(stats.register_banners,
                                     stats.get_banners_for_day)

    def test_get_sources_for_day(self, stats):
        self._test_get_items_for_day(stats.register_sources,
                                     stats.get_sources_for_day)
