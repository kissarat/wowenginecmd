from datetime import datetime
import pytest
from unittest.mock import patch
from wowengine.models import NewsItem, Banner, Source
from wowengine.stats import BannerStats, ArticleStats


@pytest.fixture
def banner_stats(redisdb, request):
    stats = BannerStats(redisdb)
    p = patch('wowengine.admin.views.banner_stats',
              new_callable=lambda: stats)
    request.addfinalizer(p.stop)
    return p.start()


@pytest.fixture
def article_stats(redisdb, request):
    stats = ArticleStats(redisdb)
    p = patch('wowengine.admin.views.article_stats',
              new_callable=lambda: stats)
    request.addfinalizer(p.stop)
    return p.start()


@pytest.mark.usefixtures('redisdb')
def test_stats_api_for_news(client, db_session, article_stats):
    news_item = NewsItem(title='title',
                         is_gallery=False,
                         is_published=True)
    db_session.add(news_item)
    db_session.commit()
    article_stats.register_clicked(news_item.id)
    response = client.get('/ed/api/stats?type=news&period=month')
    print(response.json)
    assert response.json['data'][0]['click'] == 1


@pytest.mark.usefixtures('redisdb')
def test_stats_api_for_banners(client, db_session, banner_stats):
    source = Source(title='source')
    db_session.add(source)
    banner = Banner(title='title',
                    is_gallery=False,
                    is_published=True,
                    source=source)
    db_session.add(banner)
    db_session.commit()
    banner_stats.register_shown(banner.id)
    response = client.get('/ed/api/stats?type=banners&period=month')
    assert response.json['data'][0]['show'] == 1


@pytest.mark.usefixtures('redisdb')
def test_stats_api_for_whole(client, db_session, banner_stats, article_stats):
    source = Source(title='source')
    db_session.add(source)
    banner = Banner(title='title',
                    is_gallery=False,
                    is_published=True,
                    source=source)
    db_session.add(banner)
    news_item = NewsItem(title='title',
                         is_gallery=False,
                         is_published=True,
                         source=source)
    db_session.add(news_item)
    db_session.commit()
    article_stats.register_visit(news_item.id)
    banner_stats.register_exit(banner.id)
    response = client.get('/ed/api/stats?type=whole&period=month')
    assert len(response.json['data']) == 5


@pytest.mark.usefixtures('redisdb')
def test_stats_api_for_news_for_range(client, db_session, article_stats):
    news_item = NewsItem(title='title',
                         is_gallery=False,
                         is_published=True)
    db_session.add(news_item)
    db_session.commit()
    article_stats.register_clicked(news_item.id,
                                   ts=datetime(2010, 10, 10).timestamp())
    article_stats.register_clicked(news_item.id,
                                   ts=datetime(2014, 10, 10).timestamp())
    # From 2010 to 2015:
    response = client.get('/ed/api/stats?type=news&period=range'
                          '&range_start=2010.10.01&range_end=2015.01.01')
    assert response.json['data'][0]['click'] == 2
    # From 2013 to 2015:
    response = client.get('/ed/api/stats?type=news&period=range'
                          '&range_start=2013.10.01&range_end=2015.01.01')
    assert response.json['data'][0]['click'] == 1


def test_auction_total_stats(monkeypatch, db_session, client):
    # Mocks auction:
    monkeypatch.setattr(
        'wowengine.auction.Auction.get_stats',
        lambda *_: {'total_stats': {"1": [0, 1, 0, 0],
                                    "2": [1, 1, 1, 1]},
                    'queue': [],
                    'result': []})
    # Ensures all stats presents in the result:
    response = client.get('/ed/api/auction_stats/overall/1')
    assert len(response.json['total_stats']) == 2
