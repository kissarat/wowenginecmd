import pytest
from flask_sqlalchemy import SignallingSession
from wowengine.models import Source, Banner, NewsItem, Line
from datetime import datetime


@pytest.fixture
def source():
    """
    Source with active times set to be always on.
    """
    s = Source()
    for d in ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']:
        setattr(s, 'active_%s' % d, True)
        setattr(s, 'active_%s_start' % d, 0)
        setattr(s, 'active_%s_end' % d, 24 * 60)
    return s


# Dec 3, 1975 was Wednesday.
# Every check will run for 14:00 Dec 3, 1975
@pytest.mark.parametrize("is_wed_active,start,end,expected", [
    (True, 0, 24 * 60, True),  # активен всю среду
    (False, 0, 24 * 60, False),  # среда отключена
    (True, 0, 8*60, False),  # среда включена с полночи до восьми утра
    (True, 12*60, 20*60, True),  # среда включена с полдня до восьми вечера
])
def test_source_is_active(source, is_wed_active, start, end, expected):
    """
    Проверка метода Source.is_active
    """
    source.active_wed = is_wed_active
    source.active_wed_start = start
    source.active_wed_end = end
    ts = datetime(1975, 12, 3, 14).timestamp()
    assert source.is_active(ts) == expected


def _ids(items):
    return [x.id for x in items]


def _make_banners(db_session, n, **kwargs):
    for n in range(n):
        banner = Banner(title='banner-{}'.format(n),
                        is_gallery=False,
                        **kwargs)
        db_session.add(banner)
        yield banner


@pytest.fixture
def source_news_item_for_banners(db_session):
    source = Source(title='test source')
    db_session.add(source)
    news_item = NewsItem(title='test news item',
                         is_gallery=False,
                         source=source)
    db_session.add(news_item)
    return source, news_item


def _assert_items_equal_and_shuffled(xs, ys):
    assert set(xs) == set(ys)
    assert xs != ys

@pytest.mark.usefixtures('redis_proc')
def test_banner_fetch_for_extra_block1_when_all_banners_available(
        db_session: SignallingSession,
        source_news_item_for_banners: (Source, NewsItem)):
    """
    Ensures that when 4 banners for extra block available
    `fetch_for_extra_block1` returns them.
    """
    source, news_item = source_news_item_for_banners
    banners = list(_make_banners(db_session, 4,
                                 source=source,
                                 in_extra_block1=True,
                                 is_published=True))
    db_session.commit()
    _, fetched_banners = Banner.fetch_for_extra_block1(item_id=news_item.id)
    _assert_items_equal_and_shuffled(_ids(fetched_banners),
                                     _ids(banners))

@pytest.mark.usefixtures('redis_proc')
def test_banner_fetch_for_extra_block1_when_not_all_banners_available(
        db_session: SignallingSession,
        source_news_item_for_banners: (Source, NewsItem)):
    """
    Ensures that when less then 4 banners for extra block available
    `fetch_for_extra_block1` returns them plus banners from whole mass.
    """
    source, news_item = source_news_item_for_banners
    banners = list(_make_banners(db_session, 2,
                                 source=source,
                                 in_extra_block1=True,
                                 is_published=True))
    banners += list(_make_banners(db_session, 2,
                                  source=source,
                                  is_published=True))
    db_session.commit()
    _, fetched_banners = Banner.fetch_for_extra_block1(item_id=news_item.id)
    _assert_items_equal_and_shuffled(_ids(fetched_banners),
                                     _ids(banners))

@pytest.mark.usefixtures('redis_proc')
def test_fetch_for_auction_ignores_banners_for_extra_block1(
        db_session: SignallingSession,
        source_news_item_for_banners: (Source, NewsItem)):
    """
    Ensures that `fetch_for_auction` not returns banners for extra block 1.
    """
    source, news_item = source_news_item_for_banners
    extra_block_banners = list(_make_banners(db_session, 2,
                                             source=source,
                                             in_extra_block1=True,
                                             is_published=True))
    db_session.commit()
    auction_banners_ids = _ids(Banner.fetch_for_auction(news_item.id, 4))
    assert not set(_ids(extra_block_banners)).intersection(auction_banners_ids)


@pytest.mark.usefixtures('redis_proc')
def test_fetch_for_item_page_banners_for_extra_block1(
        db_session: SignallingSession,
        source_news_item_for_banners: (Source, NewsItem)):
    """
    Ensures that `fetch_for_item_page` not returns banners for extra block 1.
    """
    source, news_item = source_news_item_for_banners
    extra_block_banners = list(_make_banners(db_session, 2,
                                             source=source,
                                             in_extra_block1=True,
                                             is_published=True))
    db_session.commit()
    auction_banners_ids = _ids(Banner.fetch_for_item_page(news_item.id))
    assert not set(_ids(extra_block_banners)).intersection(auction_banners_ids)


@pytest.mark.usefixtures('redis_proc')
def test_delete_source_cascade_deletes_related_banners(
        db_session: SignallingSession):
    """
    Ensures that when sources deleted all related banners will
    be deleted automatically.
    """
    source = Source(title='test-source')
    db_session.add(source)
    all(_make_banners(db_session, 5,
                      source=source,
                      is_published=True))
    db_session.commit()
    assert Banner.query.count() == 5
    db_session.delete(source)
    assert Banner.query.count() == 0


def test_fetch_list_item_page(db_session):
    """Ensures that the function returns only published news ordered by
    publication date.

    """
    source = Source(title='test source')
    db_session.add(source)
    news, news_1, news_2 = [NewsItem(source=source,
                                     is_published=True,
                                     is_gallery=False,
                                     title='title-{}'.format(n),
                                     date=datetime(2014, 1, n + 1))
                            for n in range(3)]
    db_session.add(news)
    db_session.add(news_1)
    db_session.add(news_2)
    news_3 = NewsItem(source=source,
                      is_published=False,
                      is_gallery=False,
                      title='news-3')
    db_session.add(news_3)
    db_session.commit()
    assert NewsItem.fetch_list_for_item_page(news.id) == [news_2, news_1, news]


def test_lines_fetch_random_active(db_session):
    """Ensures that the function returns only published lines."""
    not_published = Line(title='active line',
                         is_published=False,
                         is_gallery=False)
    db_session.add(not_published)
    db_session.commit()
    # When we don't have a published line:
    assert Line.fetch_random_active() is None
    published = Line(title='active line',
                     is_published=True,
                     is_gallery=True)
    db_session.add(published)
    db_session.commit()
    # When we have:
    assert Line.fetch_random_active() == published

