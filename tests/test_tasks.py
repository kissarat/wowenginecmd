import pytest
from unittest.mock import Mock
from wowengine.models import Banner, Source
from wowengine.tasks import sync_active_items


@pytest.mark.usefixtures('redis_proc')
class TestSyncActiveItems(object):
    @pytest.fixture
    def stats(self, monkeypatch):
        mock = Mock()
        monkeypatch.setattr('wowengine.tasks.active_items_stats', mock)
        return mock

    def test_sync_banners(self, stats, db_session):
        source = Source(title='source')
        db_session.add(source)
        active_banner = Banner(title='active',
                               is_gallery=False,
                               is_published=True,
                               source=source)
        db_session.add(active_banner)
        non_active_banner = Banner(title='non-active',
                                   is_gallery=False,
                                   is_published=False,
                                   source=source)
        db_session.add(non_active_banner)
        db_session.commit()
        sync_active_items.run()

        assert stats.register_banners.call_args_list[0][0] \
               == ([active_banner.id],)

    def test_sync_sources(self, stats, db_session, monkeypatch):
        monkeypatch.setattr('wowengine.models.Source.is_active',
                            lambda self, ts: self.title == 'active')
        active_source = Source(title='active')
        db_session.add(active_source)
        non_active_source = Source(title='non-active')
        db_session.add(non_active_source)
        db_session.commit()
        sync_active_items.run()

        assert stats.register_sources.call_args_list[0][0] \
               == ([active_source.id],)
