from unittest.mock import MagicMock
import pytest
from wowengine.auction import Auction
from wowengine.site_config import SiteConfig
from wowengine.stats import SuperCache
from wowengine.models import Item
import time


# This is for testing on live redis server:

# @pytest.fixture
# def redisdb(request):
#     redis = StrictRedis(decode_responses=True)
#
#     def fin():
#         redis.flushdb()
#
#     request.addfinalizer(fin)
#
#     return redis


@pytest.mark.usefixtures('app')
def test_auction_cycle(redisdb, db_session):
    site_config = SiteConfig(redisdb, defaults={
        'auction_max_age_days': 1,
        'auction_shows_count': 3,
    })

    item_id = 1024
    item = Item(
        id=item_id,
        title='Test Item',
        is_gallery=False,
        auction_shows_count=3,
    )
    db_session.add(item)

    super_cache = SuperCache(redisdb)

    auction = Auction(redisdb, super_cache, site_config)
    auction.init_scripts(redisdb)


    def fetch_banner_ids():
        return list(range(10))

    def cache_banner_ids():
        key = Item.parametrize_cache_key(SuperCache.CACHE_BANNER_IDS_IN_ROTATION_KEY, {})
        super_cache.overwrite_set(key, {0, 1, 2, 3, 4, 6})
        return key

    assert not redisdb.exists(auction.queue_key(item_id))

    # getting banner list should boot up the auction

    banner_ids = auction.get_banner_ids(item_id, 3, fetch_banner_ids, cache_banner_ids)
    assert len(banner_ids) == 3
    assert all(type(x) == int for x in banner_ids)

    # all banners shown 3 times, only 0,1,2 are clicked twice, and 3,4,5 are clicked once
    auction.register_clicks(item_id, [0, 1, 2])
    auction.register_clicks(item_id, [0, 1, 2])
    auction.register_clicks(item_id, [3, 4, 6])

    for i in range(4):
        for banner_id in range(7):
            auction.register_shows(item_id, {banner_id: 1})

    # all results are in
    results = list(redisdb.zscan_iter(auction.result_key(item_id)))
    assert len(results) == 7

    # three ids not in rotation (7,8,9) are left in the queue
    queue = set(redisdb.sscan_iter(auction.queue_key(item_id)))
    assert queue == {'7', '8', '9'}

    # all results are in
    results = list(redisdb.zscan_iter(auction.result_in_rotation_key(item_id)))
    exptected = {
        ('0', 2/3),
        ('1', 2/3),
        ('2', 2/3),
        ('3', 1/3),
        ('4', 1/3),
        ('5', 0.0),
        ('6', 1/3),
    }
    round_set = lambda src: set((banner_id, round(ctr, 8)) for banner_id, ctr in src)
    assert round_set(results) == round_set(exptected)

    # no items in rotation are left in the queue
    queue = set(redisdb.sscan_iter(auction.queue_in_rotation_key(item_id)))
    assert queue == set([])

    # the banners returned are the most (ctr * price) expensive (0,1,2)
    banner_ids = auction.get_banner_ids(item_id, 3, fetch_banner_ids, cache_banner_ids)
    assert len(banner_ids) == 3
    assert set(banner_ids) == {0, 1, 2}

    # now show these banners 4 times but do not click them
    for i in range(4):
        for banner_id in range(3):
            auction.register_shows(item_id, {banner_id: 1})

    # assert set(redisdb.zscan_iter(auction.result_key(item_id))) == {
    # ('0', 0.0),
    #     ('1', 0.0),
    #     ('2', 0.0),
    #     ('3', 1.0/3),
    #     ('4', 1.0/3),
    #     ('5', 1.0/3),
    #     ('6', 1.0/3),
    #     ('7', 0.0),
    #     ('8', 0.0),
    #     ('9', 0.0),
    # }
    # assert set(redisdb.zscan_iter(auction.result_in_rotation_key(item_id))) == {
    #     ('0', 0.0),
    #     ('1', 0.0),
    #     ('2', 0.0),
    #     ('3', 1.0/3),
    #     ('4', 1.0/3),
    #     ('6', 1.0/3),
    # }

    # now the most expensive banners are 3,4,6
    banner_ids = auction.get_banner_ids(item_id, 3, fetch_banner_ids, cache_banner_ids)
    assert len(banner_ids) == 3
    assert set(banner_ids) == {3, 4, 6}


@pytest.mark.usefixtures('app')
def test_reset_all_auctions():
    """
    Ensures that `reset_all_auctions` deletes all available
    auction keys.
    """
    redis_mock = MagicMock(
        keys=lambda wildcard: [wildcard.replace('*', str(n))
                               for n in range(2)])
    auction = Auction(redis_mock, MagicMock(), None)
    auction.reset_all_auctions()

    redis_mock.delete.assert_called_once_with(
        'auction:queue:0', 'auction:queue:1',
        'auction:result:0', 'auction:result:1',
        'auction:show:0', 'auction:show:1',
        'auction:click:0', 'auction:click:1',
        'auction:result_in_rotation:0', 'auction:result_in_rotation:1',
        'auction:queue_in_rotation:0', 'auction:queue_in_rotation:1',
        'auction:total_stats:0', 'auction:total_stats:1')


@pytest.mark.usefixtures('app')
def test_reset_all_auctions_without_auctions_keys():
    """
    Ensures that `reset_all_auctions` don't calls `delete`
    when no auctions keys available.
    """
    redis_mock = MagicMock(keys=lambda _: [])
    auction = Auction(redis_mock, MagicMock(), None)
    auction.reset_all_auctions()

    assert redis_mock.delete.call_count == 0
