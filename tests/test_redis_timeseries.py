import pytest
from pytest_dbfixtures import factories
from wowengine.redis_timeseries import *

redis_proc = factories.redis_proc(port=6381)
redisdb = factories.redisdb('redis_proc', port=6381)

# noinspection PyProtectedMember
@pytest.mark.parametrize("val,expected", [
    (60, 60),
    ('60', 60),
    ('1m', 60),
    ('5m', 300),
    ('2h', 3600 * 2),
    ('5d', 86400 * 5)
])
def test_resolve_expire_time(val, expected):
    assert RedisTimeseries._resolve_expire_time(val) == expected


# noinspection PyProtectedMember
@pytest.mark.parametrize("val,expected_dt", [
    ('1m', datetime(2000, 6, 22, 16, 33)),
    ('5m', datetime(2000, 6, 22, 16, 30)),
    ('1h', datetime(2000, 6, 22, 16, 0)),
    ('daily', datetime(2000, 6, 22, 0, 0)),
    ('monthly', datetime(2000, 6, 1, 0, 0)),
    ('eternity', datetime.fromtimestamp(0))
])
def test_time_bucket_fn(val, expected_dt):
    f = RedisTimeseries._time_bucket_fn(val)
    assert f(datetime(2000, 6, 22, 16, 33, 20).timestamp()) == expected_dt.timestamp()


def test_topchart(redisdb):
    t = RedisTimeseries(redisdb, 'articles', {
        'day': {
            'step': 'daily',
            'expire': '2d'
        }
    })

    ts = time.time()
    t.update_topchart('show', 1, amount=2, ts=ts)
    t.update_topchart('show', 2, amount=3, ts=ts)
    assert list(t.redis.zscan_iter(t._gen_key('day', 'show', ts), score_cast_func=int)) == [('1', 2), ('2', 3)]
