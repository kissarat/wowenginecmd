import pytest
from pytest_dbfixtures import factories
from wowengine.site_config import SiteConfig

redis_proc = factories.redis_proc(port=6382)
redisdb = factories.redisdb('redis_proc', port=6382)

@pytest.mark.usefixtures('app')
def test_site_config(redisdb):
    cfg = SiteConfig(redisdb, defaults={
        'str_key': 'some-str',
        'int_key': 123
    })

    # default values
    assert cfg['int_key'] == 123
    assert cfg['str_key'] == 'some-str'
    assert cfg['wrong_key'] is None

    # str value
    cfg['foo'] = 'bar'
    assert cfg['foo'] == 'bar'
    assert cfg.get('foo', 'baz') == 'bar'
    assert cfg.get('foo-foo', 'baz') == 'baz'

    # int value
    cfg['foo'] = 1
    assert cfg['foo'] == 1

    # float value
    cfg['foo'] = 3.14
    assert cfg['foo'] == 3.14
