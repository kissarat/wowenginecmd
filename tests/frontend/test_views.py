# coding: utf-8

from unittest.mock import Mock
from datetime import date, datetime, timedelta
import pytest
from wowengine.extensions import banner_stats, line_stats
from wowengine.models import NewsItem, Banner, Source, Line


@pytest.fixture
def register_visit(monkeypatch):
    mock = Mock()
    monkeypatch.setattr('wowengine.extensions.generation_stats.register_visit', mock)
    return mock


@pytest.mark.usefixtures('redis_proc')
class TestTrackArticlesShown(object):
    @pytest.fixture
    def news_item(self, db_session):
        news_item = NewsItem(title='title',
                             is_gallery=False,
                             is_published=True)
        db_session.add(news_item)
        db_session.commit()
        return news_item

    def test_for_not_ahtml(self, client, register_visit, news_item):
        assert client.get(
            '/track/shown/news/{}?ext=html'.format(news_item.id),
            headers=[('Referer', 'http://localhost:5000/')]).status_code == 302
        register_visit.assert_called_once_with()

    def test_for_ahtml(self, client, register_visit, news_item):
        assert client.get(
            '/track/shown/news/{}?ext=ahtml'.format(news_item.id),
            headers=[('Referer', 'http://localhost:5000/')]).status_code == 302
        assert register_visit.call_count == 0


@pytest.mark.usefixtures('redis_proc')
class TestTrackBannerShown(object):
    @pytest.fixture
    def banner(self, db_session):
        source = Source(title='source')
        db_session.add(source)
        banner = Banner(title='title',
                        is_gallery=False,
                        is_published=True,
                        source=source)
        db_session.add(banner)
        db_session.commit()
        return banner

    def test_for_not_ahtml(self, client, register_visit, banner):
        assert client.get(
            '/track/shown/img/{}/{}/?ext=html'.format(banner.id, banner.source.id),
            headers=[('Referer', 'http://localhost:5000/')]).status_code == 302
        register_visit.assert_called_once_with()

    def test_for_ahtml(self, client, register_visit, banner):
        assert client.get(
            '/track/shown/img/{}/{}/?ext=ahtml'.format(banner.id, banner.source.id),
            headers=[('Referer', 'http://localhost:5000/')]).status_code == 302
        assert register_visit.call_count == 0



class TestSourcesStats(object):

    @pytest.fixture
    def source(self, db_session):
        source = Source(title='source')
        db_session.add(source)
        db_session.commit()
        return source

    @pytest.fixture
    def banners(self, db_session, source):
        banner_1 = Banner(title='banner-1',
                          is_gallery=False,
                          is_published=True,
                          source=source)
        db_session.add(banner_1)
        banner_2 = Banner(title='banner-2',
                          is_gallery=False,
                          is_published=True,
                          source=source)
        db_session.add(banner_2)
        db_session.commit()
        banner_stats.register_clicked(
            banner_1.id,
            ts=datetime.today().timestamp())
        banner_stats.register_shown(
            banner_2.id,
            ts=(datetime.today() - timedelta(days=1)).timestamp())
        return banner_1, banner_2

    @pytest.fixture
    def lines(self, db_session, source):
        line_1 = Line(title='line-1',
                      is_gallery=False,
                      is_published=True,
                      source=source)
        db_session.add(line_1)
        line_2 = Line(title='line-2',
                      is_gallery=False,
                      is_published=True,
                      source=source)
        db_session.add(line_2)
        db_session.commit()
        line_stats.register_clicked(
            line_1.id,
            ts=datetime.today().timestamp())
        line_stats.register_shown(
            line_2.id,
            ts=(datetime.today() - timedelta(days=1)).timestamp())
        return line_1, line_2

    def test_default_date_range(self, client, source):
        """Ensures that stats only for today presents when range not passed."""
        response = client.get('/api/sources_stats/{}'.format(source.id))
        assert list(response.json.keys()) == [str(date.today())]

    def test_stats_for_date_range(self, client, source, banners, lines):
        yesterday = (datetime.today() - timedelta(days=1)).date()
        today = datetime.today().date()
        response = client.get('/api/sources_stats/{}?from={}&to={}'.format(
            source.id, yesterday, today))
        # Ensures that all dates from range presents:
        assert set(response.json.keys()) == {str(yesterday), str(today)}
        # Ensures that banners presents:
        assert response.json[str(today)]['banners'][str(banners[0].id)]['click'] > 0
        assert response.json[str(yesterday)]['banners'][str(banners[1].id)]['show'] > 0
        # Ensures that lines presents:
        assert response.json[str(today)]['lines'][str(lines[0].id)]['click'] > 0
        assert response.json[str(yesterday)]['lines'][str(lines[1].id)]['show'] > 0
