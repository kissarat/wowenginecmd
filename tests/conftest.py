# coding: utf-8

import sys
import os

sys.path.append(os.path.join(os.path.dirname(os.path.dirname(__file__))))

import pytest
from pytest_dbfixtures import factories
from wowengine import create_app, db as _db

try:
    from wowengine.config.profile_tests import Config
except ImportError:
    from wowengine.config import Config


class TestConfig(Config):
    SQLALCHEMY_DATABASE_URI = Config.SQLALCHEMY_TEST_DATABASE_URI
    TESTING = True
    CACHE_REDIS_URL = 'redis://localhost:6384?db=0'

    redis_proc = factories.redis_proc(port=6384)
    redisdb = factories.redisdb('redis_proc', port=6384, strict=True)


@pytest.fixture
def app():
    return create_app(TestConfig)


@pytest.fixture
def db(app, request):
    _db.init_app(app)
    _db.create_all(app=app)
    request.addfinalizer(_db.drop_all)
    return _db


@pytest.fixture
def db_session(db, request):
    connection = db.engine.connect()
    transaction = connection.begin()
    session = db.create_scoped_session(options={'bind': connection})
    db.session = session

    def teardown():
        transaction.rollback()
        connection.close()
        session.remove()

    request.addfinalizer(teardown)
    return session


redis_proc = factories.redis_proc(port=6384)
redisdb = factories.redisdb('redis_proc', port=6384, strict=True)
