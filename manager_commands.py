# !/usr/bin/env python3
from _curses import halfdelay
from functools import cmp_to_key
import logging
from flask import url_for
from flask.ext.assets import ManageAssets

from flask.ext.script import Manager
from flask.ext.migrate import Migrate, MigrateCommand
import time

from wowengine import create_app, db
from wowengine.config import config
from wowengine.auction import Auction
from wowengine.extensions import generation_stats, cache, banner_stats
import wowengine.legacy_db
from wowengine.models import NewsItem


def create_manager(cfg):
    logging.basicConfig(level=logging.INFO)
    application = create_app(cfg=cfg)

    migrate = Migrate(application, db)

    manager = Manager(application)
    manager.add_command('db', MigrateCommand)
    manager.add_command('assets', ManageAssets())

    # Legacy DB tasks

    @manager.command
    def legacy_cleanup():
        wowengine.legacy_db.legacy_cleanup(cfg)

    @manager.command
    def import_legacy_db():
        wowengine.legacy_db.import_legacy_db(cfg)

    # Stat tasks

    @manager.command
    def fetch_and_register_liveinternet_stats():
        """
        Run from cron every 5 minutes.
        """
        generation_stats.fetch_and_register_liveinternet_stats()


    @manager.command
    def list_routes():
        """
        List available flask routes.
        """
        import urllib.parse

        output = []
        for rule in application.url_map.iter_rules():

            options = {}
            for arg in rule.arguments:
                # TODO: check if argument have string type, and paste argument name if so.
                # options[arg] = "[{0}]".format(arg)
                options[arg] = '0'

            methods = ','.join(rule.methods)
            url = url_for(rule.endpoint, **options)
            line = urllib.parse.unquote("{:30s} {:35s} {}".format(rule.endpoint, methods, url))
            output.append(line)

        for line in sorted(output):
            print(line)


    @manager.command
    def cache_reset():
        """
        Reset cache (delete everything).
        """
        from redis import StrictRedis

        r = StrictRedis.from_url(config.CACHE_REDIS_URL)
        r.flushdb()



    @manager.command
    def foo():
        """
        For testing.
        """
        from wowengine.extensions import auction

        item_id = 7930
        banner_id = 12229

        # print(auction.lua_register_show(keys=[
        #     auction.show_key(item_id),
        #     auction.click_key(item_id),
        #     auction.queue_key(item_id),
        #     auction.queue_in_rotation_key(item_id),
        #     auction.result_key(item_id),
        #     auction.result_in_rotation_key(item_id),
        # ], args=[banner_id, 1, 1000]))

        # print(auction.lua_get_banner_ids(keys=[
        #     auction.queue_key(item_id),
        #     auction.queue_in_rotation_key(item_id),
        #     auction.result_key(item_id),
        #     auction.result_in_rotation_key(item_id),
        # ], args=[3]))

        from wowengine.extensions import redis
        foo = redis.register_script("""
        local q = KEYS[1]
        local q2 = KEYS[2]
        local a = tonumber(ARGV[1])

        local c = tonumber(redis.call('SCARD', q))
        if c >= a then
            return redis.call('SRANDMEMBER', q2, a)
        else
            return 'zhopa'
        end
        """)
        print(foo(keys=['q', 'q2'], args=[2]))


    @manager.command
    def create_users_and_roles():
        """
        Initial roles and root user.
        """
        from wowengine.models import user_datastore
        from flask_security import utils
        for role in ['root', 'admin', 'chief_editor', 'editor', 'client']:
            user_datastore.find_or_create_role(name=role)
        if not user_datastore.find_user(email=cfg.ROOT_EMAIL):
            user_datastore.create_user(email=cfg.ROOT_EMAIL,
                                       password=utils.encrypt_password(cfg.ROOT_PASSWORD),
                                       firstname='Root',
                                       lastname='Root')
        user_datastore.commit()
        root = user_datastore.find_user(email=cfg.ROOT_EMAIL)
        user_datastore.add_role_to_user(root, 'root')
        user_datastore.commit()


    @manager.command
    def create_tagged_banners():
        from wowengine.models import Banner, Tag, Source
        import uuid
        from wowengine import db

        src = Source.query.filter_by(title='Tagged banners').first()
        if not src:
            src = Source(
                title='Tagged banners',
            )
            db.session.add(src)
            db.session.commit()

        assert src.is_active()

        for tag in Tag.query.all():
            for i in range(50):
                banner = Banner(
                    unid=str(uuid.uuid4()),
                    mnemonic_id='banner-{}-{}'.format(tag.title, i),
                    title='Banner with tag {} #{}'.format(tag.title, i),
                    body=tag.title * 5,
                    source=src,
                    is_gallery=False,
                    is_published=True,
                    is_distilled=True,
                    tags=[tag],
                )
                db.session.add(banner)
                print("Added banner with title {} #{}".format(banner.title, i))
        db.session.commit()
    return manager
