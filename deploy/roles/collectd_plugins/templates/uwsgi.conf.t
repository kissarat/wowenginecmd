<Plugin curl_json>
    <Sock "/tmp/uwsgi_stats.{{ item }}.sock">
        Instance "uwsgi-{{ item }}"
        <Key "workers/*/requests">
            Type "http_requests"
        </Key>
        <Key "workers/*/apps/*/requests">
            Type "http_requests"
        </Key>
        <Key "workers/*/apps/*/exceptions">
            Type "http_requests"
        </Key>
    </Sock>
</Plugin>
