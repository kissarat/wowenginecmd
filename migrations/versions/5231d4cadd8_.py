"""Set source_id=NULL for articles

Revision ID: 5231d4cadd8
Revises: 1eb009e9f18
Create Date: 2015-10-02 16:06:00.842734

"""

# revision identifiers, used by Alembic.
revision = '5231d4cadd8'
down_revision = '1eb009e9f18'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.execute("update items set source_id=NULL where type='news' and source_id > 0")


def downgrade():
    pass
