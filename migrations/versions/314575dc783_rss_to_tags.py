"""rss_to_tags

Revision ID: 314575dc783
Revises: ed234c502f
Create Date: 2015-01-10 11:13:53.092951

"""

# revision identifiers, used by Alembic.
revision = '314575dc783'
down_revision = 'ed234c502f'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.rename_table('rss', 'tags')
    op.rename_table('rss_items', 'item_tags')
    op.add_column('item_tags', sa.Column('tag_id', sa.Integer(), nullable=True))
    op.execute('''
        update
            item_tags
        set
            tag_id=rss_id
    ''')
    op.create_foreign_key(
        'item_tags_fk_to_tags',
        'item_tags',
        'tags',
        ['tag_id'],
        ['id'],
    )
    op.drop_constraint('item_tags_ibfk_2', 'item_tags', type_='foreignkey')
    op.drop_column('item_tags', 'rss_id')

def downgrade():
    op.rename_table('tags', 'rss')
    op.rename_table('item_tags', 'rss_items')
