"""empty message

Revision ID: 208ac03a7d4
Revises: 17c64de71bf
Create Date: 2015-06-16 19:06:43.529001

"""

# revision identifiers, used by Alembic.
revision = '208ac03a7d4'
down_revision = '17c64de71bf'

from alembic import op
import sqlalchemy as sa


def upgrade():
    from wowengine.models import Banner, Line
    from wowengine.extensions import db

    for banner in Banner.query.all():
        banner.link = banner.link.strip()

    for line in Line.query.all():
        line.link = line.link.strip()

    db.session.commit()


def downgrade():
    pass
