"""new font sizes

Revision ID: 51513d69918
Revises: 341c4471fc3
Create Date: 2014-09-19 18:37:02.101668

"""

# revision identifiers, used by Alembic.
revision = '51513d69918'
down_revision = '341c4471fc3'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('items', sa.Column('new_font_sizes', sa.Boolean(), nullable=False))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('items', 'new_font_sizes')
    ### end Alembic commands ###
