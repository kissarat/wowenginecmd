Py3Env=/var/py3/bin/python
args?=

help:
	@echo "Run make test [args="... args to py.test"] | server"
test:
	vagrant ssh -c 'cd /vagrant && $(Py3Env) setup.py test -a "$(args)"'

server:
	$(Py3Env) manage.py runserver -h 0.0.0.0
