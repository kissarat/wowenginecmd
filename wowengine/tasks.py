from collections import Counter
import time
import json
from flask import g

from . import create_app, register_extensions
from . import celery
# from flask import current_app, session
from werkzeug.datastructures import MultiDict
# from wowengine.frontend import events

from flask import current_app, session
from wowengine.frontend import events

from .extensions import (
    banner_stats, line_stats, source_stats, db,
    active_items_stats, generation_stats, auction,
    redis, site_config, article_stats
)
from wowengine.config import config
from wowengine.models import Banner, Source, Line, NewsItem, Tag, Item
from celery.utils.log import get_task_logger

app = create_app()

logger = get_task_logger(__name__)

@celery.app.task
def sync_items_stats():
    with app.app_context():
        task_stats = Counter()
        defaults = {'show': 0, 'click': 0}
        for model, module in ((Banner, banner_stats), (Line, line_stats)):
            today_stats = module.get_for_today()
            # for item in model.query.filter(model.id.in_(today_stats.keys())).all():
            for item in model.query.all():
                shows = today_stats.get(item.id, defaults)['show']
                clicks = today_stats.get(item.id, defaults)['click']

                if item.shows != shows or item.clicks != clicks:
                    task_stats['items'] += 1
                    task_stats['shows'] += shows - item.shows
                    task_stats['clicks'] += clicks - item.clicks

                    item.shows = shows
                    item.clicks = clicks

            logger.info(
                "Stats for {items}  updated, total {shows} shows and {clicks} clicks counted"
                .format(**task_stats)
            )

        db.session.commit()


@celery.app.task
def sync_active_items():
    """Registers active banners/sources each 5 minutes."""
    with app.app_context():
        active_items_stats.register_banners([
            banner.id for banner in Banner.query.filter(Banner.is_published)],
            ts=time.time())
        active_items_stats.register_sources([
            source.id for source in Source.get_all_active(time.time())],
            ts=time.time())


@celery.app.task
def calc_daily_overall_price():
    with app.app_context():
        generation_stats.register_overall_daily_price(
            Source.get_overall_price_for_today())


@celery.app.task
def update_caches_frequent():
    with app.app_context():
        g.flask_cache_forced_update = True

        sources = Source.list_of_all_source_id()
        Line.fetch_random_active(sources)


@celery.app.task
def update_caches_normal():
    with app.app_context():
        g.flask_cache_forced_update = True

        random_item = NewsItem.query.limit(1)[0]

        Source.get_all_titles()
        Source.get_all_ids_in_rotation()  # filters

        NewsItem.fetch_list_for_morda()
        NewsItem.fetch_popular()

        Banner._cache_top_strip_ids()
        Banner.fetch_ids_for_last_days(site_config['auction_max_age_days'])

        sources = Source.list_of_all_source_id()
        for tags in [[t.id] for t in Tag.query.all()] + [[]]:
            for for_gallery in (False, True):
                logger.info("Caching for tags: %s", tags)
                # Fetch for random item (to avoid duplication of code that
                # compose src_filters and filters)
                Banner.fetch_for_item_page(
                    random_item.id, sources, tag_ids=tags, for_gallery=for_gallery)
                Banner.fetch_flow(
                    random_item.id, sources, 1, tag_ids=tags, for_gallery=for_gallery)


@celery.app.task
def update_caches_infrequent():
    with app.app_context():
        g.flask_cache_forced_update = True

        Item.get_all_titles_and_source_ids()
        Item.get_all_published_ids()

        NewsItem._cache_gallery_ids()
        Banner._get_ids_by_average_ctrs()

        for tags in [[t.id] for t in Tag.query.all()] + [[]]:
            logger.info('Caching _cache_gallery_ids for tag %s', tags)
            NewsItem._cache_article_ids(tags)

@celery.app.task
def update_geoip_db():
    from .geo.geoip import (
        Geo, UpdateCSVDatabaseException, UpdateBinaryDatabaseException
    )
    try:
        Geo.update_db_csv()
        Geo.update_yaml()
        logger.info("Geolocation databases has been successfully updated")
    except (UpdateCSVDatabaseException, UpdateBinaryDatabaseException) as e:
        logger.error(e.message)

@celery.app.task(rate_limit=1)
def save_fast_tracker_data():
    with app.app_context():
        while True:
            data = redis.lpop(config.FAST_TRACKER_QUEUE_KEY)

            if data is None:
                break

            data = json.loads(data)
            print(data)

            def make_int_keys(data):
                return {int(k): v for k, v in data.items()}

            if data.get('BannerShows'):
                banner_stats.register_shown(make_int_keys(data['BannerShows']))

            if data.get('SourceShows'):
                source_stats.register_shown(make_int_keys(data['SourceShows']))

            if data.get('BannerVisits'):
                banner_stats.register_visit(
                    make_int_keys(data['BannerVisits']))

            if data.get('GenerationVisits'):
                generation_stats.register_visit(
                    amount=data['GenerationVisits'])

            if data.get('GenerationExtVisits'):
                generation_stats.register_ext_visit(
                    amount=data['GenerationExtVisits'])

            if data.get('AuctionShows'):
                # статистику по баннерам и источникам здесь не обновляем,
                # т.к. данные по ней приходят отдельно в BannerStats и
                # SourceStats
                for from_id, shows in data['AuctionShows'].items():
                    items = MultiDict([
                        (int(banner_id), float(price))
                        for banner_id, price in shows
                    ])

                    auction.register_shows(from_id, items)

            if data.get('LinesShows'):
                line_stats.register_shown(make_int_keys(data['LinesShows']))

            if data.get('ArticleVisits'):
                article_stats.register_visit(
                    make_int_keys(data['ArticleVisits']))

            if data.get('ArticleShows'):
                article_stats.register_shown(
                    make_int_keys(data['ArticleShows']))
