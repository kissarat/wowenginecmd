from datetime import timedelta
from celery import Celery
from celery.schedules import crontab
from .config import config

app = Celery()

app.config_from_object(config)

app.conf.CELERYBEAT_SCHEDULE = {
    'sync-item-stats': {
        'task': 'wowengine.tasks.sync_items_stats',
        'schedule': timedelta(seconds=60),
    },
    'sync-exists-items': {
        'task': 'wowengine.tasks.sync_active_items',
        'schedule': timedelta(minutes=1),
    },
    'calc-daily-overall-price': {
        'task': 'wowengine.tasks.calc_daily_overall_price',
        'schedule': crontab(minute=0, hour='*'),
    },
    'update-caches-1': {
        'task': 'wowengine.tasks.update_caches_frequent',
        'schedule': timedelta(seconds=10),
    },
    'update-caches-2': {
        'task': 'wowengine.tasks.update_caches_normal',
        'schedule': timedelta(seconds=30),
    },
    'update-caches-3': {
        'task': 'wowengine.tasks.update_caches_infrequent',
        'schedule': timedelta(seconds=60),
    },
    'update-geoip-db': {
        'task': 'wowengine.tasks.update_geoip_db',
        'schedule': crontab(minute=0, hour=0, day_of_week=6),
    },
    'save-fast-tracker-data': {
        'task': 'wowengine.tasks.save_fast_tracker_data',
        'schedule': timedelta(seconds=5),
    },
}
