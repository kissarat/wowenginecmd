"""
Конвертация из старой базы в новую.
"""

from collections import OrderedDict
import logging
import os

import re
import phpserialize
from sqlalchemy import create_engine, text
from sqlalchemy.sql import table, column

from wowengine import db
from wowengine.models import Item, Source, Tag

# часы и минуты из "07:45"
STR_TIME_RE = re.compile('^(\d{1,2}):(\d\d)$')

# имя файла из длинного пути
FILENAME_RE = re.compile('/([^/]+)$')


def _row_to_source(row):
    """
    Convert a legacy sql row to Source object.
    """
    php_active_time = phpserialize.loads(bytes(row['active_time'], 'UTF-8'))

    def c(day, tag):
        """
        Convert "01:01" to 61.
        """
        st = php_active_time[day][tag].decode('utf-8')
        m = STR_TIME_RE.match(st)
        return int(m.group(1)) * 60 + int(m.group(2))

    return Source(id=row['id'],
                  num=row['num'],
                  title=row['title'],
                  daily_click_limit=row['daily_limit'],
                  total_click_limit=row['total_click_limit'],
                  daily_show_limit=row['show_daily_limit'],
                  total_show_limit=row['total_show_limit'],
                  hide_referer=row['hide_referer'] == 1,
                  click_price=row['click_price'],

                  active_mon=php_active_time[1].get(b'enabled', 0) == b'1',
                  active_tue=php_active_time[2].get(b'enabled', 0) == b'1',
                  active_wed=php_active_time[3].get(b'enabled', 0) == b'1',
                  active_thu=php_active_time[4].get(b'enabled', 0) == b'1',
                  active_fri=php_active_time[5].get(b'enabled', 0) == b'1',
                  active_sat=php_active_time[6].get(b'enabled', 0) == b'1',
                  active_sun=php_active_time[7].get(b'enabled', 0) == b'1',

                  active_mon_start=c(1, b'start'),
                  active_mon_end=c(1, b'end'),
                  active_tue_start=c(2, b'start'),
                  active_tue_end=c(2, b'end'),
                  active_wed_start=c(3, b'start'),
                  active_wed_end=c(3, b'end'),
                  active_thu_start=c(4, b'start'),
                  active_thu_end=c(4, b'end'),
                  active_fri_start=c(5, b'start'),
                  active_fri_end=c(5, b'end'),
                  active_sat_start=c(6, b'start'),
                  active_sat_end=c(6, b'end'),
                  active_sun_start=c(7, b'start'),
                  active_sun_end=c(7, b'end'))


RSS_IDS_RE = re.compile('<stream(\d+)>')


def _row_to_news_item(row, tag_items):
    """
    Convert a legacy sql row to News object.
    """

    def _detect_news_type(r):
        """Return 'news', 'banner' or 'line'."""
        if r['is_article'] == '' and r['is_line'] == '':
            return 'banner'
        elif r['is_line'] == '1':
            return 'line'
        else:
            return 'news'

    def _extract_tag_ids(s):
        return [int(x) for x in RSS_IDS_RE.findall(s)]

    def _convert_images(r):
        s = r['gallery']
        cx = r['img_cut_x']
        cy = r['img_cut_y']

        php_gallery = phpserialize.loads(bytes(s, 'UTF-8'), array_hook=OrderedDict) if s else {1: {b'files': {}}}
        images = [{"num": os.path.splitext(filename.decode())[0],
                   "width": v[b'width'],
                   "height": v[b'height'],
                   "caption": v.get(b'description', b'').decode(),
                   "copyright": v.get(b'title', b'').decode(),
                   "cx": 50,
                   "cy": 50} for filename, v in php_gallery[1][b'files'].items()]

        if len(images) > 0:
            images[0]['cx'] = cx
            images[0]['cy'] = cy
            return images
        else:
            # если нет картинок в поле gallery, взять ее из поля img_src
            try:
                filename = os.path.basename(r['img_src'])
                num = int(filename[:filename.index('.')])
            except ValueError:
                num = 0
            return [{
                'num': num,
                'width': r['img_width'],
                'height': r['img_height'],
                'caption': '',
                'copyright': '',
                'cx': cx,
                'cy': cy
            }]

    def _convert_heights(s):
        d = phpserialize.loads(bytes(s, 'UTF-8'), decode_strings=True) if s else {}
        for width, width_dict in d.items():
            for size, size_dict in width_dict.items():
                d[width][size] = {
                    'bh': int(size_dict['block_height']),
                    'th': int(size_dict['title_height']),
                    'l': int(size_dict['title_lines'])
                }

        return d

    tags = [tag_items[x] for x in _extract_tag_ids(row['in_rss'])]

    return Item(id=row['id'],
                num=row['num'],
                type=_detect_news_type(row),
                unid=row['unid'],
                mnemonic_id=row['mnemonic_id'],
                title=row['title'].replace('_', ''),
                annotation=row['annotation'],
                body=row['bodytext'],
                date=row['date'],
                is_distilled=row['doc_type'] == '2',
                is_published=row['is_published'] == '1',
                is_on_morda=row['on_morda'] == '1',
                is_gallery=row['is_gallery'] == '1',
                text_link=row['text_link'],
                text_link_caption=row['text_link_caption'],
                source_id=row['source_id'],
                in_extra_block1=row['in_extra_block1'] == '1',
                video=row['video'],
                link=row['link'],
                tags=tags,
                images=_convert_images(row),
                font_sizes=_convert_heights(row['heights']))


def _remove_legacy_duplicate_unids(old_engine):
    result = old_engine.execute("SELECT unid, count(*) AS c FROM news GROUP BY unid HAVING c > 1")
    unids = [row['unid'] for row in result]

    count = 0

    for unid in unids:
        result = old_engine.execute(text("SELECT id FROM news WHERE unid = :unid ORDER BY date"), unid=unid)
        ids = [row['id'] for row in result]
        ids_to_delete = ids[:-1]  # all but the most recent
        count += len(ids_to_delete)

        old_engine.execute(table('news').delete(column('id').in_(ids_to_delete)))

    logging.info("%d duplicate news items deleted" % count)


def legacy_cleanup(cfg):
    """
    Clean up legacy database before importing it into the new schema.
    """
    old_engine = create_engine(cfg.SQLALCHEMY_LEGACY_DATABASE_URI)
    _remove_legacy_duplicate_unids(old_engine)


def import_legacy_db(cfg):
    """
    Import data from legacy wowimpulse db.
    """

    # Delete existing news and sources
    for x in [Item, Source, Tag]:
        x.query.delete()

    db.metadata.tables['item_tags'].delete()

    old_engine = create_engine(cfg.SQLALCHEMY_LEGACY_DATABASE_URI)

    # Remove duplicates
    _remove_legacy_duplicate_unids(old_engine)

    # Import tag
    for tag_id, tag_title in [(1, 'all'),
                              (2, 'hard'),
                              (3, 'soft'),
                              (4, 'kremlin'),
                              (5, 'lily-white'),
                              (6, 'imhonet')]:
        db.session.add(Tag(id=tag_id, title=tag_title))
    db.session.commit()
    logging.info("tag added")

    # Import sources
    sources = old_engine.execute("SELECT * FROM sources")
    cnt = 0
    source_ids = []
    for row in sources:
        s = _row_to_source(row)
        source_ids.append(s.id)
        db.session.add(s)
        cnt += 1
    db.session.commit()
    logging.info("%s sources added." % cnt)

    # Import news
    tag_items = {x.id: x for x in Tag.query.all()}

    cnt = 0
    news = old_engine.execute("SELECT * FROM news")

    for row in news:
        item = _row_to_news_item(row, tag_items)
        if item.source_id not in source_ids:
            item.source_id = None
        db.session.add(item)
        cnt += 1
    db.session.commit()

    logging.info("%s news added." % cnt)
