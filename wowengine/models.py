"""
ORM-модели для работы с объектами в mysql.
"""
from collections import OrderedDict, defaultdict
import time
from dateutil.parser import parse
from urllib.parse import urlencode
from urllib.request import urlparse
import uuid
from datetime import datetime, timedelta
import math
import simplejson as json
import re
import copy
import random
import itertools
from flask import current_app, g
from sqlalchemy import event, desc, asc, func, or_
from sqlalchemy.ext.hybrid import hybrid_property
from .extensions import db, cache, source_stats, article_top_chart, super_cache, banner_stats, site_config
from wowengine import util
from wowengine.images import resized_image_url
from wowengine.stats import SuperCache
from wowengine.config import config
import fnmatch
from flask.ext.security import UserMixin, RoleMixin, SQLAlchemyUserDatastore


def is_forced_update():
    """
    Если true, то flask-cache будет игнорировать значения в кеше,
    что позволяет обновлять мемоизированные функции в селери
    """
    return getattr(g, 'flask_cache_forced_update', None)


def gen_unid():
    return uuid.uuid4().hex


CYR_TR = {"а": "a", "б": "b", "в": "v", "г": "g", "д": "d", "е": "e", "ё": "e", "ж": "zh",
          "з": "z", "и": "i", "й": "j", "к": "k", "л": "l", "м": "m", "н": "n", "о": "o",
          "п": "p", "р": "r", "с": "s", "т": "t", "у": "u", "ф": "f", "х": "h", "ц": "c",
          "ч": "ch", "ш": "sh", "щ": "sch", "ъ": "", "ы": "y", "ь": "", "э": "e",
          "ю": "yu", "я": "ya", '_': ''}


def gen_mnemonic_id(context):
    title = context.current_parameters['title']
    return ''.join(CYR_TR.get(i, '-') for i in title.lower())


MINUTES_IN_DAY = 24 * 60
WEEKDAYS = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']


class JSON(db.TypeDecorator):
    """
    Тип колонки БД для хранения JSON-структур.
    """

    impl = db.Text

    def process_bind_param(self, value, dialect):
        if value is not None:
            value = json.dumps(value, sort_keys=True)
        return value

    def process_result_value(self, value, dialect):
        if value is not None:
            value = json.loads(value)
        return value

class SourceGroup(db.Model):
    """
    Группа источника новостей.
    """
    __tablename__ = 'sources_groups'
    api_collection_name = 'sources_groups'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
    name = db.Column(db.String(1024), nullable=False)

    def __repr__(self):
        return self.name

# Define models
roles_users = db.Table('roles_users',
                       db.Column(
                           'user_id', db.Integer(), db.ForeignKey('auth_user.id')),
                       db.Column('role_id', db.Integer(),
                                 db.ForeignKey('auth_role.id')))


class Role(db.Model, RoleMixin):
    __tablename__ = 'auth_role'

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __repr__(self):
        return '<Role %r>' % self.name

    def __str__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)


class User(db.Model, UserMixin):

    __tablename__ = 'auth_user'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    firstname = db.Column(db.String(255))
    lastname = db.Column(db.String(255))
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

    def __str__(self):
        return '<User id=%s email=%s>' % (self.id, self.email)

    def __repr__(self):
        return '<User id=%s email=%s>' % (self.id, self.email)


user_datastore = SQLAlchemyUserDatastore(db, User, Role)

class Source(db.Model):
    """
    Источник новости.
    """
    __tablename__ = 'sources'
    api_collection_name = 'sources'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
    num = db.Column(db.Integer, default=100, nullable=False, index=True)
    title = db.Column(db.String(1024), nullable=False)

    # переходов в день
    daily_click_limit = db.Column(db.Integer, default=0, nullable=False)
    # переходов всего
    total_click_limit = db.Column(db.Integer, default=0, nullable=False)
    # показов в день
    daily_show_limit = db.Column(db.Integer, default=0, nullable=False)
    # показов всего
    total_show_limit = db.Column(db.Integer, default=0, nullable=False)
    # скрывать ли реферер
    hide_referer = db.Column(db.Boolean, default=False, nullable=False)
    # показывать в обычных галереях
    show_in_galleries = db.Column(db.Boolean, default=False, nullable=False, index=True)
    # показывать в адовых галереях
    show_in_hell_galleries = db.Column(db.Boolean, default=False, nullable=False, index=True)

    is_fullfill = db.Column(db.Boolean, default=False, nullable=False, index=True)

    click_price = db.Column(db.Numeric(10, 2), default=0.0, nullable=False)

    geo = db.Column(db.String(1024), default='', nullable=False)

    group_id = db.Column(db.Integer, db.ForeignKey('sources_groups.id'))
    group = db.relationship('SourceGroup', backref=db.backref('sources', lazy='dynamic'))

    def is_active(self, ts):
        """
        Проверяет, активен ли источник в данное время.

        :rtype: bool
        """
        dt = datetime.fromtimestamp(ts)
        wd_name = WEEKDAYS[dt.weekday()]

        if getattr(self, 'active_%s' % wd_name):
            minutes = dt.hour * 60 + dt.minute
            start = getattr(self, 'active_%s_start' % wd_name)
            end = getattr(self, 'active_%s_end' % wd_name)
            return start <= minutes <= end

        return False

    def is_within_limits(self, today_stats, total_stats):
        """
        Проверяет, превышен ли какой-либо лимит по показам/кликам.

        :type today_stats: dict[str, int]
        :type total_stats: dict[str, int]
        :rtype: bool
        """
        return (self.daily_click_limit == 0 or self.daily_click_limit >= today_stats['click']) and \
               (self.total_click_limit == 0 or self.total_click_limit >= total_stats['click']) and \
               (self.daily_show_limit == 0 or self.daily_show_limit >= today_stats['show']) and \
               (self.total_show_limit == 0 or self.total_show_limit >= total_stats['show'])

    @classmethod
    @cache.memoize(timeout=90)
    def get_all_titles(cls):
        """
        Все заголовки источников, отсортированные как в админке.

        :rtype: OrderedDict[int, str]
        """
        sources = cls.query.with_entities(cls.id, cls.title).order_by(cls.num).all()
        return OrderedDict((x.id, x.title) for x in sources)

    # Эти методы передаются как фальшивые поля в REST API.

    def today_stats(self):
        return source_stats.get_for_today(self.id)

    @classmethod
    @cache.memoize(timeout=90)
    def list_of_right_geo_sources_test(cls, city, country):
        """
         Получаем id города и страны пользователя, сночала выбираем все источки, соответствующие городу.
         Потом все источники соответствующие стране.
         Потом все источники где в качестве гео выбран весь мир.
         В таком порядке и передаем
        """
        sources_for_city = cls.query.filter(cls.geo.like('%,' + city + ',%')).all()
        sources_for_country = cls.query.filter(cls.geo.like('%,' + country + ',%')).all()
        all_world = cls.query.filter(cls.geo == '').all()
        sources = sources_for_city + sources_for_country + all_world
        sources_id = [[each.id, each.title] for each in sources]

        return sources_id

    @classmethod
    @cache.memoize(timeout=90)
    def list_of_all_source_id(cls):

        """
         Получаем id всех источников из базы
        """

        return [each.id for each in cls.query.all()]

    @classmethod
    @cache.memoize(timeout=90)
    def list_of_right_geo_sources(cls, city, country):
        """
         Получаем id города и страны пользователя, сночала выбираем все источки, соответствующие городу.
         Потом все источники соответствующие стране.
         Потом все источники где в качестве гео выбран весь мир.
         В таком порядке и передаем
        """
        query = db.select([cls.id]).where(or_(
            cls.geo.like('%,' + city + ',%'),
            cls.geo.like('%,' + country + ',%'),
            cls.geo == ''))

        return set(source[0] for source in db.session.execute(query))

    def total_stats(self):
        return source_stats.get_total(self.id)

    @staticmethod
    def reset_total_stats(id):
        source_stats.reset_total(id)

    @classmethod
    @cache.memoize(timeout=90)
    def get_all_active(cls, ts, filters=None):
        """
        :rtype: collections.Iterable[Source]
        """

        # Make a copy of filters because we going to change it
        filters_copy = {} if filters is None else copy.copy(filters)
        filters_copy.setdefault('is_fullfill', False)

        if 'ids' in filters_copy:
            ids = filters_copy.pop('ids')
        else:
            ids = None

        sources = cls.query \
            .filter_by(**filters_copy)

        if ids:
            sources = sources.filter(Source.id.in_(ids))

        return [x for x in sources.all() if x.is_active(ts)]

    @classmethod
    @cache.memoize(timeout=config.SOURCES_CHECK_TIMEOUT)
    def get_all_ids_in_rotation(cls, filters=None):
        """
        Список ID всех источников, которые
        1) активны на данный момент;
        2) не исчерпали лимит открутки.
        3) участвуют в адовых галереях (опционально)
        4) являются источниками-заглушками для адовых галерей (опционально)

        :return: collections.Iterable[int]
        """
        sources = cls.get_all_active(time.time(), filters=filters)
        today_stats = source_stats.get_all_for_today()
        total_stats = source_stats.get_all_total()
        pending_sources = (x for x in sources if x.is_within_limits(today_stats[x.id], total_stats[x.id]))
        return [x.id for x in pending_sources]

    @classmethod
    @cache.memoize(timeout=config.SOURCES_CHECK_TIMEOUT)
    def _get_prices_dict(cls):
        """
        Словарь source_id -> click_price (для обсчета аукционов).
        """
        result = cls.query.with_entities(cls.id, cls.click_price).all()
        return {x.id: x.click_price for x in result}

    @classmethod
    def get_prices(cls, ids):
        prices_dict = cls._get_prices_dict()
        return [prices_dict[i] for i in ids]

    @classmethod
    def get_overall_price_for_today(cls):
        """
        Средняя цена клика за сегодня

        """
        today_stats = source_stats.get_all_for_today()
        prices = defaultdict(int)
        prices.setdefault(1)
        prices.update(cls._get_prices_dict()) 

        weighted_clicks = [
            float(v['click'])*float(prices[k]) for k, v in today_stats.items()
        ]
        unweighted_clicks = [
            float(v['click']) for v in today_stats.values()
        ]

        w_sum = sum(weighted_clicks) 
        u_sum = sum(unweighted_clicks) 
        print("w_sum %s" % w_sum)
        print("u_sum %s" % u_sum)

        average_price = w_sum/u_sum if u_sum else 0

        return average_price

    def __repr__(self):
        return "Source #%d: %s" % (self.id, self.title)

# dynamically create the attributes for Source's weekdays activity
for d in WEEKDAYS:
    setattr(Source, 'active_%s' % d, db.Column(db.Boolean, default=True, nullable=False))
    # время для показа источника - по дням недели
    # по умолчанию - показывать всегда (каждый день весь день)
    setattr(Source, 'active_%s_start' % d, db.Column(db.SmallInteger, default=0, nullable=False))
    setattr(Source, 'active_%s_end' % d, db.Column(db.SmallInteger, default=MINUTES_IN_DAY, nullable=False))


class Item(db.Model):
    """This serves as a generic content item on the website -- may be an article or a banner
    or any other text or media format."""

    __tablename__ = 'items'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
    num = db.Column(db.Integer, default=100, nullable=False, index=True)
    type = db.Column(db.String(20), index=True)

    unid = db.Column(db.String(32), nullable=False, default=gen_unid, unique=True)
    mnemonic_id = db.Column(db.String(255), default=gen_mnemonic_id, nullable=False)
    title = db.Column(db.String(2048), nullable=False)
    annotation = db.Column(db.Text(), default='', nullable=False)
    body = db.Column(db.Text(), default='')
    date = db.Column(db.DateTime(), default=datetime.now(), nullable=False)

    source_id = db.Column(db.Integer, db.ForeignKey('sources.id'))
    source = db.relationship(Source, backref=db.backref("items", cascade="all, delete-orphan"))

    is_distilled = db.Column(db.Boolean(), index=True, default=False, nullable=False)
    is_published = db.Column(db.Boolean(), index=True, default=False, nullable=False)
    is_on_morda = db.Column(db.Boolean(), index=True, default=False, nullable=False)
    in_extra_block1 = db.Column(db.Boolean(), index=True, default=False, nullable=False)

    images = db.Column(JSON(), default={}, nullable=False)

    font_sizes = db.Column(JSON(), default={}, nullable=False)
    new_font_sizes = db.Column(db.Boolean(), default=False, nullable=False)

    is_gallery = db.Column(db.Boolean, nullable=False)
    is_gallery_from_hell = db.Column(db.Boolean, default=False, nullable=False)

    text_link = db.Column(db.String(1024), nullable=True)
    text_link_caption = db.Column(db.String(1024), nullable=True)

    video = db.Column(db.Text, nullable=True)
    link = db.Column(db.String(1024), nullable=False, default='')

    redirect_url = db.Column(db.String(1024), nullable=True)
    redirect_gen = db.Column(db.Numeric(5, 2), nullable=True)
    redirect_period = db.Column(db.SmallInteger, nullable=True)
    redirect_visits = db.Column(db.SmallInteger, nullable=True)
    is_redirect_on = db.Column(db.Boolean, default=True, nullable=False)

    morda_num = db.Column(db.Integer, default=100, nullable=False)

    shows = db.Column(db.Integer, default=0, nullable=False)
    clicks = db.Column(db.Integer, default=0, nullable=False)
    ctrsort = db.column_property(clicks / shows)
    auction_shows_count = db.Column(db.SmallInteger, nullable=True)
    auction_clicks_count = db.Column(db.SmallInteger, nullable=True)
    auction_max_age_days = db.Column(db.SmallInteger, nullable=True)

    auction_algorithm = db.Column(db.String(64), nullable=True)

    fast_auction_ctr_threshold = db.Column(db.SmallInteger, nullable=True)

    __mapper_args__ = {
        'polymorphic_on': type,
    }

    @property
    @hybrid_property
    def ctr(self):
        return self.clicks / self.shows if self.shows else None

    def __unicode__(self):
        return self.title

    def __repr__(self):
        return "%s #%d, src: #%s: %s" % (
            self.api_collection_name,
            self.id,
            self.auction_algorithm,
            self.title
        )

    # ## Helper methods

    def _fake_filename(self):
        if self.mnemonic_id:
            return '%s-%s' % (self.id, self.mnemonic_id)
        else:
            return '%s' % self.id

    def _banner_path(self, ext='html', **kwargs):
        """
        Path for banners and lines.
        """
        params = '?' + urlencode(kwargs) if len(kwargs) > 0 else ''
        return '/read/%s.%s%s' % (self._fake_filename(), ext, params)

    def path(self, ext='html', **kwargs):
        return self._banner_path(ext, **kwargs)

    def image_url_from_image_data(self, i, w, h):
        """
        Full url path to resized image based on image data structure.
        """
        return resized_image_url(
            self.unid, int(i['num']), w, h, int(i['cx']), int(i['cy'])
        )

    def image_url(self, w, h):
        """
        Full url path to resized image (main image of the item).
        """
        if not self.images:
            return ""
        else:
            return self.image_url_from_image_data(self.images[0], w, h)

    def is_video(self):
        return self.video is not None and self.video.strip() != ''

    def title_size_class(self, width_class, block_height, min_title_size, max_title_size,
                         is_coloured, min_img_height=120, default_class='s20'):
        """
        В словаре font_sizes найти оптимальный размер шрифта для блока
        (наибольший, подходящий под высоту блока минус минимальную высоту картинки).

        Возвращает тапл (класс_заголобка, высота_картинки)

        :type width_class: str
        :type block_height: int
        :type min_title_size: int
        :type max_title_size: int
        :type is_coloured: bool
        :rtype: (str, int)
        """
        # цветные блоки закодированы классом 'wcNN', обычные - 'wNN'
        wc = width_class if not is_coloured else 'wc' + width_class[1:]
        max_height = block_height - min_img_height
        sizes_dict = self.font_sizes.get(wc, {})
        sorted_size_items = sorted(sizes_dict.items(), key=lambda x: x[1]['bh'], reverse=True)
        try:
            ts_class, bh, lines = next((ts, attrs['bh'], attrs['l']) for ts, attrs in sorted_size_items
                                       if attrs['bh'] <= max_height
                                       and min_title_size <= int(ts[1:]) <= max_title_size
                                       and attrs['l'] <= 5)
            return ts_class, block_height - bh
        except StopIteration:
            # безопасное определение высоты заголовка по умолчанию, на случай, если sizes_dict пуст
            # в мирное время такого быть не должно, только при аберрациях импорта из легаси-бд
            default_bh = sizes_dict.get(default_class, {}).get('bh', 54)
            return default_class, block_height - default_bh

    def is_article(self):
        return self.type == 'news'

    def is_banner(self):
        return self.type == 'banner'

    def is_line(self):
        return self.type == 'line'

    def stripped_body(self):
        return util.strip_tags(self.body)

    @classmethod
    @cache.memoize(timeout=90)
    def get_all_titles_and_source_ids(cls):
        """
        A dictionary id->dict for all items.
        """
        items = cls.query.with_entities(cls.id, cls.title, cls.source_id).all()
        return {
            x.id: {
                'title': x.title,
                'source_id': x.source_id
            } for x in items
        }

    @classmethod
    @cache.memoize(timeout=90)
    def get_all_published_ids(cls):
        """
        Returns a list of IDs of active items
        """
        return [x.id for x in cls.query.with_entities(cls.id).filter(cls.is_published==True)]

    @classmethod
    @cache.memoize(timeout=90)
    def last_update_time(cls):
        """
        Время последнего обновления.

        :rtype: datetime
        """
        return db.session.query(func.max(cls.date)).scalar()

    @staticmethod
    def _apply_tags_filter(queryset, tag_ids):
        if tag_ids:
            return queryset.filter(
                Item.tags.any(
                    Tag.id.in_(tag_ids)
                )
            )
        else:
            return queryset.filter(
                ~Item.tags.any()
            )

    @classmethod
    def parametrize_cache_key(cls, base_key, options):
        if not options:
            return base_key

        key_chunks = []
        for key in sorted(options.keys()):
            key_chunks.append('%s=%s' % (key, options[key]))

        return '%s--%s' % (base_key, ','.join(key_chunks))

    @classmethod
    def fetch_ids_from_cache(cls, key, count, tag_ids=None):
        cache_keys = []
        if tag_ids:
            for tag_id in tag_ids:
                cache_keys.append(cls.parametrize_cache_key(key, dict(tag=tag_id)))
        else:
            cache_keys.append(key)

        return super_cache.sample_from_multiple_sets(cache_keys, count)

    @classmethod
    @cache.memoize(timeout=300)
    def get_item_auction_limits(cls, item_id):
        if config.OVERALL_AUCTION:
            return site_config['auction_shows_count'], site_config['auction_clicks_count']

        item = cls.query.with_entities(cls.auction_shows_count, cls.auction_clicks_count).filter(cls.id == item_id).first()

        return ((item.auction_shows_count if item else 0) or site_config['auction_shows_count'],
                (item.auction_clicks_count if item else 0) or site_config['auction_clicks_count'])

    @classmethod
    @cache.memoize(timeout=300)
    def get_item_age_days(cls, item_id):
        item = cls.query.with_entities(cls.auction_max_age_days).filter(cls.id == item_id).first()
        auction_days = site_config['auction_max_age_days']

        if item.auction_max_age_days is not None:
                auction_days = item.auction_max_age_days
        return auction_days

    @hybrid_property
    def search_fields(self):
        '''
        Return concatenation of fields for searching in REST API
        '''
        return self.title + self.link

    @classmethod
    @cache.memoize(timeout=90)
    def get_for_read(cls, item_id):
        """
        Достать данные для перехода по линку (и его регистрации в статистике)
        """
        item = cls.query.filter(cls.id == item_id).first()
        return item


VIDEO_CODE_RE = re.compile(r'^\s*(<iframe\s+[^>]*width\s*=\s*\"*)(\d+)(\"*\s[^>]*height\s*=\s*\"*)(\d+)(.*)$',
                           re.MULTILINE | re.IGNORECASE | re.DOTALL)


class NewsItem(Item):
    """
    Новость (типа статья, не путать с баннером или строчкой).
    """
    __mapper_args__ = {
        'polymorphic_identity': 'news'
    }
    api_collection_name = 'news'

    def path(self, ext='html', direct=False, **kwargs):
        if direct:
            return '/news/%s.%s' % (self._fake_filename(), ext)
        else:
            return self._banner_path(ext, **kwargs)

    def video_code(self, width):
        """
        Подстроить код вставки видео под нужную ширину.
        """
        m = VIDEO_CODE_RE.search(str(self.video))
        if m:
            orig_width, orig_height = int(m.group(2)), int(m.group(4))
            height = math.ceil(width * (orig_height / orig_width))
            return '%s%d%s%d%s' % (m.group(1), width, m.group(3), height, m.group(5))

    @classmethod
    @cache.memoize(timeout=90)
    def fetch_list_for_morda(cls, offset=0, limit=50):
        """
        Набор новостей для морды.
        """
        items = cls.query.filter(cls.is_on_morda, cls.is_published) \
            .order_by(desc(cls.date)) \
            .offset(offset) \
            .limit(limit) \
            .all()

        # первые 50 могут иметь застолбленные места на морде, если указан morda_num
        placements = min(50, len(items))
        head = items[:placements]
        tail = items[placements:]
        return sorted(head, key=lambda x: 100 if x.morda_num == 0 else x.morda_num) + tail

    @classmethod
    @cache.memoize(timeout=90)
    def fetch_list_for_morda_with_geo(cls, geo, limit=63):
        """
        Набор новостей для морды от источников, чье гео соответствует гео пользователя.
        """

        my_items = cls.query.filter(cls.source_id.in_(geo)).limit(limit).all()

        return my_items

    CACHE_ARTICLE_IDS_KEY = 'article_ids'

    @classmethod
    @cache.memoize(timeout=300)
    def _cache_article_ids(cls, tag_ids):
        """
        Закэшировать айди опубликованных статей, чтобы выбирать из них случайные каждый раз.
        :rtype: None
        """
        cache_key = cls.parametrize_cache_key(cls.CACHE_ARTICLE_IDS_KEY, dict(tags=tag_ids))

        objects = cls.query.with_entities(cls.id) \
            .filter(cls.is_published)
        objects = cls._apply_tags_filter(objects, tag_ids)
        ids = [x.id for x in objects.all()]
        super_cache.overwrite_set(
            cache_key,
            ids
        )

        return super_cache.gen_key(cache_key)

    @classmethod
    @cache.memoize(timeout=config.RANDOM_ACTION_TIMEOUT)
    def fetch_list_for_item_page_with_geo(cls, geo, count=36, tag_ids=None):
        """
        Набор новостей для обвеса статьи (в .ahtml).
        Рандомные опубликованные статьи.
        """

        qs = cls.query.filter(cls.source_id.in_(geo)).offset(0).limit(count).all()

        return qs

    @classmethod
    @cache.memoize(timeout=config.RANDOM_ACTION_TIMEOUT)
    def fetch_list_for_item_page(cls, item_id, sources=None, count=36, tag_ids=None):
        """
        Набор новостей для обвеса статьи (в .ahtml).
        Рандомные опубликованные статьи.
        """
        if sources is None:
            sources = []

        cache_key = cls._cache_article_ids(tag_ids)

        ids = set(super_cache.sample_from_set(cache_key, count + 1)) - {item_id}
        if len(ids) == 0:
            ids = [0]

        qs = cls.query \
            .filter(or_(NewsItem.source_id.in_(sources),NewsItem.source_id.is_(None)))\
            .filter(NewsItem.is_published == True) \
            .order_by(desc(func.field(cls.id, *ids))) \

        qs = cls._apply_tags_filter(qs, tag_ids) \
            .order_by(NewsItem.date.desc()) \
            .offset(0) \
            .limit(count).all()

        return qs

    @classmethod
    @cache.memoize(timeout=90)
    def fetch_popular(cls, sources=None, limit=13):
        """
        Самые посещаемые статьи по статистике за два последних дня.
        """
        if sources is None:
            sources = []

        ids = article_top_chart.get_top_popular(limit)

        # это на крайняк, если вообще нет статистики (без параметров sql-запрос не сработает)
        if len(ids) == 0:
            ids = [0]

        return cls.query.filter(or_(NewsItem.source_id.in_(sources),NewsItem.source_id.is_(None)))\
            .filter(cls.is_on_morda, cls.is_published) \
            .order_by(desc(func.field(cls.id, *ids)), desc(cls.date)) \
            .offset(0) \
            .limit(limit) \
            .all()

    @classmethod
    @cache.memoize(timeout=300)
    def _cache_gallery_ids(cls):
        """
        Закэшировать айди галерей, чтобы выбирать из них случайные каждый раз.

        :rtype: None
        """
        result = cls.query.with_entities(cls.id) \
            .filter(cls.is_published, cls.is_gallery) \
            .all()
        ids = [x.id for x in result]
        super_cache.overwrite_set(SuperCache.CACHE_GALLERY_IDS_KEY, ids)
        return 1  # for caching

    @classmethod
    @cache.memoize(timeout=config.RANDOM_ACTION_TIMEOUT)
    def fetch_galleries(cls, item_id, count=16):
        """
        Случайный набор галереек.
        """

        # memoized, only actually called once in a while
        cls._cache_gallery_ids()
       
        gallery_ids = cls.fetch_ids_from_cache(SuperCache.CACHE_GALLERY_IDS_KEY, count)

        return cls.query \
             .filter(cls.id.in_(gallery_ids))\
             .order_by(func.rand()) \
             .offset(0) \
             .limit(count) \
             .all()


class Banner(Item):
    """
    Баннер (не имеет собственной страницы, перекидывает на внешнюю ссылку.
    Как правило, имеет источник, который регламентирует количество показов баннера.
    """
    __mapper_args__ = {
        'polymorphic_identity': 'banner'
    }
    api_collection_name = 'banners'


    @classmethod
    @cache.memoize(timeout=300)
    def _get_ids_by_average_ctrs(cls):
        """
        Список ID, отсортированный по мере уменьшения среднего CTR.
        :rtype: list[int]
        """
        ctrs = banner_stats.get_average_ctrs()
        sorted_ctrs = sorted(ctrs.items(), key=lambda a: a[1], reverse=True)
        return [x[0] for x in sorted_ctrs]

    @classmethod
    @cache.memoize(timeout=config.SOURCES_CHECK_TIMEOUT)
    def cache_banner_ids_in_rotation(cls, cache_key=SuperCache.CACHE_BANNER_IDS_IN_ROTATION_KEY, filters=None, src_filters=None):
        """
        Закэшировать айди всех баннеров в ротации (чьи источники актуальны на данный момент).

        :rtype: None
        """
        source_ids = Source.get_all_ids_in_rotation(filters=src_filters)

        # This query generates rather complex SQL. TODO: optimize it
        expr = db.select([cls.__table__.c.id, item_tags_assoc_table.c.tag_id]) \
            .select_from(
                cls.__table__.outerjoin(item_tags_assoc_table)
            ) \
            .where(cls.type == 'banner') \
            .where(cls.is_published == 1) \
            .where(cls.source_id.in_(source_ids)) \
            .where(cls.in_extra_block1 == False)

        if filters:
            expr = expr.where(*filters)

        result = db.session.execute(expr)

        tag_items = defaultdict(list)
        unique_items = set()

        for item_id, tag_id in result:
            tag_items[tag_id].append(item_id)
            unique_items.add(item_id)

        for tag_id, items in tag_items.items():
            if src_filters is not None:
                cache_key_options = copy.copy(src_filters)
            else:
                cache_key_options = {}

            cache_key_options['tag'] = tag_id

            super_cache.overwrite_set(
                cls.parametrize_cache_key(cache_key, cache_key_options),
                items
            )

        result_key = cls.parametrize_cache_key(cache_key, src_filters)

        super_cache.overwrite_set(
            result_key,
            unique_items
        )
        # current_app.logger.debug("Caching banners ids in rotation (filters: %s). %d banners found",
        #     str(src_filters),
        #     len(ids)
        # )
        return result_key


    @classmethod
    @cache.memoize(timeout=config.RANDOM_ACTION_TIMEOUT)
    def fetch_for_item_page_with_geo(cls, geo, count=50):

        qs = cls.query.filter(cls.source_id.in_(geo)).limit(count).all()
        return qs


    @classmethod
    @cache.memoize(timeout=config.RANDOM_ACTION_TIMEOUT)
    def fetch_for_item_page(cls, item_id, sources=None, exclude_ids=None, count=50, tag_ids=None, for_gallery=False):
        """
        Загрузить баннеры для страницы новости.

        Актуальные баннеры по убыванию средневзвешенного ctr, не входящие в exclude_ids.
        """
        if sources is None:
            sources = []
        if exclude_ids is None:
            exclude_ids = []

        src_filters = { 'show_in_galleries': for_gallery }

        cache_key = cls.cache_banner_ids_in_rotation(src_filters=src_filters)

        if type(exclude_ids)!=list:
            exclude_ids=[exclude_ids]

        if not exclude_ids:
            exclude_ids = [item_id]
        else:
            exclude_ids = exclude_ids + [item_id]

        ctr_order = cls._get_ids_by_average_ctrs()
        if not ctr_order:
            ctr_order = [item_id]

        ids = cls.fetch_ids_from_cache(
            cache_key,
            count + len(exclude_ids),
            tag_ids
        )
        ids = set(ids) - set(exclude_ids)
        
        if for_gallery:
            qs = cls.query \
                .filter(NewsItem.source_id.in_(sources))\
                .filter(cls.id.in_(ids)) \
                .order_by(func.rand())
        else:    
            qs = cls.query \
                .filter(NewsItem.source_id.in_(sources))\
                .filter(cls.id.in_(ids)) \
                .order_by(desc(func.field(cls.id, *ctr_order)), desc(cls.date)) 
                
        qs = cls._apply_tags_filter(qs, tag_ids) \
            .offset(0) \
            .limit(count) \
            .all()

        return qs

    @classmethod
    @cache.memoize(timeout=config.RANDOM_ACTION_TIMEOUT)
    def list_is_published(cls):
        return cls.query.filter(cls.in_extra_block1 == True,cls.is_published == True).all()

    @classmethod
    @cache.memoize(timeout=config.RANDOM_ACTION_TIMEOUT)
    def list_is_extra_block1(cls):
        output = cls.query.filter(cls.in_extra_block1 == True).all()
        return output

    @classmethod
    @cache.memoize(timeout=config.RANDOM_ACTION_TIMEOUT)
    def fetch_for_extra_block1(cls, item_id, sources, exclude_ids=None, count=4, tag_ids=None):
        """
        :returns: [Banners from whole], [Banners with `in_extra_block1 == True`]
        """
        if exclude_ids is None:
            exclude_ids = []
        for_extra_block1 = cls.query \
                    .filter(NewsItem.source_id.in_(sources))\
                    .filter(cls.in_extra_block1 == True,
                    cls.is_published == True) \
                    .filter(~cls.id.in_(exclude_ids)) \
                    .order_by(func.rand()).offset(0) \
                    .limit(count) \
                    .all()

        if len(for_extra_block1) < count:
            for_item = cls.fetch_for_item_page(
                item_id, sources, exclude_ids,
                count - len(for_extra_block1), tag_ids)
        else:
            for_item = []
        for_extra_block1 += for_item
        random.shuffle(for_extra_block1)
        return for_item, for_extra_block1

    @classmethod
    @cache.memoize(timeout=config.RANDOM_ACTION_TIMEOUT)
    def fetch_for_hell_gallery(cls, item_id, count=5):
        """
        Загрузить баннеры для адовой галереи

        Выбираются баннеры из источников с опцией show_in_hell_galleries,
        если таких нет или мало, добавляются баннеры из источников с опцией is_fullfill

        Сортируются баннеры по убыванию средневзвешенного ctr
        """
        #current_app.logger.debug("Creating a banners set for hell gallery")

        src_filters={
            'show_in_hell_galleries': True,
        }

        cache_key = cls.cache_banner_ids_in_rotation(src_filters=src_filters)

        ctr_order = cls._get_ids_by_average_ctrs()
        if not ctr_order:
            ctr_order = [item_id]

        ids = super_cache.sample_from_set(
            cache_key,
            count + 1
        )
        #current_app.logger.debug("cache key: %s",
        #    cls.parametrize_cache_key(SuperCache.CACHE_BANNER_IDS_IN_ROTATION_KEY, src_filters),)
        #current_app.logger.debug("banner IDS: %s", str(ids))

        ids = set(ids)
        ids.discard(item_id)

        items = cls.query \
            .filter(cls.id.in_(ids)) \
            .order_by(desc(func.field(cls.id, *ctr_order)), desc(cls.date)) \
            .offset(0) \
            .limit(count) \
            .all()

        if len(items) < count:
            src_filters = OrderedDict({ # OrderedDict have the only one string interpretation so @memoize works fine
                'show_in_hell_galleries': True,
                'is_fullfill': True,
            })
            cache_key_fullfill = cls.cache_banner_ids_in_rotation(src_filters=src_filters)

            items_shortage = count - len(items)

            ids = super_cache.sample_from_set(
                cache_key_fullfill,
                items_shortage,
            )

            fullfill_items = cls.query \
                .filter(cls.id.in_(ids)) \
                .order_by(desc(func.field(cls.id, *ctr_order)), desc(cls.date)) \
                .offset(0) \
                .limit(items_shortage) \
                .all()

            items.extend(fullfill_items)

        return items

    @classmethod
    @cache.memoize(timeout=config.RANDOM_ACTION_TIMEOUT)
    def fetch_distilled_for_item_page(cls, item_id, sources, count=40, tag_ids=None):
        """
        Загрузить дистилляты для страницы новости.

        Случайные дистилляты (нужно редко, дистиллятов мало, поэтому не паримся с рандомизацией).
        """
        qs = cls.query \
            .filter(cls.source_id.in_(sources))\
            .filter(cls.is_published,
                    cls.is_distilled,
                    cls.id != item_id)

        qs = cls._apply_tags_filter(qs, tag_ids)

        return qs.order_by(func.rand()) \
            .offset(0) \
            .limit(count) \
            .all()

    @classmethod
    @cache.memoize(timeout=config.SOURCES_CHECK_TIMEOUT)
    def _cache_top_strip_ids(cls):
        """
        Закэшировать айди ушей, чтобы выбирать из них случайные каждый раз.

        :rtype: None
        """
        source_ids = Source.get_all_ids_in_rotation()
        result = cls.query.with_entities(cls.id) \
            .filter(cls.is_published, cls.in_extra_block1, cls.source_id.in_(source_ids)) \
            .all()
        ids = [x.id for x in result]
        super_cache.overwrite_set(SuperCache.CACHE_TOP_STRIP_IDS_KEY, ids)
        return 1  # for caching

    @classmethod
    @cache.memoize(timeout=config.RANDOM_ACTION_TIMEOUT)
    def fetch_for_top_strip_with_geo(cls, geo, count=4):
        """
        Загрузить уши (вверху страниц статей).
        """
        cls._cache_top_strip_ids()

        # todo in rotation


        qs = cls.query.filter(cls.source_id.in_(geo)).offset(0).limit(count).all()

        return qs

    @classmethod
    @cache.memoize(timeout=config.RANDOM_ACTION_TIMEOUT)
    def fetch_for_top_strip(cls,  item_id, sources, count=4):
        """
        Загрузить уши (вверху страниц статей).
        """
        cls._cache_top_strip_ids()

        # todo in rotation

        ids = set(super_cache.sample_from_set(SuperCache.CACHE_TOP_STRIP_IDS_KEY, count + 1)) - {item_id}
        if len(ids) == 0:
            ids = [0]

        return cls.query \
            .filter(cls.source_id.in_(sources))\
            .order_by(desc(func.field(cls.id, *ids)), desc(cls.date)) \
            .offset(0) \
            .limit(count) \
            .all()

    @classmethod
    @cache.memoize(timeout=30)
    def fetch_ids_for_last_days(cls, days, tag_ids=None):
        objects = cls.query.with_entities(cls.id).filter(cls.date > (datetime.now() - timedelta(days=days)))
        objects = cls._apply_tags_filter(objects, tag_ids)

        return [i[0] for i in objects.all()]

    @classmethod
    def _get_auction_banners(cls, item_id, src_filters=None, from_result=False):
        from .extensions import auction
        stat_dict = auction.get_stats(item_id)

        items = {
            i: {
                'id': i,
                'shows': s,
                'clicks': c,
                'ctr': (100.0 * c / s) if s else 0
            }
            for i, (s, c, fft, lc) in stat_dict['total_stats'].items()
        }

        if from_result:
            result_ids = set(int(x[0]) for x in stat_dict['result'])
            items = {
                k: v
                for k, v in items.items()
                if k in result_ids
            }

        if not items:
            return {}

        # Отфильтровываем неактивные баннеры
        cache_key = cls.cache_banner_ids_in_rotation(src_filters=src_filters)
        rotation_ids = set(super_cache.smembers(cache_key))

        return {
            k: v
            for k, v in items.items()
            if k in rotation_ids
        }

    @classmethod
    def mix_lists(cls, primary, addition, mix_in, mix_count):
        """
        "Вкрапляет", если возможно, часть листа addition в primary, 
        чередуя элементы
        затем соединяет 
        """
        count = min(len(primary),len(addition),mix_count) 

        # копируем срез
        cut_primary = primary[mix_in:(mix_in+count)]
        cut_addition = addition[:count]

        # вырезаем срез 
        primary[mix_in:mix_in+count] = []
        addition[:count] = []

        mix = []
        # мешаем срезы 
        while cut_addition and cut_primary:
              mix_d = [cut_addition.pop(0), cut_primary.pop(0)]
              current_app.logger.debug(mix_d)
              mix += mix_d

        primary[mix_in:mix_in] = mix
        # current_app.logger.debug("mix: %s" % mix)
        response = primary + addition 
        return response

    @classmethod
    def _get_settled_auction_banners(cls, item_id, exclude_ids, count, src_filters, admin_mode=False):
        items = cls._get_auction_banners(item_id, src_filters, from_result=True)

        if not items:
            return []

        items_list = list(items.values())
        items_list.sort(key=lambda x: x['ctr'], reverse=True)

        settled_ids = [x['id'] for x in items_list]

        # выбираем оценённые баннеры по списку id сортированному по 
        # в аукционе CTR
        banners = cls.query \
            .filter(cls.id.in_(settled_ids)) \
            .filter(~cls.id.in_(exclude_ids)) \
            .order_by(func.field(cls.id, *settled_ids), desc(cls.date)) \
            .limit(count) \
            .all()

        if admin_mode:
            for banner in banners:
                banner.auction_stats = items[banner.id]
        return banners


    @classmethod
    def _get_unsettled_auction_banners(cls, item_id, exclude_ids, count, src_filters, tag_ids):
        from .extensions import auction

        def cache_banner_ids_in_rotation_fn():
            return cls.cache_banner_ids_in_rotation(src_filters=src_filters)

        def fetch_banner_ids_fn():
            return cls.fetch_ids_for_last_days(
                Item.get_item_age_days(item_id),
                tag_ids=tag_ids,
            )

        unsettled_ids = auction.get_banner_ids(
            item_id, count,
            fetch_banner_ids_fn, cache_banner_ids_in_rotation_fn,
            cleanup=False,
        )

        ctr_order = cls._get_ids_by_average_ctrs()
        if not ctr_order:
             ctr_order = [item_id]

        # выбираем неоценённые баннеры по списку
        # сортируем по убыванию общесистемного CTR 
        if unsettled_ids:
            return cls.query \
                .filter(cls.id.in_(unsettled_ids)) \
                .filter(~cls.id.in_(exclude_ids)) \
                .order_by(func.field(cls.id, *ctr_order)) \
                .limit(count) \
                .all()
        else:
            return []

    @classmethod
    @cache.memoize(timeout=config.RANDOM_ACTION_TIMEOUT)
    def fetch_flow(cls, item_id, sources, count=42, tag_ids=None, for_gallery=False, exclude_ids=None, mix_in=1,
                   mix_count=0, admin_mode=False):
        #Поставил count значение по умолчанию 42, чтобы избежать ошибки вызова, когад этот аргумент не указывается

        """
        Возвращает единый список сортированный для страницы новостей.
        Issue #60
        """
        if exclude_ids is None:
            exclude_ids = []

        # OrderedDict here to make FlaskCache happy: usual dict have several string
        # representations (with different order of keys) but OrderedDict have only one
        src_filters = OrderedDict({
            'show_in_hell_galleries': False,
        })

        src_filters['show_in_galleries'] = for_gallery

        if sources:
            src_filters['ids'] = sources
            src_filters['ids'].add(None)

        settled_banners = cls._get_settled_auction_banners(item_id, exclude_ids, count, src_filters, admin_mode)
        exclude_ids += [x.id for x in settled_banners]
        unsettled_banners = cls._get_unsettled_auction_banners(item_id, exclude_ids, count - len(settled_banners), src_filters, tag_ids)

        if not mix_count:
            mix_count = len(unsettled_banners)

        if admin_mode:
            for banner in unsettled_banners:
                banner.auction_tag = 'testing'

        response = cls.mix_lists(settled_banners,
                                 unsettled_banners,
                                 mix_in,
                                 mix_count)
        return response

    @classmethod
    @cache.memoize(timeout=config.RANDOM_ACTION_TIMEOUT)
    def fetch_for_auction(cls, item_id, sources, count, tag_ids=None, exclude_ids=None):
        """
        Загрузить баннеры для аукциона.
        (Для неустаканенного item_id - очередные баннеры в ротацию;
        для устаканенного - рассчитанный "оптимальный" набор.
        """
        if exclude_ids is None:
            exclude_ids = []

        def fetch_banner_ids_fn():
            return cls.fetch_ids_for_last_days(
                Item.get_item_age_days(item_id),
                tag_ids=tag_ids,
            )

        from .extensions import auction
        auction_ids = auction.get_banner_ids(item_id, count, fetch_banner_ids_fn,
                                             cls.cache_banner_ids_in_rotation)

        return cls.query \
            .filter(NewsItem.source_id.in_(sources))\
            .filter(cls.id.in_(auction_ids)) \
            .filter(~cls.id.in_(exclude_ids)) \
            .order_by(func.rand()) \
            .offset(0) \
            .limit(count) \
            .all()

    @classmethod
    @cache.memoize(timeout=config.RANDOM_ACTION_TIMEOUT)
    def fetch_fast_flow(
            cls, item, sources, count, tag_ids=None,
            for_gallery=False, exclude_ids=[], mix_in=1,
            mix_count=0, admin_mode=False,
    ):
        # OrderedDict here to make FlaskCache happy: usual dict have several string
        # representations (with different order of keys) but OrderedDict have only one
        src_filters = OrderedDict({
            'show_in_hell_galleries': False,
            'show_in_galleries': for_gallery,
        })

        if sources:
            src_filters['ids'] = sources
            src_filters['ids'].add(None)

        auction_stats = cls._get_auction_banners(item.id, src_filters)

        ctr_threshold = item.fast_auction_ctr_threshold or site_config['fast_auction_ctr_threshold']
        winner_banner_ids_set = set([x['id'] for x in auction_stats.values() if x['ctr'] > ctr_threshold])

        winners = []
        settled_banners = []
        unsettled_banners = []
        selected_banners_count = 0

        winner_banner_ids = list(winner_banner_ids_set)[0:count]
        selected_banners_count += len(winner_banner_ids)
        winners.sort(key=lambda x: auction_stats[x.id]['ctr'], reverse=True)

        winners = cls.query \
            .filter(cls.id.in_(winner_banner_ids)) \
            .all()
        winners.sort(key=lambda x: auction_stats[x.id]['ctr'], reverse=True)


        # Если баннеров-победителей недостаточно, посмотрим, есть ли неоттестированные баннеры
        if selected_banners_count < count:
            unsettled_banners = cls._get_unsettled_auction_banners(item.id, itertools.chain(winner_banner_ids, [item.id]),
                                                                   count - selected_banners_count, src_filters, tag_ids)

            # TODO: посмотреть, не делается ли для этого отдельный запрос
            selected_banners_count += len(unsettled_banners)

        # Если баннеров недостаточно, добавим открученные
        if selected_banners_count < count:
            shows_threshold, _ = item.get_item_auction_limits(item.id)
            settled_banner_ids = [
                x['id'] for x in auction_stats.values()
                if x['shows'] >= shows_threshold and not x['id'] in winner_banner_ids_set
            ]
            settled_banner_ids.sort(key=lambda x: auction_stats[x]['ctr'], reverse=True)
            settled_banner_ids = settled_banner_ids[0:count - selected_banners_count]
            selected_banners_count += len(settled_banners)

            settled_banners = cls.query \
                .filter(cls.id.in_(settled_banner_ids)) \
                .all()
            settled_banners.sort(key=lambda x: auction_stats[x.id]['ctr'], reverse=True)

        if admin_mode:
            for banner in winners:
                banner.auction_tag = 'winner'
                banner.auction_stats = auction_stats.get(banner.id)

            for banner in unsettled_banners:
                banner.auction_tag = 'testing'
                banner.auction_stats = auction_stats.get(banner.id)

            for banner in settled_banners:
                banner.auction_tag = 'settled'
                banner.auction_stats = auction_stats.get(banner.id)

        return winners + unsettled_banners + settled_banners

@event.listens_for(Banner, 'after_insert')
def add_to_auction(mapper, connection, banner):
    from .extensions import auction

    cache_key = NewsItem._cache_article_ids(tag_ids=banner.tags)
    article_ids = super_cache.redis.smembers(cache_key)

    source = Source.query.filter(Source.id==banner.source_id).first()
    is_active = source.is_active(time.time())
    current_app.logger.debug(
        "Adding banner %s to %d auctions (is_active=%s)",
        banner, len(article_ids), is_active
    )
    auction.add_banner_to_auctions(banner, article_ids, is_active=is_active)

@event.listens_for(NewsItem, 'after_update')
def add_to_auction(mapper, connection, item):
    from .extensions import auction
    state = db.inspect(item)

    attr_history = state.get_history('auction_algorithm', True)
    if attr_history.has_changes():
        prev_setting = attr_history.deleted[0]
        new_setting = attr_history.added[0]
        effective_prev_setting = prev_setting or site_config['auction_algorithm']
        effective_new_setting = new_setting or site_config['auction_algorithm']
        if effective_prev_setting != effective_new_setting:
            current_app.logger.info(
                "Auction algorithm for item %d has changed (%s -> %s), resetting auction",
                item.id,
                prev_setting,
                new_setting,

            )
            auction.reset_auction(item.id)

class Line(Item):
    """
    Строчка (баннер, состоящий только из заголовка и ссылки).
    """
    __mapper_args__ = {
        'polymorphic_identity': 'line'
    }
    api_collection_name = 'lines'

    CACHE_ACTIVE_LINE_IDS_KEY = 'active_line_ids'

    @classmethod
    @cache.memoize(timeout=config.SOURCES_CHECK_TIMEOUT)
    def _cache_active_line_ids(cls):
        """
        Закэшировать айди активных строчек, чтобы выбирать из них случайные каждый раз.

        :rtype: None
        """
        source_ids = Source.get_all_ids_in_rotation()
        result = cls.query.with_entities(cls.id) \
            .filter(cls.is_published, cls.source_id.in_(source_ids)) \
            .all()
        ids = [x.id for x in result]
        super_cache.overwrite_set(cls.CACHE_ACTIVE_LINE_IDS_KEY, ids)
        return 1  # for caching

    @classmethod
    @cache.memoize(timeout=config.RANDOM_ACTION_TIMEOUT)
    def fetch_random_active(cls, sources):
        """
        Получить случайную активную строчку.

        :rtype: Line
        """
        cls._cache_active_line_ids()

        ids = set(super_cache.sample_from_set(cls.CACHE_ACTIVE_LINE_IDS_KEY, 1))
        if len(ids) == 0:
            ids = [0]

        return cls.query \
            .filter(Line.source_id.in_(sources))\
            .filter(Line.is_published == True) \
            .order_by(desc(func.field(cls.id, *ids)), desc(cls.date)) \
            .offset(0) \
            .limit(1) \
            .first()


# many-to-many item-tag relationship
item_tags_assoc_table = db.Table('item_tags', db.Model.metadata,
                                 db.Column('tag_id', db.Integer, db.ForeignKey('tags.id')),
                                 db.Column('item_id', db.Integer, db.ForeignKey('items.id', ondelete='CASCADE')))


class Tag(db.Model):
    """
    Теги баннеров, новостей и источников
    """
    __tablename__ = 'tags'
    api_collection_name = 'tags'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
    title = db.Column(db.String(128), nullable=False)

    items = db.relationship(Item, secondary=item_tags_assoc_table, backref='tags')

    def __repr__(self):
        return "tag #%d, '%s'" % (
            self.id,
            self.title
        )


class Provider(db.Model):
    """
    Поставщик траффика
    """
    __tablename__ = 'providers'
    api_collection_name = 'providers'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
    name = db.Column(db.String(128), nullable=False)
    domain_pattern = db.Column(db.String(128), nullable=False)
    click_price = db.Column(db.Numeric(10, 2), default=0.0, nullable=False)

    def __init__(self, **kwargs):
        super(Provider, self).__init__(**kwargs)
        self.domain_patterns_list = [
            p.strip() for p in self.domain_pattern.split(',') if p.strip()
        ]

    def __repr__(self):
        return self.name

    def is_pattern_match(self, referrer):
        domain_patterns_list = [
            p.strip() for p in self.domain_pattern.split(',') if p.strip()
        ]
        host = urlparse(referrer).netloc
        # for p in self.domain_patterns_list:
        for p in domain_patterns_list:
            _p = fnmatch.translate(p)
            _r = re.compile(_p)
            if _r.match(host, re.IGNORECASE):
                return True
        return False

    @classmethod
    def get_provider(cls, referrer):
        providers = cls.query.all()
        for p in providers:
            if p.is_pattern_match(referrer):
                return p


class _ClickPriceHistory:
    """
    Примесный класс инструментов для классов истории цены кликов
    """
    @classmethod
    def get_latest_click_price(cls):
        latest_click_price_item = cls.query.order_by(desc(cls.id)).first()
        return latest_click_price_item.click_price if latest_click_price_item else None

    @classmethod
    def create_entry(cls, obj):
        """
        :param obj: Json of Provider or Source instance
        :return:
        """
        entry = cls()
        entry.click_price = obj['click_price']
        entry.source_id = obj['id']
        return entry

    @classmethod
    def append_entry(cls, obj):
        """
        :param obj: Json of Provider or Source instance
        :return:
        """
        if obj['click_price'] != cls.get_latest_click_price():
            entry = cls.create_entry(obj)
            db.session.add(entry)
            db.session.commit()

    @classmethod
    def click_price_items_updates(cls, id, range):
        if cls.__name__ == 'ProviderClickPriceHistory':
            click_price_items_all, click_price_items_in_range = (
                cls.query.filter_by(provider_id=id).order_by(desc(cls.id)).all(), []
            )
        elif cls.__name__ == 'SourceClickPriceHistory':
            click_price_items_all, click_price_items_in_range = (
                cls.query.filter_by(source_id=id).order_by(desc(cls.id)).all(), []
            )
        else:
            click_price_items_all, click_price_items_in_range = [], []

        def _is_item_in_range(item):
            range_start, range_end = range
            item_date = item.date
            if range_start < item_date < range_end:
                return True
            return False

        item_prev = None
        _is_range_entered = False
        for item in click_price_items_all:
            if _is_item_in_range(item):
                _is_range_entered = True
                click_price_items_in_range.append(item)
                if item_prev:
                    click_price_items_in_range.append(item_prev)
                    item_prev = None
            else:
                if _is_range_entered:
                    break
                item_prev = item

        return click_price_items_in_range

    @classmethod
    def _get_ranges(cls, timenodes):
            ranges, i = [], 0
            while i < len(timenodes)-1:
                ranges.append(timenodes[i:i+2])
                i += 1
            return ranges

    @classmethod
    def click_price_items_updates_ranges(cls, id, range):
        click_price_items_in_range_dates = [
            item.date for item
            in cls.click_price_items_updates(id, range)
        ]

        node_dates = sorted(click_price_items_in_range_dates + list(range))
        _ranges = cls._get_ranges(node_dates)
        # Ликвидируем пересечение временных промежутков
        # for _r in _ranges[1:]:
        #     _r[0] += timedelta(days=1)

        return _ranges

    @classmethod
    def get_price_by_date(cls, id, date):
        if cls.__name__ == 'ProviderClickPriceHistory':
            click_price_items_all = cls.query.filter_by(provider_id=id).order_by(asc(cls.id)).all()
            obj = Provider.query.get(id)
        elif cls.__name__ == 'SourceClickPriceHistory':
            click_price_items_all = cls.query.filter_by(source_id=id).order_by(asc(cls.id)).all()
            obj = Source.query.get(id)
        else:
            click_price_items_all, obj = [], None

        if not click_price_items_all:
            return obj.click_price

        _obj_now = cls()
        _obj_now.date = datetime.now()

        _range_total = cls._get_ranges(click_price_items_all + [_obj_now])

        for _i, _j in _range_total:
            if _i.date <= date < _j.date:
                return _i.click_price
        return 0

    @classmethod
    def click_price_average(cls, id, range):
        _prices = [
            item.click_price for item
            in cls.click_price_items_updates(id, range)
        ]
        return sum(_prices)/len(_prices) if len(_prices) else 0

    def __repr__(self):
        return str(self.click_price)


class SourceClickPriceHistory(db.Model, _ClickPriceHistory):
    __tablename__ = 'source_click_price_history'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
    click_price = db.Column(db.Numeric(10, 2), default=0.0, nullable=False)
    date = db.Column(db.DateTime(), default=datetime.now(), nullable=False)
    source_id = db.Column(db.Integer, db.ForeignKey('sources.id'))
    source = db.relationship(
        Source, backref=db.backref('source_click_price_history', cascade='all, delete-orphan')
    )


class ProviderClickPriceHistory(db.Model, _ClickPriceHistory):
    __tablename__ = 'provider_click_price_history'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
    click_price = db.Column(db.Numeric(10, 2), default=0.0, nullable=False)
    date = db.Column(db.DateTime(), default=datetime.now(), nullable=False)
    provider_id = db.Column(db.Integer, db.ForeignKey('providers.id'))
    provider = db.relationship(
        Provider, backref=db.backref('provider_click_price_history', cascade='all, delete-orphan')
    )


class AuditTrail(db.Model):
    """
    Таблица, куда логируются действия пользователей (аудит).
    """
    __tablename__ = 'audit_trail'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
    date = db.Column(db.DateTime(), default=datetime.now(), nullable=False)
    user = db.Column(db.String(1024), nullable=False)
    before_after = db.Column(db.String(6), nullable=False)
    action = db.Column(db.String(128), nullable=False)
    model = db.Column(db.String(128))
    json_data = db.Column(JSON(), default={}, nullable=False)
    rest_api_method = db.Column(db.String(128))

