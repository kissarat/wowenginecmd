from collections import OrderedDict
from datetime import datetime
from datetime import timedelta
import inspect
from json import dumps, JSONEncoder
import random
from math import ceil
from functools import wraps
from dateutil.parser import parse
from flask import render_template, send_file, redirect, current_app, request, jsonify, make_response, session
from flask.ext.login import current_user

from . import frontend
from sqlalchemy import func
from sqlalchemy.ext.declarative import DeclarativeMeta
from ..extensions import site_config, banner_stats, line_stats, statsd
from wowengine.frontend import events
import wowengine.images as img
from . import helpers
from .helpers import init_session 
from wowengine.models import NewsItem, Banner, Line, Item, Source, Provider
from wowengine.util import partition
from wowengine.config import config
from wowengine.util.useragents import is_robot


# endpoints that serve content (and that can serve as landing pages, for visits counting)
CONTENT_ENDPOINTS = ['frontend.%s' % x for x in ['news_item', 'index']]

from ..geo.geoip import Geo

GEO_IDS = Geo.short_yaml()


def get_sources_with_geo():

    city = '*'
    country = '*'

    if session.geoip.get_country_geoname_id():
        if str(session.geoip.get_country_geoname_id()) in GEO_IDS.keys():
            country = str(session.geoip.get_country_geoname_id())

            if session.geoip.get_city_geoname_id():
                if str(session.geoip.get_city_geoname_id()) in GEO_IDS[str(session.geoip.get_country_geoname_id())]['cities'].keys():
                    city = str(session.geoip.get_city_geoname_id())

    return Source.list_of_right_geo_sources(city=city, country=country)


def _redirect_to_another_installation(redirect_site):
    url = '%s%s' % (redirect_site, request.path)

    if request.query_string:
        url = '%s?%s' % (url, request.query_string.decode('ascii'))

    return redirect(url)


def redirect_to_empty_gif():
    return redirect('/images/dot.gif')


def referer_host_check(redirect_on_fail=None):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            if request.referrer and request.referrer.startswith(request.host_url):
                return func(*args, **kwargs)
            else:
                if redirect_on_fail:
                    return redirect(redirect_on_fail)
                else:
                    return make_response('Referrer check failed', 400)
        return wrapper
    return decorator


@frontend.context_processor
def add_template_helpers():
    helper_functions = inspect.getmembers(helpers, predicate=inspect.isfunction)
    return dict(helper_functions)


# @frontend.after_request
# def log_vars(r):
# if request.endpoint in CONTENT_ENDPOINTS:
# current_app.logger.info(session['_id'])
# current_app.logger.info(request.referrer)
#
# return r


# --- Jinja2 filters and tests

@frontend.app_template_filter('dash_str')
def dash_str(l):
    """
    Слепить строчку из списка элементов через дефис.
    """
    return '-'.join(map(str, l))


@frontend.app_template_test('is_banner')
def is_banner(item):
    return item.type == 'banner'


@frontend.app_template_test('is_article')
def is_article(item):
    return item.type == 'news'


@frontend.app_template_test('is_line')
def is_line(item):
    return item.type == 'line'

# --- Error handlers


@frontend.errorhandler(404)
def not_found(error=None):
    return redirect('/')


# --- Views

@frontend.route(
    "/images/news/<string:unid_subdir>/<string:unid>/<int:num>-res-<string:ext>-w<int:w>h<int:h>cx<int:cx>cy<int:cy>.jpg")
def image_resize(num, w, h, cx, cy, unid_subdir, unid, ext):
    """
    Поменять размер картинки, сохранить получившуюся картинку и отправить ее пользователю.
    """
    filename = img.resized_image(unid, num, w, h, cx, cy)
    if filename is None and current_app.config.get('FALLBACK_TO_PRODUCTION_IMAGES'):
        return redirect('http://%s%s' % (current_app.config.get('PRODUCTION_HOSTNAME'), request.path))
    return send_file(filename) if filename else not_found()


GALLERY_MAX_WIDTH = 648
GALLERY_MAX_HEIGHT = 375
GALLERY_RATIO = GALLERY_MAX_HEIGHT / GALLERY_MAX_WIDTH


@frontend.route("/geo/")
def index1():
    """
    Морда с гео.
    """
    news_items = NewsItem.fetch_list_for_morda_with_geo(get_sources_with_geo())

#   news_items = NewsItem.fetch_list_for_morda_with_geo(Source.list_of_right_geo_sources(city=session['geoip']['city_geoname_id'],country=session['geoip']['country_geoname_id']))
    if config.REDIRECT_MAINPAGE_URL:
        return _redirect_to_another_installation(config.REDIRECT_MAINPAGE_URL)

    # TODO haven't implemented following vars
    # title
    # image_src (for og:image)

    args = ({
        'path_fn': _gen_path_fn(ext='ahtml'),
        'news': news_items[13:],
        'popular': news_items[:13]
    })

    return render_template('morda.html', **args)


@frontend.route("/geotest")
def geotest():
    item_id = 24931
    output = ''
    output += 'id страны %s <br>' % session.geoip.get_country_geoname_id()

    city = '*'
    country = '*'

    if session.geoip.get_country_geoname_id():
        if session.geoip.get_country_geoname_id() in GEO_IDS.keys():
            country = session.geoip.get_country_geoname_id()

            if session.geoip.get_city_geoname_id():
                if session.geoip.get_city_geoname_id() in GEO_IDS[session.geoip.get_country_geoname_id()]['cities'].keys():
                    city = session.geoip.get_city_geoname_id()

    output = output + "Город входа: "+city+"<br>"
    output = output + "Страна входа: "+country+"<br>"

    sources = Source.list_of_right_geo_sources_test(city=city, country=country)
    output += "<b>Отобранные источники</b>" + str(sources) + '<br>'

    sources = Source.list_of_right_geo_sources(city=city, country=country)

    output += "<b>А вот так выглядит список источников, по которым я ищу </b> " + str(sources)+"<br>"
    output += '<b>popular_items</b> ' + str(NewsItem.fetch_popular(get_sources_with_geo())) + '<br>'
    output += '<b>banners fetch for item page</b> ' + str(Banner.fetch_for_item_page(item_id, sources=sources))+'<br>'
    output += '<b>banners fetch for auction</b> ' + str(Banner.fetch_for_auction(item_id, sources=sources, count=50)) + '<br>'
    output += '<b>banners fetch flow</b> ' + str(Banner.fetch_flow(item_id,sources=sources, count=50))+'<br>'
    output += '<b>banners fetch for top string</b> '+str(Banner.fetch_for_top_strip(item_id, sources))+'<br>'
    output += '<b>news gallery_items</b> ' + str(NewsItem.fetch_galleries(item_id))+'<br>'
    output += '<b>news popular_items</b> ' + str(NewsItem.fetch_popular(sources))+'<br>'

    output += '<b>banner fetch distilled for item page</b> ' + str(Banner.fetch_distilled_for_item_page(item_id, sources)) + '<br>'
    output += '<b>news fetch list for item page</b> ' + str(NewsItem.fetch_list_for_item_page(item_id, sources))+'</br>'
    output += '<b>top banners for html news</b> ' + str(Banner.fetch_for_extra_block1(item_id, sources=sources))
    output += '<b>hell gallery</b>' + str(Banner.fetch_for_hell_gallery(item_id, sources))

    return output


@frontend.route("/api/geo")
@frontend.route("/api/geo/<int:item_id>")
def api_geo(item_id=None):
    output = {
        'country_id': session.geoip.get_country_geoname_id() or None,
        'city_id': session.geoip.get_city_geoname_id() or None
    }
    city = '*'
    country = '*'

    if output['country_id']:
        if str(output['country_id']) in GEO_IDS.keys():
            country = str(output['country_id'])
            if output['city_id']:
                if str(output['city_id']) in GEO_IDS[session.geoip.get_country_geoname_id()]['cities'].keys():
                    city = str(output['city_id'])

    output['sources_test'] = Source.list_of_right_geo_sources_test(city=city, country=country)
    sources = Source.list_of_right_geo_sources(city=city, country=country)
    output['sources'] = list(sources)

    if item_id and Item.query.filter(Item.id == item_id).count() > 0:
        def _to_dict(l):
            return {item.id: item.title for item in l}

        output['items'] = {
            'geo_popular': _to_dict(NewsItem.fetch_popular(get_sources_with_geo())),
            'popular': _to_dict(NewsItem.fetch_popular(sources)),
            'gallery': _to_dict(NewsItem.fetch_galleries(item_id)),
            'page': _to_dict(NewsItem.fetch_list_for_item_page(item_id, sources)),
        }

        output['banners'] = {
            'page': _to_dict(Banner.fetch_for_item_page(item_id, sources=sources)),
            'flow': _to_dict(Banner.fetch_flow(item_id, sources=sources, count=50)),
            'top': _to_dict(Banner.fetch_for_top_strip(item_id, sources)),
            'distilled': _to_dict(Banner.fetch_distilled_for_item_page(item_id, sources)),
            'extra': [_to_dict(banners) for banners in Banner.fetch_for_extra_block1(item_id, sources=sources)]
        }
    elif item_id and item_id > 0:
        output['error'] = 'Item with id = %s does not exists' % item_id

    response = jsonify(output)
    response.content_type += ';charset=utf-8'
    return response


@frontend.route("/")
@statsd.timer('index.timings')
def index():
    """
    Морда.
    """
    statsd.incr('index.requests')
    if config.REDIRECT_MAINPAGE_URL:
        return _redirect_to_another_installation(config.REDIRECT_MAINPAGE_URL)

    # TODO haven't implemented following vars
    # title
    # image_src (for og:image)

    news_items = NewsItem.fetch_list_for_morda()
    popular_items = NewsItem.fetch_popular(get_sources_with_geo())

    args = ({
        'path_fn': _gen_path_fn(ext='ahtml'),
        'news': news_items,
        'popular': popular_items
    })

    return render_template('morda.html', **args)


def _gen_path_fn(ext='html', **kwargs):
    """
    Возвращает функцию, которая берет объект материала и возвращает путь согласно заданным доп. параметрам.
    """
    return lambda item, **new_args: item.path(ext=ext, **dict(kwargs, **new_args))


def _right_blocks_count(body, item_type):
    line_height = 19
    extra_height = 650  # approximation
    block_height = 100

    lines = int(len(body) / 140)
    body_height = lines * 19
    total_height = body_height + extra_height

    block_count = int(total_height / block_height)

    if item_type == 'news':
        if config.BANNER_RIGHT_COL:
            block_count += 1
        else:
            block_count += 3

#    current_app.logger.info("blocks count %d, lines %d, body %s, total %s" % (
#                            block_count,
#                            lines,
#                            body_height,
#                            total_height))
#
    return block_count


@frontend.route('/html_test')
def html_test():

    item_id = 24931
    output = ''
    output += 'id страны %s <br>' % str(session.geoip.get_country_geoname_id())

    city = '*'
    country = '*'

    if session.geoip.get_country_geoname_id():
        if str(session.geoip.get_country_geoname_id()) in GEO_IDS.keys():
            country = str(session.geoip.get_country_geoname_id())

            if session.geoip.get_city_geoname_id():
                if str(session.geoip.get_city_geoname_id()) in GEO_IDS[str(session.geoip.get_country_geoname_id())]['cities'].keys():
                    city = str(session.geoip.get_city_geoname_id())

    output = output + "Город входа: " + city + "<br>"
    output = output + "Страна входа: " + country + "<br>"
    sources = Source.list_of_right_geo_sources_test(city=city, country=country)
    output += "<b>Отобранные источники</b>" + str(sources) + '<br>'
    sources = Source.list_of_right_geo_sources(city=city, country=country)
    output += "<b>А вот так выглядит список источников, по которым я ищу </b> " + str(sources) + "<br>"

    init_session()

    sources = get_sources_with_geo()

    item = Item.query.get(item_id)

    item_tag_ids = [t.id for t in item.tags]

    right_blocks_count = _right_blocks_count(item.stripped_body(), item.type)

    args = {
        'path_fn': _gen_path_fn('html', from_id=item.id, is_auction=1),
        'item': item,
        'title': item.title,
        'line_ad': Line.fetch_random_active(sources),
        'admin_mode': current_user.is_authenticated(),
        'admin_messages': [],
        'show_echobanners_banners': config.SHOW_ECHOBANNERS_BANNERS,  # probably temporarly feature
    }

    if args['admin_mode']:
        add_admin_message = lambda msg, *fmt_args: args['admin_messages'].append(msg.format(*fmt_args))
    else:
        add_admin_message = lambda msg, *fmt_args: None

    rule_name = site_config.get('banner_excluding_rule')
    if rule_name == 'clicks':
        exclude_ids = session['clicked_banners'][:]
    elif rule_name == 'shows':
        exclude_ids = session['showed_banners'][:]
    else:
        exclude_ids = []

    while True:
        # новая механика: обвес и подвал суть аукцион
        # https://bitbucket.org/wowow/wowengine/issue/60/-------------------------------
        flow = Banner.fetch_flow(item_id,
                                 sources=sources,
                                 count=50 + right_blocks_count,
                                 tag_ids=item_tag_ids,
                                 for_gallery=False,
                                 exclude_ids=exclude_ids
                                 )
        if len(flow) < 30:
            if rule_name == 'clicks':
                session['clicked_banners'] = []
            elif rule_name == 'shows':
                session['showed_banners'] = []
            exclude_ids = []
            flow = Banner.fetch_flow(item_id,
                                     sources=sources,
                                     count=50 + right_blocks_count,
                                     tag_ids=item_tag_ids,
                                     for_gallery=False,
                                     exclude_ids=exclude_ids,
                                     )

        exclude_ids += [x.id for x in flow]
        extra_for_item, extra_block = Banner.fetch_for_extra_block1(
            item_id, sources, exclude_ids, tag_ids=item_tag_ids)
        random_galleries = NewsItem.fetch_galleries(item_id, 2)
        auction = flow[:right_blocks_count]
        banners = flow[right_blocks_count:]
        auction_prices = Source.get_prices(x.source_id for x in auction)
        extra_prices = Source.get_prices(x.source_id for x in banners)

        if item.is_gallery and random_galleries:
            random_next_gallery = random_galleries.pop()
            args['random_next_gallery'] = _gen_path_fn(ext)(random_next_gallery)
        break

    args.update({
        'top_items': extra_block,
        'sidebar_items': auction,
        'extra_items': banners,
        'prices': auction_prices,
        'popular_items': NewsItem.fetch_popular(get_sources_with_geo()),
    })

    output += '<b> Конфигурационное правило сайта </b> %s <br>' % (rule_name)
    output += '<b> Id исключений </b> %s <br>' % (str(exclude_ids))

    for each in args:
        output += '<b> %s </b> %s <br>' % (each, str(args[each]))

    output += '<b> is published </b> %s <br>' % (str(Banner.list_is_published()))
    #output +='<b> is extra block1 </b> %s <br>' % (str(Banner.list_is_extra_block1()))

    return output


@frontend.route('/news/<int:item_id>-<string:title>.<string:extension>')
@frontend.route('/news/<int:item_id>.<string:extension>', defaults={'title': None})
@frontend.route('/banners/<int:item_id>.<string:extension>', defaults={'title': None})
@statsd.timer('news_item.timings')
def news_item(item_id, title, extension):
    """
    Показать страницу материала.
    """
    statsd.incr('news_item.requests')

    ext = extension.lower()

    init_session()

    sources = get_sources_with_geo()

    if ext == 'ahtml' and config.REDIRECT_AHTML_URL:
        return _redirect_to_another_installation(config.REDIRECT_AHTML_URL)

    if ext == 'dhtml' and config.REDIRECT_DHTML_URL:
        return _redirect_to_another_installation(config.REDIRECT_DHTML_URL)

    item = Item.query.get(item_id)
    if item is None:
        return redirect('/')

    item_tag_ids = [t.id for t in item.tags]

    right_blocks_count = _right_blocks_count(item.stripped_body(), item.type)

    args = {
        'path_fn': _gen_path_fn('html', from_id=item.id, is_auction=1),
        'item': item,
        'title': item.title,
        'line_ad': Line.fetch_random_active(sources),
        'admin_mode': current_user.is_authenticated(),
        'admin_messages': [],
        'show_echobanners_banners': config.SHOW_ECHOBANNERS_BANNERS,  # probably temporarly feature
    }

    if args['admin_mode']:
        add_admin_message = lambda msg, *fmt_args: args['admin_messages'].append(msg.format(*fmt_args))
    else:
        add_admin_message = lambda msg, *fmt_args: None

    extra_prices = []
    exclude_ids = []
    if ext == 'html':
        # * обвес из опубликованных элементов типа "баннер", ротирующихся в логике показа баннеров
        # (не открученные источники)
        # * ротируются как дистилляты так и остальные??

        # Будем искать баннеры два приема:
        #   1. Возьмем нужное количество баннеров с учетом опции "показывать в галереях"
        #      (обычных или адовых)
        #   2. Если у нас галерея, и баннеров нашлось недостаточно, переключимся
        #      в режим обычного материала (не галереи), и наберем еще раз обычных баннеров.

        # в соответствии с заданым правилом исключаем баннеры из подборки
        rule_name = site_config.get('banner_excluding_rule')
        if rule_name == 'clicks':
            exclude_ids = session['clicked_banners'][:]
        elif rule_name == 'shows':
            exclude_ids = session['showed_banners'][:]
        else:
            exclude_ids = []

        auction_algorithm = item.auction_algorithm or site_config['auction_algorithm']
        current_app.logger.info("Auction algorithm: %s", auction_algorithm)

        if auction_algorithm == 'old':
            #  кликнутые баннеры добавляем в исключённия
            with statsd.timer('news_item.fetch_for_auction.timings'):
                auction = Banner.fetch_for_auction(
                    item_id,
                    sources=sources,
                    count=right_blocks_count,
                    tag_ids=item_tag_ids,
                    exclude_ids=exclude_ids
                )
            exclude_ids_from_extra = [x.id for x in auction]
            exclude_ids += exclude_ids_from_extra
            extra_for_item, extra_block = Banner.fetch_for_extra_block1(
                item_id,
                sources=sources,
                exclude_ids=exclude_ids_from_extra,
                tag_ids=item_tag_ids,
            )
            exclude_ids += [x.id for x in extra_for_item]

            # если в right_block понабрали мало, в общий
            # поток набираем недостачу и добиваем ею
            additive_count = right_blocks_count - len(auction)

            banners = Banner.fetch_for_item_page(
                item_id,
                sources=sources,
                exclude_ids=exclude_ids,
                count=50 + additive_count,
                tag_ids=item_tag_ids,
                for_gallery=False,
            )
            auction += banners[:additive_count]
            banners = banners[additive_count:]
            random_galleries = NewsItem.fetch_galleries(item_id, 2)
            auction_prices = Source.get_prices(x.source_id for x in auction)
        elif auction_algorithm == 'fast':
            with statsd.timer('news_item.fetch_fast_flow.timings'):
                flow = Banner.fetch_fast_flow(
                    item,
                    sources=sources,
                    count=50 + right_blocks_count,
                    tag_ids=item_tag_ids,
                    for_gallery=False,
                    exclude_ids=exclude_ids,
                    admin_mode=current_user.is_authenticated(),
                )
            if len(flow) < 30:
                add_admin_message("Обнулили список просмотренных и кликнутых баннеров, т.к. с учетом этих ограничений подходящих баннеров было недостаточно")
                if rule_name == 'clicks':
                    session['clicked_banners'] = []
                elif rule_name == 'shows':
                    session['showed_banners'] = []
                exclude_ids = []
                flow = Banner.fetch_fast_flow(
                    item,
                    sources=sources,
                    count=50 + right_blocks_count,
                    tag_ids=item_tag_ids,
                    for_gallery=False,
                    exclude_ids=exclude_ids,
                    admin_mode=current_user.is_authenticated(),
                )

            # current_app.logger.debug(flow)
            # current_app.logger.debug(len(flow))
            exclude_ids += [x.id for x in flow]
            extra_for_item, extra_block = Banner.fetch_for_extra_block1(
                item_id, sources, exclude_ids, tag_ids=item_tag_ids)
            random_galleries = NewsItem.fetch_galleries(item_id, 2)
            auction = flow[:right_blocks_count]
            banners = flow[right_blocks_count:]
            auction_prices = Source.get_prices(x.source_id for x in auction)
            extra_prices = Source.get_prices(x.source_id for x in banners)

            if item.is_gallery and random_galleries:
                random_next_gallery = random_galleries.pop()
                args['random_next_gallery'] = _gen_path_fn(ext)(random_next_gallery)
        else:
            # новая механика: обвес и подвал суть аукцион
            # https://bitbucket.org/wowow/wowengine/issue/60/-------------------------------
            with statsd.timer('news_item.fetch_flow.timings'):
                flow = Banner.fetch_flow(
                    item_id,
                    sources=sources,
                    count=50 + right_blocks_count,
                    tag_ids=item_tag_ids,
                    for_gallery=False,
                    exclude_ids=exclude_ids,
                    admin_mode=current_user.is_authenticated(),
                )
            if len(flow) < 30:
                add_admin_message("Обнулили список просмотренных и кликнутых баннеров, т.к. с учетом этих ограничений подходящих баннеров было недостаточно")
                if rule_name == 'clicks':
                    session['clicked_banners'] = []
                elif rule_name == 'shows':
                    session['showed_banners'] = []
                exclude_ids = []
                flow = Banner.fetch_flow(
                    item_id,
                    sources=sources,
                    count=50 + right_blocks_count,
                    tag_ids=item_tag_ids,
                    for_gallery=False,
                    exclude_ids=exclude_ids,
                    admin_mode=current_user.is_authenticated(),
                )

            # current_app.logger.debug(flow)
            # current_app.logger.debug(len(flow))
            exclude_ids += [x.id for x in flow]
            extra_for_item, extra_block = Banner.fetch_for_extra_block1(
                item_id, sources, exclude_ids, tag_ids=item_tag_ids)
            random_galleries = NewsItem.fetch_galleries(item_id, 2)
            auction = flow[:right_blocks_count]
            banners = flow[right_blocks_count:]
            auction_prices = Source.get_prices(x.source_id for x in auction)
            extra_prices = Source.get_prices(x.source_id for x in banners)

            if item.is_gallery and random_galleries:
                random_next_gallery = random_galleries.pop()
                args['random_next_gallery'] = _gen_path_fn(ext)(random_next_gallery)

        args.update({
            'top_items': extra_block,
            'sidebar_items': auction,
            'extra_items': banners,
            'prices': auction_prices,
            'popular_items': NewsItem.fetch_popular(get_sources_with_geo()),
        })

    elif ext == 'dhtml':
        # * обвес только из опубликованных элементов типа "баннер", имеющих признак "дистиллят"
        banners = Banner.fetch_distilled_for_item_page(
            item_id,
            sources,
            count=right_blocks_count + 45,
            tag_ids=item_tag_ids,
            for_gallery=item.is_gallery()
        )
        args.update({
            'top_items': banners[0:4],
            'sidebar_items': banners[4:right_blocks_count + 4],
            'extra_items': banners[right_blocks_count + 4:]
        })

    elif ext == 'ahtml':
        # * обвес из опубликованных элементов типа "новость", ротирующихся в логике ????
        # * вставляются топы и галереи
        news = NewsItem.fetch_list_for_item_page(item_id, sources, count=right_blocks_count + 37, tag_ids=item_tag_ids)

        args.update({
            'top_items': Banner.fetch_for_top_strip(item_id, sources),
            'sidebar_items': news[:right_blocks_count],
            'extra_items': news[right_blocks_count:],
            'gallery_items': NewsItem.fetch_galleries(item_id),
            'popular_items': NewsItem.fetch_popular(sources),
        })
    else:
        return not_found()

    if item.is_gallery:
        gallery_max_width = 0
        gallery_max_height = 0
        gallery_widths = {}
        gallery_heights = {}

        for i in item.images:
            item_ratio = i['height'] / i['width']

            if item_ratio > GALLERY_RATIO:
                # высоковатая картиночка
                height = GALLERY_MAX_HEIGHT
                width = round(height / item_ratio)
                if width > GALLERY_MAX_WIDTH - 140:
                    width = GALLERY_MAX_WIDTH
            else:
                # широковатая картиночка
                width = GALLERY_MAX_WIDTH
                height = round(width * item_ratio)
                if height > GALLERY_MAX_HEIGHT - 80:
                    height = GALLERY_MAX_HEIGHT

            gallery_max_width = max(width, gallery_max_width)
            gallery_max_height = max(height, gallery_max_height)
            gallery_widths[i['num']] = width
            gallery_heights[i['num']] = height

        # Usual gallery
        effective_ads_freq = len(item.images)
        effective_is_gallery_from_hell = False

        # Hell gallery
        hell_teasers = []
        if item.is_gallery_from_hell and ext not in ('ahtml', 'dhtml') and config.GALLERY_ADS_FREQ:
            required_teasers_count = ceil(len(item.images) / config.GALLERY_ADS_FREQ)

            _teasers = Banner.fetch_for_hell_gallery(item_id, sources, count=required_teasers_count)

            if len(_teasers) == required_teasers_count:
                effective_is_gallery_from_hell = True
                effective_ads_freq = config.GALLERY_ADS_FREQ
                hell_teasers = _teasers
            else:
                add_admin_message('Показывается как обычная галерея, т.к. недостаточно баннеров для адовых галерей')

        gallery_chunks = list(partition(item.images, effective_ads_freq))

        gallery_banners = Banner.fetch_for_item_page(item_id,
                                                     sources=sources,
                                                     exclude_ids=exclude_ids,
                                                     count=1,
                                                     tag_ids=item_tag_ids,
                                                     for_gallery=item.is_gallery)

        if ext == 'html':
            if gallery_banners:
                teaser = gallery_banners.pop()
            elif random_galleries:
                teaser = random_galleries.pop()
            else:
                teaser = None
        else:
            teaser = None

        args.update({
            'effective_is_gallery_from_hell': effective_is_gallery_from_hell,
            'gallery_max_height': gallery_max_height,
            'gallery_widths': gallery_widths,
            'gallery_heights': gallery_heights,
            'gallery_chunks': gallery_chunks,
            'ads_freq': effective_ads_freq,
            'frame_num': request.args.get('frame', 0),
            'total_gallery_items': len(item.images) + len(gallery_chunks),  # картинки плюс баннеры
            'hell_teasers': hell_teasers,
            'last_frame_teaser': teaser,
        }
        )

    if not request.referrer:
        referrer = ''
    else:
        referrer = request.referrer
    args.update({
        'slider_news': random.sample(args['extra_items'], min(len(args['extra_items']), 3)),
        'page_ext': ext,
        'extra_prices': extra_prices
    })

    if hasattr(config, 'SERVER_NAME'):
        args['external'] = int(not(config.SERVER_NAME in referrer))

    # Включать в урл баннера информацию о том, с какой страницы сделан клик.
    if item.is_banner() and ext != 'ahtml':
        args['path_fn'] = _gen_path_fn(ext, from_id=item.id, is_from_banner=1, is_auction=1)
    elif ext != 'ahtml':
        args['path_fn'] = _gen_path_fn(ext, from_id=item.id, is_auction=1)

    if not is_robot(request):
        events.on_page_visited(item, extension)
        if item.is_banner():
            events.on_banners_shown([item.id], [])
        else:
            statsd.incr('news_item.robot_visits')

    if config.MAIL_RU_PARAM:
        args['from_mail_ru'] = request.args.get(config.MAIL_RU_PARAM)
    else:
        args['from_mail_ru'] = True
    #
    # sources = get_sources_with_geo()
    # news = NewsItem.fetch_list_for_morda_with_geo(sources)
    # print (news)
    # print (sources)
    # args.update({
    #     'top_items': Banner.fetch_for_top_strip_with_geo(sources),
    #     'sidebar_items': news[:right_blocks_count],
    #     'extra_items': news[right_blocks_count:],
    #     'gallery_items': NewsItem.fetch_galleries(item_id),
    #     'popular_items': NewsItem.fetch_popular(),
    # })

    return render_template('news_item.html', **args)


def get_provider_id(referrer=None):
    _r = referrer or request.referrer
    provider = Provider.get_provider(_r)
    return provider.id if provider else None


@frontend.route("/read/<int:item_id>-<string:_title>.<string:ext>")
#@referer_host_check(redirect_on_fail='/')
@statsd.timer('ad_redirect.timings')
def ad_redirect(item_id, _title, ext):
    """
    Учесть переход по ссылке и сделать редирект
    """
    statsd.timer('ad_redirect.requests')
    init_session()
    current_app.logger.debug("append item %s" % item_id)
    session['clicked_banners'].append(item_id)

    item = Item.get_for_read(item_id)
    from_id = request.args.get('from_id')
    is_from_banner = int(request.args.get('is_from_banner', False))
    is_auction = int(request.args.get('is_auction', False))

    current_app.logger.debug(request.args)

    # 'mobile' argument is added by javascript to check if user have javascript enabled
    is_javascript_enabled = 'mobile' in request.args

    is_good_user = is_javascript_enabled and not is_robot(request)
    if current_app.debug:
        is_good_user = True
    if not is_good_user:
        statsd.incr('ad_redirect.robot_visits')

    need_tracking = request.referrer and '.html' in request.referrer

    if item.type == 'banner':
        if need_tracking:
            events.on_banner_clicked(
                item_id, item.source_id, from_id, is_from_banner, is_auction,
                provider_id=get_provider_id()
            )
        return redirect(item.link)
    elif item.type == 'line':
        if need_tracking:
            events.on_line_clicked(
                item_id, item.source_id, from_id, is_from_banner, is_auction,
                provider_id=get_provider_id()
            )
        return redirect(item.link)
    else:
        if is_good_user and need_tracking:
            events.on_article_teaser_clicked(
                item_id, provider_id=get_provider_id()
            )
            
        if ext not in ('ahtml', 'dhtml', 'html'):
            ext = 'html'

        return redirect(item.path(ext=ext, direct=True))


@frontend.route("/track/shown/news/<articles>")
@referer_host_check()
def track_articles_shown(articles):
    """
    Учесть показы тизеров статей
    """
    articles = [int(x) for x in articles.split('-') if x]
    if request.args.get('external'):
        external = int(request.args.get('external'))
    else:
        external = False
    
    referrer = request.args.get('referrer')

    if not is_robot(request):
        events.on_article_shown(
            articles, 
            request.args.get('ext') == 'html', 
            external, 
            get_provider_id(referrer)
        )
    if request.is_xhr:
        return jsonify({})
    else:
        return redirect_to_empty_gif()


@frontend.route("/track/shown/lines/<lines>")
@referer_host_check()
def track_lines_shown(lines):
    """
    Учесть показы строк
    """
    lines = [int(x) for x in lines.split('-') if x]
    referrer = request.args.get('referrer')
    events.on_line_shown(
        lines, 
        get_provider_id(referrer)
    )

    if request.is_xhr:
        return jsonify({})
    else:
        return redirect_to_empty_gif()


@frontend.route("/track/shown/img/<string:banners>/<string:sources>/")
@frontend.route("/track/shown/img/<string:banners>/<string:sources>/<string:prices>")
@referer_host_check()
def track_banners_shown(banners, sources, prices=None):
    """
    Учесть показы баннеров и источников.
    """
    init_session()

    session['showed_banners'] += [int(x) for x in banners.split('-') if x]

    banner_ids = [int(x) for x in banners.split('-') if x]
    source_ids = [int(x) for x in sources.split('-') if x]
    price_ids = [float(x) for x in prices.split('-') if x] if prices else None
    from_id = int(request.args.get('from_id', 0))
    referrer = request.args.get('referrer')

    if not is_robot(request):
        events.on_banners_shown(
            banner_ids, 
            source_ids, 
            from_id, 
            price_ids,
            equest.args.get('ext') == 'html',
            get_provider_id(referrer)
        )

    #current_app.logger.debug("tracked: %s" % banner_ids)
     #current_app.logger.debug("prices: %s" % price_ids)

    """
    shown_tracker_queue.enqueue_banners(
        banners, sources, prices, request.args.get('from_id', 0),
        request.args.get('ext'), is_robot(request),
        get_provider_id(referrer)
    )
    """

    if request.is_xhr:
        return jsonify({})
    else:
        return redirect_to_empty_gif()


@frontend.route('/test')
def test():
    return render_template('test.html')


@frontend.route('/foo/<string:one>/<string:two>')
@frontend.route('/foo/<string:one>/<string:two>/<string:three>')
def foo(one, two, three='deflt'):
    return "one=%s, two=%s, three=%s" % (one, two, three)


@frontend.route('/api/sources_stats/<int:source_id>', methods=['GET'])
def sources_stats(source_id):
    """Returns stats for banners for specified source for a date range.

    GET params:

        from -- start of the date range, by default - today;
        to -- end of the date range, by default - tomorrow.

    """
    source = Source.query.get(source_id)
    if not source:
        return 'Source not found', 404

    def _get_date_or_default(param, default):
        try:
            return parse(request.args.get(param), dayfirst=False)
        except (AttributeError, TypeError):
            return default

    def _get_days_range():
        from_ = _get_date_or_default('from', datetime.today())
        to = _get_date_or_default('to', datetime.today()) + timedelta(days=1)
        return (from_ + timedelta(days=days)
                for days in range((to - from_).days))

    def _prepare_item_stats(item, show, click, **_):
        show, click = int(show), int(click)
        return {'show': show,
                'click': click,
                'price': click * source.click_price,
                'title': item.title,
                'link': item.link}

    def _get_stats_for_type(items, stats_obj, day):
        stats = stats_obj.get_for_today(ts=day.timestamp())
        return {id: _prepare_item_stats(items[int(id)], **val)
                for id, val in stats.items()
                if int(id) in items}

    def _get_stats_for_day(day):
        banners = {banner.id: banner
                   for banner in Banner.query.filter(Banner.source == source)}
        lines = {line.id: line
                 for line in Line.query.filter(Line.source == source)}
        return {'banners': _get_stats_for_type(banners, banner_stats, day),
                'lines': _get_stats_for_type(lines, line_stats, day)}

    stats = OrderedDict([(str(day.date()), _get_stats_for_day(day))
                         for day in _get_days_range()])
    response = jsonify(stats)
    response.content_type += '; charset=utf-8'
    return response
