"""
Здесь собраны все события, по которым пишутся записи в Редис.

Это чтобы все было в одном месте, а не размазано по всему коду.

В принципе, эти ивенты можно сделать фоновыми задачами через Селери,
если возникнет такая необходимость.

(часть этих ивентов все-таки размазалась по коду и продублировалась в tasks.py.
Там ивенты немного свои, т.к. в них поступают уже саггрегированные данные из
fast-tracker)
"""

from wowengine.extensions import (
    banner_stats, source_stats, generation_stats,
    article_stats, line_stats, article_top_chart, auction
)


def on_banners_shown(banner_ids, source_ids, from_id=None, prices=None, update_generation=False, provider_id=None):
    """
    Когда пользователь увидел набор баннеров.
    """
    if update_generation:
        generation_stats.register_visit()
        banner_stats.register_visit(banner_ids[0], provider_id=provider_id)
        
    else:
        banner_stats.register_shown(banner_ids, provider_id=provider_id)
        source_stats.register_shown(source_ids)

        if prices:
            id_prices_dict = {banner_ids[i]: prices[i] for i in range(len(prices))}
            auction.register_shows(from_id, id_prices_dict)


def on_article_shown(article_ids, update_generation, provider_id=None, external=False):
    """
    Когда пользователь увидел тизер статьи.
    """
    if update_generation:
        generation_stats.register_visit()
        article_stats.register_visit(article_ids[0], provider_id=provider_id)
        
        if external:
            generation_stats.register_ext_visit(provider_id=provider_id)
        
    else:
        article_stats.register_shown(article_ids, provider_id=provider_id)


def on_line_shown(line_ids, provider_id=None):
    """
    Когда пользователь увидел строчку.
    """
    line_stats.register_shown(line_ids, provider_id=provider_id)


def on_article_teaser_clicked(article_ids, provider_id=None):
    """
    Пользователь кликнул по тизеру статьи
    """
    article_stats.register_clicked(article_ids, provider_id=provider_id)


def _on_item_clicked(item_id, source_id, from_id, is_from_banner, is_auction, provider_id=None):
    """Generic event for Lines/Banners click events."""
    source_stats.register_clicked(source_id, provider_id=provider_id)
    generation_stats.register_exit()

    if is_from_banner:
        banner_stats.register_exit(from_id, provider_id=provider_id)
    else:
        article_stats.register_exit(from_id, provider_id=provider_id)

    if is_auction:
        auction.register_clicks(from_id, item_id)


def on_line_clicked(line_id, source_id, from_id, is_from_banner, is_auction, provider_id=None):
    """
    Пользователь кликнул по тизеру статьи
    """
    line_stats.register_clicked(line_id, provider_id=provider_id)
    _on_item_clicked(line_id, source_id, from_id, is_from_banner, is_auction, provider_id=provider_id)


def on_banner_clicked(banner_id, source_id, from_id, is_from_banner, is_auction, provider_id=None):
    """
    Когда пользователь кликнул по баннеру и ушел по внешней ссылке.
    """
    banner_stats.register_clicked(banner_id, provider_id=provider_id)
    _on_item_clicked(
        banner_id, source_id, from_id, is_from_banner, is_auction, provider_id=provider_id
    )


def on_page_visited(item, extension, external=False):
    """
    Когда пользователь зашел на страницу материала.
    """
    # Зарегистрировать заход (без отрисовки) для статистики 
    if extension != 'ahtml':
        generation_stats.register_pre_visit()

    # Зарегистрировать заход на статью для расчета популярности.
    if item.is_article():
        article_top_chart.register_shown(item.id)
        #article_stats.register_visit(item.id)

    # Зарегистрировать заход на баннер как на материал.
    #if item.is_banner() and extension != 'ahtml':
        #banner_stats.register_visit(item.id)
