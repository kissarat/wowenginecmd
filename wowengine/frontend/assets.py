from webassets import Bundle

from ..extensions import assets

JS_FILES = ['frontend/js/compiled/app.js',
            'frontend/js/vendor/jquery.unveil.js',
            'frontend/js/vendor/jquery.hypher.js',
            'frontend/js/vendor/hypher.ru.js']

CSS_FILES = ['frontend/css/%s.css' % x for x in ['blocks', 'style']]


def register_assets():
    js_bundle = Bundle(*JS_FILES,
                       **{'filters': 'uglifyjs', 'output': 'frontend/gen/js_all.js'})
    assets.register('frontend_js_all', js_bundle)

    css_bundle = Bundle(*CSS_FILES,
                        **{'filters': 'cssmin', 'output': 'frontend/gen/css_all.css'})
    assets.register('frontend_css_all', css_bundle)
