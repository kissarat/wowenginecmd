import random
from datetime import datetime
from . import session_dict
from flask import session, current_app
from sqlalchemy import func
from wowengine.extensions import db, cache
from wowengine.models import NewsItem


def chance(prob):
    """Returns true in 0 <= prob <= 1 cases."""
    return random.random() > prob


MONTHS = ['', "января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября",
          "ноября", "декабря"]
WEEKDAYS = ['понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье']


def human_current_date():
    """
    Сегодняшняя дата в виде '22 августа, пятница'
    """
    n = datetime.now()
    return '%d %s, %s' % (n.day, MONTHS[n.month], WEEKDAYS[n.weekday()])


def last_update_time():
    """
    Время последнего обновления сайта.
    """
    dt = NewsItem.last_update_time()
    if dt:
        return '%2d:%2d' % (dt.hour, dt.minute)
    else:
        return ''

def init_session():
    for key in session_dict.keys():
        if not key in session.keys():
           session[key] = session_dict[key][:]

