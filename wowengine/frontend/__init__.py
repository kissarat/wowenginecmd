from flask import Blueprint, session

from .assets import register_assets
from ..config import Config


frontend = Blueprint('frontend', __name__,
                     static_folder='static',
                     template_folder=Config.FRONTEND_TEMPLATE_FOLDER,
                     static_url_path='')

register_assets()

session_dict = {'clicked_banners': [],
                'showed_banners': []}

from .views import *

