# Добавить в избранное
window.addFavourite = (a) ->
  title = document.title
  url = document.location
  try
    # Internet Explorer
    window.external.AddFavorite url, title
  catch e
    try
      # Mozilla
      window.sidebar.addPanel title, url, ""
    catch e
      # Opera и Firefox 23+
      if typeof(opera) == "object" or window.sidebar
        a.rel = "sidebar"
        a.title = title
        a.url = url
        a.href = url
        return true
      else
        # Unknown
        alert 'Нажмите Ctrl-D чтобы добавить страницу в закладки'
  return false

$ ->
  # подгружать картинки при прокрутке страницы
  $('.img-picture').unveil(200)
  
  $('.img-tracker').unveil(0)

  # автоматически вставлять переносы
  $('div.hyphenate, div.hyphenate a').hyphenate 'ru'

  # Добавить параметр mobile=0 к ссылкам у баннеров, чтобы отличать ботов от нормальных юзеров
  $('a[href]').each ->
    link = $(this).attr('href')
    if link.indexOf('/read/') >= 0
      join_character = if link.indexOf('?') >= 0 then '&' else '?'
      newlink = link + join_character + 'mobile=0'
      $(this).attr('href' , newlink)

  # Changes mail.ru banners after 5 seconds:
  if $('.mail-ru-pixel').length
    setTimeout ->
      src = $('.mail-ru-pixel').attr('src')
      $('.mail-ru-pixel').attr 'src', "#{src}?delete=1"
    , 5000
