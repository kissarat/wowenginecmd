-- Зарегистрировать заход.
-- Если заходов больше лимита - убрать из очереди, посчитать ctr и обнулить статистику.

local show_key = KEYS[1]
local click_key = KEYS[2]
local queue_key = KEYS[3]
local queue_in_rotation_key = KEYS[4]
local result_key = KEYS[5]
local result_in_rotation_key = KEYS[6]
local total_stats_key = KEYS[7]
local current_unix_timestamp = KEYS[8]

local banner_id = ARGV[1]
local banner_price = tonumber(ARGV[2])
local auction_shows_limit = tonumber(ARGV[3])
local auction_clicks_limit = tonumber(ARGV[4])

local current_show = tonumber(redis.call('HGET', show_key, banner_id)) or 0
local current_click = tonumber(redis.call('HGET', click_key, banner_id)) or 0

redis.call('HINCRBY', total_stats_key, 'shows:' .. banner_id, 1)
redis.call('HINCRBY', show_key, banner_id, 1)

-- если баннер еще не открутился
if (auction_shows_limit and current_show + 1 >= auction_shows_limit) or (auction_clicks_limit and current_click >= auction_clicks_limit) then
    -- баннер открутился, баннер может уходить
    local current_click = tonumber(redis.call('HGET', click_key, banner_id)) or 0
    local ctr = current_show == 0 and 0 or current_click / current_show
    local price = ctr * (banner_price+1)

    -- убрать открученный баннер из очереди
    redis.call('SREM', queue_key, banner_id)
    redis.call('SREM', queue_in_rotation_key, banner_id)

    --- убрать запись о предыдущей оценке
    redis.call('ZREM', result_key, banner_id)
    redis.call('ZREM', result_in_rotation_key, banner_id)

    -- убрать открученный баннер из статистики
    redis.call('HDEL', show_key, banner_id)
    redis.call('HDEL', click_key, banner_id)

    -- сохранить его ctr * price в result
    redis.call('ZADD', result_key, price, banner_id)
    redis.call('ZADD', result_in_rotation_key, price, banner_id)

    -- сохранить дату первой открутки в статистике
    redis.call('HSETNX', total_stats_key, 'first_finish_time:' .. banner_id, current_unix_timestamp)

    -- сохранить СTR (для SP)
    redis.call('HSET', total_stats_key, 'last_ctr:' .. banner_id, ctr)
end
