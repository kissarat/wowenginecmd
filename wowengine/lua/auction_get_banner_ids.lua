--Получить набор баннеров для показа на странице материала.
--
--Сперва попробовать взять из очереди на аукцион (queue).
--Если очередь пуста или там недостаточно баннеров, взять топовые (самые дорогие) баннеры из
--результата аукциона и переместить их в очередь, пусть опять тестируются и доказывают свой высокий
--ситиар.

-- Тут немного нелогичная последовательность действий из-за того, что скриптам редиса разрешено пользоваться
-- командами со случайными результатами (типа SRANDMEMBER) только после команд, вносящих изменения в базу.
-- Отсюда лишний вызов SCARD.

local queue_key = KEYS[1]
local queue_in_rotation_key = KEYS[2]
local result_key = KEYS[3]
local result_in_rotation_key = KEYS[4]
local count = tonumber(ARGV[1])
local cleanup = tonumber(ARGV[2])
local banners_in_queue = tonumber(redis.call('SCARD', queue_in_rotation_key))

if banners_in_queue < count then
    -- в очереди недостаточно баннеров, надо добрать из результата
    local rest_count = count - banners_in_queue
    -- взять самые ситиаристые
    local additional_banner_ids = redis.call('ZREVRANGE', result_in_rotation_key, 0, rest_count - 1)

    -- переместить баннеры, набранные из результата, в очередь, чтобы снова тестировали ctr
    if (#additional_banner_ids > 0) and (cleanup > 0) then
            redis.call('ZREM', result_key, unpack(additional_banner_ids))
            redis.call('ZREM', result_in_rotation_key, unpack(additional_banner_ids))
            redis.call('SADD', queue_key, unpack(additional_banner_ids))
            redis.call('SADD', queue_in_rotation_key, unpack(additional_banner_ids))
    end
end

return redis.call('SRANDMEMBER', queue_in_rotation_key, count)
