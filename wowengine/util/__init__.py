from html.parser import HTMLParser
import os.path


def unid_hierarchy_path(unid):
    """
    "abcdef" -> "ab/abcdef"
    """
    return os.path.join(unid[:2], unid)


def partition(l, n):
    """
    Return a seq partitioned by n-length chunks.
    partition([0,1,2,3,4], 2) = [[0,1],[2,3],[4]]
    """
    return (l[i:i + n] for i in range(0, len(l), n))


class _MLStripper(HTMLParser):
    def __init__(self):
        super().__init__()
        self.reset()
        self.strict = False
        self.convert_charrefs = True
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return ''.join(self.fed)


def strip_tags(html):
    """
    Вычистить html-тэги из строки.
    """
    s = _MLStripper()
    s.feed(html)
    return s.get_data()
