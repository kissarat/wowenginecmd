# Shameless copy-paste from https://github.com/cyberdelia/flask-statsd
# But it have no default connection options. In case if STATSD_HOST is empty it will use dummy statsd client.

from statsd import StatsClient
from functools import wraps

class _NullWrapper(object):
    def __init__(self, *args, **kwargs):
        pass

    def __call__(self, func, *args, **kwargs):
        @wraps(func)
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)
        return wrapper

    def __enter__(self, *args, **kwargs):
        pass

    def __exit__(self, *args, **kwargs):
        pass


class DummyStatsdClient(object):
    """
    This mock class is used in case if no statsd configuration provided
    """
    def timer(*args, **kwargs):
        return _NullWrapper()

    def timing(*args, **kwargs):
        pass

    def incr(*args, **kwargs):
        pass

    def decr(*args, **kwargs):
        pass

    def gauge(*args, **kwargs):
        pass


class StatsD(object):
    def __init__(self, app=None, config=None):
        self.config = None
        self.statsd = None
        if app is not None:
            self.init_app(app)
        else:
            self.app = None

    def init_app(self, app, config=None):
        if config is not None:
            self.config = config
        elif self.config is None:
            self.config = app.config


        self.config.setdefault('STATSD_PORT', 8125)
        self.config.setdefault('STATSD_PREFIX', None)

        self.app = app

        if self.config.get('STATSD_HOST') is not None:
            self.statsd = StatsClient(self.config['STATSD_HOST'],
                                      self.config['STATSD_PORT'], self.config['STATSD_PREFIX'])
        else:
            self.statsd = DummyStatsdClient()

    def timer(self, *args, **kwargs):
        return self.statsd.timer(*args, **kwargs)

    def timing(self, *args, **kwargs):
        return self.statsd.timing(*args, **kwargs)

    def incr(self, *args, **kwargs):
        return self.statsd.incr(*args, **kwargs)

    def decr(self, *args, **kwargs):
        return self.statsd.decr(*args, **kwargs)

    def gauge(self, *args, **kwargs):
        return self.statsd.gauge(*args, **kwargs)
