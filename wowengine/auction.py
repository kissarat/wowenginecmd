# coding: utf-8
#

import os
import time
from collections import defaultdict
from itertools import chain
from redis.lock import LuaLock
from werkzeug.datastructures import MultiDict
from wowengine.config import config
from wowengine.extensions import cache
from wowengine.models import Item


class Auction(object):
    """
    Управление аукционами.

    Каждый баннер не старше auction_max_age_days дней
    показывается на каждой странице auction_shows_count раз.

    Когда баннеры истекают, разгонный блок на странице устаканивается по списку баннеров,
    отсортированных по цене ctr * source.price.

    В дальнейшем отслеживаются последние auction_shows_count (3 параметр, =1000) показов/кликов баннеров,
    высчитывается текущий ctr и пересчитывается топ на основе данных, сохраненных в ходе аукциона.
    Таким образом топ может меняться со временем, если ctr * цена на основе последних данных упадет.

    Структуры данных в Редисе (prefix='auction'):

    auction:show:item_id - HASH айди к показам баннера
    auction:click:item_id - HASH то же самое для кликов
    auction:queue:item_id - SET id баннеров, которые ожидают открутки
    auction:result:item_id - ZSET id -> ctr

    закэшированные результаты, отфильтрованные по актуальным источникам,
    обновляются периодически (раз в несколько секунд):
    auction:queue_in_rotation:item_id - SET id баннеров, которые ожидают открутки
    auction:result_in_rotation:item_id - ZSET id -> ctr
    """

    lua_register_show = None
    lua_get_banner_ids = None
    lua_debug = None

    AUCTION_ALGORITHMS = (
        ('old', 'Старый аукцион'),
        ('flow', 'Новый (flow, с ёлочкой)'),
        ('fast', 'Быстрый (при CTR больше порога баннеры выстраиваются сверху)'),
    )

    def __init__(self, redis_db, super_cache, site_config, prefix='auction'):
        self.redis = redis_db
        self.super_cache = super_cache
        self.site_config = site_config
        self.prefix = prefix

    @classmethod
    def init_scripts(cls, redis):
        """
        Зарегистрировать луа-скрипты в данном экземпляре редиса.
        """

        scripts_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'lua')

        def register_script_from_file(script_name):
            filename = os.path.join(scripts_dir, '%s.lua' % script_name)
            with open(filename, 'r', encoding="utf-8") as f:
                script = f.read()
            return redis.register_script(script)

        cls.lua_register_show = register_script_from_file('auction_register_show')
        cls.lua_get_banner_ids = register_script_from_file('auction_get_banner_ids')
        cls.lua_debug = register_script_from_file('auction_debug')

    def _gen_key(self, key, item_id):
        """Returns redis key for auction.

        If `config.OVERALL_AUCTION` is True, item_id will be
        replaced to `config.OVERALL_ITEM_ID`.

        """
        if config.OVERALL_AUCTION:
            item_id = config.OVERALL_ITEM_ID
        return '%s:%s:%s' % (self.prefix, key, item_id)

    def show_key(self, item_id):
        return self._gen_key('show', item_id)

    def click_key(self, item_id):
        return self._gen_key('click', item_id)

    def queue_key(self, item_id):
        return self._gen_key('queue', item_id)

    def result_key(self, item_id):
        return self._gen_key('result', item_id)

    def queue_in_rotation_key(self, item_id):
        return self._gen_key('queue_in_rotation', item_id)

    def result_in_rotation_key(self, item_id):
        return self._gen_key('result_in_rotation', item_id)

    def total_stats_key(self, item_id):
        return self._gen_key('total_stats', item_id)

    @cache.memoize(timeout=config.SOURCES_CHECK_TIMEOUT)
    def _cache_for_rotation(self, item_id, cache_banner_ids_in_rotation_fn):
        """
        Закэшировать результаты, по которым делаются текущие выборки.
        (Держать баннеры только для актуальных источников).

        SET of all current ids

        """
        result_key = self.result_key(item_id)
        queue_key = self.queue_key(item_id)
        result_in_rotation_key = self.result_in_rotation_key(item_id)
        queue_in_rotation_key = self.queue_in_rotation_key(item_id)

        valid_ids_key = self.super_cache.gen_key(cache_banner_ids_in_rotation_fn())

        with self.redis.pipeline() as pipe:
            pipe.sinterstore(queue_in_rotation_key, [queue_key, valid_ids_key])
            pipe.zinterstore(result_in_rotation_key, {result_key: 1, valid_ids_key: 0})
            pipe.execute()

        return 1  # for caching
    # Exclude `cache_banner_ids_in_rotation_fn` from memoization cache key name so we can flush cache without knowing it.
    _cache_for_rotation.make_cache_key = lambda _, __, item_id, ___: ':auction_cache_for_rotation:' + str(item_id)

    def register_show(self, item_id, banner_id, banner_price, auction_shows_count, auction_clicks_count):
        """
        :type item_id: int
        :type banner_id: int
        :type banner_price: float
        """

        self.lua_register_show(
            keys=[
                self.show_key(item_id),
                self.click_key(item_id),
                self.queue_key(item_id),
                self.queue_in_rotation_key(item_id),
                self.result_key(item_id),
                self.result_in_rotation_key(item_id),
                self.total_stats_key(item_id),
                int(time.time()),
            ],
            args=[
                banner_id,
                banner_price,
                auction_shows_count,
                auction_clicks_count,
            ]
        )

    def register_shows(self, item_id, id_prices_dict):
        """
        Зарегистрировать заходы.
        Если заходов больше лимита - убрать из очереди, посчитать ctr и обнулить статистику.

        :type item_id: int
        :type id_prices_dict: dict[int, float]
        """
        if isinstance(id_prices_dict, MultiDict):
            items = id_prices_dict.items(multi=True)
        else:
            items = id_prices_dict.items()

        for banner_id, banner_price in items:
            # зарегистрировать показ через луа-скрипт
            self.register_show(item_id, banner_id, banner_price, *Item.get_item_auction_limits(item_id))

    def register_clicks(self, item_id, id_or_ids):
        """
        Зарегистрировать клик.

        Тут все мегапросто, поэтому обойдемся без скрипта на Луа.
        """

        ids = [id_or_ids] if not isinstance(id_or_ids, list) else id_or_ids

        click_key = self.click_key(item_id)
        total_stats_key = self.total_stats_key(item_id)

        with self.redis.pipeline() as pipe:
            for i in ids:
                pipe.hincrby(click_key, i, 1)  # увеличить счетчик кликов
                pipe.hincrby(total_stats_key, 'clicks:%s' % (i,), 1)  # увеличить счетчик кликов

            pipe.execute()

    def get_banner_ids(self, item_id, count, fetch_banner_ids_fn, get_banner_ids_in_rotation_fn, cleanup=True):
        """
        Получить набор баннеров для показа на странице материала.

        Сперва попробовать взять из очереди на аукцион (queue).
        Если очередь пуста или там недостаточно баннеров, взять топовые (самые дорогие) баннеры из
        результата аукциона и переместить их в очередь, пусть опять тестируются и доказывают свой высокий
        ситиар.

        :type item_id: int
        :type count: int
        :rtype: frozenset[int]
        """

        queue_key = self.queue_key(item_id)
        queue_in_rotation_key = self.queue_in_rotation_key(item_id)
        result_key = self.result_key(item_id)
        result_in_rotation_key = self.result_in_rotation_key(item_id)

        # synchronised operation
        lock = LuaLock(self.redis, 'auction_fetch_lock:%d' % item_id, timeout=5)
        with lock:
            if not self.redis.exists(queue_key) and not self.redis.exists(result_key):
                # создать очередь для аукциона
                new_auction_ids = fetch_banner_ids_fn()
                try:
                    new_auction_ids.remove(item_id)
                except ValueError:
                    pass

                if len(new_auction_ids) > 0:
                    self.redis.sadd(queue_key, *new_auction_ids)

        self._cache_for_rotation(item_id, get_banner_ids_in_rotation_fn)  # удостовериться, что текущие ids для данного аукциона закэшированы

        # получить результаты через луа-скрипт
        banner_results = self.lua_get_banner_ids(
            keys=[
                queue_key,
                queue_in_rotation_key,
                result_key,
                result_in_rotation_key
            ],
            args=[count, int(cleanup)]
        )

        return [int(x) for x in banner_results]

    def _get_all_queued(self, item_id):
        return [int(id) for id in self.redis.smembers(self.queue_key(item_id))]

    def _get_total_stats(self, item_id, include_not_shown):
        queued = self._get_all_queued(item_id)
        total_stats_key = self.total_stats_key(item_id)
        total_stats = defaultdict(lambda: [0, 0, 0, 0])
        for k, v in self.redis.hgetall(total_stats_key).items():
            value_type, _, banner_id = k.partition(':')
            if value_type == 'shows':
                index = 0
            elif value_type == 'clicks':
                index = 1
            elif value_type == 'first_finish_time':
                index = 2
            elif value_type == 'last_ctr':
                index = 3
            try:
                total_stats[int(banner_id)][index] = int(v)
            except ValueError:
                total_stats[int(banner_id)][index] = float(v)

        if include_not_shown:
            for id in queued:
                if id not in total_stats:
                    total_stats[id] = 0, 0, 0, 0

        return total_stats

    def _get_stats_queue(self, item_id, include_not_shown):
        queued = self._get_all_queued(item_id)
        show_key = self.show_key(item_id)
        click_key = self.click_key(item_id)
        shows = sorted(self.redis.hgetall(show_key).items(), key=lambda x: -int(x[1]))  # reverse sorted by shows
        clicks = self.redis.hgetall(click_key)
        queue = [(k, (int(v), clicks.get(k, 0))) for k, v in shows]
        queue_ids = [int(id) for id, _ in queue]

        if include_not_shown:
            for id in queued:
                if id not in queue_ids:
                    queue.append((id, (0, 0)))
        return queue

    def get_stats(self, item_id, include_not_shown=False):
        """
        Статистика по текущему аукциону.

        :type item_id: int
        :param include_not_shown: Include not shown banners in `queue` and `total_stats`.
        :rtype: dict[str, list]
        """
        total_stats = self._get_total_stats(item_id, include_not_shown)
        queue = self._get_stats_queue(item_id, include_not_shown)
        result_key = self.result_key(item_id)
        result = self.redis.zrevrange(result_key, 0, -1, withscores=True)
        return {'queue': queue,
                'result': result,
                'total_stats': total_stats}

    def _get_keys_for_item(self, item_id):
        """
        Returns all redis keys for specified `item_id`.
        """
        return [self.queue_key(item_id),
                self.result_key(item_id),
                self.show_key(item_id),
                self.click_key(item_id),
                self.result_in_rotation_key(item_id),
                self.queue_in_rotation_key(item_id),
                self.total_stats_key(item_id)]

    def reset_auction(self, item_id):
        """
        Запустить аукцион для материала заново.
        Просто стереть все ключи, относящиеся к аукциону конкретного материала.
        Они восстановятся автоматически.
        """
        self.redis.delete(*self._get_keys_for_item(item_id))
        cache.delete_memoized(self._cache_for_rotation, self, item_id, None)

    def reset_all_auctions(self):
        """
        Resets all auctions for all items.
        """
        all_keys = list(chain.from_iterable(
            self.redis.keys(wildcard)
            for wildcard in self._get_keys_for_item('*')))
        if all_keys:
            self.redis.delete(*all_keys)
        cache.delete_memoized(self._cache_for_rotation)

    def add_banner_to_auctions(self, banner, article_ids, is_active):
        """
        Add new banner to all auctions
        """
        for key in self.redis.keys(self.queue_key('*')):
            self.redis.sadd(key, banner.id)

        if is_active:
            for key in self.redis.keys(self.queue_in_rotation_key('*')):
                self.redis.sadd(key, banner.id)
