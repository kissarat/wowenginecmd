import re
from collections import defaultdict
from datetime import date, datetime, timedelta
import time
import json
import decimal
from itertools import chain

from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
from flask import render_template, current_app, jsonify, request, redirect, url_for, flash
from flask.ext.login import login_user, login_required, current_user, logout_user
from flask.ext.security import current_user, login_required, utils, roles_accepted

from ..admin import admin
from wowengine import images
from wowengine.config import config
from wowengine.extensions import (
    article_stats, line_stats, banner_stats,
    generation_stats, site_config, auction, active_items_stats
)
from wowengine.models import (
    Item, Source, NewsItem, Banner, Provider,
    ProviderClickPriceHistory, SourceClickPriceHistory,
    User, user_datastore
)
from wowengine import util
from ..geo.geoip import Geo
from wowengine.util.pagination import Pagination


@admin.route('/users/', defaults={'page': 1}, methods=['GET'])
@admin.route('/users/page/<int:page>', methods=['GET'])
@login_required
@roles_accepted('root', 'admin')
def show_users(page):
    per_page = 10
    skip = per_page * (page - 1)
    users = user_datastore.db.session.query(User).all()
    total_count = len(users)
    pagination = Pagination(page=page, per_page=per_page, total_count=total_count)
    users = users[skip:][:per_page]
    return render_template('users.html', users=users, pagination=pagination, total_count=total_count)


def is_email_address_valid(email):
    """Validate the email address using a regex."""
    if not re.match("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$", email):
        return False
    return True

@admin.route('/users/', methods=['PUT'])
@login_required
@roles_accepted('root', 'admin')
def save_user():
    if request.form.get('id') and request.form.get('email'):

        # Check email address.
        if not is_email_address_valid(request.form.get('email')):
            return jsonify({'result': 'error', 'message': 'Неверный email'})

        user = user_datastore.find_user(id=request.form.get('id'))
        user.email = request.form.get('email')
        user.firstname = request.form.get('firstname')
        user.lastname = request.form.get('lastname')
        if 'root' in user.roles:
            return jsonify({'result': 'error', 'message': 'Нельзя изменить данные суперпользователя'})
        user.active = True if request.form.get('active') == 'true' else False
        # Change password if it containing in request.
        if request.form.get('password'):
            user.password = utils.encrypt_password(request.form.get('password'))

        user_datastore.commit()
        # Delete old roles, and adding new role.
        for item in user.roles:
            user_datastore.remove_role_from_user(user, item)
            user_datastore.commit()
        user_datastore.add_role_to_user(user, request.form.get('role'))
        user_datastore.commit()
        return jsonify({'result': 'success'})

    return jsonify({'result': 'error', 'message': 'Проверьте корректность данных'})


@admin.route('/users/', methods=['POST'])
@login_required
@roles_accepted('root', 'admin')
def create_user():
    if not request.form.get('email') or not request.form.get('password'):
        return jsonify({'result': 'error', 'message': 'Проверьте корректность данных'})

    active = True if request.form.get('active') == 'true' else False

    if user_datastore.find_user(email=request.form.get('email')):
        return jsonify({'result': 'error', 'message': 'Пользователь с таким email уже существует'})

    # Check email address.
    if not is_email_address_valid(request.form.get('email')):
        return jsonify({'result': 'error', 'message': 'Неверный email'})

    # Create user
    user_datastore.create_user(email=request.form.get('email'),
                               firstname=request.form.get('firstname'),
                               lastname=request.form.get('lastname'),
                               active=active,
                               password=utils.encrypt_password(request.form.get('password'))
                               )

    user_datastore.commit()

    # Add role to user
    user = user_datastore.find_user(email=request.form.get('email'))
    if user:
        user_datastore.add_role_to_user(user, request.form.get('role'))
        user_datastore.commit()
    return jsonify({'result': 'success'})


@admin.route('/users/', methods=['DELETE'])
@login_required
@roles_accepted('root', 'admin')
def delete_user():
    if request.form.get('id'):
        user = user_datastore.find_user(id=request.form.get('id'))
        if user is None:
            return jsonify({'result': 'error', 'message': 'Нельзя удалить несуществующего пользователя'})

        elif 'root' in user.roles:
            return jsonify({'result': 'error', 'message': 'Нельзя удалить суперпользователя'})

        user_datastore.delete_user(user=user)
        user_datastore.commit()
        return jsonify({'result': 'success'})
    return jsonify({'result': 'error', 'message': 'Нельзя удалить несуществующего пользователя'})


@admin.route('/', defaults={'path': ''})
@admin.route('/<path:path>')
@login_required
def index(path):
    """
    Single-page admin interface.
    """
    return render_template('admin_index.html',
                           DEBUG=current_app.debug,
                           auction_algorithms_json=json.dumps(auction.AUCTION_ALGORITHMS),
                           current_user=current_user)


@admin.route('/api/upload', methods=['GET', 'POST'])
@login_required
@roles_accepted('root', 'admin', 'chief_editor', 'editor')
def upload():
    """
    Upload image.
    """
    try:
        img_data = images.save_upload(request.files['qqfile'], request.form)
        return jsonify({"success": True, 'img': img_data})
    except RuntimeError as e:
        return jsonify({"success": False, "error": str(e)}), 400


@admin.route('/api/images/<string:unid>/<int:num>', methods=['DELETE'])
@login_required
@roles_accepted('root', 'admin', 'chief_editor', 'editor')
def delete_image(unid, num):
    """
    Delete image.
    """
    images.delete_image(unid, num)
    return jsonify({'success': True})


@admin.route('/api/morda', methods=['GET'])
@login_required
@roles_accepted('root', 'admin', 'chief_editor', 'editor')
def morda_items():
    """
    Фейковый REST API для морды - только для выдачи
    списка материалов для показа на морде в нужном порядке.
    Проще сделать так, чем настраивать кастомный Flask-Restless.

    ВНИМАНИЕ! передается урезанный набор полей,
    достаточный только для /ed/morda.
    """

    fields = ['id', 'title', 'body', 'date', 'is_published', 'morda_num']

    # поле date надо обрабатывать по-особенному
    def fval(clazz, fieldname):
        v = getattr(clazz, fieldname)
        if fieldname == 'date':
            return v.isoformat()
        elif fieldname == 'body':
            s = util.strip_tags(v)
            dot_pos = s.find('.', 200)
            if dot_pos > 0:
                return s[0:dot_pos + 1]
            else:
                return s
        else:
            return v

    items = NewsItem.fetch_list_for_morda()
    json_items = [{f: fval(i, f) for f in fields} for i in items]
    return jsonify({
        'num_results': len(json_items),
        'objects': json_items,
        'page': 1,
        'total_pages': 1
    })

def _get_stats_api(period, range=None, provider=None):
    def _get_stats_for_period(stats_object, period):
        if period == 'range':
            if range and isinstance(range, list) and len(range) == 2:
                range_start, range_end = range[0], range[1]
            else:
                _s = request.args.get('range_start', '')
                _e = request.args.get('range_end', '')
                range_start = parse(_s, dayfirst=True)
                range_end = parse(_e, dayfirst=True)
            return stats_object.get_for_range(
                range_start, range_end, provider_id=provider
            )
        try:
            result = getattr(stats_object, 'get_for_%s' % period)(
                ts=time.mktime(
                    (datetime.now()-_get_timedelta_to_round()).timetuple()
                ), provider_id=provider
            )
            return result
        except AttributeError:
        # default get_for_* method
            result = stats_object.get_for_5min(
                ts=time.mktime(
                    (datetime.now()-_get_timedelta_to_round()).timetuple()
                ), provider_id=provider
            )
            return result

    def _get_timedelta_to_round(divider=5):
        now = datetime.now()
        td = timedelta(
            minutes=now.minute % divider,
            seconds=now.second,
            microseconds=now.microsecond
        )
        return td

    def _get_stats_for_item_type(item_type, period):
        if item_type == 'news':
            stats_object = article_stats
        elif item_type == 'banners':
            stats_object = banner_stats
        elif item_type == 'lines':
            stats_object = line_stats
        else:
            return _get_stats_for_item_type('news', period) \
                + _get_stats_for_item_type('banners', period) \
                + _get_stats_for_item_type('lines', period)

        stats = _get_stats_for_period(stats_object, period)

        # подмешать названия статей и источников
        items = Item.get_all_titles_and_source_ids()
        sources = Source.get_all_titles()

        for i, d in stats.items():
            d['item_type'] = item_type
            if i in items:
                d['title'] = items[i]['title']
                d['source_id'] = items[i]['source_id']
                source = Source.query.filter_by(id=d['source_id']).first()
                d['source_group_id'] = source.group_id if source else None
                d['source_name'] = sources.get(items[i]['source_id'], 'NONE')
            else:
                d['source_id'] = -1
                d['source_group_id'] = -1
                d['title'] = d['source_name'] = 'NONE'

        # если элемент -- новость, берём дополнительую инфу по элементу:
        #       is_gallery, video..
        # в противном случае -- выставляем False для соответствующих полей

            if item_type == 'news':
                item = NewsItem.query.get(i) or NewsItem()
                d['is_gallery'] = item.is_gallery
                d['is_gallery_from_hell'] = item.is_gallery_from_hell
                d['video'] = item.video
                d['source_name'] = 'WOW'
            else:
                for key in ('is_gallery', 'is_gallery_from_hell', 'video'):
                    d[key] = False
        # а нам нужен список из словарей с id и данными
        return [dict(id=i, **d) for i, d in stats.items()]

    data = _get_stats_for_item_type(request.args.get('type'), period)

    return data

def _get_sources_stats(period, range=None, sources=None):
    """
    Отдать dict с суммарной статистикой по источникам за период period.
    По умолчанию за 5 минут (5min).
    """
    
    stats_data = _get_stats_api(period, range=range) 

    def _get_stat_data(source_id=None, ret='click'):
        """
        Значение ret может быть 'click' или 'show' - данные которые вернет функция
        """
        _data = 0
        if source_id: 
            for item in stats_data:
                if item['item_type'] == 'banners' and item['source_id'] == source_id: 
                    if ret == 'click':
                        _data += int(item['click'])
                    if ret == 'show':
                        _data += int(item['show'])
        return _data

    ret_data = []
    
    for source in sources:
        _click = _get_stat_data(source.id, 'click')
        _show = _get_stat_data(source.id, 'show')
        _item_data = {
            'source_id': source.id,
            'source': source.title,
            'click': _click,
            'show': _show,
            'click_price': float(source.click_price),
            'ctr': ((_click / _show * 100) if _show > 0 else 0),
            'total_price': (_click * float(source.click_price)) #TODO: нужно доделать, чтоб выводилось в зависимости от истории цены
        }
        
        ret_data.append(_item_data)

    return ret_data

@admin.route('/api/sources/stats/', methods=['GET'])
@login_required
def sources_stats_api():
    """
    Отдать json с суммарной статистикой по источникам за период period.
    По умолчанию за 5 минут (5min).
    """
    period = request.args.get('period')
    now = datetime.now()
    sources = Source.query.all()

    periods = {
        '5min': timedelta(minutes=5),
        '15min': timedelta(minutes=15),
        '30min': timedelta(minutes=30),
        '60min': timedelta(minutes=60),
        'range': parse(request.args.get('range_end', ''), dayfirst=True) -
                parse(request.args.get('range_start', ''), dayfirst=True),
        'today': timedelta(days=1),
        'yesterday': timedelta(days=2),
        'month': timedelta(days=31),
        # 'all_time': timedelta(seconds=time.time()),
        'all_time': now - (now - relativedelta(years=1)),
        None: timedelta(minutes=5),
    }

    if period != 'range':
        td = _get_timedelta_to_round()
        _range_start = now - periods[period] - td
        _range_end = now - td
        if period == 'yesterday':
            #TODO: костыль для "Вчера"
            _range_end -= timedelta(days=1)
    else:
        _range_start = parse(request.args.get('range_start', ''), dayfirst=True) - timedelta(days=1)
        _range_end = parse(request.args.get('range_end', ''), dayfirst=True)
      
    #период для выборки данных
    range = [_range_start, _range_end]
    
    if period in ['5min', '15min', '30min', '60min']:
        #используем период как есть
        data = _get_sources_stats(period=period, range=range, sources=sources)
    else:
        #используем диапазон дат
        data = _get_sources_stats(period='range', range=range, sources=sources)

    response = jsonify({
        'data': data,
        'range_start': time.strftime("%m/%d/%H:%M", _range_start.timetuple()),
        'range_end':  time.strftime("%m/%d/%H:%M", _range_end.timetuple())
    })
    response.content_type += '; charset=utf-8'
    return response


@admin.route('/api/stats/', methods=['GET'])
@login_required
def stats_api():
    """
    Отдать json со статистикой за период period, по умолчанию за 5 минут (5min).
    """
    period = request.args.get('period')
    data = _get_stats_api(period)

    intervals = {
        '5min': timedelta(minutes=5),
        '15min': timedelta(minutes=15),
        '30min': timedelta(minutes=30),
        '60min': timedelta(minutes=60),
        'range': parse(request.args.get('range_end', ''), dayfirst=True) -
                 parse(request.args.get('range_start', ''), dayfirst=True),
        'today': timedelta(days=1),
        'yesterday': timedelta(days=2),
        'month': timedelta(days=31),
        'all_time': timedelta(seconds=time.time()),
        None: timedelta(minutes=5),
    }
    now = datetime.now()
    if period != 'range':
       td = _get_timedelta_to_round()
       _range_start = now - intervals[period] - td
       _range_end = now - td
    else:
       _range_start = parse(request.args.get('range_start', ''), dayfirst=True)
       _range_end = parse(request.args.get('range_end', ''), dayfirst=True)

    response = jsonify(
        {
            'data': data,
            'range_start': time.strftime("%H:%M", _range_start.timetuple()),
            'range_end':  time.strftime("%H:%M", _range_end.timetuple())
        }
    )
    response.content_type += '; charset=utf-8'
    return response


def _get_traffic_price(period, range=None):
    """
    Отдать dict с суммарной статистикой по стоимости траффика за период period.
    По умолчанию за 5 минут (5min).
    """
    source_group_id = request.args.get('source_group')

    provider_id = request.args.get('provider')
    provider = Provider.query.get(provider_id) if provider_id else None

    source_id = request.args.get('source')
    source = Source.query.get(source_id) if source_id else None
    
    stats_data = _get_stats_api(period, range=range)

    def _get_total_clicks(ret='in'):
        """
        Значение ret может быть 'in' или 'out' - данные которые вернет функция
        """
        clicks = 0
        for item in stats_data:
            if item['source_id']: 
                if item['item_type'] == 'news' and ret == 'in':
                    clicks += int(item['in'])
                if item['item_type'] == 'banners' and ret == 'out':
                    clicks += int(item['click'])
        return clicks

    def _get_out(ret='clicks'):
        """
        Стоимость исходящего трафика и клики для данного клиента (группы).
        Значение ret может быть 'clicks' или 'price' - данные которые вернет функция
        """
        clicks = 0
        price = 0

        ############ Метрика группы ############
        if source_group_id:
            for item in stats_data:
                if item['source_id'] and item['item_type'] == 'banners' and item['source_group_id'] == int(source_group_id):
                    _source_id = item['source_id']
                    _source = Source.query.get(_source_id)
                    _clicks = (
                        float(item.get('click_%s' % provider_id, 0)) if provider_id else float(item.get('click', 0))
                    )
                    _click_price = float(_source.click_price) if _source else 0
                    
                    clicks += _clicks
                    price += _clicks * _click_price

        ############ Метрика клиента ############
        elif source:
            item_clicks = 0
            click_price = float(source.click_price)
            for item in stats_data:
                if item['item_type'] == 'banners' and item['source_id'] == int(source_id):
                    item_clicks += (
                        float(item.get('click_%s' % provider_id, 0)) if provider_id else float(item.get('click', 0))
                    )
            
            clicks += item_clicks
            price += item_clicks * click_price

        ############ Метрика отсутствует ############
        else:
            item_clicks_price = 0
            for item in stats_data:
                if item['item_type'] == 'banners':
                    _source_id = item['source_id']
                    _source = Source.query.get(_source_id) if _source_id else None
                    _click_price = float(_source.click_price) if _source else 0
                    _clicks = (
                        float(item.get('click_%s' % provider_id, 0)) if provider_id else float(item.get('click', 0))
                    )

                    clicks += _clicks
                    price += _clicks * _click_price
        
        #возвращаем клики или стоимость
        return clicks if ret is 'clicks' else price
    
    def _get_in(ret='clicks'):
        """
        Стоимость входящего трафика и клики для данного источника (группы).
        Значение ret может быть 'clicks' или 'price' - данные которые вернет функция
        """
        nonlocal provider
        clicks = 0
        price = 0

        if provider:
            ############ Метрика группы ############
            if source_group_id:
                for item in stats_data:
                    if item['item_type'] == 'news' and item['source_group_id'] == int(source_group_id):
                        clicks += item.get('in_%s' % provider_id, 0)

            ############ Метрика клиента ############
            elif source:
                for item in stats_data:
                    if item['item_type'] == 'news' and item['source_id'] == int(source_id):
                        clicks += item.get('in_%s' % provider_id, 0)

            ############ Метрика отсутствует ############
            else:
                for item in stats_data:
                    if item['item_type'] == 'news':
                        clicks += item.get('in_%s' % provider_id, 0)

            price += clicks * float(provider.click_price)
        else:
            # Данных о поставщике траффика нам никто не предоставил,
            # следовательно нужно учесть статистику по всем поставщиках
            providers_total = Provider.query.all()
            stats_data_per_provider = {}
            for _p in providers_total:
                stats_data_per_provider[_p.id] = _get_stats_api(
                    period, range=range, provider=_p.id
                )
                
            for _provider_id, _stats_data in stats_data_per_provider.items():
                _provider = Provider.query.get(_provider_id)
                
                ############ Метрика группы ############
                if source_group_id:
                    for item in _stats_data:
                        if item['item_type'] == 'news' and item['source_group_id'] == int(source_group_id):
                            #clicks += item.get('in_%s' % _provider_id, 0)
                            #временно берем значение без учета провайдера
                            clicks += float(item.get('in', 0))

                ############ Метрика клиента ############
                elif source:
                    for item in _stats_data:
                        if item['item_type'] == 'news' and item['source_id'] == int(source_id):
                            #clicks += item.get('in_%s' % _provider_id, 0)
                            #временно берем значение без учета провайдера
                            clicks += float(item.get('in', 0))

                ############ Метрика отсутствует ############
                else:
                    for item in _stats_data:
                        if item['item_type'] == 'news':
                            #clicks += item.get('in_%s' % _provider_id, 0)
                            #временно берем значение без учета провайдера
                            clicks += float(item.get('in', 0))

                price += clicks * float(_provider.click_price)
        
        #возвращаем клики или стоимость
        return clicks if ret is 'clicks' else price

    def _get_out_rate():
        """
        Доля исходящего трафика данного клиента (группы) в общем исходящем трафике.
        """
        clicks = 0
        total_clicks = _get_total_clicks('out') #исходящие клики

        ############ Метрика группы ############
        if source_group_id:
            for item in stats_data:
                if item['source_id'] and item['item_type'] == 'banners' and item['source_group_id'] == int(source_group_id):
                    clicks += item['click']

        ############ Метрика клиента ############
        elif source:
            for item in stats_data:
                if item['item_type'] == 'banners' and item['source_id'] == int(source_id):
                    clicks += item['click']

        ############ Метрика отсутствует ############
        else:
            clicks = total_clicks

        return clicks/total_clicks if total_clicks else 0

    def _get_in_balance():
        """
        Баланс по стоимости для клиента (группы).
        """
        return _get_out('price') - _get_in('price')

    def _get_out_balance():
        """
        Баланс по трафику для клиента (группы).
        """
        return _get_out('clicks') - _get_in('clicks')

    price_data = {}
    price_data['out_clicks'] = _get_out('clicks')
    price_data['out_price'] = _get_out('price')
    price_data['in_clicks'] = _get_in('clicks')
    price_data['in_price'] = _get_in('price')
    #price_data['out_rate'] = _get_out_rate()
    #price_data['in_balance'] = _get_in_balance()
    #price_data['out_balance'] = _get_out_balance()

    return price_data


@admin.route('/api/stats/traffic-price', methods=['GET'])
@login_required
def traffic_price_api():
    """
    Отдать json с суммарной статистикой по стоимости траффика за период period.
    Данные отдаются временными фрагментами с шагом interval.
    По умолчанию оба временных промежутка составляют 5 минут (5min).
    """
    period = request.args.get('period')
    interval = request.args.get('interval')
    now = datetime.now()

    intervals = {
        '5min': timedelta(minutes=5),
        '10min': timedelta(minutes=10),
        '15min': timedelta(minutes=15),
        '30min': timedelta(minutes=30),
        '60min': timedelta(minutes=60),
        'day': timedelta(days=1),
        'month': timedelta(days=31),
        None: timedelta(minutes=5),
    }

    periods = {
        '5min': timedelta(minutes=5),
        '15min': timedelta(minutes=15),
        '30min': timedelta(minutes=30),
        '60min': timedelta(minutes=60),
        'range': parse(request.args.get('range_end', ''), dayfirst=True) -
                 parse(request.args.get('range_start', ''), dayfirst=True),
        'today': timedelta(days=1),
        'yesterday': timedelta(days=2),
        'month': timedelta(days=31),
        # 'all_time': timedelta(seconds=time.time()),
        'all_time': now - (now - relativedelta(years=1)),
        None: timedelta(minutes=5),
    }

    now = datetime.now()
    if period != 'range':
        td = _get_timedelta_to_round()
        _range_start = now - periods[period] - td
        _range_end = now - td
    else:
        _range_start = parse(request.args.get('range_start', ''), dayfirst=True)
        _range_end = parse(request.args.get('range_end', ''), dayfirst=True)
        if period == 'yesterday':
            #TODO: костыль для "Вчера"
            _range_end -= timedelta(days=1)

    def _get_timenodes():
        _period = periods[period]
        _interval = intervals[interval]
        if period == 'range':
            #TODO: при проверке кода оказалось что first_timenode это range_end, а last_timenode это range_start (кто это придумал!?)
            #также нужно от last_timenode отнимать 1 день чтоб выбирало с нужного числа
            first_timenode = parse(request.args.get('range_end', ''), dayfirst=True)
            last_timenode = parse(request.args.get('range_start', ''), dayfirst=True) - timedelta(days=1)
        else:
            first_timenode = now
            last_timenode = first_timenode - _period
            if period == 'yesterday':
                #TODO: костыль для "Вчера"
                first_timenode -= timedelta(days=1)
                
        timenodes, t = [], first_timenode
        while t >= last_timenode:
            timenodes.append(t)
            t -= _interval
        return sorted(timenodes)

    def _get_ranges():
        timenodes = _get_timenodes()
        ranges, i = [], 0
        while i < len(timenodes)-1:
            ranges.append(timenodes[i:i + 2])
            i += 1
        return ranges
        
    price_data = []
    for range in _get_ranges():
        if period in ['5min', '15min', '30min', '60min']:
            #используем период как есть
            _data = _get_traffic_price(period=period, range=range)
        else:
            #используем диапазон дат
            _data = _get_traffic_price(period='range', range=range)
            
        price_data.append({
            'data': _data,
            'range_start': time.strftime("%m/%d/%H:%M", range[0].timetuple()),
            'range_end':  time.strftime("%m/%d/%H:%M", range[1].timetuple())
        })

    response = jsonify({
        'data': price_data,
        'range_start': time.strftime("%H:%M", _range_start.timetuple()),
        'range_end':  time.strftime("%H:%M", _range_end.timetuple())
    })
    response.content_type += '; charset=utf-8'
    return response

CHART_ORDER = ['in', 'out', 'gen', 'pre_in', 'ext_in', 'overall_price']


def _get_generation_series_for_date(date, decimal_ctx):
    """
    Собрать статистику генерации за конкретный день за каждые пять минут.
    Интервал статистики - время с начала суток до времени в date.

    :type decimal_ctx: decimal.Context
    :type date: datetime
    :rtype: list[dict]
    """
    data_today = defaultdict(lambda: [])

    ts = int(date.timestamp())
    ts_day_start = int(date.replace(hour=0, minute=0, second=0).timestamp())

    # get 5 min intervals
    for its in range(ts_day_start, ts, 300):
        s = generation_stats.get_for_5min(ts=its)
        for k, v in s.items():
            data_today[k].append([its * 1000, decimal_ctx.create_decimal(v)])  # convert seconds -> milliseconds
    return [
        {
            'name': k,
            'data': data_today[k],
            'yAxis': 1 if k in ['li_gen', 'gen'] else 0
        } for k in CHART_ORDER
    ]


def _get_generation_series_for_all_time(decimal_ctx):
    """
    Собрать статистику генерации за все время (за те дни, которые есть в Редисе).

    :type decimal_ctx: decimal.Context
    :rtype: list[dict]
    """
    data_all_time = defaultdict(lambda: [])

    all_pairs = generation_stats.get_for_all_days()
    for ts, d in all_pairs:
        for k, v in d.items():
            data_all_time[k].append([ts * 1000, decimal_ctx.create_decimal(v)])

    return [
        {
            'name': k,
            'data': data_all_time[k],
            'yAxis': 1 if k in ['li_gen', 'gen', 'overall_price'] else 0
        } for k in CHART_ORDER
    ]


def _get_generation_source_banner(date):
    """Returns stats for active banners and sources for generation
    in format::

        {'active_banners': [{'added': ['added-banner-title'],
                             'removed': ['removed-banner-title'],
                             'count': 10}],
         'active_sources': [{'added': ['added-source-title'],
                             'removed': ['removed-source-title'],
                             'count': 10}]}

    """
    def _get_all_changed_items(model, stats):
        ids = set(chain.from_iterable(
            list(stat['added']) + list(stat['removed'])
            for stat in stats))
        if ids:
            return {str(x.id): x.title
                    for x in model.query.filter(model.id.in_(ids))}
        else:
            return {}

    def _prepare(model, stats):
        items = _get_all_changed_items(model, stats)
        ts = time.time()
        for stat in stats:
            # To be sure that only lines with filled B/S will be shown:
            if ts - stat['ts'] > 120:
                yield dict(stat,
                           added=[items.get(id) for id in stat['added']],
                           removed=[items.get(id) for id in stat['removed']])

    start_ts = int(date.replace(hour=0, minute=0, second=0).timestamp())

    return {
        'active_banners': list(_prepare(
            Banner, active_items_stats.get_banners_for_day(start_ts))),
        'active_sources': list(_prepare(
            Source, active_items_stats.get_sources_for_day(start_ts)))
    }


@admin.route('/api/generation', methods=['GET'])
@login_required
@roles_accepted('root', 'admin')
def generation_api():
    try:
        date_arg = request.args.get('date')
        date = datetime.strptime(
            date_arg, '%Y-%m-%d').replace(hour=23, minute=59)
    except (TypeError, ValueError):
        date = datetime.now()

    ts = int(date.timestamp())

    decimal_ctx = decimal.Context(prec=3)
    series_today = _get_generation_series_for_date(date, decimal_ctx)
    series_total = _get_generation_series_for_all_time(decimal_ctx)

    data = {
        'date': date.strftime('%d.%m.%Y'),
        'day_totals': generation_stats.get_for_day(ts=ts),
        'series_today': series_today,
        # не пересылать данные за все время если запрошена конкретная дата
        'series_total': series_total if 'date' not in request.args else []
    }
    # Adds stats for active source/banners:
    if request.args.get('with_source_banner') == 'true':
        data.update(_get_generation_source_banner(date))
    return jsonify({'data': data})


@admin.route('/api/source_titles', methods=['GET'])
@roles_accepted('root', 'admin', 'chief_editor', 'editor')
@login_required
def source_titles_api():
    sources = Source.get_all_titles()
    response = jsonify({'data': list(sources.items())})
    response.content_type += '; charset=utf-8'
    return response


@admin.route('/api/source/stats/total/<int:num>', methods=['DELETE'])
@roles_accepted('root', 'admin', 'chief_editor', 'editor')
@login_required
def source_reset_total_stats_api(num):
    Source.reset_total_stats(num)
    return jsonify({'success': True})


@admin.route('/api/config', methods=['GET', 'POST'])
@roles_accepted('root', 'admin')
@login_required
def config_api():
    if request.method == 'GET':
        return jsonify(site_config.get_all())
    else:
        config_vars = request.get_json()
        for k, v in config_vars.items():
            site_config[k] = v
        return jsonify({'success': True})


def _get_auction_hour_stats(item_type, item_id):
    item_stats = banner_stats if item_type == 'banner' else article_stats
    now = datetime.now() 
    ts = (now - timedelta(minutes=now.minute % 5,  hours=1)).timestamp()
    return [
        (dt.strftime('%H:%M'), vals)
        for dt, vals in item_stats.get_for_each_5min_in_60min(item_id, ts)
    ]


def _prepare_item_id(item_id):
    if item_id.isdigit():
        return int(item_id)
    else:
        return item_id


def _get_title(titles, id):
    try:
        return titles[int(id)]['title']
    except KeyError:
        return 'NONE'


def _get_source_title(titles, source_titles, id):
    try:
        return source_titles[titles[int(id)]['source_id']]
    except KeyError:
        return 'NONE'


def _prepare_total_stats(source_titles, titles, total_stats, curr_auction_stats):
    source_ids = {item['source_id'] for item in titles.values()}
    is_source_active = {
        source.id: source.is_active(time.time())
        for source in Source.query.filter(Source.id.in_(source_ids))}

    published_items = set(Item.get_all_published_ids())

    for id, (shows, clicks, first_finish_time, last_ctr) in total_stats.items():
        if first_finish_time:
            first_finish_time = datetime.fromtimestamp(first_finish_time)\
                                        .strftime('%Y-%m-%d %H:%M:%S')
        else:
            first_finish_time = ''

        is_active = is_source_active.get(titles[int(id)]['source_id'])\
            and int(id) in published_items

        yield {
            'id': id,
            'shows': shows,
            'clicks': clicks,
            'ctr': (100.0 * clicks / shows) if shows else 0,
            'last_ctr': 100 * last_ctr if last_ctr else '',
            'first_finish_time': first_finish_time,
            'title': _get_title(titles, id),
            'source_title': _get_source_title(titles, source_titles, id),
            'is_active': is_active,
            'curr_auction_shows': curr_auction_stats.get(id, {}).get('shows'),
            'curr_auction_clicks': curr_auction_stats.get(id, {}).get('clicks')
        }


@admin.route('/api/auction_stats/<string:item_type>/<string:item_id>')
@login_required
@roles_accepted('root', 'admin', 'chief_editor')
def auction_stats(item_type='overall', item_id=config.OVERALL_ITEM_ID):
    item_id = _prepare_item_id(item_id)
    stats = auction.get_stats(item_id, include_not_shown=True)
    source_titles = Source.get_all_titles()
    source_titles.update({0: 'NONE'})
    titles = defaultdict(lambda: {'title': 'NONE',
                                  'source_id': 0})
    titles.update(Item.get_all_titles_and_source_ids())
    curr_auction_stats = {
        int(i): {
            'shows': s,
            'clicks': c,
        }
        for i, (s, c) in stats['queue']
    }
    stats['queue'] = [{'id': int(i),
                       'shows': s,
                       'clicks': c,
                       'title': _get_title(titles, i),
                       'source_title': _get_source_title(titles, source_titles, i)}
                      for i, (s, c) in stats['queue']]
    stats['result'] = [{'id': int(i),
                        'ctr': ctr,
                        'title': _get_title(titles, i),
                        'source_title': _get_source_title(titles, source_titles, i)}
                       for i, (ctr) in stats['result']]
    stats['total_stats'] = sorted(_prepare_total_stats(source_titles,
                                                       titles,
                                                       stats['total_stats'],
                                                       curr_auction_stats,
                                                       ),
                                  key=lambda v: (v['ctr'], v['shows']),
                                  reverse=True)

    stats['id'] = item_id
    stats['title'] = titles[item_id]['title']

    stats['hour_stat'] = _get_auction_hour_stats(item_type, item_id)
    
    current_app.logger.debug(stats)
    response = jsonify(stats)
    response.content_type += '; charset=utf-8'
    return response


@admin.route('/api/banners/reset_auction/<int:banner_id>', methods=['POST'])
@login_required
@roles_accepted('root', 'admin', 'chief_editor', 'editor')
def reset_auction(banner_id):
    auction.reset_auction(banner_id)
    return jsonify({'success': True})


@admin.route('/api/banners/reset_all_auctions/', methods=['POST'])
@login_required
@roles_accepted('root', 'admin', 'chief_editor', 'editor')
def reset_all_auctions():
    auction.reset_all_auctions()
    return jsonify({'success': True})


@admin.route('/api/geo/short_config', methods=['GET'])
@login_required
def get_short_config():
    response = jsonify(Geo.short_yaml())
    response.content_type += '; charset=utf-8'
    return response


@admin.route('/api/geo/search', methods=['GET'])
@login_required
def get_search():
    data = Geo.short_yaml()
    query = request.args.get('query')
    if query:
        query = query.lower()
    size = request.args.get('size')
    if size:
        size = int(size)
    selected_ids = request.args.get('selected')
    if selected_ids:
        selected_ids = [geo_id for geo_id in selected_ids.split('-') if geo_id.isdigit()]

    def _lookup(fn):
        for i in data:
            country = data[i]
            fn(country, i)
            cities = country['cities']
            for j in cities:
                fn(cities[j], j, country, i)

    found = []
    selected = []

    def _find(region, geo_id, country=None, country_id=None):
        item = {'name': region if country else region['country_name']}
        if selected_ids and geo_id in selected_ids:
            item['selected'] = True
        elif size and len(found) > size:
            return
        elif query and query not in item['name'].lower():
            return
        if country:
            item['country_name'] = country['country_name']
            item['country_id'] = country_id
        item['id'] = geo_id
        if 'selected' in item:
            selected.append(item)
        else:
            found.append(item)

    _lookup(_find)

    def _order(region):
        return region['name']

    response = jsonify({
        'result': sorted(selected, key=_order) + sorted(found, key=_order)
    })
    response.content_type += ';charset=utf-8'
    return response
