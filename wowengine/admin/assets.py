from webassets import Bundle

from ..extensions import assets

JS_FILENAMES = ['jquery', 'jquery-ui',
                'underscore', 'moment',
                'highcharts',
                'bootstrap', 'nunjucks',
                'backbone',
                'backbone.queryparams',
                'backbone.stickit',
                'backbone.picky',
                'backbone-validation',
                'backbone.paginator',
                'backbone.marionette',
                'backbone.backgrid',
                'backbone.backgrid.paginator',
                'notify-combined',
                'fineuploader/jquery.fineuploader-5.0.2',
                'bootstrap-editable',
                'bootstrap-datepicker',
                'bootstrap-datepicker.ru']
JS_FILES = ['admin/js/vendor/%s.js' % x for x in JS_FILENAMES] + \
           ['frontend/js/vendor/jquery.hypher.js',
            'frontend/js/vendor/hypher.ru.js']

CSS_FILES = ['admin/js/vendor/fineuploader/fineuploader-5.0.2.min.css'] + \
            ['admin/css/%s.css' % x for x in ['bootstrap.min',
                                              'bootstrap-theme.min',
                                              'bootstrap-editable',
                                              'font-awesome.min',
                                              'backgrid',
                                              'backgrid-paginator',
                                              'bootstrap-datepicker3',
                                              'app']] + \
            ['frontend/css/%s.css' % x for x in ['blocks']]  # for calculating title sizes


def register_assets():
    js_bundle = Bundle(*JS_FILES,
                       **{'filters': 'uglifyjs', 'output': 'admin/gen/admin_js_all.js'})
    assets.register('admin_js_all', js_bundle)

    css_bundle = Bundle(*CSS_FILES,
                        **{'filters': 'cssmin', 'output': 'admin/gen/admin_css_all.css'})
    assets.register('admin_css_all', css_bundle)
