# # Customization the forms from flask-security.
# # See also documentation at
# # https://pythonhosted.org/Flask-Security/customizing.html


from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import DataRequired
from flask_security import forms


class ExtendedLoginForm(forms.LoginForm):
    email = StringField('Почта', [DataRequired()])
    password = PasswordField('Пароль', [DataRequired()])
    remember = BooleanField('Запомнить меня')


class ExtendedForgotPasswordForm(forms.ForgotPasswordForm):
    email = StringField('Почта', validators=[
                        forms.email_required,
                        forms.email_validator,
                        forms.valid_user_email])


class ExtendedResetPasswordForm(forms.ResetPasswordForm):
    password = PasswordField('Пароль',
                             validators=[forms.password_required,
                                         forms.password_length])

    password_confirm = PasswordField('Ещё раз',
                                     validators=[
                                         forms.EqualTo(
                                             'password',
                                             message='RETYPE_PASSWORD_MISMATCH'
                                             )])
