App.module 'Navigation', (Navigation) ->

  class Navigation.Item extends Backbone.Model
    initialize: ->
      i = new Backbone.Picky.Selectable(@)
      _.extend(@, i)


  class Navigation.Items extends Backbone.Collection
    model: Navigation.Item

    initialize: ->
      i = new Backbone.Picky.SingleSelect(@)
      _.extend(@, i)


  class Navigation.ItemView extends Marionette.ItemView
    tagName: 'li'
    template: '#nav-item-template'

    events:
      'click a': (e) ->
        e.preventDefault()
        App.navigate(@model.get('url'))

    modelEvents:
      'selected': 'onSelectionChange'
      'deselected': 'onSelectionChange'

    onSelectionChange: ->
      @$el.toggleClass('active', @model.selected)


  class Navigation.MenuView extends Marionette.CollectionView
    tagName: 'ul'
    className: 'nav navbar-nav'
    childView: Navigation.ItemView

    # change active field whose url matches the current path
    onChangeActiveMenu: (path) ->
      item = @collection.find (x) ->
        path.indexOf(x.get('url')) == 0
      @collection.select(item) if item?


  class Navigation.MiscToolsMenuView extends Marionette.ItemView
    tagName: 'li'
    className: 'dropdown'
    template: '#misc-tools-menu-template'
    events:
      'click a.tool': (e) ->
        e.preventDefault()
        App.navigate $(e.target).attr('data-url')
