### Модели ###

App.module 'Sources.Model', (Model, App) ->
  class Model.Item extends Backbone.Model
    urlRoot: '/ed/api/sources'

    validation:
      title:
        required: true
        msg: 'Нужен заголовок.'
      daily_click_limit:
        required: true
        pattern: 'number'
        msg: 'Нужен суточный лимит переходов'
      total_click_limit:
        required: true
        pattern: 'number'
        msg: 'Нужен суммарный лимит переходов'
      daily_show_limit:
        required: true
        pattern: 'number'
        msg: 'Нужен суточный лимит показов'
      total_show_limit:
        required: true
        pattern: 'number'
        msg: 'Нужен суммарный лимит показов'

    initialize: ->
      if @isNew() #issue10 All weekdays must be checked and set to 00:00-23:59 time interval
        for wd in WEEKDAYS
          @set "active_#{wd.en}", true
          @set "active_#{wd.en}_start", 0
          @set "active_#{wd.en}_end", 1439
        @set 'daily_click_limit': '0', 'total_click_limit': '0', 'daily_show_limit': '0', 'total_show_limit': '0'

  class Model.Items extends App.Models.RestlessCollection
    model: Model.Item
    url: '/ed/api/sources'

  class Model.Region extends Backbone.Model
    validation:
      id:
        require: true
        pattern: 'number'
      name:
        require: true
        pattern: 'string'
      parent_id:
        pattern: 'number'
      parent_name:
        pattern: 'string'

  class Model.RegionList extends Backbone.Collection
    model: Model.Region

    url: ->
      params = size: @size || 10
      if @query
        params.query = @query
      selected = $('#source-geo').val()
      if selected
        params.selected = selected.slice(1, -1).replace(/,/g, '-')
      return '/ed/api/geo/search?' + $.param params

    initialize: ->
      @query = ''
      @size = 10

    fetch: ->
      collection = this
      $.get(_.result(@, 'url')).success (data) ->
        collection.reset(data.result)

    selected: ->
      @filter (m) -> m.get 'selected'

    ### Несколько утилит для построения формы редактирования источника ###
two_digit_num = (n) ->
  if n < 10 then "0#{n}" else n.toString()

ACTIVE_START_TIMES = _.flatten(
  for h in [0..23]
    for m in [0, 30]
      value: h * 60 + m
      caption: "#{two_digit_num(h)}:#{two_digit_num(m)}"
)

ACTIVE_END_TIMES = ACTIVE_START_TIMES.slice(0)
ACTIVE_END_TIMES.push
  value: 23 * 60 + 59
  caption: "23:59"

WEEKDAYS = [
  {'en': 'mon', 'ru': 'Пн'}
  {'en': 'tue', 'ru': 'Вт'}
  {'en': 'wed', 'ru': 'Ср'}
  {'en': 'thu', 'ru': 'Чт'}
  {'en': 'fri', 'ru': 'Пт'}
  {'en': 'sat', 'ru': 'Сб'}
  {'en': 'sun', 'ru': 'Вс'}
]

### Вьюшки ###
App.module 'Sources.Views', (Views, App) ->

  class Views.RegionView extends Marionette.ItemView
    tagName: 'label'
    template: '#region-template'
    attributes:
      'class': 'geo-region'

    initialize: ->
      @model.on 'change', (m) =>
        selected = m.collection.selected()
        if selected.length > 0
          $('#source-geo').val(',' + _.pluck(selected, 'id').join(',') + ',')
        else
          $('#source-geo').val('')
        @$el.find('[type="checkbox"]').prop('checked', @model.get 'selected')
    events:
      'change [type="checkbox"]': (e) ->
        selected = !@model.get 'selected'
        if selected
          if @model.get 'country_id'
            region = @model.collection.findWhere id: @model.get 'country_id'
          else
            region = @model.collection.findWhere country_id: @model.get 'id'
          if region
              region.set 'selected', false
        @model.set 'selected', selected

    onRender: ->
      @$el.find('[type="checkbox"]').prop('checked', @model.get 'selected')

  class Views.RegionSearchView extends Marionette.CompositeView
    template: '#region-search-template'
    childView: Views.RegionView
    childViewContainer: '.children'
    events:
      'keyup [type=search]': (e) ->
        @collection.query = e.target.value
        @collection.fetch()

    # Вьюха редактирования источника
  class Views.EditItemView extends Marionette.ItemView

    initialize: ->
      Backbone.Validation.bind @

    template: '#edit-source-template'

    templateHelpers:
      WEEKDAYS: WEEKDAYS

    bindings:
      '#source-title': 'title'
      '#source-daily-click-limit': 'daily_click_limit'
      '#source-total-click-limit': 'total_click_limit'
      '#source-daily-show-limit': 'daily_show_limit'
      '#source-total-show-limit': 'total_show_limit'
      '#source-hide-referer': 'hide_referer'
      '#source-show-in-galleries': 'show_in_galleries'
      '#source-show-in-hell-galleries': 'show_in_hell_galleries'
      '#source-is-fullfill': 'is_fullfill'
      '#source-geo': 'geo'
      '#source-click-price':
        observe: 'click_price'
        onGet: App.decimal
        
    ui:
      price_history_collapse: '#source-click-price-history-collapse'

    onRender: ->
    # add some stickit bindings programmatically
      for wd in WEEKDAYS
        @addBinding null, "#source-active-#{wd.en}", "active_#{wd.en}"
        @addBinding null, "#source-active-#{wd.en}-start",
          observe: "active_#{wd.en}_start"
          selectOptions:
            collection: ACTIVE_START_TIMES
            labelPath: 'caption'
            valuePath: 'value'
        @addBinding null, "#source-active-#{wd.en}-end",
          observe: "active_#{wd.en}_end"
          selectOptions:
            collection: ACTIVE_END_TIMES
            labelPath: 'caption'
            valuePath: 'value'
                  
      @sourcePriceHistory()
      @stickit()

    events:
      'click button.sources-save': (e) ->
        @model.attributes.geo = ($('#source-geo').val()).replace /\s+/g, ''
        if Array.isArray(@model.attributes.geo)
          @model.attributes.geo = @model.attributes.geo.join()
        e.preventDefault()
        @onSave()
      'click button.show-banners': (e) ->
        e.preventDefault()
        App.navigate "banners?source=#{@model.id}"
      'click button.reset-counters': (e) ->
        $.ajax '/ed/api/source/stats/total/' + @model.attributes.id,
          type: 'DELETE'
          dataType: 'json'
          error: ->
            alert 'reset-counters failed'
          success: ->
            $("button.reset-counters").attr("disabled", true)

    onSave: ->
      v = @model.validate()
      if @model.isValid()
        @model.save().success ->
          App.navigate "sources"
        .fail ->
          alert 'save failed'
      else
        alert _.values(v).join("\n")
    
    #формирование истории цены если она есть
    sourcePriceHistory: -> 
      history = @model.get 'source_click_price_history'
      if history? and history.length > 0
        html = ( 
          """
            <table class="table table-condensed">
              <thead>
                <tr>
                  <th>Дата</th>
                  <th>Цена</th>
                </tr>
              </thead>
              <tbody>
                #{(
                    for i in history
                      """
                        <tr>
                          <th>#{moment(i.date).format('DD.MM.YYYY HH:mm')}</th>
                          <td>#{App.floatFormat(i.click_price)}</td>
                        </tr>                    
                      """
                ).join ''}
              </tbody>
            </table>
          """
        )
        @ui.price_history_collapse.find('.panel').html html
      return

  class Views.ItemView extends Marionette.ItemView
    tagName: 'tr'
    template: '#source-template'

    bindings:
      'input.mark': 'is_marked'

    events:
      'click .edit-source': (e) ->
        e.preventDefault()
        App.navigate "sources/#{@model.id}"
      'click .show-banners': (e) ->
        e.preventDefault()
        App.navigate "banners?source=#{@model.id}"

    ui:
      num_edit: 'a.num-edit'

    serializeData: ->
      data = super
      return _.extend data,
        idx: @options.collIndex + 1

    onRender: ->
      @stickit()

    onShow: ->
      @ui.num_edit.editable
        mode: 'inline'
        showbuttons: false
        send: 'never'
        type: 'text'
        validate: (v) ->
          if isNaN(v)
            return 'нужен номер'
        success: (_, newValue) =>
          @model.save({num: newValue}, {patch: true})

    triggers:
      'click input.mark':
        event: 'do:uncheck'
        preventDefault: false

  class Views.ListView extends Marionette.CompositeView
    model: new Backbone.Model()
    template: '#source-list-template'
    childView: Views.ItemView
    childViewContainer: 'tbody#sources-list'
    childViewOptions: (m) ->
      collIndex: @collection.indexOf m

    ui:
      selectAllCheckBox:'#select-all'

    events:
      'click .add-new-source': ->
        App.navigate "sources/new"
      'click button.delete-selected-sources': ->
        App.confirmAndDelete @collection
      'click #select-all': (e) ->
        for model in @collection.models
          model.set({'is_marked':  e.target.checked})

    onChildviewDoUncheck: (e) ->
      @ui.selectAllCheckBox.prop 'checked', false

    onBeforeRender: =>
      dd = {}
      for i in ['in', 'out', 'price']
        dd[i] = @collection.reduce (c, m) ->
          c + switch i
            when 'in' then m.get('today_stats').show
            when 'out' then m.get('today_stats').click
            when 'price' then (m.get('click_price') * m.get('today_stats').click)
        , 0

      dd.ctr = if dd.in == 0 then 0 else dd.out / dd.in
      dd.price = dd.price / dd.out
      @model.set 'totals', dd


### Навигация ###
App.module 'Sources', (Sources, App) ->
  class Sources.NavController extends Marionette.Controller
    list: ->
      c = new Sources.Model.Items()
      App.fetchCollectionView c, Sources.Views.ListView, {order_by: [
        field: 'num', direction: 'asc'
      ]}

    edit: (id) ->
      if id == 'new'
        @_editModel new Sources.Model.Item()
      else
        model = new Sources.Model.Item({id: id})
        model.fetch().done =>
          @_editModel model
        .fail ->
          App.showErrorView

    _editModel: (model) ->
      view = new Sources.Views.EditItemView({model: model})
      regionList = new Sources.Model.RegionList()
      regionSearchView = new Sources.Views.RegionSearchView collection: regionList
      App.mainRegion.show(view)
      geo = $('#source-geo')[0]
      geo.parentNode.appendChild regionSearchView.el
      geo.style.display = 'none'
      regionList.fetch()
      regionSearchView.render()
      window.regionList = regionList


  class Sources.Router extends App.Router
    controller: new Sources.NavController()

    appRoutes:
      'sources': 'list'
      'sources/:id': 'edit'

  Sources.addInitializer ->
    Sources.router = new Sources.Router()
