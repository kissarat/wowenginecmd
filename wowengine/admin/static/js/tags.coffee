App.module 'Tags.Model', (Model, App) ->

  class Model.Item extends Backbone.Model
    urlRoot: '/ed/api/tags'

    initialize: ->
      i = new Backbone.Picky.Selectable(@)
      _.extend(@, i)

    validation:
      title:
        required: true
        msg: 'Нужен заголовок'

  class Model.Items extends App.Models.RestlessCollection
    model: Model.Item
    url: '/ed/api/tags'

    initialize: ->
      i = new Backbone.Picky.MultiSelect(@)
      _.extend(@, i)


### Вьюшки ###
App.module 'Tags.Views', (Views, App) ->

  class Views.EditItemView extends Marionette.ItemView

    initialize: ->
      Backbone.Validation.bind @

    template: '#edit-tag-template'

    bindings:
      '#tag-title': 'title'

    onRender: ->
      @stickit()

    events:
      'click button.sources-save': (e) ->
        e.preventDefault()
        @onSave()

    onSave: ->
      v = @model.validate()
      if @model.isValid()
        @model.save().success ->
          App.navigate "tags"
        .fail ->
          alert 'save failed'
      else
        alert _.values(v).join("\n")


  class Views.ItemView extends Marionette.ItemView
    tagName: 'tr'
    template: '#tag-template'

    bindings:
      'input.mark': 'is_marked'

    events:
      'click .edit-tag': ->
        App.navigate "tags/#{@model.id}"

    onRender: ->
      @stickit()


  class Views.ListView extends Marionette.CompositeView
    template: '#tag-list-template'
    childView: Views.ItemView
    childViewContainer: 'tbody#tag-list'

    events:
      'click .add-new-tag': ->
        App.navigate "tags/new"
      'click button.delete-selected-tag': ->
        App.confirmAndDelete @collection


### Навигация ###
App.module 'Tags', (Tags, App) ->

  class Tags.NavController extends Marionette.Controller
    list: ->
      c = new Tags.Model.Items()
      App.fetchCollectionView c, Tags.Views.ListView, {order_by: [field: 'id', direction: 'asc']}

    edit: (id) ->
      if id == 'new'
        @_editModel new Tags.Model.Item()
      else
        model = new Tags.Model.Item({id: id})
        model.fetch().done =>
          @_editModel model
        .fail ->
          App.showErrorView()

    _editModel: (model) ->
      view = new Tags.Views.EditItemView({model: model})
      App.mainRegion.show(view)


  class Tags.Router extends App.Router
    controller: new Tags.NavController()

    appRoutes:
      'tags': 'list'
      'tags/:id': 'edit'

  Tags.addInitializer ->
    Tags.router = new Tags.Router()
