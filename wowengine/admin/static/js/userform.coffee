(($) ->
  $('.sh-user').unbind("click").bind "click", ->
    $('#email').val((@).textContent)
    $('#id').val((@).dataset['id'])
    $('#firstname').val((@).dataset['firstname'])
    $('#lastname').val((@).dataset['lastname'])
    $('#role').val((@).dataset['role'])

    if (@).dataset['active'] == 'False'
      $('#active').attr 'checked', false
    else
      $('#active').attr 'checked', true

    if (@).dataset['role'] == 'root'
      $('#delete').hide()
      disable_form(true)
      $('#save').hide()

    else
      $('#delete').show()
      $('option[value=root]').attr 'disabled', true
      disable_form(false)
      $('#save').show()


  $('#add-new-user').unbind('click').bind 'click', ->
    disable_form(false)
    $('#save').show()
    $('#email').val('')
    $('#id').val('')
    $('#firstname').val('')
    $('#lastname').val('')
    $('#role').val('client')
    $('#active').attr 'checked', true
    $('option[value=root]').attr 'disabled', true
    $('#delete').hide()


  # Save or create user
  $('#save').unbind('click').bind 'click', ->
    email = $('#email')
    if ! validate_email(email.val())
      show_error('Неверный email')
      return
    data =
        email: email.val(),
        firstname: $('#firstname').val(),
        lastname: $('#lastname').val(),
        password: $('#password').val(),
        role: $('#role').val(),
        active: $('#active').prop('checked'),
        id: $('#id').val()
    # Save user
    if data['id']
      send_request(data, 'PUT')

    # Create user
    else
      send_request(data, 'POST')

  # Delete user
  $('#delete').unbind('click').bind 'click', ->
    data = id: $('#id').val()
    if data['id']
      send_request(data, 'DELETE')


  # Clear a form from error message.
  $('#myModal').unbind('hidden.bs.modal').bind 'hidden.bs.modal', ->
    $('#error').html('')


  disable_form = (enabled) ->
    $('#role').attr('disabled', enabled)
    $('#email').attr('disabled', enabled)
    $('#firstname').attr('disabled', enabled)
    $('#lastname').attr('disabled', enabled)
    $('#password').attr('disabled', enabled)
    $('#active').attr('disabled', enabled)

  show_error = (msg) ->
    $('#error').html('<div class="alert alert-danger">' + msg + '</div>')

  validate_email = (email) ->
    re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
    return re.test(email)

  send_request = (data, type) ->
    save = $('#save')
    del = $('#delete')
    save.attr 'disabled', true
    del.attr 'disabled', true
    $.ajax
      url:'/ed/users/'
      type: type
      timeout: 60000
      data: data
      async: true
      success: (data) ->
        if data['result'] == 'error'
          show_error(data['message'])
        else
          location.reload()

      error: (data) ->
        show_error(data['message'])

    save.attr 'disabled', false
    del.attr 'disabled', false

) jQuery