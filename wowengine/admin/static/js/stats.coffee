# --- Статистика

App.module 'Tools.Stats', (Stats, App) ->
  class Stats.Items extends Backbone.Collection
    url: '/ed/api/stats/'

    initialize: (args...) ->
      @_meta = {}
      super args...

    meta: (key, value) =>
      if value?
        @_meta[key] = value
      else
        @_meta[key]

    parse: (data) =>
      @meta 'range_start', data.range_start
      @meta 'range_end', data.range_end
      data.data

    comparator: (x) ->
      -1 * (x.get 'show')


  class Stats.ItemView extends Marionette.ItemView
    template: '#tools-stats-item-template'
    tagName: 'tr'
    serializeData: ->
      data = super
      return _.extend data,
        idx: @options.collIndex + 1

  class Stats.ListView extends App.withSorting(Marionette.CompositeView)
    model: new Backbone.Model()
    template: '#tools-stats-template'
    childView: Stats.ItemView
    childViewContainer: 'tbody#stat-data'
    childViewOptions: (m) ->
      collIndex: @collection.indexOf m

    initialize: (opts) ->
      @model.set 'source_titles', opts.source_titles
      @model.set 'period', opts.period
      @model.set 'item_type', opts.item_type
      @model.set 'source_id', opts.source_id

      if opts.period == 'range'
        @model.set 'range_start', opts.range_start
        @model.set 'range_end', opts.range_end
      else
        @model.set 'display_range_start', opts.range_start
        @model.set 'display_range_end', opts.range_end

      @loadData().then =>
        @applySorting 'in', -1
        @render()

    ui:
      sort_headers: 'th.sort'
      nav_pills: 'ul.nav-period-pills li'
      date_range: '.input-daterange'
      range_start: 'input.range-start'
      range_end: 'input.range-end'

    events:
      'click li.nav-period': 'changePeriod'
      'change #select-source': 'changeSource'
      'click a.show-date-range': 'showDateRange'
      'click button.apply-date-range': 'applyDateRange'
      'click .menu-stats-filters a.tool': 'menuStatsFilters'

    bindings:
      '#display-range-start': 'display_range_start'
      '#display-range-end': 'display_range_end'

    sortFieldSelector: 'th.sort'

    applySorting: (field, order) ->
      @collection.comparator = (x) => 
        order * +x.get(field)
      @collection.sort()
      super field, order
      
    menuStatsFilters: (e) ->
      e.preventDefault()
      target = $(e.currentTarget)
      App.navigate target.attr('data-url')
      return

    changeSource: (e) =>
      e.preventDefault()
      sourceId = $(e.currentTarget.selectedOptions).attr 'value'
      @applyFilters undefined , sourceId

    changePeriod: (e) =>
      e.preventDefault()
      period = $(e.currentTarget).attr 'data-period'
      @applyFilters "#{period}"

    getPeriodForUrl: (period) ->
      if _.isUndefined period
        if @model.get('period') == 'range'
          "#{@model.get('range_start')}--#{@model.get('range_end')}"
        else
          @model.get('period')
      else
        period

    getSourceForUrl: (sourceId) ->
      if _.isUndefined sourceId
        sourceId = @model.get('source_id')

      if _.isUndefined(sourceId) or _.isNull(sourceId) or _.isNaN(sourceId) or not sourceId
        return undefined

      sourceTitle = _.filter @model.get('source_titles'), ([id]) ->
        id == parseInt(sourceId, 10)
      sourceTitle = sourceTitle[0][1]
      return "#{sourceId}-#{sourceTitle}"

    applyFilters: (period=undefined, sourceId=undefined) ->
      itemType = @model.get('item_type')
      period = @getPeriodForUrl(period)
      source = @getSourceForUrl(sourceId)

      if _.isUndefined source
        App.navigate "/tools/stats/#{itemType}/#{period}"
      else
        App.navigate "/tools/stats/#{itemType}/#{source}/#{period}"

    applyDateRange: ->
      start = @ui.range_start.val()
      end = @ui.range_end.val()
      @applyFilters "#{start}--#{end}"

    getRangeEnd: ->
      rawEnd = @model.get('range_end')
      if rawEnd
        moment(rawEnd, 'DD.MM.YYYY').add(1, 'days').format('DD.MM.YYYY')

    reloadData: ->
      App.loader on
      requestData =
        period: @model.get('period')
        range_start: @model.get('range_start')
        range_end: @getRangeEnd()
        type: @model.get('item_type')
      @collection.fetch({data: requestData}).then =>
        @render()  # update totals and reset source select
        App.loader off

    onBeforeRender: =>
      dd = {}
      for i in ['in', 'out', 'show', 'click']
        dd[i] = @collection.reduce ((c, m) ->
          c + m.get(i)), 0
      dd.gen = if dd.in == 0 then 0 else dd.out / dd.in
      dd.ctr = if dd.show == 0 then 0 else dd.click / dd.show
      @model.set 'totals', dd

    onRender: =>
      _.each @ui.nav_pills, (x) =>
        $(x).toggleClass 'active', @model.get('period') == $(x).attr('data-period')

      if _.isUndefined(@sortField) and @model.get('item_type') == 'whole'
        @applySorting 'in', -1
      @renderDateRange()
      @stickit
      super

    renderDateRange: ->
      @ui.range_start.val @model.get('range_start')
      @ui.range_end.val @model.get('range_end')
      @ui.date_range.datepicker
        orientation: 'top'
        format: 'dd.mm.yyyy'
        language: "ru"
      if @model.get('period') == 'range'
        @ui.date_range.show()
      else
        @ui.date_range.hide()

    showDateRange: (e) ->
      e.preventDefault()
      @ui.date_range.toggle()


  # sources statistics

  class Stats.Sources extends Backbone.Collection

    comparator: (x) ->
      -1 * (x.get 'shows')

  class Stats.SourceView extends Marionette.ItemView
    template: '#tools-sources-stats-item-template'
    tagName: 'tr'
    serializeData: ->
      data = super
      return _.extend data,
        idx: @options.collIndex + 1

  class Stats.SourcesListView extends App.withSorting(Marionette.CompositeView)
    model: new Backbone.Model()
    template: '#tools-sources-stats-template'
    childView: Stats.SourceView
    childViewContainer: 'tbody'
    childViewOptions: (m) ->
      collIndex: @collection.indexOf m
      
    def:
      period: '5min'

    initialize: (opts) ->
      @model.set 'period', @def.period
      @model.set 'limit', 1000

    ui:
      sort_headers: 'th.sort'
      nav_pills: 'ul.nav-period-pills li'
      date_range: '.input-daterange'
      range_start: 'input.range-start'
      range_end: 'input.range-end'
      alert: '.alert-box'

    events:
      'click li.nav-period': 'changePeriod'
      'click a.show-date-range': 'showDateRange'
      'click button.apply-date-range': 'applyDateRange'
      'click a.show-source-stats': 'showSourceStats'

    sortFieldSelector: 'th.sort'

    applySorting: (field, order) ->
      @collection.comparator = (x) => 
        if field is 'name'
          # сортировка по текстовому полю
          res = App.genSortString x.get(field), order
        else
          # сортировка по числовому полю
          res = order * +x.get(field)
        return res
      @collection.sort()
      super field, order

    changePeriod: (e) =>
      e.preventDefault()
      target = $(e.currentTarget)
      @model.set 'period', target.attr 'data-period'
      @loadData()

    applyDateRange: ->
      @model.set 'period', 'range'
      @model.set 'range_start', @ui.range_start.val()
      @model.set 'range_end', @ui.range_end.val()
      @loadData()
      
    showSourceStats: (e) ->
      e.preventDefault()
      target = $(e.currentTarget)
      App.global['source_id'] = target.attr 'data-id'
      App.navigate "history/prices"
      return

    getRangeEnd: ->
      rawEnd = @model.get('range_end')
      if rawEnd
        moment(rawEnd, 'DD.MM.YYYY').add(1, 'days').format('DD.MM.YYYY')

    loadData: ->
      App.loader on
      $(@ui.alert).empty()
      @collection.reset()
      params =
        period: @model.get('period')
      
      if 'range' is params.period then (
        params['range_start'] = @model.get('range_start')
        params['range_end'] = @model.get('range_end')
      )

      @request.abort() if @request
      @request = $.getJSON "/ed/api/sources/stats/?#{$.param params}" 
      .done (result) =>
        if result.data? then (
          for record in result.data
            model = new Backbone.Model
            model.set 'id', record.source_id
            model.set 'name', record.source
            model.set 'clicks', record.click
            model.set 'shows', record.show
            model.set 'ctr', App.floatFormat(record.ctr)
            model.set 'ask', App.floatFormat(record.click_price)
            model.set 'bid', App.floatFormat(record.total_price)
            @collection.add(model)
          @render()
        )
        App.loader off
        return
      .fail (e) =>
        text = """
                Произошел сбой, не удалось получить данные.<br>
                Код ошибки: #{e.status} #{e.statusText}
               """
        App.alert @ui.alert, text, 'warning'
        @render()
        App.loader off
        return

    onBeforeRender: =>
      totals = {}
      for i in ['clicks', 'shows', 'ask', 'bid']
        value = 0
        @collection.each (m) ->
          value += parseFloat(m.get(i))
        totals[i] = value
      totals.ctr = (if totals.shows then 100 * totals.clicks / totals.shows else 0)
      totals.ask = (if totals.clicks then totals.bid / totals.clicks else 0)
      for n in ['ask', 'bid', 'ctr']
        totals[n] = App.floatFormat(totals[n])
      @model.set 'totals', totals

    onRender: =>
      @toggleBtn()
      @renderDateRange()
      
    toggleBtn: ->
      _.each @ui.nav_pills, (x) =>
        period = @model.get('period') or @def.period
        status = (period is $(x).attr('data-period'))
        $(x).toggleClass 'active', status
        return

    renderDateRange: ->
      @ui.range_start.val @model.get('range_start')
      @ui.range_end.val @model.get('range_end')
      @ui.date_range.datepicker
        orientation: 'top'
        format: 'dd.mm.yyyy'
        language: "ru"
      if @model.get('period') == 'range'
        @ui.date_range.show()
      else
        @ui.date_range.hide()

    showDateRange: (e) ->
      e.preventDefault()
      @ui.date_range.toggle()


  class Stats.Router extends App.Router
    controller:
      index: (item_type, source_id=undefined , period=undefined ) ->
        coll = App.FilteredCollection(new Stats.Items())

        if _.isUndefined period
          period = source_id
          source_id = null

        if _.isUndefined period
          period = '5min'

        if not _.isUndefined source_id
          source_id = parseInt source_id, 10

        getPeriod =  ->
          if period.indexOf('--') != -1
            [start, end] = period.split '--'

            period: 'range'
            range_start: start
            range_end: end
          else
            period: period
            range_start: coll.meta 'range_start'
            range_end: coll.meta 'range_start'

        $.when(
          coll.fetch(data: {type: item_type}),
          $.ajax("/ed/api/source_titles")
        ).then (__, [source_titles]) ->
          options =
            collection: coll
            source_titles: source_titles.data
            item_type: item_type
            source_id: source_id
          _.extend options, getPeriod()
          App.mainRegion.show new Stats.ListView(options)


      sources: () ->
        listView = new Stats.SourcesListView(collection: new Stats.Sources())
        listView.loadData()
        App.mainRegion.show listView

    appRoutes:
      'tools/stats/:item_type': 'index'
      'tools/stats/:item_type/:period': 'index'
      'tools/stats/:item_type/:source_id/:period': 'index'
      'tools/sources/stats': 'sources'


  Stats.addInitializer ->
    Stats.router = new Stats.Router()
