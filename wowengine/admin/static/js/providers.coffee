### Модели ###

App.module 'Providers.Model', (Model, App) ->
  class Model.Item extends Backbone.Model
    urlRoot: '/ed/api/providers'

    validation:
      name:
        required: true
        msg: 'Введите заголовок'
      domain_pattern:
        required: true
        msg: 'Введите паттерн URL'
      click_price:
        required: true
        pattern: 'number'
        msg: 'Введите цену клика'

    initialize: ->
      if @isNew()
        @set 'click_price': '0'

  class Model.Items extends App.Models.RestlessCollection
    model: Model.Item
    url: '/ed/api/providers'

### Вьюшки ###
App.module 'Providers.Views', (Views, App) ->

  # Вьюха редактирования провайдера
  class Views.EditItemView extends Marionette.ItemView

    initialize: ->
      Backbone.Validation.bind @

    template: '#edit-provider-template'

    bindings:
      '#provider-name': 'name'
      '#provider-domain-pattern': 'domain_pattern'
      '#provider-click-price':
        observe: 'click_price'
        onGet: App.decimal
        
    ui:
      price_history_collapse: '#provider-click-price-history-collapse'

    onRender: ->
      @providerPriceHistory()
      @stickit()

    events:
      'click button.provider-save': (e) ->
        e.preventDefault()
        @onSave()

    onSave: ->
      v = @model.validate()
      if @model.isValid()
        @model.save().success ->
          App.navigate "providers"
        .fail ->
          alert 'save failed'
      else
        alert _.values(v).join("\n")
    
    #формирование истории цены если она есть
    providerPriceHistory: -> 
      history = @model.get 'provider_click_price_history'
      if history? and history.length > 0
        html = ( 
          """
            <table class="table table-condensed">
              <thead>
                <tr>
                  <th>Дата</th>
                  <th>Цена</th>
                </tr>
              </thead>
              <tbody>
                #{(
                    for i in history
                      """
                        <tr>
                          <th>#{moment(i.date).format('DD.MM.YYYY HH:mm')}</th>
                          <td>#{App.floatFormat(i.click_price)}</td>
                        </tr>                    
                      """
                ).join ''}
              </tbody>
            </table>
          """
        )
        @ui.price_history_collapse.find('.panel').html html
      return

  class Views.ItemView extends Marionette.ItemView
    tagName: 'tr'
    template: '#provider-template'

    bindings:
      'input.mark': 'is_marked'

    events:
      'click .edit-provider': (e) ->
        e.preventDefault()
        App.navigate "providers/#{@model.id}"

    serializeData: ->
      data = super
      return _.extend data,
        idx: @options.collIndex + 1

    onRender: ->
      @stickit()

    triggers:
      'click input.mark':
        event: 'do:uncheck'
        preventDefault: false

  class Views.ListView extends Marionette.CompositeView
    model: new Backbone.Model()
    template: '#provider-list-template'
    childView: Views.ItemView
    childViewContainer: 'tbody#provider-list'
    childViewOptions: (m) ->
      collIndex: @collection.indexOf m

    ui:
      selectAllCheckBox:'#select-all'

    events:
      'click .add-new-provider': ->
        App.navigate "providers/new"
      'click button.delete-selected-providers': ->
        App.confirmAndDelete @collection
      'click #select-all': (e) ->
        for model in @collection.models
          model.set({'is_marked':  e.target.checked})

    onChildviewDoUncheck: (e) ->
      @ui.selectAllCheckBox.prop 'checked', false

    onBeforeRender: =>
      dd = 
        clicks: 0
        price: 0
        
      for i in ['clicks', 'price']
        dd[i] = @collection.reduce (c, m) ->
          c + switch i
            when 'clicks' then 0
            when 'price' then m.get('click_price')
        , 0

      @model.set 'totals', dd


### Навигация ###
App.module 'Providers', (Providers, App) ->
  class Providers.NavController extends Marionette.Controller
    list: ->
      c = new Providers.Model.Items()
      App.fetchCollectionView c, Providers.Views.ListView

    edit: (id) ->
      if id == 'new'
        @_editModel new Providers.Model.Item()
      else
        model = new Providers.Model.Item({id: id})
        model.fetch().done =>
          @_editModel model
        .fail ->
          App.showErrorView
          
    _editModel: (model) ->
      view = new Providers.Views.EditItemView({model: model})
      App.mainRegion.show(view)

  class Providers.Router extends App.Router
    controller: new Providers.NavController()

    appRoutes:
      'providers': 'list'
      'providers/:id': 'edit'

  Providers.addInitializer ->
    Providers.router = new Providers.Router()
