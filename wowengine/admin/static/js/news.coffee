###
Тут описывается работа с объектами, которые содержатся в таблице items (новости, баннеры, строки).
Так как по большей части эти объекты очень похожи, большая часть их функционала выделена в классы
с названиями *Base, от них наследуются уже конкретные классы.

Шаблоны для этих объектов тоже одни и те же, их отличия прописаны внутри самих шаблонов с помощью
условий.
###

### Модели ###

App.module 'News.Model', (Model, App) ->

  # картинка
  class Model.Image extends Backbone.Model
    defaults:
      caption: ''
      copyright: ''
      cx: 50
      cy: 50

    destroy: (unid) ->
      Backbone.ajax
        url: "/ed/api/images/#{unid}/#{@get 'num'}"
        type: 'DELETE'
      .success =>
        @trigger('destroy', @, @collection)


  # картинки
  class Model.Images extends Backbone.Collection
    model: Model.Image


  # база для модели: общий функционал
  class Model.ItemBase extends Backbone.Model
    # validate
    defaults:
      date: new Date()
      unid: ->
        App.gen_uuid()
      is_gallery: false
      is_published: true
      
      title: ''

    urlRoot: ->
      "/ed/api/#{@newsType}"

    getIndex: ->
      if @collection
        index = @collection.indexOf(this)
        if @collection.state
          index += (@collection.state.currentPage - 1 ) * @collection.state.pageSize
        index
      else
        0

  # база для коллекции: общий функционал
  class Model.ItemsBase extends App.Models.RestlessCollection

  # новость
  class Model.NewsItem extends Model.ItemBase
    newsType: 'news'
    validation:
      title:
        required: true
        msg: 'Нужен заголовок.'


  # список новостей
  class Model.NewsItems extends Model.ItemsBase
    model: Model.NewsItem
    url: '/ed/api/news'

  # глагне
  class Model.MordaItems extends Model.ItemsBase
    model: Model.NewsItem
    url: '/ed/api/morda'

  # баннер
  class Model.Banner extends Model.ItemBase
    newsType: 'banners'
    validation:
      title:
        required: true
        msg: 'Нужен заголовок.'
      link:
        required: true
        msg: 'Нужна ссылка.'
      source_id:
        required: true
        msg: 'Нужен источник.'


  # список баннеров
  class Model.Banners extends Model.ItemsBase
    model: Model.Banner
    url: '/ed/api/banners'


  # строчка
  class Model.Line extends Model.ItemBase
    newsType: 'lines'
    validation:
      title:
        required: true
        msg: 'Нужен заголовок.'


  # список строчек
  class Model.Lines extends Model.ItemsBase
    model: Model.Line
    url: '/ed/api/lines'


### Вьюхи ###

App.module 'News.Views', (Views, App) ->

  # Строчка с картинкой внутри формы новости.
  class Views.Image extends Marionette.ItemView

    template: '#news-image-template'
    className: 'row well well-sm'

    events:
      'click button.delete-image': 'onDelete'

      'drop': (event, position) ->
        @trigger 'sortable:drop', @model, position

    onDelete: ->
      if confirm('стереть картинку?')
        @model.destroy(@options.unid)
        .success ->
          App.commands.execute 'updateImagePreview'
          App.commands.execute 'saveNewsItem'

    bindings:
      'input[name=caption]': 'caption'
      'input[name=copyright]': 'copyright'

    onRender: ->
      @stickit()

    serializeData: ->
      data = super
      return _.extend(data, {unid: @options.unid})


  # Набор строчек с картинкой внутри формы новости.
  class Views.Images extends Marionette.CollectionView
    initialize: (options) ->
      super options

      @on 'childview:sortable:drop', (view, model, position) ->
        @reorderCollection(model, position)

    childView: Views.Image
    childViewOptions: =>
      { unid: @options.unid }

    onShow: ->
      @$el.sortable
        containment: 'parent'
        tolerance: 'pointer'
        stop: (event, ui) ->
          ui.item.trigger 'drop', ui.item.index()

    # передвинуть модель на новую позицию внутри коллекции
    reorderCollection: (model, position) ->
      @collection.remove(model)
      @collection.add(model, {at: position})
      App.commands.execute 'updateImagePreview'
      App.commands.execute 'saveNewsItem'

    getModelImages: ->
      @collection.toJSON()


  # Чекбокс для тега внутри формы новости
  class Views.TagItem extends Marionette.ItemView
    template: '#news-tag-item-template'
    tagName: 'div'
    className: 'checkbox'

    events:
      'click input[type=checkbox]': (e) ->
        console.log('update selected!')
        if e.target.checked then @model.select() else @model.deselect()

    ui:
      checkbox: 'input[type=checkbox]'

    modelEvents:
      'selected': ->
        @ui.checkbox.prop 'checked', true
      'deselected': ->
        @ui.checkbox.prop 'checked', false


  # Набор чекбоксов для списка тегов внутри формы новости.
  class Views.TagItems extends Marionette.CollectionView
    childView: Views.TagItem

    updateFromModel: (modelTagList) ->
      # вызвать select() для каждого тега, который отмечен в модели, чтобы правильно инициализировать чекбоксы
      ids = _.map modelTagList, (x) ->
        x.id
      for r in @collection.models
        r.select() if r.id in ids

    getModelTag: ->
      # вернуть выбранные tag в формате, пригодном для сохранения модели новости
      r_selected = _.filter @collection.models, (x) ->
        x.selected
      _.map r_selected, (x) ->
        {id: x.id}


  # Виджет для превью картинки внутри формы новости.
  class Views.Preview extends Marionette.ItemView
    className: 'row form-group'
    template: '#image-preview'

    ui:
      img: '#preview-img'
      img_aim: '#preview-img-aim'

    onShow: ->
      @ui.img_aim.draggable
        containment: 'parent'
      @updateAimPosition()
      @stickit()

    events:
      'drag': (e, ui) ->
        @updateModelPosition ui.position

    # обновить координаты центрирования по текущим координатам красного кружочка
    updateModelPosition: (p) ->
      [preview_width, preview_height] = @getPreviewSize()
      @model.set 'cx', Math.round(p.left * 100 / preview_width)
      @model.set 'cy', Math.round(p.top * 100 / preview_height)

    # установить красный кружочек на основании данных модели
    updateAimPosition: ->
      if @model?
        [preview_width, preview_height] = @getPreviewSize()
        aim_x = Math.round(preview_width * @model.get('cx') / 100)
        aim_y = Math.round(preview_height * @model.get('cy') / 100)
        @ui.img_aim.css 'left', aim_x
        @ui.img_aim.css 'top', aim_y

    getPreviewSize: ->
      if @model?
        preview_width = 800
        preview_height = Math.round(preview_width / @model.get('width') * @model.get('height'))
        [preview_width, preview_height]
      else
        [0, 0]

    bindings:
      '#preview-img':
        observe: 'num'
        update: ($el, value) ->
          @updateImage(value)
      '#preview-cx': 'cx'
      '#preview-cy': 'cy'

    updateImage: (num) ->
      # при изменении номера картинки поменять картинку и ее размеры
      [preview_width, preview_height] = @getPreviewSize()
      cy = @model.get 'cy'
      cx = @model.get 'cx'
      url = App.newsImageUrl @model.get('unid'), num, preview_width, preview_height, cx, cy
      @ui.img.attr 'src', url
      @ui.img.attr 'width', preview_width
      @ui.img.attr 'height', preview_height


  # Форма редактирования.
  class Views.Edit extends Marionette.LayoutView

    @getCollectionNameByType: (type) ->
      switch type
        when 'banner' then 'banners'
        when 'news' then 'news'
        else type + 's'

    initialize: (opts) ->
      super opts

      @hasImages = @hasTags = @model.newsType isnt 'lines'

      # команда для обновления превьюшной картинки, может вызываться из дочерних вьюх
      App.commands.setHandler 'updateImagePreview', =>
        @updateImagePreview()

      # команда для сохранения новости, может вызываться из дочерних вьюх
      App.commands.setHandler 'saveNewsItem', =>
        @saveNewsItem()

      Backbone.Validation.bind @

      if @hasTags
        @addRegion 'tagItemsRegion', '#tag-items-region'
        @tag_list = opts.tag_list

      if @hasImages
        @addRegion 'previewRegion', '#preview-region'
        @addRegion 'imagesRegion', '#images-region'

      @sources = opts.sources
      console.log @sources

    serializeData: ->
      data = _.extend super, newsType: @model.newsType
      data.tag_list = @tag_list if @hasTags
      data.first_img_num = @first_img_num if @hasImages
      data

    getTitle: ->
      if @model.get('title')
        @model.get('title').replace('_', '')
      else
        'Untitled'

    initImageAttachments: ->
        # настраиваем дочернюю вьюху для картинок
        imagesView = new Views.Images
          collection: new App.News.Model.Images(@model.get 'images')
          unid: @model.get 'unid'

        @imagesRegion.show imagesView

        initImageUploader = =>
          # настраиваем fineuploader
          @ui.uploader.fineUploader
            template: 'fine-uploader-template'
            request:
              endpoint: '/ed/api/upload'
              params:
                unid: @model.get('unid')
            deleteFile:
              enabled: true,
              forceConfirm: true,
              endpoint: '/ed/api/upload'
          .on 'complete', (event, id, name, response) =>
            @onUploadComplete(response)

        unless @model.isNew()
          initImageUploader()
        else
          @imagesRegion.$el.hide()
          @.ui.secondarySaveButton.hide() if @model.newsType isnt 'news'

          @model.once 'save', =>
            @.ui.secondarySaveButton.show()
            initImageUploader()
            @imagesRegion.$el.show()

        # настраиваем дочернюю вьюху для превьюшки главной картинки
        App.commands.execute 'updateImagePreview'

    onShow: ->
      @ui.editorTextareas.ckeditor ->
        this.on 'change', (ev) =>
          $(ev.editor.element.$).trigger 'change'

      # не меняем мнемоник автоматом если документ не новый и мнемоник не соответствует заголовку
      @is_mnemonic_locked = @model.get('id')? and @ui.mnemonicIdField.val() != App.genMnemonicId(@ui.titleField.val())

      if @hasTags
        # настраиваем дочернюю вьюху для тегов
        tagView = new Views.TagItems(collection: @tag_list)
        @tagItemsRegion.show tagView
        tagView.updateFromModel @model.get('tags')

      if @hasImages
          @initImageAttachments()

      @fit_text = new App.Lib.FitText()

    template: '#edit-news-item-template'

    # для stickit
    bindings:
      '#news-item-mnemonic-id': 'mnemonic_id'
      '#news-item-title': 'title'
      '#news-item-is-published': 'is_published'
      '#news-item-in-extra-block1': 'in_extra_block1'
      '#news-item-is-on-morda': 'is_on_morda'
      '#news-item-is-gallery-from-hell': 'is_gallery_from_hell'
      '#news-item-date':
        observe: 'date'
        onGet: (v) ->
          moment(v).format 'DD.MM.YYYY hh:mm'

      '#news-item-annotation': 'annotation'
      '#news-item-body': 'body'
      '#news-item-text-link': 'text_link'
      '#news-item-text-link-caption': 'text_link_caption'
      '#news-item-video': 'video'
      '.checksize-title': 'title'
      '#news-item-link':
        observe: 'link'
        onSet: (val) -> val.trim()
      '#news-item-source-id':
        observe: 'source_id'
        selectOptions:
          collection: 'this.sources'
          labelPath: 'title'
          valuePath: 'id'
          defaultOption:
            value: null
            label: 'Выберите источник...'

    ui:
      editorTextareas: 'textarea.ckeditor'
      mnemonicIdField: '#news-item-mnemonic-id'
      titleField: '#news-item-title'
      uploader: '#fine-uploader'
      secondarySaveButton: '.news-item-save-secondary'

    onRender: ->
      @stickit()

    events:
      'click button.news-item-save': (e) ->
        e.preventDefault()
        @saveNewsItem()

      'click button.news-add-more': (e) ->
        e.preventDefault()
        @model.dirty
        App.navigate "#{@model.newsType}/new"

      'click button.auction-stats-button': ->
        App.navigate("#{@model.newsType}/auction_stats/#{@model.id}")

      'input #news-item-mnemonic-id': ->
        # если мнемоник отредактирован, больше его не обновляем автоматом
        @is_mnemonic_locked = true

      'input #news-item-title': ->
        unless @is_mnemonic_locked
          @ui.mnemonicIdField.val App.genMnemonicId(@ui.titleField.val())


    updateImagePreview: ->
      images = @imagesRegion.currentView.collection
      if images.length > 0
        m = images.at(0).clone()
        m.set 'unid', @model.get('unid')
        v = new Views.Preview
          model: m
        @previewRegion.show(v)
      else
        @previewRegion.empty()

    # синхронизировать модель с полями, не завязанными с моделью напрямую через stickit
    syncModel: ->
      @model.unset 'video' if @model.newsType isnt 'news'
      @model.unset 'search_fields'
      @model.unset 'source'

      if @hasTags
        # синхронизировать tag-источники из дочерней вьюхи
        @model.set 'tags', @tagItemsRegion.currentView.getModelTag()

      if @hasImages
        # синхронизировать картинки из дочерней вьюхи
        images = @imagesRegion.currentView.getModelImages()
        @model.set 'images', images
        @model.set 'is_gallery', images.length > 1

        if images.length > 0 and @previewRegion.currentView?
          m = @previewRegion.currentView.model
          images[0].cx = m.get 'cx'
          images[0].cy = m.get 'cy'

      font_sizes = @fit_text.calculate_for @model.get('title'), @model.get('annotation')
      @model.set 'font_sizes', font_sizes
      @model.set 'new_font_sizes', true

    saveNewsItem: ->
      @syncModel()
      v = @model.validate()
      if @model.isValid()
        @model.save().success (model) ->
          App.notify 'Сохранилось!'
          collectionName = Views.Edit.getCollectionNameByType(model.type)
          App.navigate "#{collectionName}/#{model.id}"
        .fail ->
          alert 'ошибка при сохранении'
      else
        alert _.values(v).join("\n")

    onUploadComplete: (response) ->
      # добавить новую картинку к дочерней вьюхе картинок
      @imagesRegion.currentView.collection.add(response.img)
      App.commands.execute 'updateImagePreview'
      App.commands.execute 'saveNewsItem'


  # Строчка новости в списке.
  class Views.Item extends Marionette.ItemView
    tagName: 'tr'
    template: '#news-item-template'

    modelEvents:
      # это чтобы колбочка дистиллята менялась после успешного сохранения модели
      'sync': 'render'

    serializeData: ->
      data = super
      return _.extend data,
        newsType: @model.newsType
        idx: @options.collIndex + 1

    bindings:
      'input.mark': 'is_marked'

    events:
      'click img.distilled-switch': 'toggleDistilled'
      'click img.published-switch': 'togglePublished'
      'test': 'toggleSelect'
      'click a.news-item-title': (e) ->
        e.preventDefault()
        App.navigate "#{@model.newsType}/#{@model.id}.html"
      'click button.auction-stats-button': ->
        App.navigate("#{@model.newsType}/auction_stats/#{@model.id}")
      'click banner-source-link': ->
        App.navigate("sources/#{@model.source_id}")
      'mouseover': ->
        @$('.onmouseover').removeClass('hidden')
      'mouseout': ->
        @$('.onmouseover').addClass('hidden')
      'click input.mark': ->
        console.log('item event catched!')
    triggers: 
        'click input.mark': 
            event: 'do:uncheck'
            preventDefault: false

    onRender: ->
      @stickit()

    # переключить и сохранить булеву переменную
    toggleAndSave: (attr) ->
      o = {}
      o[attr] = not @model.get(attr)
      @model.save o, {patch: true}

    toggleDistilled: ->
      @toggleAndSave 'is_distilled'

    togglePublished: ->
      @toggleAndSave 'is_published'


  # Вьюха списка новостей.
  class Views.List extends App.withSorting(Marionette.CompositeView)
    template: '#news-list-template'
    sortFieldSelector: 'th.sort'
    childView: Views.Item
    childViewContainer: 'tbody'
    childViewOptions: (m) ->
      collIndex: m.getIndex()


    # добавим newsType в данные, которые передаются в шаблон
    serializeData: ->
      data = super
      return _.extend data, {
        newsType: @collection.model.prototype.newsType
        sourceFilterValue: @options.get_params.source
        tagFilterValue: @options.get_params.tag
      }

    events:
    # переключение новости/галереи/видосы
      'click #type-switch a': 'selectTypeSwitch'
      'click .add-new-item': ->
        t = @collection.model.prototype.newsType
        App.navigate "#{t}/new"
      'click .delete-selected': ->
        App.confirmAndDelete @collection
      'submit #search-form': 'onSearch'
      'click #disable-source-filter-button': ->
        @navigate _.extend(@options.get_params, source: null)
      'click #disable-tag-filter-button': ->
        @navigate _.extend(@options.get_params, tag: null)
      'click #select-all': (e) ->
        for model in @collection.models
            model.set({'is_marked':  e.target.checked})

    ui:
      typeSwitchItems: '#type-switch li'
      searchTermItem:'#search'
      selectAllCheckBox:'#select-all'
      disableFilterButtonSourceTitle: '#disable-filter-source-title'
      disableFilterButtonTagTitle: '#disable-filter-tag-title'

    currentTargetFilter: 'type-switch-all'

#    childEvents:
#        'childview:click': ->
#             console.log('child event catched!')

    navigate: (options) ->
      params = _.extend(_.clone(@options.get_params), options)
      query_string = (key + '=' + value for key, value of params when value).join('&')
      App.navigate "#{@collection.model.prototype.newsType}?" + query_string

    selectTypeSwitch: (e) ->
      e.preventDefault()
      type = e.currentTarget.id.replace('type-switch-', '')
      @navigate type: type
    
    onChildviewDoUncheck: (e) ->
      @ui.selectAllCheckBox.prop 'checked', false


    onSearch: (e) ->
      e.preventDefault()
      @navigate search: $(@ui.searchTermItem).val()

    onShow: ->
      _.each @ui.typeSwitchItems, (x) =>
        $(x).toggleClass 'active', 'type-switch-' + @options.get_params.type == x.children[0].id
      $(@ui.searchTermItem).val(@options.get_params.search)

      if @options.get_params.source
        source = new App.Sources.Model.Item(id: @options.get_params.source)
        source.fetch().done =>
          button_caption = @$(@ui.disableFilterButtonSourceTitle)
          button_caption.html source.get('title').toLowerCase()

      if @options.get_params.tag
        tag = new App.Tags.Model.Item id: @options.get_params.tag
        tag.fetch().done =>
          button_caption = @$(@ui.disableFilterButtonTagTitle)
          button_caption.html tag.get('title').toLowerCase()

      paginator = new Backgrid.Extension.Paginator
        collection: @collection
      $('#news-list-paginator').append(paginator.render().$el)

    typeFilters:
      'news': [
          {name: 'is_gallery', 'op': '==', val: false}
          {name: 'video', op: '==', val: ''}
      ]
      'gallery': [
          {name: 'is_gallery', op: '==', val: true}
      ]
      'video': [
          {name: 'video', op: '!=', val: ''}
      ]
      'all': []

    getFilters: (get_params) ->
      filters = []

      filters.push.apply(filters, @typeFilters[get_params.type or 'all'])

      if get_params.source
        filters.push
          name: 'source_id'
          op: '=='
          val: get_params.source

      filters

    applySorting: (field, order) ->
      query = JSON.parse @collection.queryParams.q
      query.order_by = [
        field: if field == 'ctr' then 'ctrsort' else field
        direction: if order == 1 then 'asc' else 'desc']
      @collection.queryParams.q = JSON.stringify(query)
      @collection.fetch().then =>
        @render()
        @onShow()
      super field, order

  # Строчка новости на морде.
  class Views.MordaItem extends Marionette.ItemView
    tagName: 'tr'
    className: ->
      # раскрасить первые 2000 строчек
      if @options.collIndex < 200 then 'warning' else null
    template: '#morda-item-template'

    ui:
      num_edit: 'a.morda-num-edit'

    bindings:
      'input.mark': 'is_marked'

    events:
      'click a.news-item-title': (e) ->
        e.preventDefault()
        App.navigate "#{@model.newsType}/#{@model.id}.html"

    serializeData: ->
      data = super
      return _.extend data,
        newsType: @model.newsType
        idx: @options.collIndex + 1

    onRender: ->
      @stickit()

    onShow: ->
      @ui.num_edit.editable
        mode: 'inline'
        showbuttons: false
        send: 'never'
        type: 'text'
        validate: (v) ->
          if isNaN(v)
            return 'нужен номер'
        success: (_, newValue) =>
          @model.save({morda_num: newValue}, {patch: true}).then =>
            @trigger 'morda_num:updated'


  # Список новостей на морде.
  class Views.MordaList extends Marionette.CompositeView
    template: '#morda-list-template'
    childView: Views.MordaItem
    childViewContainer: 'tbody'

    childViewOptions: (m) ->
      collIndex: @collection.indexOf m

    childEvents:
      'morda_num:updated': (v) ->
        c = v.model.collection
        c.fetch()

    events:
      'click .delete-selected': ->
        App.confirmAndDelete @collection


### Навигация ###

App.module 'News', (News, App) ->
  class News.NavController extends Marionette.Controller
    initialize: (opts) ->
      {@newsType} = opts

      @modelClass = switch @newsType
        when 'news', 'morda' then News.Model.NewsItem
        when 'banners', 'distilled' then News.Model.Banner
        else
          News.Model.Line

      @collectionClass = switch @newsType
        when 'news' then News.Model.NewsItems
        when 'morda' then News.Model.MordaItems
        when 'banners', 'distilled' then News.Model.Banners
        else
          News.Model.Lines

    getSearchFilter: (search) ->
      urlRegEx = /.*\/(\d+)-.*html/
      if search.match urlRegEx
        name: 'id'
        op: 'eq'
        val: search.match(urlRegEx)[1]
      else
        name: 'search_fields'
        op: 'like',
        val: '%' + search + '%'

    list: (get_params={}) ->
      viewClass = if @newsType is 'morda' then News.Views.MordaList else News.Views.List

      opts =
        order_by: [field: 'is_published', direction: 'desc']
        filters: []

      opts.order_by.push
        field: 'date'
        direction: 'desc'

      if @newsType == 'distilled'
        opts.filters.push
          name: 'is_distilled'
          op: '=='
          val: true

      if get_params.search
        opts.filters.push @getSearchFilter(get_params.search)

      if get_params.tag
        opts.filters.push
          name: 'tags__id'
          op: 'any'
          val: get_params.tag

      # Добавить фильтры элементов из вьюхи, если есть
      if viewClass.prototype.getFilters?
        new_filters = viewClass.prototype.getFilters(get_params)
        for filter in new_filters
          opts.filters.push filter

      c = new @collectionClass [],
        queryParams:
          q: JSON.stringify(opts)

      page = parseInt(get_params.page) or 1

      c.getPage(page).done ->
        view = new viewClass
          collection: c
          get_params: get_params
        App.mainRegion.show(view)
      .fail ->
        App.showErrorView()

    edit: (id) ->
      tag_list = new App.Tags.Model.Items()
      sources = new App.Sources.Model.Items()
      $.when(tag_list.fetch(), sources.fetch()).then =>
        if id == 'new'
          @_editModel new @modelClass(), tag_list, sources
        else
          model = new @modelClass {id: id}
          model.fetch().done =>
            @_editModel model, tag_list, sources
          .fail ->
            App.showErrorView()
      .fail ->
        App.showErrorView()

    _editModel: (model, tag_list, sources) ->
      view = new News.Views.Edit
        model: model,
        tag_list: tag_list
        sources: sources
      App.mainRegion.show view


  # Наследуется от App.Router, чтобы отслеживать активный раздел в меню.
  class News.Router extends App.Router
    constructor: (opts) ->
      super opts

      {newsType} = opts

      @controller = new News.NavController {newsType: newsType}

      # программно задекларировать маршруты
      @appRoute "#{newsType}", "list"
      @appRoute "#{newsType}/?*get_params", "list"
      @appRoute "#{newsType}/:id", 'edit'

  News.addInitializer ->
    # генерируем модели+вьюшки для всех типов новостных элементов
    _.each ['morda', 'news', 'banners', 'lines', 'distilled'], (n) ->
      new News.Router({newsType: n})
