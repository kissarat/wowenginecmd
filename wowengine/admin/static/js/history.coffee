App.module 'History', (History, App) ->
  class History.Prices extends Backbone.Collection
    model: Backbone.Model

  class History.PriceView extends Marionette.ItemView
    template: '#history-price-template'
    tagName: 'tr'
    onRender: ()->
      if new Date(@model.id).getDate() % 2
        @$('.time').css('color', 'gray')

  class History.PricesListView extends App.withSorting(Marionette.CompositeView)
    model: new Backbone.Model()
    className: 'prices-list'
    template: '#history-prices-list-template'
    childView: History.PriceView
    childViewContainer: 'tbody'
    childViewOptions: (m) ->
      collIndex: @collection.indexOf m
      
    def:
      interval: '30min'
      period: '30min'
      disabled: []

    initialize: () ->
      @model.set 'interval', @def.interval
      @model.set 'period', @def.period
      @model.set 'limit', 1000
      return

    ui:
      source: '.source'
      period: '.period'
      periodBtn: '.period > .btn'
      periodRange: '.period-range'
      fixedPeriod: '.fixed'
      start: '.start'
      end: '.end'
      interval: '.interval'
      intervalBtn: '.interval.btn'
      more: '.more'
      alert: '.alert-box'

    events:
      'change .source': 'changeSource'
      'click .fixed': 'changePeriod'
      'click .period-range .apply': 'changePeriod'
      'click .period .fa-calendar': 'showDateRange'
      'click .interval': 'changeInterval'
      'click .more': 'more'

    sortFieldSelector: 'th.sort'

    changeSource: () ->
      @model.set 'source_id', if @ui.source.val() then parseFloat(@ui.source.val()) else null
      @load()

    changePeriod: (e) ->
      e.preventDefault()
      $target = $(e.target)
      if not $target.hasClass 'disabled' then (
        @ui.periodRange.addClass('hidden')
        if $target.hasClass('apply')
          @model.set 'start', @ui.start.val()
          @model.set 'end', @ui.end.val()
          @model.set 'period', 'range'
        else
          @model.set 'period', $target.data('value')
        @load()
      )

    more: () ->
      console.error 'unimplemented'

    changeInterval: (e) ->
      e.preventDefault()
      $target = $(e.target)
      if not $target.hasClass 'disabled' then (
        @model.set 'interval', $target.data('value')
        @load()
      )

    applySorting: (field, order) ->
      field = 'id' if 'time' is field
      @collection.comparator = (x) => 
        order * +x.get(field)
      @collection.sort()
      super field, order

    load: () ->
      App.loader on
      $(@ui.alert).empty()
      @collection.reset()
      
      params =
        #TODO: нужно сделать нормальное получение провайдера из селекта, а пока хардкод провайдера закомментим
        #provider: 1
        interval: @getInterval()
        period: @model.get('period') or @def.period
      if @model.get('source_id')
        params.source = @model.get('source_id')
      if @model.get('period') == 'range'
        params.range_start = @model.get('start')
        params.range_end = @model.get('end')

      @request.abort() if @request
      @request = $.getJSON "/ed/api/stats/traffic-price?#{$.param params}"
      .done (result) =>
        if result.data? then (
          for record in result.data
            time = moment(record.range_end, 'M/D/H:m')
            
            #нормализируем числа в нужный формат и берем абсолютное значение числа
            in_clicks = App.floatFormat(record.data.in_clicks, 0, true)
            in_price = App.floatFormat(record.data.in_price, 2, true)
            out_clicks = App.floatFormat(record.data.out_clicks, 0, true)
            out_price = App.floatFormat(record.data.out_price, 2, true)
            
            model = new Backbone.Model
            #model.set 'id', time.unix()
            model.set 'time', time.format('DD.MM HH:mm')
            model.set 'input', in_clicks
            model.set 'ask', in_price
            model.set 'output', out_clicks
            model.set 'bid', out_price
            model.set 'price_balance', App.floatFormat(out_price - in_price)
            model.set 'traffic_balance', (out_clicks - in_clicks)
            @collection.add(model)
          @render()
        )
        App.loader off
        return
      .fail (e) =>
        text = """
                Произошел сбой, не удалось получить данные.<br>
                Код ошибки: #{e.status} #{e.statusText}
               """
        App.alert @ui.alert, text, 'warning'
        @render()
        App.loader off
        return
      
    onBeforeRender: =>
      totals = {}
      for i in ['input', 'ask', 'output', 'bid', 'traffic_balance', 'price_balance']
        value = 0
        @collection.each (m) ->
          value += parseFloat(m.get(i))
        totals[i] = App.floatFormat(value, (if i in ['input', 'output', 'traffic_balance'] then 0 else 2))
      @model.set 'totals', totals

    onRender: ->
      @toggleBtn()
      @renderDateRange()

    toggleBtn: ->
      @disabledIntervals()
      
      _.each @ui.intervalBtn, (x) =>
        interval = @model.get('interval') or @def.interval
        status = (interval is $(x).attr('data-value'))
        $(x).toggleClass 'btn-primary', status
        return
        
      _.each @ui.periodBtn, (x) =>
        period = @model.get('period') or @def.period
        status = (period is $(x).attr('data-value'))
        $(x).toggleClass 'btn-primary', status
        return

    disabledIntervals: ->
      _.each @ui.intervalBtn, (x) =>
        value = $(x).attr('data-value')
        status = (value in @def.disabled)
        $(x).toggleClass 'disabled', status
        return
        
    getInterval: ->
      interval = @model.get('interval') or @def.interval
      period = @model.get('period') or @def.period
      
      ### TODO: интревалы пока не работают нормально
      if period in ['range', 'month'] 
        @def.disabled = ['5min', '10min', '15min', '60min', 'month']
        newInterval = 'day'
      else if period is 'all_time'
        @def.disabled = ['5min', '10min', '15min', '60min'] 
        newInterval = 'day'
      else if period in ['today', 'yesterday']
        @def.disabled = ['5min', '10min', 'day', 'month'] 
        newInterval = '60min'
      else if period in ['15min', '30min', '60min']
        @def.disabled = ['60min', 'day', 'month'] 
        newInterval = '15min'
      else 
        @def.disabled = [] 
        newInterval = @def.interval
        
      if interval in @def.disabled
        @model.set 'interval', newInterval
        interval = newInterval
      ###
      
      #TODO: временная доработка: блокируем переключение интервалов и выставляем интервал 'day' для всех более 60min
      @def.disabled = ['5min', '10min', '15min', '30min', '60min', 'day', 'month', 'month'] 
      if period in ['15min', '30min', '60min']
        interval = period
      else 
        interval = 'day'
      #разблокировака текущего выбранного
      delete @def.disabled[@def.disabled.indexOf(interval)]
      #TODO: конец кода с временной доработкой
      
      @model.set 'interval', interval

      return interval

    renderDateRange: ->
      @ui.start.val @model.get('start')
      @ui.end.val @model.get('end')
      @ui.periodRange.datepicker
        orientation: 'top'
        format: 'dd.mm.yyyy'
        language: "ru"
      if @model.get('period') == 'range'
        @ui.periodRange.removeClass('hidden')
      else
        @ui.periodRange.addClass('hidden')

    showDateRange: (e) ->
      e.preventDefault()
      @ui.periodRange.toggleClass('hidden')

  class History.Router extends App.Router
    controller:
      prices: (source_id) ->
        $.getJSON "/ed/api/source_titles", (sources) ->
          listView = new History.PricesListView
            collection: App.FilteredCollection new History.Prices()
          listView.model.set 'sources', sources.data
          if App.global.source_id? then (
            listView.model.set 'source_id', App.global.source_id
            delete App.global.source_id
          )
          App.mainRegion.show listView
          listView.load()

    appRoutes:
      'history/prices': 'prices'

  History.addInitializer ->
    History.router = new History.Router()
