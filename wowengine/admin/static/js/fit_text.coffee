App.module 'Lib', (Lib) ->
  class Lib.FitText
    # max title size for each cell width
    CELL_WIDTHS:
      1: 72
      12: 60
      13: 45
      14: 42
      23: 63
      34: 63

    # all title sizes to check
    TITLE_SIZES: [20, 24, 27, 30, 33, 36, 39, 42, 45, 48, 51, 54, 57, 60, 63, 66, 69, 72]

    template: '
      <div id="fit-text-block" class="v2newsBlock">
        <div id="fit-text-cell" class="v2cell">
            <div id="fit-text-cell-inner" class="cellInner">
                <div id="fit-text-title" class="title hyphenate">
                    <a id="fit-text-title-a" href="#"></a>
                </div>
                <div class="annotation">
                    <span id="fit-text-annotation"></span>
                    <div class="more"><a href="#">Подробнее&hellip;</a>
                    </div>
                </div>
            </div>
        </div>
      '

    reloadFittingBlock: ->
      # почему-то нужно пересоздавать для каждого измерения заново, иначе плохо работает

      # construct an invisible div and save some selectors for later
      $('#fit-text-block').remove()
      @container_div = $(@template)
      $('body').append(@container_div.hide())
      @$fit_cell = $('#fit-text-cell')
      @$fit_cell_inner = $('#fit-text-cell-inner')
      @$fit_title = $('#fit-text-title')
      @$fit_title_a = $('#fit-text-title-a')
      @$fit_annotation = $('#fit-text-annotation')

    calculate_for: (title, annotation) ->

      @reloadFittingBlock()

      # must show the div for calculations

      @container_div.show()

      # fill it in with content
      @$fit_title_a.html(title)
      @$fit_title_a.hyphenate 'ru'
      @$fit_annotation.html(annotation)

      prev_wc = null
      prev_tc = null
      sizes = {}  # the result

      # cycle through all widths and sizes
      for cell_width, max_title_size of @CELL_WIDTHS
        for [wi, wc] in [["w#{cell_width}", "w#{cell_width}"], ["wc#{cell_width}", "w#{cell_width} colorbg"]]

          # change cell's width class
          @$fit_cell.removeClass prev_wc
          @$fit_cell.addClass wc

          w_sizes = {}  # results for current width

          for title_size in (x for x in @TITLE_SIZES when x <= max_title_size)
            tc = "s#{title_size}"

            # change title's size class
            @$fit_title.removeClass prev_tc
            @$fit_title.addClass tc

            th = @$fit_title.height()
            bh = @$fit_cell.height()
            l = Math.floor(th / (title_size * 1.01))

            w_sizes[tc] = {
              l: l
              bh: bh
              th: th
            }

            prev_tc = tc

          sizes[wi] = w_sizes
          prev_wc = wc

      # hide it back to prevent clutter
      @container_div.hide()

      sizes
