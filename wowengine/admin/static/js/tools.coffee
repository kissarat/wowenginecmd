# --- Калибровка размеров заголовков

App.module 'Tools.FitText', (FitText, App) ->

  # пересчет размеров заголовков
  # берет постранично документы из /ed/api/items_for_sizing_fonts
  # пересчитывает все размеры для каждого внутри браузера
  # и записывает обратно в базу
  class FitText.Item extends Backbone.Model
    urlRoot: "/ed/api/items_for_sizing_fonts"


  class FitText.Collection extends App.Models.RestlessCollection
    model: FitText.Item
    url: "/ed/api/items_for_sizing_fonts"


  class FitText.View extends Marionette.ItemView
    initialize: ->
      @collection = new FitText.Collection()

      @on 'process-collection', =>
        @processCollection()

    template: '#tools-fit-text-template'

    ui:
      progress_div: '#progress'
      progress_text: '#progress-text'
      progress_bar: '#progress div.progress-bar'
      recalculate_button: '#recalculate-font-sizes'
      recalculate_all_checkbox: '#recalculate-all'

    onShow: ->
      @fit_text = new App.Lib.FitText()
      @ui.progress_div.hide()

    enableUI: (b) ->
      if b
        @ui.recalculate_button.removeAttr 'disabled'
        @ui.recalculate_all_checkbox.removeAttr 'disabled'
      else
        @ui.recalculate_button.attr 'disabled', 'disabled'
        @ui.recalculate_all_checkbox.attr 'disabled', 'disabled'

    events:
      'click #recalculate-font-sizes': ->
        @ui.progress_div.show()
        @enableUI false

        # если чекбокс не включен, отфильтровать только необработанные
        @recalc_all = @ui.recalculate_all_checkbox.prop('checked')
        @coll_filters = if @recalc_all
          filters: []
        else
          filters: [
            {name: 'new_font_sizes', op: '==', val: false}
          ]

        console.time('recalculating font sizes')
        @recalculateSizes()

    updateProgress: ->
      @ui.progress_text.html "#{@processed_items} / #{@total_items}"
      percent = Math.round(100 / @total_items * @processed_items)
      @ui.progress_bar.attr('aria-valuenow', percent).css('width': "#{percent}%")

    recalculateSizes: ->
      @collection.fetch({data: {q: JSON.stringify(@coll_filters)}}).done =>
        @total_items = @collection.total_results
        @processed_items = 0
        @updateProgress()
        @trigger 'process-collection'

    processCollection: ->
      # process...

      # gather deferreds with PATCH requests
      patches = @collection.map (m) =>
        font_sizes = @fit_text.calculate_for m.get('title'), m.get('annotation')
        m.save {font_sizes: font_sizes, new_font_sizes: true}, {patch: true}

      # wait until all deferreds complete
      $.when.apply($, patches).then =>
        # update progress bar and trigger the next batch
        @processed_items += @collection.length
        @updateProgress()
        if @processed_items < @total_items
          # если мы обрабатываем только необработанные, то листать страницы не нужно
          # (обработанные сами исчезают из списка)
          new_page = if @recalc_all then @collection.page + 1 else 1
          @collection.fetch({data: {q: JSON.stringify(@coll_filters), page: new_page}}).done =>
            @trigger 'process-collection'
        else
          console.timeEnd('recalculating font sizes')
          @enableUI true


  class FitText.Router extends App.Router
    controller:
      index: ->
        App.mainRegion.show(new FitText.View())

    appRoutes:
      'tools/recalculate_title_sizes': 'index'


  FitText.addInitializer ->
    FitText.router = new FitText.Router()


# -- Настройки
App.module 'Tools.Config', (Config, App) ->

  class Config.Model extends Backbone.Model
    url: '/ed/api/config'

  class Config.View extends Marionette.ItemView
    template: '#tools-config-template'

    ui:
      var_edit: 'a.config-var-edit'
      var_select: 'input.config-var-edit'
      reset_succeed: 'p.reset-succeed'


    events:
      'click button.reset-auctions-btn': 'resetAuctions'
      'click input[type="radio"]': 'changeRule'

    bindings:
      'input.exclude-rule-selector': 'banner_excluding_rule'
      'input.banner-sorting-rule-selector': 'banner_sorting_rule'
      'input.auction-algorithm-selector': 'auction_algorithm'

    # создать элемент для редактирования на месте
    makeEditable: (el) ->
      $el = $(el)
      var_name = $el.attr('data-var-name')
      $el.editable
        mode: 'inline'
        showbuttons: false
        send: 'never'
        type: 'text'
        validate: (v) ->
          if isNaN(v)
            return 'нужен номер'
        success: (_, newValue) =>
          payload = {}
          payload[var_name] = newValue
          @model.save(payload)
     
    changeRule: (e) ->
       $el = $(e.currentTarget)
       var_name = $el.attr('data-var-name')
       payload = {}
       console.log($el)
       payload[var_name] = $el.val()
       @model.save(payload)

    resetAuctions: (e) ->
      if confirm "Вы уверены что хотите перезапустить все аукционы?"
        $el = $(e.currentTarget)
        $el.prop 'disabled', true

        Backbone.ajax
          url: "/ed/api/banners/reset_all_auctions/"
          type: 'POST'
        .success =>
          $el.prop 'disabled', false
          window.r = @ui.reset_succeed
          @ui.reset_succeed.show()

    onRender: ->
      _.each @ui.var_edit, (x) =>
        @makeEditable x
      @stickit()
      @ui.reset_succeed.hide()

    onShow: ->
      console.log(@model.get('banner_excluding_rule'))
      console.log(@model.get('banner_sorting_rule'))
      console.log(@model.get('auction_algorithm'))
      $('input[value="'+@model.get('banner_sorting_rule')+'"]').prop('checked', true)
      $("input[value='"+@model.get('banner_excluding_rule')+"']").prop('checked', true)
      $("input[value='"+@model.get('auction_algorithm')+"']").prop('checked', true)


  class Config.Router extends App.Router
    controller:
      index: ->
        cfg = new Config.Model()
        cfg.fetch().then ->
          App.mainRegion.show(new Config.View({model: cfg }))

    appRoutes:
      'tools/config': 'index'

  Config.addInitializer ->
    Config.router = new Config.Router()


# --- Генерация

App.module 'Tools.Generation', (Generation, App) ->
  class Generation.GenerationModel extends Backbone.Model
    url: '/ed/api/generation'

    _getRequestData: (date, withSourceBanner) ->
      data = {}
      if not _.isNull(date)
        data.date = date
      if withSourceBanner
        data.with_source_banner = withSourceBanner
      data

    forDate: (date=null, withSourceBanner=false) ->
      @fetch {data: @_getRequestData date, withSourceBanner}

    getSeria: (name) ->
      [seria, ...] = _.filter @get('data').series_today, (x) => x.name == name
      seria

    seriaToList: (name) ->
      seria = @getSeria name
      _.map seria.data, ([..., val]) -> val

    dateFromSeria: (name) ->
      seria = @getSeria name
      _.map seria.data, ([date, ...]) -> moment(date).format 'HH:mm'

    prepareForTable: ->
      lists = [@dateFromSeria('in'),
               @seriaToList('in'),
               @seriaToList('pre_in'),
               @seriaToList('ext_in'),
               @seriaToList('overall_price'),
               @seriaToList('out'),
               @seriaToList('gen'),
               @get('data').active_banners,
               @get('data').active_sources]
      _.chain(_.zip.apply null, lists)
      .filter ([..., sources]) -> not _.isUndefined sources
      .groupBy ([date, ...]) -> date.split(':')[0]
      .pairs()
      .sortBy ([hours, ...]) -> parseInt hours, 10
      .map ([hours, data]) ->
        sumOf = (i) -> _.reduce data, ((memo, val) -> memo + val[i]), 0
        hours: hours
        in_: sumOf 1
        pre_in_: sumOf 2 
        ext_in_: sumOf 3
        overall_price_: sumOf 4
        out: sumOf 5 
        gen: if data.length then sumOf(6) / data.length else "-"
        byMinutes: data
      .value()

    forTable: (args...) ->
      result = @forDate args...
      result.then =>
        @set 'seriesList', @prepareForTable()
      result

  class Generation.GraphView extends Marionette.CompositeView
    template: '#tools-generation-graph-template'
    #childView: Generation.ItemView
    #childViewContainer: 'tbody'

    ui:
      chart_today: 'div#chart-today'
      chart_total: 'div#chart-total'

    onShow: ->
      Highcharts.setOptions
        global:
          useUTC: false

      @updateCharts()

    updateCharts: (date = null) ->
      (new Generation.GenerationModel).forDate(date).done (gen_stats) =>
        @ui.chart_today.highcharts
          chart:
            type: 'line'
          title:
            text: "За #{gen_stats.data.date}"
          xAxis:
            type: 'datetime'
          yAxis: [
            {
              type: 'linear'
              min: 0
              title:
                text: 'Переходы'
            }
            {
              type: 'linear'
              min: 0
              title:
                text: 'Генерация'
              opposite: true
            }
          ]
          series: gen_stats.data.series_today
          credits:
            enabled: false

        if date is null
          @ui.chart_total.highcharts
            chart:
              type: 'line'
            title:
              text: "За все время"
            xAxis:
              type: 'datetime'
            yAxis: [
              {
                type: 'linear'
                min: 0
                title:
                  text: 'Переходы'
              }
              {
                type: 'linear'
                min: 0
                title:
                  text: 'Генерация'
                opposite: true
              }
            ]
            series: gen_stats.data.series_total
            credits:
              enabled: false
            plotOptions:
              series:
                events:
                  click: (e) =>
                    ts = e.point.x
                    @updateCharts(moment(ts).format('YYYY-MM-DD'))

  class Generation.TableContentView extends Marionette.ItemView
    template: '#tools-generation-table-content-template'

    onShow: ->
      @$el.find('[data-toggle="tooltip"]').tooltip()
      @$el.find('.tools-generation-table-collapser').collapse()

  class Generation.TableView extends Marionette.LayoutView
    template: '#tools-generation-table-template'
    ui:
      date: '.tools-generation-date'
    regions:
      contentRegion: '.tools-generation-table-content'

    onShow: ->
      $(@ui.date).data('date', moment().format('YYYY-MM-DD'))
      .datepicker
          format: 'yyyy-mm-dd'
      .on('changeDate', @onChangeDate)
      .find('>div')
      .attr('style', 'display: block !important')
      @onChangeDate()

    onChangeDate: =>
      date = $(@ui.date).datepicker 'getFormattedDate'
      model = new Generation.GenerationModel
      model.forTable(date, true).then =>
        @contentRegion.show new Generation.TableContentView
          model: model

  class Generation.GenerationView extends Marionette.LayoutView
    template: '#tools-generation-template'
    regions:
      contentRegion: ".tools-generation-content"
    ui:
      menu: ".nav li"
    events:
      "click .nav li a": "switchContent"
    contents:
      graph: Generation.GraphView
      table: Generation.TableView

    onShow: ->
      @contentRegion.show new @contents.graph

    switchContent: (e) ->
      e.preventDefault()
      @ui.menu.removeClass 'active'
      $el = $(e.currentTarget)
      @contentRegion.show new @contents[$el.data 'view']
      $el.parent().addClass 'active'

  class Generation.Router extends App.Router
    controller:
      index: ->
        App.mainRegion.show new Generation.GenerationView()

    appRoutes:
      'tools/generation': 'index'


  Generation.addInitializer ->
    Generation.router = new Generation.Router()


App.module 'Tools.AuctionStats', (AuctionStats, App) ->

  class AuctionStats.BannerStats extends Backbone.Model
    urlRoot: '/ed/api/auction_stats/banner'

    initialize: ->
      @on 'change:title', @updateCtr

    updateCtr: =>
      @set 'queue', _.map @get('queue'), (data) ->
        if data.shows
          data.ctr = 100.0 * data.clicks / data.shows
        data

  class AuctionStats.NewsStats extends Backbone.Model
    urlRoot: '/ed/api/auction_stats/news'

  class AuctionStats.OverallStats extends Backbone.Model
    urlRoot: '/ed/api/auction_stats/overall'

  class AuctionStats.ConfigView extends Marionette.ItemView
    template: '#tools-auction-config'

    ui:
      var_edit: '.config-var-edit'

    bindings:
      'input.auction-algorithm-selector': 'auction_algorithm'

    events:
      'click input[type="radio"]': 'changerRadioSetting'

    initialize: (options) ->
      super options
      @defaultsModel = options.defaultsModel
      @isBanner = @model.get('type') is 'banner'

    saveModel: (data) ->
      @model.unset 'video' if @isBanner
      @model.unset 'search_fields'
      @model.unset 'source'
      result = @model.save data, patch: true
      if result is false
        window.alert 'Не удалось сохранить настройки. Проверьте введенные значения'
      else
        result.fail ->
          window.alert 'Не удалось сохранить настройки ввиду непредвиденной ошибки'

    changerRadioSetting: (e) ->
       $el = $(e.currentTarget)
       var_name = $el.attr('data-var-name')
       payload = {}
       console.log($el)
       payload[var_name] = $el.val() or null
       @saveModel payload

    onRender: ->
      for field in [
          'auction_algorithm',
          'auction_banner_age_days',
          'auction_shows_count',
          'auction_clicks_count',
          'auction_max_age_days',
          'fast_auction_ctr_threshold',
      ]
        @addBinding @model, ".config-var-edit[data-var-name='#{field}']",
          observe: field
          updateModel: false

    makeEditable: (el) ->
      $el = $(el)
      var_name = $el.attr('data-var-name')
      $el.editable
        mode: 'inline'
        showbuttons: false
        send: 'never'
        type: 'text'
        emptytext: 'по умолчанию: ' + @defaultsModel.get(var_name)
        validate: (v) ->
          if v != '' and not $.isNumeric(v)
            'нужно число'
        success: (_, newValue) =>
          if newValue == ''
            newValue = null

          @saveModel("#{var_name}": newValue)
            .done ->
              App.notify 'Изменения сохранены', className: 'success'
        error: =>
          window.alert "Не удалось сохранить значение #{var_name}"

    onShow: ->
      _.each @ui.var_edit, (x) =>
        @makeEditable x
      $("input[value='"+ ( @model.get('auction_algorithm') or '')+"']").prop('checked', true)

  class AuctionStats.QueueView extends App.withSorting(Marionette.ItemView)
    sortFieldSelector: 'th.sort'
    template: '#tools-auction-queue'

    applySorting: (field, order) ->
      @model.set 'queue', _.sortBy @model.get('queue'), (data) =>
        @orderField data[field], order
      @render()
      super field, order

  class AuctionStats.ResultView extends App.withSorting(Marionette.ItemView)
    sortFieldSelector: 'th.sort'
    template: '#tools-auction-result'

    applySorting: (field, order) ->
      @model.set 'result', _.sortBy @model.get('result'), (data) =>
        @orderField data[field], order
      @render()
      super field, order

  class AuctionStats.HourStatsView extends App.withSorting(Marionette.ItemView)
    sortFieldSelector: 'th.sort'
    template: '#tools-auction-hour-stats'

    applySorting: (field, order) ->
      @model.set 'hour_stat', _.sortBy @model.get('hour_stat'), ([time, data]) =>
        if field == 'time'
          [hours, minutes] = _.map time.split(':'), (x) -> parseInt x, 10
          order * (hours * 60 + minutes)
        else
          @orderField data[field], order
      @render()
      super field, order

  class AuctionStats.TotalStatsView extends App.withSorting(Marionette.ItemView)
    template: '#tools-auction-total-stats'
    sortFieldSelector: 'th.sort'

    events:
      'change .only-active': 'toggleInactive'

    initialize: ->
      @model.set 'onlyActive', true

    toggleInactive: (e) ->
      @model.set 'onlyActive', $(e.currentTarget).is ':checked'
      @render()
      
    applySorting: (field, ordering) ->
      super field, ordering
      @model.set 'total_stats', _.sortBy @model.get('total_stats'), (data) =>
        if field == 'first_finish_time'
            if data[field]==''
              data[field]=' '
            @orderField data[field], ordering
        else
          @orderField data[field], ordering
      @render()

  class AuctionStats.View extends Marionette.LayoutView
    template: '#tools-auction-stats'

    regions:
      configRegion: '.auction-config'
      queueRegion: '#auction-queue-tab'
      resultRegion: '#auction-result-tab'
      hourStatsRegion: '#auction-hour-stats-tab'
      totalStatsRegion: '#auction-total-stats-tab'

    events:
      'click .reset-auction': 'onReset'
      'click .reload-stats': 'refreshStats'

    renderConfig: (configModel, defaultsModel) ->
      @configRegion.show(new AuctionStats.ConfigView(model: configModel, defaultsModel: defaultsModel))

    renderTabs: ->
      @queueRegion.show new AuctionStats.QueueView(model: @model)
      @resultRegion.show new AuctionStats.ResultView(model: @model)
      @hourStatsRegion.show new AuctionStats.HourStatsView(model: @model)
      @totalStatsRegion.show new AuctionStats.TotalStatsView(model: @model)

    getConfigModel: ->
      cfg = new App.Tools.Config.Model
      if App.OVERALL_AUCTION
        [cfg, null]
      else if @getOption('isBanner')
        [new App.News.Model.Banner(id: @model.id), cfg]
      else
        [new App.News.Model.NewsItem(id: @model.id), cfg]

    renderConfigByModel: ->
      [configModel, defaultsModel] = @getConfigModel()
      configModel.fetch()
      .done =>
        if defaultsModel is null
          @renderConfig configModel, null
        else
          defaultsModel.fetch()
            .done =>
              @renderConfig configModel, defaultsModel
            .fail ->
              window.alert 'Не удалось загрузить глобальные настройки аукциона'
      .fail ->
        window.alert 'Не удалось загрузить настройки аукциона'

    initialize: ->
      @renderConfigByModel()

    getResetAuctionUrl: ->
      if App.OVERALL_AUCTION
        "/ed/api/banners/reset_all_auctions/"
      else
        # "/ed/api/#{@collectionName}/reset_auction/#{@model.get 'id'}"
        #временно прикидываемся баннером, т. к. для news нет соответствующего route на сервере
        "/ed/api/banners/reset_auction/#{@model.get 'id'}"

    onReset: ->
      if confirm('Действительно перезапустить?')
        Backbone.ajax
          url: @getResetAuctionUrl()
          type: 'POST'
        .success =>
          @refreshStats()

        @renderConfigByModel()

    onRender: ->
      @renderTabs()

    refreshStats: ->
      @model.fetch().success =>
        @render()
      @renderConfigByModel()

  class AuctionStats.Router extends App.Router
    controller:
      index: (id, isBanner) ->
        if App.OVERALL_AUCTION
          m = new AuctionStats.OverallStats {id: id}
        else if isBanner
          m = new AuctionStats.BannerStats {id: id}
        else
          m = new AuctionStats.NewsStats {id: id}
        m.fetch().then ->
          App.mainRegion.show(new AuctionStats.View({
            model: m,
            isBanner: isBanner}))
      indexNews: (id) ->
        @index id, false
      indexBanner: (id) ->
        @index id, true
      indexOverall: ->
        @index App.OVERALL_ITEM_ID, false

    appRoutes:
      'banners/auction_stats/:id': 'indexBanner'
      'news/auction_stats/:id': 'indexNews'
      'tools/auction_stats': 'indexOverall'

  AuctionStats.addInitializer ->
    new AuctionStats.Router()
