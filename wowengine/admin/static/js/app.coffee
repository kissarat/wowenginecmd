moment.lang 'ru'

App = new Backbone.Marionette.Application()

App.module 'Views', (Views) ->
  class Views.DatabaseErrorView extends Marionette.ItemView
    template: "#db-error-template"

App.module 'Models', (Models) ->
  # parses REST API provided by Flask-Restless
  class Models.RestlessCollection extends Backbone.PageableCollection

    state:
      firstPage: 1
      pageSize: 50
    queryParams:
      currentPage: "page",
      pageSize: "results_per_page"

    parseRecords: (resp) ->
      resp.objects

    parseState: (resp, queryParams, state, options) ->
      console.log('Num results: ' + resp.num_results)
      return totalRecords: resp.num_results


# the base router to extend from, tracks route changes
class App.Router extends Marionette.AppRouter
  onRoute: ->
    App.commands.execute "changeActiveMenu", Backbone.history.fragment
#console.log "current route: #{Backbone.history.fragment}"

App.addRegions
  navigationRegion: '#generated-navigation-menu-region'
  navigationMiscToolsRegion: '#navigation-misc-tools-region'
  mainRegion: '#content'

App.mainRegion.on 'show', (view) ->
  if view.getTitle?
    document.title = App.CONFIG_SERVER_NAME + ' | ' + view.getTitle() + ' | Админочка'
  else
    document.title = App.CONFIG_SERVER_NAME + ' | Админочка'

### Commands ###

# поменять подсветку активного раздела в меню админки
App.commands.setHandler "changeActiveMenu", (path) ->
  App.navigationRegion.currentView.triggerMethod "change:active:menu", path


### Utils ###

App.notify = (text, custom_opts={}) ->
  opts =
    globalPosition: 'top center'

  _.extend opts, custom_opts

  $.notify(text, opts)

App.FilteredCollection = (original) ->
  filtered = new original.constructor()

  # allow this object to have its own events
  filtered._callbacks = {}

  # copy our meta information as well (non-standard property)
  filtered._meta = original._meta

  filtered.where = (criteria=null) ->
    items = if criteria then original.where criteria else original.models
    filtered._currentCriteria = criteria
    filtered.reset items

  filtered.fetch = (args...) ->
    promise = original.fetch args...
    promise.then -> filtered.where filtered._currentCriteria
    promise

  # when the original collection is reset,
  # the filtered collection will re-filter itself
  # and end up with the new filtered result set
  original.on 'reset', ->
    filtered.where filtered._currentCriteria

  filtered.where()
  filtered


# Return data needed to build a calendar for one month
# (array of arrays representing weeks aligned by weekdays)
# contains Date instances
App.calendarData = (year, month) ->
  m = moment(new Date(year, month - 1, 1))
  a = new Array(m.weekday())
  days = _.range 1, m.daysInMonth() + 1
  a = a.concat days
  r = a.length % 7
  if r != 0
    a = a.concat(new Array(7 - r))
  result = []
  while a.length > 0
    result.push a.splice(0, 7)
  result

App.decimal = (x) ->
  try
    App.floatFormat x, 2
  catch error
    App.floatFormat 0, 2

App.integer = (x) ->
  try
    App.floatFormat x, 0
  catch error
    0

bust_cache_param = ->
  "?#{Math.floor(Math.random() * 9999999999) + 1}"

App.newsImageUrl = (unid, num, width, height, cx, cy, bust_cache = true) ->
  s = "/images/news/#{unid.substring 0, 2}/#{unid}/#{num}-res-jpg-w#{width}h#{height}cx#{cx}cy#{cy}.jpg"
  if bust_cache then s + bust_cache_param() else s

App.newsImageOriginalUrl = (unid, num, bust_cache = true) ->
  s = "/images/news/#{unid.substring 0, 2}/#{unid}/#{num}.jpg"
  if bust_cache then s + bust_cache_param() else s

# Use for links
App.navigate = (route) ->
  Backbone.history.navigate(route, {trigger: true})

App.gen_uuid = ->
  'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace /[xy]/g, (c) ->
    r = Math.random() * 16 | 0
    (if c == 'x' then r else r & 0x3 | 0x8).toString 16

# Fetch flask-restless collection using query options.
App.fetchCollection = (coll, opts = {}) ->
#  defaultOpts =
#    order_by: [
#      field: 'date'
#      direction: 'desc'
#    ]
#  combinedOpts = _.extend(defaultOpts, opts)
#  console.log combinedOpts
  coll.fetch({data: {q: JSON.stringify(opts)}})

# Fetch collection and show collection view, or show the database error message.
App.fetchCollectionView = (coll, viewClass, opts = {}) ->
  App.fetchCollection(coll, opts).done ->
    view = new viewClass({collection: coll})
    App.mainRegion.show(view)
  .fail ->
    App.showErrorView()

# show a message about database error in the main region
App.showErrorView = ->
  view = new App.Views.DatabaseErrorView()
  App.mainRegion.show(view)

App.confirmAndDelete = (coll) ->
  modelsToDelete = coll.filter (x) ->
    x.get 'is_marked'
  if confirm("Удалить #{modelsToDelete.length}?")
    for m in modelsToDelete
      m.destroy()
  else
    console.log 'отмена удаления'
    for m in modelsToDelete
      console.log m.get('id')

CYRILLIC_TO_LATIN = {
  "а": "a", "б": "b", "в": "v", "г": "g", "д": "d", "е": "e", "ё": "e", "ж": "zh", "з": "z", "и": "i", "й": "j",
  "к": "k", "л": "l", "м": "m", "н": "n", "о": "o", "п": "p", "р": "r", "с": "s", "т": "t", "у": "u", "ф": "f",
  "х": "h", "ц": "c", "ч": "ch", "ш": "sh", "щ": "sch", "ъ": "", "ы": "y", "ь": "", "э": "e", "ю": "yu",
  "я": "ya", '_': ''
}

App.genMnemonicId = (title) ->
  title.toLowerCase().replace /./g, (x) ->
    CYRILLIC_TO_LATIN[x] ? '-'

###
Генерация строки для сортировки в методе applySorting, предназначена только для текстовых полей
@param string string Строка которою нужно преобразовать
@param int order Сортировка которая приходит в метод applySorting, имеет занаяение 1 или -1

  ```
  class Stats.ListView extends App.withSorting(Marionette.CompositeView)
    sortFieldSelector: 'th.sort'

    applySorting: (field, order) ->
      @collection.comparator = (x) => 
        if field is 'name'
          res = App.genSortString x.get(field), order
        else
          res = order * +x.get(field)
        return res
      @collection.sort()
      super field, order
  ```
###
App.genSortString = (string, order) ->
  #массив чаркодов для букв
  char = (start, end) ->
    normal: [String(start).charCodeAt(0)..String(end).charCodeAt(0)]
    reverse: [String(end).charCodeAt(0)..String(start).charCodeAt(0)]
  #массив чаркодов для цифр
  numb = 
    normal: [String(0).charCodeAt(0)..String(9).charCodeAt(0)]
    reverse: [String(9).charCodeAt(0)..String(0).charCodeAt(0)]
  #собираем массивы алфавитов для русских и английских букв
  letter = 
    eng: char 'a', 'z'
    rus: char 'а', 'я'

  first = true
  #посимвольно заменяем строку
  String(string).toLowerCase().replace /./g, (x) ->
    #делаем реверс строки только если нужна сортировака по убыванию
    if order < 0 then (
      code = String(x).charCodeAt(0)
      #проверяем не является ли символ числом
      if (index = numb.normal.indexOf code) >= 0 
        #реверс чисел в строке
        x = String.fromCharCode(numb.reverse[index])
      else
        for i of letter 
          if (index = letter[i].normal.indexOf code) >= 0
            #реверс букв
            x = String.fromCharCode(letter[i].reverse[index])
            #русский алфавит ставим в начало
            x = (if i is 'rus' then "1#{x}" else "2#{x}") if first is true
            break
    )
    first = false
    return x

App.newsTypeGenitive = (t) ->
  switch t
    when 'news' then 'новости'
    when 'banners' then 'баннера'
    when 'lines' then 'строчки'
    else
      'неизвестно чего'

App.newsTypeAccusative = (t) ->
  switch t
    when 'news' then 'новость'
    when 'banners' then 'баннер'
    when 'lines' then 'строчку'
    else
      'неизвестно что'

nunjucksEnv = new nunjucks.Environment()

# make nunjucks the default template engine

Backbone.Marionette.TemplateCache.prototype.compileTemplate = (rawTemplate) ->
  nunjucks.compile(rawTemplate, nunjucksEnv)

Backbone.Marionette.Renderer.render = (template, data) ->
  t = Marionette.TemplateCache.get(template)
  t.render _.extend(data, {App: App})

### Nunjucks filters ###

NUNJUCKS_FILTERS =
  onOff: (x) ->
    if x then 'on' else 'off'

  decimal: App.decimal
  
  integer: App.integer

  dateFormat: (x, fmt = 'DD.MM.YYYY') ->
    if window.moment then moment(x).format(fmt) else x

  humanDate: (x) ->
    if window.moment then moment(x).calendar() else x

  json: (x) ->
    JSON.stringify x

  floatFormat: (x, precision) ->
    x.toFixed(precision)

nunjucksEnv.addFilter n, f for n, f of NUNJUCKS_FILTERS


App.withSorting = (ViewCls) ->
  ###
  Special function for adding ability to sort to specified class, usage:

  ```
  class Stats.ListView extends App.withSorting(Marionette.CompositeView)
    sortFieldSelector: 'th.sort'

    applySorting: (field, order) ->
      # Actually sort data.
      super field, order
  ```

  ###

  class SortedView extends ViewCls
    sortFieldSelector: ''

    constructor: (args...) ->
      if _.isUndefined @events
        @events = {}
      @events["click #{@sortFieldSelector}"] = @sortByColumn
      super args...

    applySorting: (field, order) ->
      @sortOrder = order
      @sortField = field
      @updateHeaders()

    onRender: ->
      @updateHeaders()

    updateHeaders: ->
      orderClass = if @sortOrder == 1 then 'glyphicon-sort-by-attributes' else 'glyphicon-sort-by-attributes-alt'
      @$el.find('.sort i').removeClass()
      @$el.find("#{@sortFieldSelector}[by=#{@sortField}] i")
        .addClass("glyphicon #{orderClass}")

    sortByColumn: (e) =>
      e.preventDefault()
      @applySorting $(e.currentTarget).attr('by'), if @sortOrder == 1 then -1 else 1

    orderField: (val, ordering) ->
      if ordering == 1
        val
      else if _.isString val
        _.map val.toLowerCase().split(''), (ch) ->
          String.fromCharCode(-(ch.charCodeAt(0)))
      else
        -1 * val
        
###
Преобразование чисел в флоат формат
@param int|float num Число которое нужно преобразовать
@param int decimals Количество знаков после зяпятой (по умолчанию 2)
@param boolean abs Взять абсолютное значение числа (по умолчанию false)
###
App.floatFormat = (num, decimals, abs) ->
  num =  if isNaN(num) is true then 0 else +num
  decimals = if isNaN(decimals) is true then 2 else +decimals
  num = Math.abs(num) if abs is true
  (Math.round((num)*100)/100).toFixed(decimals)
  
### 
Лоадер для ajax запросов и других случаев где есть ожидание выполнения
@param boolean статус on или off (по умолчанию off)
###
App.loader = (status) ->
  if status is on
    $('.ajax-loader').removeClass 'hidden'
  else
    $('.ajax-loader').addClass 'hidden'
  return
  
###
Bootstrap alert http://getbootstrap.com/components/#alerts
@param string|object el Селектор внутрь которог обудет помещен алерт
@param string text Текст который будет выведен в алерте
@param string type Тип алерта, может быть 'info', 'success', 'warning', 'danger' (по умолчанию 'info')
###
App.alert = (el, text, type) ->
  if $(el).length > 0 then (
    type = 'info' if type not in ['info', 'success', 'warning', 'danger']
    html = """
            <div class="app-alert alert alert-#{type}">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <strong class="capitalize">#{type}!</strong> #{text}
            </div>
           """
    $(el).html(html)
  )
  return

# глобальный массив для общего доступа  
App.global = {}

# глобальная переменная для общего доступа
window.App = App
