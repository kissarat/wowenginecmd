"""
Administration interface.
"""

from flask import Blueprint

from .assets import register_assets


admin = Blueprint('admin', __name__,
                  static_folder='static',
                  template_folder='templates')

register_assets()

from .views import *
