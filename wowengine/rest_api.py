"""
Конфигурация Flask-Restless (автоматический REST API для моделей SQLAlchemy).
"""

import inspect
from functools import partial
from flask.ext.login import current_user
from flask.ext.restless import ProcessingException
from .models import (
    NewsItem, Banner, Line, Source, Tag, Item, Provider,
    ProviderClickPriceHistory, SourceClickPriceHistory
)
from .extensions import cache
from . import audit

PREPROCESSOR_TYPES = ['GET_SINGLE', 'GET_MANY', 'PATCH_SINGLE', 'PUT_SINGLE',
                      'PATCH_MANY', 'PUT_MANY', 'POST', 'DELETE']

DATA_ALTERING_ACTIONS = ['PUT_SINGLE', 'POST', 'PUT_MANY']

PROCESSOR_TYPES_DESCRIPTION = {
    # Flask-Restless http-methods
    'DELETE': 'delete',
    'POST': 'create',
    'PUT': 'update',
    'PUT_SINGLE': 'update',
    'PATCH': 'update',
    'PATCH_SINGLE': 'update',
    'PATCH_MANY': 'update',
}

# processors for PUT are applied to PATCH because PUT is just a
# redirect to PATCH. See the details in Flask_restless sources.
AUDIT_PREPROCESSOR_TYPES = ['PATCH_SINGLE', 'PATCH_MANY', 'DELETE']
AUDIT_POSTPROCESSOR_TYPES = ['PATCH_SINGLE', 'PATCH_MANY', 'DELETE', 'POST']


def _generate_order_preprocessor(field, direction='asc'):
    """
    Returns a preprocessor that orders by field and direction.
    """

    def f(search_params=None, **kw):
        if search_params is not None:
            if 'order_by' not in search_params:
                search_params['order_by'] = []
            search_params['order_by'].append({'field': field, 'direction': direction})

    return f


def _generate_ignore_attributes_preprocessor(attributes):
    """
    Returns a preprocessor that deletes some attributes from the model.
    """

    def f(**kw):
        for a in attributes:
            kw['data'].pop(a, None)
    return f


def _generate_invalidate_cache_preprocessor(clazz):
    """
    Returns a preprocessor invalidating all memoized methods for a class.
    """
    def f(**kw):
        for _, method in inspect.getmembers(clazz, predicate=inspect.ismethod):
            cache.delete_memoized(method)

    return f


def _generate_audit_processor(ptype, model, before_after=None):
    action = PROCESSOR_TYPES_DESCRIPTION.get(ptype, 'unknown_action')

    def f(**kw):
        audit.log(before_after=before_after,
                  action=action,
                  model=model,
                  json_data=kw,
                  rest_api_method=ptype)

    return f


_generate_audit_preprocessor = partial(_generate_audit_processor, before_after='before')
_generate_audit_postprocessor = partial(_generate_audit_processor, before_after='after')


def add_audit_processors(config, model, api_args):
    """
    Generates and adds logging preprocessors to `api_args`.
    `model` must be a string describing a model type (e.g. 'Banner').
    """
    if not config.get('ENABLE_REST_API_AUDIT', False):
        return

    for ptype in AUDIT_PREPROCESSOR_TYPES:
        pp = _generate_audit_preprocessor(ptype, model)
        api_args.setdefault('preprocessors', {}).setdefault(ptype, []).append(pp)

    for ptype in AUDIT_POSTPROCESSOR_TYPES:
        pp = _generate_audit_postprocessor(ptype, model)
        api_args.setdefault('postprocessors', {}).setdefault(ptype, []).append(pp)


def _common_api_args(clazz):

    def auth_preprocessor(**kw):
        if not current_user.is_authenticated():
            raise ProcessingException(description='Not authenticated!', code=401)
        return True

    d = {
        'url_prefix': '/ed/api',
        'max_results_per_page': 50,
        'results_per_page': 50,
        'methods': frozenset(['GET', 'POST', 'DELETE', 'PUT', 'PATCH']),
        # всегда проверять логин
        'preprocessors': {t: [auth_preprocessor] for t in PREPROCESSOR_TYPES}
    }

    invalidate_cache_fn = _generate_invalidate_cache_preprocessor(clazz)
    for a in DATA_ALTERING_ACTIONS:
        d['preprocessors'][a].append(invalidate_cache_fn)

    return d


def patch_single_postprocessor_for_source(result=None, **kw):
    def _add_click_price_to_history(_result):
        SourceClickPriceHistory.append_entry(_result)
    _add_click_price_to_history(result)


def patch_single_postprocessor_for_provider(result=None, **kw):
    def _add_click_price_to_history(_result):
        ProviderClickPriceHistory.append_entry(_result)
    _add_click_price_to_history(result)


def register_rest_api(manager):
    # def AUDIT_preprocessor(**kw):
    # logging.info(kw)

    # --- NEWS ITEMS
    for clazz in [NewsItem, Banner, Line]:
        item_args = _common_api_args(clazz)
        add_audit_processors(manager.app.config, model=clazz.__name__, api_args=item_args)
        manager.create_api(clazz,
                           collection_name=clazz.api_collection_name,
                           **item_args)

    # --- SOURCES
    sources_args = _common_api_args(Source)
    sources_args['postprocessors'] = {'PUT_SINGLE': [patch_single_postprocessor_for_source]}
    sources_args['max_results_per_page'] = -1
    sources_args['preprocessors']['GET_MANY'].append(_generate_order_preprocessor('num'))

    # Если хочется получать результаты вычисления методов в качестве атрибутов модели через GET,
    # приходится их затирать перед тем, как сохранить модель через PUT.
    included_methods = ['today_stats', 'total_stats']
    sources_args['include_methods'] = included_methods
    ignore_included_methods_fn = _generate_ignore_attributes_preprocessor(included_methods)
    for a in DATA_ALTERING_ACTIONS:
        sources_args['preprocessors'][a].append(ignore_included_methods_fn)

    add_audit_processors(manager.app.config, model='Source', api_args=sources_args)

    manager.create_api(Source,
                       collection_name=Source.api_collection_name,
                       exclude_columns=['items'],
                       **sources_args)

    # --- Tags
    tag_args = _common_api_args(Tag)
    add_audit_processors(manager.app.config, model='Tag', api_args=tag_args)
    manager.create_api(Tag,
                       collection_name=Tag.api_collection_name,
                       exclude_columns=['items'],
                       **tag_args)

    # --- Providers
    provider_args = _common_api_args(Provider)
    provider_args['postprocessors'] = {'PUT_SINGLE': [patch_single_postprocessor_for_provider]}
    manager.create_api(Provider,
                       collection_name=Provider.api_collection_name,
                       **provider_args)

    # --- Специальная коллекция материалов ТОЛЬКО для рассчета размеров заголовков.
    fsc_args = _common_api_args(Item)
    fsc_args['methods'] = frozenset(['GET', 'PATCH'])
    fsc_args['preprocessors']['GET_MANY'].append(_generate_order_preprocessor('id', 'desc'))
    manager.create_api(Item,
                       collection_name='items_for_sizing_fonts',
                       include_columns=['id', 'title', 'annotation'],
                       **fsc_args)
