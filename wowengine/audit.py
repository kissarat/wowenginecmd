from flask import current_app
from flask.ext.security import current_user
from wowengine import db
from .models import AuditTrail


def log(before_after, action, model, json_data=None, rest_api_method=None):
    """
    `json_data` must be a json serializable dict-like object.
    """
    try:
        record = AuditTrail(user=str(current_user),
                            before_after=str(before_after),
                            action=str(action),
                            json_data=json_data,
                            model=str(model),
                            rest_api_method=str(rest_api_method))
        db.session.add(record)
        db.session.commit()
    except Exception as e:
        current_app.logger.exception(str(e))
