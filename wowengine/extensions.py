"""
Плагины для Flask.
Собраны в один модуль, чтобы было легче их импортировать из разных мест кода.
"""

from flask.ext.cache import Cache
cache = Cache()

from flask.ext.sqlalchemy import SQLAlchemy
db = SQLAlchemy()

from flask.ext.assets import Environment
assets = Environment()

from flask.ext.restless import APIManager
api = APIManager()

from .flask_redis import Redis
redis = Redis()

from .session import RedisSessionInterface
session_interface = RedisSessionInterface(redis)

from flask_mail import Mail
mail = Mail()

from flask_security import Security
security = Security()

from flask.ext.debugtoolbar import DebugToolbarExtension
toolbar = DebugToolbarExtension()

from .flask_statsd import StatsD
statsd = StatsD()

from .stats import (
    SourceStats, BannerStats, GenerationStats,
    ArticleStats, ArticleTopChart, SuperCache,
    ActiveItemsStats, LineStats,
)

source_stats = SourceStats(redis, statsd)
banner_stats = BannerStats(redis, statsd)
line_stats = LineStats(redis, statsd)
generation_stats = GenerationStats(redis, statsd)
article_stats = ArticleStats(redis, statsd)
active_items_stats = ActiveItemsStats(redis)
article_top_chart = ArticleTopChart(redis)
super_cache = SuperCache(redis)

from wowengine.site_config import SiteConfig
site_config = SiteConfig(redis, defaults={
    'auction_shows_count': 1000,  # сколько раз крутить баннер в аукционе
    'auction_max_age_days': 3650,  # максимальный возраст баннера для участия в аукционе
    'banner_excluding_rule': 'none', # правило исключения показов баннеров для уника 
    'banner_sorting_rule': 'last', # правило сортировки баннеров 
    'auction_algorithm': 'flow',
    'fast_auction_ctr_threshold': '5.0',
})

from wowengine.auction import Auction
auction = Auction(redis, super_cache, site_config)
