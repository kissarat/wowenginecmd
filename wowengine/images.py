"""
Работа с файлами картинок (пути, ресайз).
"""

import os
import glob
import re
import itertools
from pgmagick.api import Image
from pgmagick import Geometry

from flask import current_app


# ## Private functions
from wowengine.util import unid_hierarchy_path


def _get_abs_dir(unid):
    """
    Path to save the file in two-level directory hierarchy based on unid.
    """
    return os.path.join(current_app.static_folder,
                        current_app.config['IMG_UPLOAD_DIR'],
                        unid_hierarchy_path(unid))


def _orig_size_filename(directory, num):
    orig_filenames = glob.iglob(os.path.join(directory, '%s.*' % num))
    try:
        return next(orig_filenames)
    except StopIteration:
        return None


def _filenames(unid):
    """
    List all uploaded files for the path.
    """
    all_files = glob.iglob(os.path.join(_get_abs_dir(unid), '*'))
    # only files matching [0-9]+\.jpg
    return (n for n in (os.path.basename(x) for x in all_files) if re.match('^[0-9]+\..{3}$', n))


def _first_free_filename(path):
    """
    The first natural-number.jpg filename that is available.
    """
    taken_nums = {int(os.path.basename(x)[:-4]) for x in _filenames(path)}
    num = next(x for x in itertools.count(1) if x not in taken_nums)
    return "%s.jpg" % num


def _resized_image_filename(num, w, h, cx_percent, cy_percent):
    return "%d-res-jpg-w%dh%dcx%dcy%d.jpg" % (num, w, h, cx_percent, cy_percent)


# ## API

def image_url(unid, filename):
    """
    Урл для оригинальной картинки.
    """
    return os.path.join(current_app.config['IMG_DIR_URL'], unid_hierarchy_path(unid), filename)


def resized_image_url(unid, num, w, h, cx_percent, cy_percent):
    """
    Урл для картинки, отформатированной под заданный размер.
    """
    return os.path.join(current_app.config['IMG_DIR_URL'], unid_hierarchy_path(unid),
                        _resized_image_filename(num, w, h, cx_percent, cy_percent))


def image_urls(unid):
    """
    Урлы всех оригинальных картинок для заданного unid.
    """
    return (image_url(unid, x) for x in _filenames(unid))


def save_upload(f, params):
    """
    Save an uploaded file (chunking is not supported for now).
    Return new file's name.
    """

    path = _get_abs_dir(params['unid'])
    if not os.path.exists(path):
        os.makedirs(path)
    name = _first_free_filename(path)  # TODO race condition, synchronise?
    fullname = os.path.join(path, name)

    with open(fullname, 'wb') as dest:
        dest.write(f.read())

    im = Image(fullname)
    return {
        'num': name[:-4],
        'height': im.height,
        'width': im.width
    }


def resized_image(unid, num, w, h, cx_percent, cy_percent):
    """
    Creates a resized image if it does not exist and returns its url.
    """
    dest_dir = _get_abs_dir(unid)

    src_filename = _orig_size_filename(dest_dir, num)
    if src_filename is None:
        return None

    dest_filename = os.path.join(dest_dir, _resized_image_filename(num, w, h, cx_percent, cy_percent))

    try:
        im = Image(src_filename)
    except RuntimeError:
        return None

    cx = round(im.width * cx_percent / 100)
    cy = round(im.height * cy_percent / 100)

    im_ratio = im.width / im.height
    dest_ratio = w / h

    if im_ratio > dest_ratio:
        # landscape -> portrait, crop sides
        crop_h = im.height
        crop_w = round((w * crop_h) / h)
    else:
        # portrait -> landscape, crop top and bottom
        crop_w = im.width
        crop_h = round((h * crop_w) / w)

    off_x = round(cx - crop_w / 2, 0)
    off_y = round(cy - crop_h / 2, 0)

    # correct if too small
    off_x = max(off_x, 0)
    off_y = max(off_y, 0)

    # correct if too large
    off_x = round(min(off_x, im.width - crop_w))
    off_y = round(min(off_y, im.height - crop_h))

    im.crop(Geometry(crop_w, crop_h, off_x, off_y))
    im.scale('%dx%d' % (w, h))

    im.img.quality(75)

    if not os.path.exists(dest_dir):
        os.makedirs(dest_dir)
    im.write(dest_filename)

    return dest_filename


def delete_image(unid, num, delete_resized=True):
    """
    Delete image and optionally all its auto-generated clones.
    """
    img_dir = _get_abs_dir(unid)
    try:
        orig_filenames = glob.iglob(os.path.join(img_dir, '%d.*' % num))
        for n in orig_filenames:
            os.remove(n)
    except FileNotFoundError:
        pass

    if delete_resized:
        resized_filenames = glob.iglob(os.path.join(img_dir, '%d-*.jpg' % num))
        for n in resized_filenames:
            os.remove(n)
