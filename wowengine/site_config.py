"""
Настраиваемые параметры функционирования сайта (ключ-значение).
"""
from wowengine.extensions import cache


class SiteConfig(object):
    """
    Dict-like object supporting x['field'] as well as x.get('field', default).
    """

    VAR_NAMES = ['auction_shows_count', 'auction_clicks_count', 'auction_max_age_days',
                 'banner_excluding_rule', 'banner_sorting_rule',
                 'auction_algorithm', 'fast_auction_ctr_threshold',]

    def __init__(self, redis, name='site_config', defaults=None):
        self.redis = redis
        self.name = name
        self.defaults = defaults if defaults is not None else {}

    @staticmethod
    def _convert(v):
        try:
            v.index('.')
            return float(v)
        except ValueError:
            try:
                return int(v)
            except ValueError:
                return v

    @cache.memoize(timeout=60)
    def get_value(self, key):
        v = self.redis.hget(self.name, key)
        return self._convert(v) if v is not None else self.defaults.get(key)

    def set_value(self, key, value):
        cache.delete_memoized(self.get_value, self, key)
        return self.redis.hset(self.name, key, value)

    def __getitem__(self, item):
        return self.get_value(item)

    def __setitem__(self, key, value):
        return self.set_value(key, value)

    def get(self, key, default=None):
        v = self.get_value(key)
        return v if v is not None else default

    def get_all(self):
        return {name: self.get(name) for name in self.VAR_NAMES}
