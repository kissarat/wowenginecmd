"""
Store timeseries stats in Redis.

Inspired by https://github.com/agoragames/kairos (which doesn't work on Python 3).
"""
import uuid

__author__ = 'Sergei Barbarash <sgt.sgt@gmail.com>'

import time
import itertools
from datetime import datetime
import re


class RedisTimeseries(object):
    """
    Позволяет хранить статистику о временных отрезках заданных интервалами.

    ВАЖНО: при изменении интервалов ранее собранная статистика может прийти в негодность, ее нужно либо
    сконвертировать, либо полностью удалить и начать собирать заново.
    """

    NUMBER_RE = re.compile('^\d+$')
    STEP_FORMAT = re.compile('^(\d+)([mhdM])')
    EXPIRE_FORMAT = re.compile('^(\d+)([mhd])')
    TIME_MULTIPLIER = {
        'm': 60,
        'h': 3600,
        'd': 86400
    }

    def __init__(self, redis, name, intervals):
        """
        :param redis: экземпляр redis.StrictRedis
        :param name: название серии
        :param intervals: интервалы для серии в словаре (см. stats.py)
        """
        self.redis = redis
        self.name = name
        self.intervals = self._convert_intervals(intervals)

    @classmethod
    def _resolve_expire_time(cls, val):
        """
        Перевести условные обозначения времени типа 1d, 5m итд в количество секунд.
        """
        if isinstance(val, int):
            return val
        elif cls.NUMBER_RE.match(val):
            return int(val)
        else:
            m = cls.EXPIRE_FORMAT.match(val)
            if m:
                return int(m.group(1)) * cls.TIME_MULTIPLIER[m.group(2)]

        raise ValueError('Unsupported time format %s' % val)

    @classmethod
    def _time_bucket_fn(cls, val):
        """
        Returns a function of type ts -> ts that returns a timestamp bucket based on its argument
        (which is now by default).

        :rtype (int) -> int
        """

        def seconds_step_fn(ts=None):
            # бакет = тайстемп кратный шагу
            ts = time.time() if ts is None else ts
            return int(ts - (ts % val))

        def daily_step_fn(ts=None):
            # бакет = полночь текущего дня
            ts = time.time() if ts is None else ts
            dt = datetime.fromtimestamp(ts)
            return int(dt.replace(hour=0, minute=0, second=0).timestamp())

        def monthly_step_fn(ts=None):
            # бакет = 1 число месяца в полночь
            ts = time.time() if ts is None else ts
            dt = datetime.fromtimestamp(ts)
            return int(dt.replace(day=1, hour=0, minute=0, second=0).timestamp())

        if isinstance(val, int):
            return seconds_step_fn
        elif val == 'eternity':
            # the bucket is always zero
            return lambda *args: 0
        elif val == 'daily':
            return daily_step_fn
        elif val == 'monthly':
            return monthly_step_fn
        else:
            m = cls.STEP_FORMAT.match(val)
            if m:
                seconds = int(m.group(1)) * cls.TIME_MULTIPLIER[m.group(2)]
                return cls._time_bucket_fn(seconds)

        raise ValueError('Unsupported time format %s' % val)

    def _convert_intervals(self, intervals):
        """
        Перевести интервалы из строкового представления в функции для шагов и количество секунд для
        времени устаревания (так будет быстрее).
        """

        def c(d):
            r = {'bucket_fn': self._time_bucket_fn(d['step'])}
            if 'expire' in d:
                r['expire'] = self._resolve_expire_time(d['expire'])
            return r

        return {name: c(d) for name, d in intervals.items()}

    def _gen_key(self, interval_name, name, ts):
        """
        ключ статзаписи в Редисе состоит из:
        - названия таймлайна
        - названия интервала
        - названия данного показателя
        - таймстэмпа, указывающего начало текущего интервала (bucket)
        """
        bucket = self.intervals[interval_name]['bucket_fn'](ts)
        return "%s:%s:%s:%s" % (self.name, interval_name, name, bucket)

    def _update_stat_value(self, name, fn, amount=1, ts=None, pipeline=None):
        """
        Обновить данные в одной из структур Редиса.

        :param fn: функция с параметрами (инстанс клиента редиса, ключ, значение)
        """
        ts = time.time() if ts is None else ts
        rds = pipeline if pipeline else self.redis

        # Повторить для каждого заданного интервала.
        for interval_name, interval_dict in self.intervals.items():

            key = self._gen_key(interval_name, name, ts)

            # записать в базу
            fn(rds, key, amount)

            # Задать expiration, если указан для этого интервала.
            expire = interval_dict.get('expire')
            if expire:
                rds.expire(key, expire)

    def update_histogram(self, name, item_id, **kwargs):
        """
        Увеличить счетчик показателя 'item_id' внутри гистограммы 'name'.
        """
        return self._update_stat_value(name,
                                       lambda rds, key, amount: rds.hincrby(key, item_id, amount),
                                       **kwargs)

    def update_float_histogram(self, name, item_id, **kwargs):
        """
        Увеличить float-счетчик показателя 'item_id' внутри гистограммы 'name'.
        """
        return self._update_stat_value(name,
                                       lambda rds, key, amount: rds.hincrbyfloat(key, item_id, amount),
                                       **kwargs)

    def bulk_update_histogram(self, name, items, amount=1, ts=None):
        """
        Обновить сразу кучу значений в гистограмме через пайплайн.
        :param item_ids: ID материала или список ID
        :param amount: сколько заходов (по умолчанию 1)
        """
        ts = time.time() if ts is None else ts
        if isinstance(items, dict):
            item_ids = items.keys()
            item_amounts = items.values()
        else:
            item_ids = items
            item_amounts = itertools.repeat(amount, len(items))

        with self.redis.pipeline() as pipe:
            for i, v in zip(item_ids, item_amounts):
                self.update_histogram(name, i, amount=v, ts=ts, pipeline=pipe)
            pipe.execute()

    def update_counter(self, name, **kwargs):
        """
        Увеличить счетчик показателя 'name'.
        """
        return self._update_stat_value(name,
                                       lambda rds, key, amount: rds.incrby(key, amount),
                                       **kwargs)

    def get_counter(self, interval, name, ts=None):
        """
        Получить значение счетчика.
        """
        ts = time.time() if ts is None else ts
        key = self._gen_key(interval, name, ts)
        return self.redis.get(key)

    @staticmethod
    def _convert_hashtable_result(items, key_fn, value_fn=None):
        """
        Конвертирует ключи и значения словаря (а то Редис возвращает binary).
        Значение по умолчанию int, ключ - по желанию заказчика.
        """
        value_fn = float if not value_fn else value_fn
        return {key_fn(k or 0): value_fn(v) for k, v in items if k != 'None'}

    def get_histogram(self, interval, name, key_fn=lambda x: x, ts=None, value_fn=None):
        """
        Получить словарь со значениями гистограммы.
        """
        ts = time.time() if ts is None else ts
        key = self._gen_key(interval, name, ts)
        result = self.redis.hscan_iter(key)
        return self._convert_hashtable_result(result, key_fn, value_fn)

    def get_histogram_value(self, interval, name, item_id, ts=None):
        """
        Получить одно из значений в гистограмме.
        """
        ts = time.time() if ts is None else ts
        key = self._gen_key(interval, name, ts)
        v = self.redis.hget(key, item_id)
        return int(v) if v else 0

    def get_all_buckets(self, interval_name, name, ts_start=0, ts_end=None):
        """
        Получить таймстемпы всех бакетов интервала показателя
        (то есть ключи всех собранных статистик по интервалу показателя).
        """
        ts_end = time.time() if ts_end is None else ts_end
        keys = self.redis.keys("%s:%s:%s:%s" % (self.name, interval_name, name, "*"))
        str_keys = (k for k in keys)
        all_ts = (int(k[k.rfind(':') + 1:]) for k in str_keys)
        return (ts for ts in all_ts if ts_start <= ts <= ts_end)

    def bulk_update_topchart(self, name, item_ids, amount=1, ts=None):
        """
        Добавить элементы в рейтинг (redis' ordered set).

        :type name: str
        :type item_ids: unknown
        :type amount: int
        :type ts: int
        :rtype: None
        """
        ts = time.time() if ts is None else ts
        with self.redis.pipeline() as pipe:
            for i in item_ids:
                self.update_topchart(name, i, amount=amount, ts=ts, pipeline=pipe)
            pipe.execute()

    def update_topchart(self, name, item_id, **kwargs):
        """
        Добавить элемент в рейтинг (redis' ordered set).

        :type name: str
        :type item_id: int
        :rtype: None
        """
        return self._update_stat_value(name,
                                       lambda rds, key, amount: rds.zincrby(key, item_id, amount),
                                       **kwargs)

    def get_topchart_union(self, interval, name, bucket_weights, top_items, key_fn=lambda x: x):
        """
        Получить топ из сложения нескольких бакетов с удельными весами.

        :type interval: str
        :type name: str
        :type bucket_weights: dict[int, int]
        :type top_items: int
        :rtype: list[int]
        """
        result_with_scores = self.get_topchart_union_with_scores(interval, name, bucket_weights, top_items,
                                                                 key_fn=key_fn)
        return [i for i, _ in result_with_scores]

    def get_topchart_union_with_scores(self, interval, name, bucket_weights, top_items, key_fn=lambda x: x):
        """
        Получить топ (со скорами) из сложения нескольких бакетов с удельными весами.

        :type interval: str
        :type name: str
        :type bucket_weights: dict[int, int]
        :type top_items: int
        :rtype: list[(int, int)]
        """
        key_weights = {self._gen_key(interval, name, ts): weight for ts, weight in bucket_weights.items()}
        pipe = self.redis.pipeline()

        union_key = '%s:%s:%s:tmp_topchart_union:%s' % (self.name, interval, name, uuid.uuid4())
        pipe.zunionstore(union_key, key_weights)
        pipe.zrevrangebyscore(union_key, '+inf', 1, start=0, num=top_items, withscores=True, score_cast_func=int)
        pipe.delete(union_key)

        result = pipe.execute()[1]
        return [(key_fn(k), v) for k, v in result]
