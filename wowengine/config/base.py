from datetime import timedelta

class BaseConfig(object):
    """
    Базовая конфигурация, не используется напрямую. Пользуйтесь DevConfig или ProductionConfig.
    """
    # Это таймаут кэша для проверки актуальности (неоткрученности) источников. Играет довольно важную роль:
    # слишком низкое значение повышает нагрузку на сервер,
    # слишком высокое - повышает перекрутку баннеров сверх лимита.
    #
    # на старом сайте это была минута (потому что чаще, чем раз в минуту cron не работает)
    SOURCES_CHECK_TIMEOUT = 120

    # Это таймаут для кэширования методов, которые возвращают случайные выборки.
    # Так как обычно случайность выборки нужна только в рамках одного пользователя, а один пользователь
    # вряд ли читает страницу меньше, чем за пару секунд, тут можно немного покэшировать.
    RANDOM_ACTION_TIMEOUT = 5

    WOW_SITE_NAME = 'wow-impulse'
    IMAGES_NETLOC = None  # For example: //img.wow-impulse.net

    PERMANENT_SESSION_LIFETIME = timedelta(days=31)

    # valid admin credentials: a list of tuples (user, password)
    ADMIN_USERS = [('admin', 'Aikam3Ae')]
    ROOT_EMAIL = 'info@wow-impulse.ru'
    ROOT_PASSWORD = '123'

    MYSQL_UNIX_SOCKET = '/var/run/mysqld/mysqld.sock'
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://wowimpulse:vova@localhost/wowimpulse_new?charset=utf8&unix_socket=%s' % MYSQL_UNIX_SOCKET
    SQLALCHEMY_LEGACY_DATABASE_URI = 'mysql+pymysql://wowimpulse:vova@localhost/wowimpulse?charset=utf8&unix_socket=%s' % MYSQL_UNIX_SOCKET
    SECRET_KEY = 'XPhhEJBFpu8gpHPurSMg'

    CACHE_TYPE = 'redis'
    CACHE_KEY_PREFIX = 'flaskCache'

    REDIS_UNIX_SOCKET = '/var/run/redis/redis.sock'
    # не будем использовать нулевую базу, ее легче всего испортить т.к. она подключается по умолчанию
    REDIS_DB = 3
    CACHE_REDIS_DB_ = 1
    CELERY_REDIS_DB = 2

    # брокер асинхронных сообщений и периодических задач
    BROKER_URL = 'redis://localhost:6379/%d' % (CELERY_REDIS_DB)
    CELERY_IMPORTS = (
        'wowengine.tasks',
    )
    CELERY_IGNORE_RESULT = True

    # кэш (средствами Flask-Cache)
    CACHE_REDIS_URL = 'redis+socket://%s?db=%d' % (REDIS_UNIX_SOCKET, CACHE_REDIS_DB_)

    # база для сбора статистики по сайту
    REDIS_URL = 'file://%s' % REDIS_UNIX_SOCKET

    CSRF_ENABLED = True
    CSRF_SESSION_KEY = "56KaqXTNz8fJM4pUWFRw"

    JSON_AS_ASCII = False

    # куда загружаются картинки для новостей (путь относительно app.static_folder)
    IMG_UPLOAD_DIR = 'images/news'

    # относительный путь для урлов
    IMG_DIR_URL = '/images/news'

    UGLIFYJS_EXTRA_ARGS = ['-c', '-m']

    # настройки дизайна
    LAYOUT = {}

    # Логирование (аудит) REST-API
    ENABLE_REST_API_AUDIT = True

    # Редирект отдельных страниц на другой домен (пример значения http://wow-impulse.ru - без слеша в конце)
    # Эта фича делалась для тестового запуска, и вероятно, станет не нужна после полноценного запуска
    REDIRECT_AHTML_URL = None
    REDIRECT_DHTML_URL = None
    REDIRECT_MAINPAGE_URL = None

    SHOW_ECHOBANNERS_BANNERS = True  # probably temporary feature
    BANNER_RIGHT_COL = False

    FRONTEND_TEMPLATE_FOLDER = 'templates'

    OVERALL_AUCTION = False
    OVERALL_ITEM_ID = 'ALL'

    MAIL_RU_PARAM = '' #если нужно определять по параметру, ставим, например MAIL_RU_PARAM = 'frommail'

    SHOWN_TRACKER_KEY = 'SHWON_TRACKER_QUEUE'
    FAST_TRACKER_QUEUE_KEY = 'FAST_TRACKER_QUEUE'

    # every how many frames to show ads while gallery_from_hell
    GALLERY_ADS_FREQ = 3

    # AUTH SETTINGS
    # Also see documentation on the https://pythonhosted.org/Flask-Security/
    SECURITY_LOGIN_URL = '/ed/login'
    SECURITY_LOGOUT_URL = '/logout'
    SECURITY_POST_LOGIN_VIEW = '/ed/admin'
    SECURITY_PASSWORD_HASH = 'pbkdf2_sha512'
    SECURITY_PASSWORD_SALT = '0d9192c5ffe34e63bb4d6c1db197dd16'
    SECURITY_CONFIRMABLE = False
    SECURITY_LOGIN_WITHOUT_CONFIRMATION = True
    SECURITY_RECOVERABLE = True

    # customization templates
    SECURITY_LOGIN_USER_TEMPLATE = 'login.html'
    SECURITY_FORGOT_PASSWORD_TEMPLATE = 'forgot_password.html'
    SECURITY_RESET_PASSWORD_TEMPLATE = 'reset_password.html'

    # localization messages
    SECURITY_EMAIL_SUBJECT_CONFIRM = 'Пожалуйста подтвердите свою электронную почту'
    SECURITY_EMAIL_SUBJECT_PASSWORD_CHANGE_NOTICE = 'Ваш пароль был изменен'
    SECURITY_EMAIL_SUBJECT_PASSWORD_NOTICE = 'Ваш пароль был сброшен'
    SECURITY_EMAIL_SUBJECT_PASSWORD_RESET = 'Восстановление пароля'
    SECURITY_EMAIL_SUBJECT_PASSWORDLESS	= 'Инструкция по аутентификации'
    SECURITY_EMAIL_SUBJECT_REGISTER	= 'Добро пожаловать'
    SECURITY_MSG_ALREADY_CONFIRMED	= ('Ваш адрес электронной почты уже был подтвержден.', 'info')
    SECURITY_MSG_CONFIRM_REGISTRATION = ('Спасибо. Инструкции по подтверждению были отправлены на %(email)s.',
                                         'success')
    SECURITY_MSG_CONFIRMATION_EXPIRED = ('Вы не подтвердили свой адрес электронной почты в течении %(within).'
                                         ' Новые инструкции по подтверждению будут высланы на адрес %(email)s.',
                                         'error')
    SECURITY_MSG_CONFIRMATION_REQUEST = ('Инструкция по подтверждению была выслана на адрес %(email)s.', 'info')
    SECURITY_MSG_CONFIRMATION_REQUIRED = ('Адрес электронной почты требует подтверждения.', 'error')
    SECURITY_MSG_DISABLED_ACCOUNT = ('Учётная запись заблокирована', 'error')
    SECURITY_MSG_EMAIL_ALREADY_ASSOCIATED = ('Адрес %(email) уже был ассоциирован с аккаунтом.', 'error')
    SECURITY_MSG_EMAIL_CONFIRMED = ('Спасибо. Ваш email подтвержден.', 'success')
    SECURITY_MSG_EMAIL_NOT_PROVIDED = ('Введите email', 'error')
    SECURITY_MSG_INVALID_CONFIRMATION_TOKEN = ('Неверный ключ подтверждения.', 'error')
    SECURITY_MSG_INVALID_EMAIL_ADDRESS = ('Неверный адрес электронной почты', 'error')
    SECURITY_MSG_INVALID_LOGIN_TOKEN = ('Неверный ключ аутентификации.', 'error')
    SECURITY_MSG_INVALID_PASSWORD = ('Неверный пароль', 'error')
    SECURITY_MSG_INVALID_REDIRECT = ('Переадресация вне домена запрещена', 'error')
    SECURITY_MSG_INVALID_RESET_PASSWORD_TOKEN = ('Неверный ключ сброса пароля.', 'error')
    SECURITY_MSG_LOGIN = ('Пожалуйста авторизуйтесь на этой странице.', 'info')
    SECURITY_MSG_LOGIN_EMAIL_SENT = ('Инструкция для авторизации отправлена на адрес %(email).', 'success')
    SECURITY_MSG_LOGIN_EXPIRED = ('Вы не прошли аутентификацию в течении %(within). '
                                  'Новая инструкция будет выслана на адрес %(email)s.', 'error')
    SECURITY_MSG_PASSWORD_CHANGE = ('Вы успешно сменили пароль.', 'success')
    SECURITY_MSG_PASSWORD_INVALID_LENGTH = ('Пароль должен содержать не менее 6 символов', 'error')
    SECURITY_MSG_PASSWORD_IS_THE_SAME = ('Ваш новый пароль должен отличаться от старого', 'error')
    SECURITY_MSG_PASSWORD_MISMATCH = ('Пароль не подходит.', 'error')
    SECURITY_MSG_PASSWORD_NOT_PROVIDED = ('Неверный пароль', 'error')
    SECURITY_MSG_PASSWORD_NOT_SET = ('Неверный пароль для этого пользователя', 'error')
    SECURITY_MSG_PASSWORD_RESET = ('Вы успешно сбросили пароль и будете автоматически авторизированы.', 'success')
    SECURITY_MSG_PASSWORD_RESET_EXPIRED = ('Вы не сбросили пароль в течении %(within). '
                                           'Новые инструкции будут высланы на адрес %(email).', 'error')
    SECURITY_MSG_PASSWORD_RESET_REQUEST = ('Вам выслано письмо для восстановления пароля', 'error')
    SECURITY_MSG_PASSWORDLESS_LOGIN_SUCCESSFUL = ('Вы успешно авторизированы.', 'success')
    SECURITY_MSG_REFRESH = ('Пожалуйста пройдите повторную процедуру аутентификации для доступа к этой странице.', 'info')
    SECURITY_MSG_RETYPE_PASSWORD_MISMATCH = ('Пароли не совпадает', 'error')
    SECURITY_MSG_UNAUTHORIZED = ('Вы не имеете доступа к этому ресурсу.', 'error')
    SECURITY_MSG_USER_DOES_NOT_EXIST = ('Пользователь не существует', 'error')




    # MAIL SETTINGS
    # If you haven't mail server, you can execute the command for launch of a smtp server:
    # python -m smtpd -c DebuggingServer -n localhost:2525
    # Also you can use free mailbox on the service gmail.com etc.
    # You can see more information to https://pythonhosted.org/Flask-Mail/
    MAIL_DEFAULT_SENDER = 'wow.impulse@yandex.ru'
    SECURITY_EMAIL_SENDER = 'wow.impulse@yandex.ru'
    # MAIL_DEFAULT_SENDER = 'noreply@localhost'
    # MAIL_SERVER = 'localhost'
    # MAIL_PORT = 25
    MAIL_SERVER = 'smtp.yandex.ru'
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_USERNAME = 'wow.impulse'
    MAIL_PASSWORD = 'qwertyuiop[]'

    STATSD_HOST = None  # disabled by default
    STATSD_PORT = 8125
    STATSD_PREFIX = 'dev'
