from .base import BaseConfig

class Config(BaseConfig):
    """
    Конфигурация для продакшен-сервера.
    """
    SERVER_NAME = 'wow-impulse.net'
    IMAGES_NETLOC = 'http://img.wow-impulse.net'
    JSONIFY_PRETTYPRINT_REGULAR = False

    SOURCES_CHECK_TIMEOUT = 60

    SHOW_ECHOBANNERS_BANNERS = True

    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://wowimpulse:vova@localhost/wowimpulse_net?charset=utf8&unix_socket=%s' % BaseConfig.MYSQL_UNIX_SOCKET
    SQLALCHEMY_LEGACY_DATABASE_URI = 'mysql+pymysql://wowimpulse:vova@wow-impulse.ru/wowimpulse?charset=utf8'

    REDIS_DB = 10
    CACHE_REDIS_DB_ = 11
    CELERY_REDIS_DB = 12
   
    REDIS_UNIX_SOCKET = '/var/run/redis/redis.sock'
    BROKER_URL = 'redis://localhost:6379/%d' % (CELERY_REDIS_DB)
    CACHE_REDIS_URL = 'redis+socket://%s?db=%d' % (REDIS_UNIX_SOCKET, CACHE_REDIS_DB_)
    REDIS_URL = 'file://%s' % REDIS_UNIX_SOCKET

    NEWRELIC_WEBAPP_ID = '8804339'
    NEWRELIC_LICENSE_KEY = '8cbcbd5926'

    GOOGLE_ANALYTICS_ID = 'UA-64468426-2'

    STATSD_HOST = 'localhost'
    STATSD_PREFIX = 'prod'
