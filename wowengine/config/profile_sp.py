# Профиль для инсталляции, на которой будет совершаться "пробный запуск"
# SP = Special Project

from .base import BaseConfig

class Config(BaseConfig):
    SERVER_NAME = 'test.wow-impulse.net'
    _MAIN_INSTALLATION_URL = 'http://wow-impulse.net'
    OVERALL_AUCTION = False
    DEBUG = False

    REDIRECT_AHTML_URL = _MAIN_INSTALLATION_URL
    REDIRECT_DHTML_URL = _MAIN_INSTALLATION_URL
    REDIRECT_MAINPAGE_URL = _MAIN_INSTALLATION_URL

    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://wowimpulse:vova@localhost/wowimpulse_sp?charset=utf8&unix_socket=%s' % BaseConfig.MYSQL_UNIX_SOCKET

    SECRET_KEY = 'boy5pJLMDK1gr6CUWYef'
    CSRF_SESSION_KEY = "kc0SVCBz1GlsktgBgktH"

    REDIS_DB = 30
    CACHE_REDIS_DB_ = 31
    CELERY_REDIS_DB = 32

    BROKER_URL = 'redis://localhost:6379/%d' % (CELERY_REDIS_DB)
    CACHE_REDIS_URL = 'redis+socket://%s?db=%d' % (BaseConfig.REDIS_UNIX_SOCKET, CACHE_REDIS_DB_)

    SHOW_ECHOBANNERS_BANNERS = True  # probably temporary feature
    
    FRONTEND_TEMPLATE_FOLDER = 'templates'

    SQLALCHEMY_LEGACY_DATABASE_URI = 'mysql+pymysql://wowimpulse:vova@wow-impulse.ru/wowimpulse?charset=utf8'

    FALLBACK_TO_PRODUCTION_IMAGES = True
    PRODUCTION_HOSTNAME = 'wow-impulse.net'

    GOOGLE_ANALYTICS_ID = 'UA-64468426-1'

    NEWRELIC_WEBAPP_ID = '9437269'
    NEWRELIC_LICENSE_KEY = '8cbcbd5926'

