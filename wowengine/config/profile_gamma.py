from .base import BaseConfig

class Config(BaseConfig):
    """
    Конфигурация для продакшен-сервера.
    """
    SERVER_NAME = 'gamma.wow-impulse.ru'
    JSONIFY_PRETTYPRINT_REGULAR = False
    _MAIN_INSTALLATION_URL = 'http://wow-impulse.net'

    REDIRECT_AHTML_URL = _MAIN_INSTALLATION_URL
    REDIRECT_DHTML_URL = _MAIN_INSTALLATION_URL
    REDIRECT_MAINPAGE_URL = _MAIN_INSTALLATION_URL
    
    BANNER_RIGHT_COL = True
    SHOW_ECHOBANNERS_BANNERS = True

    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://wowimpulse:vova@localhost/wowimpulse_gamma?charset=utf8&unix_socket=%s' % BaseConfig.MYSQL_UNIX_SOCKET
    SQLALCHEMY_LEGACY_DATABASE_URI = 'mysql+pymysql://wowimpulse:vova@wow-impulse.ru/wowimpulse?charset=utf8'
