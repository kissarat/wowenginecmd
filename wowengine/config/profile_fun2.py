from .base import BaseConfig

class Config(BaseConfig):
    """
    Конфигурация для продакшен-сервера.
    """
    SERVER_NAME = '2.funnetwork.ru'
    JSONIFY_PRETTYPRINT_REGULAR = False
    _MAIN_INSTALLATION_URL = 'http://funnetwork.ru'
    
    OVERALL_AUCTION = True
    OVERALL_ITEM_ID = 'ALL'

    REDIRECT_AHTML_URL = _MAIN_INSTALLATION_URL
    REDIRECT_MAINPAGE_URL = _MAIN_INSTALLATION_URL

    SHOW_ECHOBANNERS_BANNERS = True

    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://wowimpulse:vova@localhost/wowimpulse_fun2?charset=utf8&unix_socket=%s' % BaseConfig.MYSQL_UNIX_SOCKET
    SQLALCHEMY_LEGACY_DATABASE_URI = 'mysql+pymysql://wowimpulse:vova@wow-impulse.ru/wowimpulse_fun2?charset=utf8'
    
    REDIS_DB = 13
    CACHE_REDIS_DB_ = 14
    CELERY_REDIS_DB = 15

    BROKER_URL = 'redis://localhost:6379/%d' % (CELERY_REDIS_DB)
    CACHE_REDIS_URL = 'redis+socket://%s?db=%d' % (BaseConfig.REDIS_UNIX_SOCKET, CACHE_REDIS_DB_)

    PRODUCTION_HOSTNAME = 'funnetwork.ru'
    FALLBACK_TO_PRODUCTION_IMAGES = True
