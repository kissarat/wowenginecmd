# coding: utf-8

from .base import BaseConfig


class Config(BaseConfig):
    """
    Конфигурация для разработки.
    """
    DEBUG = False
    ASSETS_DEBUG = False
    CSRF_ENABLED = False

    # если не найден файл с картинкой, редиректить на главный сервер
    # нужно только на копии разработчика, чтобы не было битых картинок на локальном сайте
    FALLBACK_TO_PRODUCTION_IMAGES = True
    PRODUCTION_HOSTNAME = 'wow-impulse.net'

    ADMIN_USERS = [('admin', 'Nepao7da')]

    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://wowimpulse:vova@localhost/wowimpulse_test3?charset=utf8&unix_socket=%s' % BaseConfig.MYSQL_UNIX_SOCKET

    DEBUG_TB_INTERCEPT_REDIRECTS = False

    SERVER_NAME = 'test3.wow-impulse.net'

    REDIS_DB = 38
    CACHE_REDIS_DB_ = 39
    CELERY_REDIS_DB = 40
   
    REDIS_UNIX_SOCKET = '/var/run/redis/redis.sock'
    BROKER_URL = 'redis://localhost:6379/%d' % (CELERY_REDIS_DB)
    CACHE_REDIS_URL = 'redis+socket://%s?db=%d' % (REDIS_UNIX_SOCKET, CACHE_REDIS_DB_)
    REDIS_URL = 'file://%s' % REDIS_UNIX_SOCKET
