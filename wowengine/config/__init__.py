import os

_config_dir = os.path.dirname(__file__)

if os.path.exists(os.path.join(_config_dir, 'profile.py')):
    from .profile import Config
else:
    from .profile_prod import Config

config = Config()
