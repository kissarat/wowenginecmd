from .base import BaseConfig

class Config(BaseConfig):
    """
    Конфигурация для разработки.
    """
    DEBUG = False
    SERVER_NAME = 'debug.wow-impulse.ru'
    ASSETS_DEBUG = True
    CSRF_ENABLED = False
    ADMIN_USERS = [('admin', 'iesu8eeX')]

    # если не найден файл с картинкой, редиректить на главный сервер
    # нужно только на копии разработчика, чтобы не было битых картинок на локальном сайте
    FALLBACK_TO_PRODUCTION_IMAGES = True
    PRODUCTION_HOSTNAME = 'test.wow-impulse.net'

    _MAIN_INSTALLATION_URL = 'http://wow-impulse.net'
    OVERALL_AUCTION = False

    REDIRECT_AHTML_URL = _MAIN_INSTALLATION_URL
    REDIRECT_DHTML_URL = _MAIN_INSTALLATION_URL
    REDIRECT_MAINPAGE_URL = _MAIN_INSTALLATION_URL
    
    BANNER_RIGHT_COL = True

    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://wowimpulse:vova@localhost/wowimpulse_debug?charset=utf8&unix_socket=%s' % BaseConfig.MYSQL_UNIX_SOCKET

    DEBUG_TB_INTERCEPT_REDIRECTS = False

    FRONTEND_TEMPLATE_FOLDER = 'templates'
