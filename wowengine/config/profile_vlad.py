from .base import BaseConfig

class Config(BaseConfig):
    """
    Конфигурация для разработки.
    """
    DEBUG = False
    SERVER_NAME = 'vlad.wow-impulse.ru'
    ASSETS_DEBUG = True
    CSRF_ENABLED = False
    ADMIN_USERS = [('admin', 'iesu8eeX')]

    # если не найден файл с картинкой, редиректить на главный сервер
    # нужно только на копии разработчика, чтобы не было битых картинок на локальном сайте
    FALLBACK_TO_PRODUCTION_IMAGES = True
    PRODUCTION_HOSTNAME = 'test.wow-impulse.net'

    _MAIN_INSTALLATION_URL = 'http://wow-impulse.net'
    OVERALL_AUCTION = False

    REDIRECT_AHTML_URL = _MAIN_INSTALLATION_URL
    REDIRECT_DHTML_URL = _MAIN_INSTALLATION_URL
    REDIRECT_MAINPAGE_URL = _MAIN_INSTALLATION_URL
    
    BANNER_RIGHT_COL = True

    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://wowimpulse:vova@localhost/wowimpulse_vlad?charset=utf8&unix_socket=%s' % BaseConfig.MYSQL_UNIX_SOCKET

    DEBUG_TB_INTERCEPT_REDIRECTS = False

    REDIS_DB = 13
    CACHE_REDIS_DB_ = 11
    CELERY_REDIS_DB = 12

    REDIS_UNIX_SOCKET = '/var/run/redis/redis.sock'
    # брокер асинхронных сообщений и периодических задач
    BROKER_URL = 'redis://localhost:6379/%d' % (CELERY_REDIS_DB)
    # кэш (средствами Flask-Cache)
    CACHE_REDIS_URL = 'redis+socket://%s?db=%d' % (REDIS_UNIX_SOCKET, CACHE_REDIS_DB_)
