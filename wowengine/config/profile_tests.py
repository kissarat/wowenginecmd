# coding: utf-8

from .base import BaseConfig


class Config(BaseConfig):
    """
    Конфигурация для разработки.
    """
    DEBUG = True
    ASSETS_DEBUG = True
    CSRF_ENABLED = False

    # если не найден файл с картинкой, редиректить на главный сервер
    # нужно только на копии разработчика, чтобы не было битых картинок на локальном сайте
    FALLBACK_TO_PRODUCTION_IMAGES = True
    PRODUCTION_HOSTNAME = 'turbo.wow-impulse.ru'

    ADMIN_USERS = BaseConfig.ADMIN_USERS + [('foo', 'bar')]
    SQLALCHEMY_TEST_DATABASE_URI = 'mysql+pymysql://root:root@localhost/wow_test?charset=utf8&unix_socket=%s' % BaseConfig.MYSQL_UNIX_SOCKET

    DEBUG_TB_INTERCEPT_REDIRECTS = False

    # local (vagrant guest) server
    SERVER_NAME = 'localhost:5000'

    REDIS_UNIX_SOCKET = '/tmp/redis.sock'
