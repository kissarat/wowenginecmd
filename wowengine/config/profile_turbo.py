# Профиль для инсталляции, на которой будет совершаться "пробный запуск"
# SP = Special Project

from .base import BaseConfig

class Config(BaseConfig):
    SERVER_NAME = 'funnetwork.ru'
    _MAIN_INSTALLATION_URL = 'http://wow-impulse.net'
    OVERALL_AUCTION = True
    OVERALL_ITEM_ID = 'ALL'
    
    REDIRECT_AHTML_URL = _MAIN_INSTALLATION_URL
    REDIRECT_DHTML_URL = _MAIN_INSTALLATION_URL
    REDIRECT_MAINPAGE_URL = _MAIN_INSTALLATION_URL

    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://wowimpulse:vova@localhost/wowimpulse_turbo?charset=utf8&unix_socket=%s' % BaseConfig.MYSQL_UNIX_SOCKET
    SQLALCHEMY_LEGACY_DATABASE_URI = 'mysql+pymysql://wowimpulse:vova@localhost/wowimpulse_turbo_legacy?charset=utf8&unix_socket=%s' % BaseConfig.MYSQL_UNIX_SOCKET

    SECRET_KEY = 'JwaqkvVIF6rcftPC0OMP'
    CSRF_SESSION_KEY = "BajbCo1RZ3GogeVrLdfe"

    REDIS_DB = 35
    CACHE_REDIS_DB_ = 36
    CELERY_REDIS_DB = 37

    BROKER_URL = 'redis://localhost:6379/%d' % (CELERY_REDIS_DB)
    CACHE_REDIS_URL = 'redis+socket://%s?db=%d' % (BaseConfig.REDIS_UNIX_SOCKET, CACHE_REDIS_DB_)

    SHOW_ECHOBANNERS_BANNERS = True  # probably temporary feature
    
    PRODUCTION_HOSTNAME = 'wow-impulse.net'
    FALLBACK_TO_PRODUCTION_IMAGES = True
