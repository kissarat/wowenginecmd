from geoip2 import database
from geoip2.errors import AddressNotFoundError
from maxminddb import MODE_MMAP, MODE_MMAP_EXT
from flask import request, session
import time
import io
import zipfile
import csv
import os
import yaml


def timeit(method):
    """
    Декоратор замера времени выполнения методов.
    """
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        print('%r (%r, %r) %2.6f msec' % (method.__name__, args, kw, (te - ts)*1000))
        return result

    return timed


class UpdateBinaryDatabaseException(Exception):
    """
    Ошибка при обновлении mmdb баз geoip.
    """
    def __init__(self, *args, **kwargs):
        super(Exception, self).__init__(*args, **kwargs)
        self.message = 'While updating geolocation BINARY databases an error occurred'


class UpdateCSVDatabaseException(Exception):
    """
    Ошибка при обновлении csv баз geoip.
    """
    def __init__(self, *args, **kwargs):
        super(Exception, self).__init__(*args, **kwargs)
        self.message = 'While updating geolocation CSV databases an error occurred'


class GeoIPCache():
    """
    Класс реализует доступ к локальному geoip кешу, с целью сократить
    количество обращений к базе maxmind.
    """
    def get_country_geoname_id(self, ip):
        pass

    def get_city_geoname_id(self, ip):
        pass


class Geo():
    """
    Класс обертка вокруг maxmind geoip2 API.
    """
    cache = GeoIPCache()
    _url = 'http://geolite.maxmind.com/download/geoip/database/{product}-{content}.mmdb.gz'
    _csv_url = 'http://geolite.maxmind.com/download/geoip/database/{product}-{content}-CSV.zip'
    _db = 'wowengine/geo/db/bin/{product}-{content}.mmdb'
    _db_loaded = None
    _csv = 'wowengine/geo/db/csv/{product}-{content}-CSV.zip'
    _csv_loaded = None
    _yaml = 'wowengine/geo/db/yaml/{product}-{content}-{locales}.yaml'
    _yaml_loaded = None
    _config_yaml = 'wowengine/geo/db/yaml/config.yaml'
    _config_yaml_loaded = None
    license_key = '000000000000'
    product = 'GeoIP2'
    content = 'Country'
    locales = 'en'

    @property
    def db(self):
        return self._db.format(
            product=self.product, content=self.content
        )

    @property
    def csv(self):
        return self._csv.format(
            product=self.product, content=self.content
        )

    @property
    def yaml(self):
        return self._yaml.format(
            product=self.product,
            content=self.content,
            locales=self.locales
        )

    @property
    def url(self):
        return self._url.format(
            product=self.product, content=self.content
        )

    @property
    def csv_url(self):
        return self._csv_url.format(
            product=self.product, content=self.content
        )

    def __enter__(self):
        return self

    def __exit__(self, type, value, tb):
        self._reader.close()

    def __str__(self):
        return str({
            'ip': self.get_ip(),
            'country_geoname_id': self.get_country_geoname_id(),
            'city_geoname_id': self.get_city_geoname_id()
        })

    # @timeit
    def __init__(self, ip=None, locales=None):
        # Geo.update_yaml(locales='ru')  # Test
        # Geo.short_yaml(locale='ru')  # Test
        # self._reader = database.Reader(self.db, locales=locales, mode=MODE_MMAP)
        self._reader = database.Reader(self.db, locales=locales, mode=MODE_MMAP_EXT)
        if ip:
            self.ip = ip
        else:
            self.ip = request.remote_addr
        try:
            if self.content == 'City':
                self.response = self._reader.city(self.ip)
            elif self.content == 'Country':
                self.response = self._reader.country(self.ip)
        except AddressNotFoundError:
            self.response = None

    def _get_csv_raw(self, locales=None, product=None, content=None):
        _product = product or self.product
        _content = content or self.content
        _locales = locales or self.locales
        _file_name = '{product}-{content}-Locations-{locales}.csv'.format(
            product=_product,
            content=_content,
            locales=_locales
        )
        with zipfile.ZipFile(self.csv) as _csvfs:
            _names = _csvfs.namelist()
            if _locales:
                _matches = [
                    name for name in _names if name.endswith(_file_name)
                ]
                if _matches:
                    _name = _matches[0]
                else:
                    return None
                return io.TextIOWrapper(_csvfs.open(_name))

    def get_csv(self, locales=None, product=None, content=None):
        _product = product or self.product
        _content = content or self.content
        _locales = locales or self.locales
        _csv_raw = self._get_csv_raw(
            locales=_locales, product=_product, content=_content
        )
        return csv.reader(_csv_raw)

    def get_yaml(self, locales=None, product=None, content=None):
        if self._yaml_loaded:
            return self._yaml_loaded
        _product = product or self.product
        _content = content or self.content
        _locales = locales or self.locales
        _y = self._yaml.format(
            product=_product, content=_content, locales=_locales
        )
        if os.path.isfile(_y):
            with open(self.yaml, encoding='utf-8') as _yaml:
                return yaml.load(_yaml)

    @classmethod
    def update_yaml(cls, locales=None, product=None, content=None):
        _product = content or cls.product
        _content = product or cls.content
        _locales = locales or cls.locales

        def _get_csv_raw(loc, prod, cont):
            _file_name = '{product}-{content}-Locations-{locales}.csv'.format(
                product=prod, content=cont, locales=loc
            )
            _c = cls._csv.format(product=prod, content=cont)
            with zipfile.ZipFile(_c) as _csvfs:
                _names = _csvfs.namelist()
                if loc:
                    _matches = [
                        name for name in _names if name.endswith(_file_name)
                    ]
                    if _matches:
                        _name = _matches[0]
                    else:
                        return None
                    return io.TextIOWrapper(_csvfs.open(_name))

        try:
            _csv_raw_country = csv.reader(
                _get_csv_raw(loc=_locales, prod=_product, cont='Country')
            )
        except FileNotFoundError:
            _csv_raw_country = None
        try:
            _csv_raw_city = csv.reader(
                _get_csv_raw(loc=_locales, prod=_product, cont='City')
            )
        except FileNotFoundError:
            _csv_raw_city = None

        country_dict = {}
        if _csv_raw_country:
            is_first_row = True
            for row in _csv_raw_country:
                if is_first_row:
                    is_first_row = False
                    continue
                country_geoname_id, country_name = row[0], row[5]
                if len(country_name) == 0:
                    continue
                country_dict[country_name] = country_geoname_id

        country_city_dict = {}

        for country_name in country_dict:
            if len(country_name) == 0:
                continue
            country_geoname_id = country_dict[country_name]
            if country_geoname_id not in country_city_dict:
                country_city_dict[country_geoname_id] = {
                    'country_name': country_name,
                    'cities': {}
                }

        is_first_row = True
        if _csv_raw_city:
            for row in _csv_raw_city:
                if is_first_row:
                    is_first_row = False
                    continue
                city_geoname_id, country_name, city_name = row[0], row[5], row[10]
                if len(country_name) == 0 or len(city_name) == 0:
                    continue
                country_geoname_id = country_dict[country_name]
                if country_geoname_id in country_city_dict:
                    country_city_dict[country_geoname_id]['cities'].update({
                        city_geoname_id: city_name
                    })
                else:
                    country_city_dict[country_geoname_id] = {
                        'country_name': country_name,
                        'cities': {city_geoname_id: city_name}
                    }

        _yy = cls._yaml.format(locales=_locales, product=_product, content=_content)
        with open(_yy, 'w', encoding='utf-8') as _y:
            _y.write(
                yaml.dump(
                    country_city_dict,
                    default_flow_style=False,
                    allow_unicode=True
                )
            )

    @classmethod
    def load_config(cls):
        if os.path.isfile(cls._config_yaml):
            with open(cls._config_yaml, encoding='utf-8') as _config:
                return yaml.load(_config)

    @classmethod
    def set_config(cls, config):
        if os.path.isfile(cls._config_yaml):
            with open(cls._config_yaml, 'w', encoding='utf-8') as _config:
                _config.write(
                    yaml.dump(config, default_flow_style=False)
                )

    @classmethod
    def update_config(cls, config):
        cls.set_config(config)

    @classmethod
    def load_full_yaml(cls, locale=None, product=None, content=None):
        _product = product or cls.product
        _content = content or cls.content
        _locales = locale or cls.locales
        _y = cls._yaml.format(
            product=_product, content=_content, locales=_locales
        )
        with open(_y, encoding='utf-8') as _yaml:
            return yaml.load(_yaml)

    @classmethod
    def short_yaml(cls, locale=None):
        _product = cls.product
        _content = cls.content
        _loaded_full_yaml = cls._yaml_loaded or cls.load_full_yaml(
            locale=locale,
            product=_product,
            content=_content
        )
        _config = cls.load_config()
        _config_copy = _config.copy()
        for country_geoname_id in _config_copy:
            cities = _config[country_geoname_id]
            _config[country_geoname_id] = {}
            _config[country_geoname_id]['country_name'] = (
                _loaded_full_yaml[country_geoname_id]['country_name']
            )
            _config[country_geoname_id]['cities'] = {}
            for city in cities:
                _city = _loaded_full_yaml[country_geoname_id]['cities'].get(city, None)
                if not _city:
                    continue
                _config[country_geoname_id]['cities'].update({
                    city: _city
                })
        return _config

    @staticmethod
    def update_csv():
        success = True
        if success:
            print('Geolocation csv databases updated!')
        else:
            raise UpdateCSVDatabaseException()

    @staticmethod
    def update_db():
        success = True
        if success:
            print('Geolocation binary databases updated!')
        else:
            raise UpdateBinaryDatabaseException()

    @classmethod
    def update_db_csv(cls):
        cls.update_csv()
        cls.update_db()

    def get_ip(self):
        return self.ip

    # @timeit
    def get_country_geoname_id(self):
        if 'geoip' in session:
            return session['geoip']['country_geoname_id']
        incache = self.cache.get_country_geoname_id(self.ip)
        if incache:
            return incache
        if self.response:
            return str(self.response.country.geoname_id)

    # @timeit
    def get_city_geoname_id(self):
        if 'geoip' in session:
            return session['geoip']['city_geoname_id']
        incache = self.cache.get_city_geoname_id(self.ip)
        if incache:
            return incache
        if self.response:
            return str(self.response.city.geoname_id)

    @classmethod
    def get_country_name(cls, geoname_id):
        if cls._yaml_loaded:
            _g = cls._yaml_loaded.get(geoname_id, None)
            if not _g:
                return
            return _g['country_name']

    @classmethod
    def get_city_name(cls, geoname_id):
        if cls._yaml_loaded:
            for country_geoname_id in cls._yaml_loaded:
                if geoname_id in cls._yaml_loaded[country_geoname_id]['cities']:
                    return cls._yaml_loaded[country_geoname_id]['cities'][geoname_id]

    def close(self):
        self._reader.close()

    def apply(self):
        session.geoip = self

    @classmethod
    def init(cls):
        """
        Настраиваем и оптимизируем класс Geo перед использованием
        """
        cls.locales = 'ru'
        cls.product = 'GeoLite2'
        cls.content = 'City'
        cls._db_loaded = None
        cls._csv_loaded = None
        cls._yaml_loaded = cls.load_full_yaml()
        cls._config_yaml_loaded = None


Geo.init()


class SessionGeoIPStorage():
    """
    Класс абстрагирует доступ к geoip данным, спрятанным в session
    """
    def __init__(self):
        self.storage = session['geoip']

    def __str__(self):
        try:
            city_geoname_id = self.get_city_geoname_id()
        except AttributeError:
            city_geoname_id = ''
        return str({
            'ip': self.get_ip(),
            'country_geoname_id': self.get_country_geoname_id(),
            'city_geoname_id': city_geoname_id
        })

    def get_ip(self):
        return self.storage['ip']

    def get_city_geoname_id(self):
        if 'geoip' in session:
            return self.storage['city_geoname_id']

    def get_country_geoname_id(self):
        if 'geoip' in session:
            return self.storage['country_geoname_id']

    def apply(self):
        session.geoip = self