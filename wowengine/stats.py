from collections import defaultdict, Counter
from datetime import datetime, timedelta
from functools import reduce
import os
import csv
import io
import time
from urllib.request import urlopen
from .redis_timeseries import RedisTimeseries
from wowengine import util


class StatsResult(defaultdict):
    """
    Defaultdict со статистикой, с дополнительными полями, в которых хранятся
    таймстампы начала и конца периода, за который были выбраны данные
    """
    def __init__(self, default_factory, range_start=None, range_end=None):
        self.range_start = range_start
        self.range_end = range_end
        return super().__init__(default_factory)

    def __iadd__(self, other):
        """
        Поддержка оператора inplace adding для более удобной аггрегации
        данных за разные периоды
        """
        for k, v in other.items():
            self[k] += v

        self.range_start = min(self.range_start, other.range_start)
        self.range_end = max(self.range_end, other.range_end)
        return self


class SuperCache(object):
    """
    Flask-Cache умеет тупо хранить сериализованные объекты в виде ключ-значение.

    Этот класс - для более хитрых случаев.
    """
    CACHE_BANNER_IDS_IN_ROTATION_KEY = 'banner_ids_in_rotation'
    CACHE_GALLERY_IDS_KEY = 'gallery_ids'
    CACHE_TOP_STRIP_IDS_KEY = 'top_strip_ids'

    def __init__(self, redis_db, prefix='supercache'):
        self.redis = redis_db
        self.prefix = prefix

    def gen_key(self, key):
        return '%s:%s' % (self.prefix, key)

    def overwrite_set(self, key, l, chunk_size=1000):
        """
        Записать SET, стерев все предыдущие значения.
        :type key: str
        :type l: Iterable
        :rtype: None
        """
        chunks = util.partition(list(l), chunk_size)
        full_key = self.gen_key(key)
        with self.redis.pipeline() as pipe:
            pipe.delete(full_key)
            for chunk in chunks:
                pipe.sadd(full_key, *chunk)
            pipe.execute()

    def sample_from_set(self, key, count, key_fn=int):
        """
        Сделать случайную выборку из множества.
        """

        full_key = self.gen_key(key)
        return [key_fn(x) for x in self.redis.srandmember(full_key, count)]

    def sample_from_multiple_sets(self, keys, count, key_fn=int):
        """
        Сделать случайную выборку из объединения нескольких множеств
        """

        if isinstance(keys, str):
            return self.sample_from_set(keys, count, key_fn)

        if len(keys) == 1:
            return self.sample_from_set(keys[0], count, key_fn)

        tmp_key = self.gen_key('srandunion-temporary--pid-%d' % os.getpid())
        with self.redis.pipeline() as pipe:
            pipe.delete(tmp_key)
            pipe.sunionstore(tmp_key, *[self.gen_key(key) for key in keys])
            pipe.srandmember(tmp_key, count)
            result = pipe.execute()[-1]

        return [key_fn(x) for x in result]

    def smembers(self, key, key_fn=int):
        full_key = self.gen_key(key)
        return [key_fn(x) for x in self.redis.smembers(full_key)]


class LiveinternetStats(object):
    """
    Статистика из лирушечки за сегодня соу фар.
    """

    IN_URL = 'http://www.liveinternet.ru/stat/wow-impulse.ru/ref_servers.csv?COOKIE_pwd=1-Utn7HxWD3PQt/rFtg'
    OUT_URL = 'http://www.liveinternet.ru/stat/wow-impulse.ru/out.csv?COOKIE_pwd=1-Utn7HxWD3PQt/rFtg'

    @staticmethod
    def get_total_reading(url):
        """
        Скачать csv и вернуть первый параметр в строчке 'всего' - это за сегодняшний календарный день
        на текущий момент.
        """
        with urlopen(url) as csv_file:
            reader_in = io.TextIOWrapper(csv_file, encoding='utf8', newline='')
            reader = csv.reader(reader_in, delimiter=';')
            for row in reader:
                if len(row) >= 2 and row[0] == 'всего':
                    return int(row[1])

    @classmethod
    def get_current_total_in(cls):
        return cls.get_total_reading(cls.IN_URL)

    @classmethod
    def get_current_total_out(cls):
        return cls.get_total_reading(cls.OUT_URL)


class SourceStats(object):
    """
    Статистика откручивания источников.
    """

    def __init__(self, redis_db, statsd):
        self.t = RedisTimeseries(redis_db, 'sources', {
            'day': {
                'step': 'daily',
                'expire': '1d'
            },
            'total': {
                'step': 'eternity'
            }
        })
        self.statsd = statsd

    def statsd_incr_counter(self, key, items):
        if isinstance(items, dict):
            self.statsd.incr(key, sum(items.values()))
        else:
            self.statsd.incr(key, len(items))

    def register_clicked(self, id_or_ids, **kwargs):
        provider_id = kwargs.pop('provider_id', None)
        ids = [id_or_ids] if not isinstance(id_or_ids, list) else id_or_ids
        self.t.bulk_update_histogram('click', ids, **kwargs)
        if provider_id:
            self.t.bulk_update_histogram('click_%s' % provider_id, ids, **kwargs)
        self.statsd_incr_counter('source_clicks', ids)

    def register_shown(self, items, **kwargs):
        if isinstance(items, int):
            items = [items]
        self.t.bulk_update_histogram('show', items, **kwargs)
        self.statsd_incr_counter('source_shows', items)

    @staticmethod
    def _normalise_result(r):
        """
        Добавить вычисляемые и отсутствующие данные.

        :type r: dict[str, int]
        :rtype: None
        """
        r['ctr'] = r['click'] / r['show'] if r['show'] != 0 else 0

    def _get_for_single_source(self, interval, source_id, provider_id=None):
        """
        Статистика по одному источнику за некий интервал.

        :type interval: str
        :type source_id: int
        :rtype: dict[str, int]
        """
        names = [
            'show_%s' % provider_id, 'click_%s' % provider_id
        ] if provider_id else ['show', 'click']

        r = {
            name.split('_')[0]: self.t.get_histogram_value(interval, name, source_id)
            for name in names
        }
        self._normalise_result(r)
        return r

    def _get_for_all_sources(self, interval, provider_id=None):
        """
        Статистика по всем источникам за некий интервал.

        :type interval:  str
        :rtype: dict[int, dict[str, int]]
        """
        d = defaultdict(lambda: {'click': 0, 'show': 0, 'ctr': 0})

        names = [
            'show_%s' % provider_id, 'click_%s' % provider_id
        ] if provider_id else ['show', 'click']

        for name in names:
            h = self.t.get_histogram(interval, name, key_fn=int)
            for i, v in h.items():
                d[i][name.split('_')[0]] = v

        for i, r in d.items():
            self._normalise_result(r)

        return d

    def get_for_today(self, source_id, provider_id=None):
        """
        Статистика по источнику за сегодня.

        :rtype: dict[str, int]
        """
        return self._get_for_single_source('day', source_id, provider_id=provider_id)

    def get_total(self, source_id, provider_id=None):
        """
        Статистика по источнику за все время.

        :rtype: dict[str, int]
        """
        return self._get_for_single_source('total', source_id, provider_id=provider_id)

    def reset_total(self, source_id):
        interval = 'total'
        keys = [self.t._gen_key(interval, name, 0) for name in ('show', 'click')]
        for key in keys:
            self.t.redis.hset(key, source_id, 0)

    def get_all_for_today(self, provider_id=None):
        """
        Статистика по всем источникам за сегодня.

        :rtype: dict[int, dict[str, int]]
        """
        return self._get_for_all_sources('day', provider_id=provider_id)

    def get_all_total(self, provider_id=None):
        """
        Статистика по всем источникам за все время.

        :rtype: dict[int, dict[str, int]]
        """
        return self._get_for_all_sources('total', provider_id=provider_id)


class ArticleTopChart(object):
    """
    Статистика показов статей (для определения популярности).
    """

    def __init__(self, redis_db):
        self.t = RedisTimeseries(redis_db, 'articles_top_chart', {
            'day': {
                'step': 'daily',
                'expire': '2d'
            }
        })

    def register_shown(self, id_or_ids, **kwargs):
        """
        Добавить заход на материал или список материалов.
        """
        ids = [id_or_ids] if not isinstance(id_or_ids, list) else id_or_ids
        self.t.bulk_update_topchart('show', ids, **kwargs)

    def get_top_popular(self, top_items, ts=None):
        """
        Получить самые популярные материалы на конкретную дату путем слияния вчерашней статистики с сегодняшней.
        (Сегодняшние данные имеют более высокий коэффициент).

        :type top_items: int
        :type ts: int
        :rtype: list[int]
        """
        ts = time.time() if ts is None else ts
        yesterday_ts = ts - 86400
        return self.t.get_topchart_union('day', 'show', {ts: 2, yesterday_ts: 1}, top_items,
                                         key_fn=int)

    def get_top_popular_with_scores(self, top_items, ts=None):
        """
        Получить самые популярные материалы на конкретную дату путем слияния вчерашней статистики с сегодняшней.
        (Сегодняшние данные имеют более высокий коэффициент).

        :type top_items: int
        :type ts: int
        :rtype: list[int]
        """
        ts = time.time() if ts is None else ts
        yesterday_ts = ts - 86400
        return self.t.get_topchart_union_with_scores('day', 'show', {ts: 2, yesterday_ts: 1}, top_items,
                                                     key_fn=int)


class _ItemStats(object):
    """
    Статистика показов и кликов для контента.

    Так как нужна статистика за последние 5, 15, 30, 60 минут, а непрерывный поток статистики
    хранить тупо, то будем хранить последние 60 минутных интервалов, и статистику будем выдавать
    за последние N _полных_ минут. То есть в 00:09:40 выдавать в качестве последних пяти минут интервалы
    с 00:04:00 по 00:09:00.

    Данные хранятся в HASH, а не в ZSET, потому что мы пишем в статистику несоизмеримо чаще, чем считаем
    топы/ситиары, так что это O(1) против O(log(N)). А раз в несколько секунд посчитать топ можно и в питоне.
    """

    redis_key_name = None

    def __init__(self, redis_db, statsd):
        self.t = RedisTimeseries(redis_db, self.redis_key_name, {
            'min': {
                'step': '1m',
                'expire': '70m'
            },
            'day': {
                'step': 'daily'
            },
            'month': {
                'step': 'monthly'
            },
            'total': {
                'step': 'eternity'
            }
        })
        self.statsd = statsd

    def statsd_incr_counter(self, subkey, items):
        key = '%s_%s' % (self.redis_key_name, subkey)
        if isinstance(items, dict):
            self.statsd.incr(key, sum(items.values()))
        else:
            self.statsd.incr(key, len(items))

    def register_shown(self, items, **kwargs):
        """
        Добавить заход на материал или список материалов.
        """
        provider_id = kwargs.pop('provider_id', None)
        ids = [id_or_ids] if not isinstance(id_or_ids, list) else id_or_ids
        self.t.bulk_update_histogram('show', ids, **kwargs)
        if provider_id:
            self.t.bulk_update_histogram('show_%s' % provider_id, ids, **kwargs)
        else:
            self.t.bulk_update_histogram('show_without_provider', ids, **kwargs)
        self.statsd_incr_counter('shows', ids)

    def register_clicked(self, item_id, **kwargs):
        """
        Добавить переход по элементу или списку элементов
        """
        provider_id = kwargs.pop('provider_id', None)
        self.t.update_histogram('click', item_id, **kwargs)
        if provider_id:
            self.t.update_histogram('click_%s' % provider_id, item_id, **kwargs)
        else:
            self.t.update_histogram('click_without_provider', item_id, **kwargs)
        self.statsd_incr_counter('clicks', (item_id,))

    def register_visit(self, items, **kwargs):
        """
        Добавить заход на баннер как на страницу материала.
        """
        provider_id = kwargs.pop('provider_id', None)
        if isinstance(items, int):
            items = [items]
        self.t.update_histogram('in', items, **kwargs)
        if provider_id:
            self.t.update_histogram('in_%s' % provider_id, items, **kwargs)
        else:
            self.t.update_histogram('in_without_provider', items, **kwargs)
        self.statsd_incr_counter('in', items)

    def register_exit(self, item_id, **kwargs):
        """
        Добавить переход наружу.
        """
        provider_id = kwargs.pop('provider_id', None)
        self.t.update_histogram('out', item_id, **kwargs)
        self.statsd_incr_counter('out', (item_id,))

        if provider_id:
            self.t.update_histogram('out_%s' % provider_id, item_id, **kwargs)
        else:
            self.t.update_histogram('out_without_provider', item_id, **kwargs)

    def _get_names(self, d=None, provider_id=None):
        names = {
            'in': ['in'],
            'out': ['out'],
            'show': ['show'],
            'click': ['click']
        }
        if d and isinstance(d, dict):
            names.update(d)
        if provider_id:
            # names = {name: '%s_%s' % (name, provider_id) for name in names}
            for nm in names:
                names[nm].append('%s_%s' % (nm, provider_id))
        names.update({
            'show_without_provider': ['show_without_provider'],
            'click_without_provider': ['click_without_provider'],
            'in_without_provider': ['in_without_provider'],
            'out_without_provider': ['out_without_provider']
        })
        return names

    def _normalise_result_for_item(self, result, provider_id=None):
        names = self._get_names(
            d={'gen': ['gen'], 'ctr': ['ctr']},
            provider_id=provider_id
        )
        for x in names:
            for nm in names[x]:
                if nm not in result:
                    result[nm] = 0
        result[names['gen'][0]] = result[names['out'][0]] / result[names['in'][0]] if result[names['in'][0]] != 0 else 0
        result[names['ctr'][0]] = result[names['click'][0]] / result[names['show'][0]] if result[names['show'][0]] != 0 else 0
        if provider_id:
            l, i = 2, 1
            while i < l:
                result[names['gen'][i]] = result[names['out'][i]] / result[names['in'][i]] if result[names['in'][i]] != 0 else 0
                result[names['ctr'][i]] = result[names['click'][i]] / result[names['show'][i]] if result[names['show'][i]] != 0 else 0
                i += 1

    def _normalise_result(self, result, provider_id=None):
        """
        Добавить вычисляемые и отсутствующие данные.
        :param result:
        :return:
        """
        for _, d in result.items():
            self._normalise_result_for_item(d, provider_id=provider_id)

    def _safe_int(self, val):
        try:
            return int(val)
        except ValueError:
            return int(''.join(ch for ch in val if ch.isnumeric()))

    def _intervals_to_ranges(self, intervals):
        """
        Определяет интервалы начала и конца периода,
        за который запрашивается статистика.
        """
        first_step, first_ts = intervals[0]
        last_step, last_ts = intervals[-1]

        range_durations = {
            'min': 60,
            'day': 86400,
            'month': 31 * 86400,  # через 31 день будет 100% следующий месяц.
                                  # потом bucket_fn найдет первый день для него
        }
        if last_step in range_durations:
            last_ts += range_durations[last_step]

        return (datetime.fromtimestamp(self.t.intervals[first_step]['bucket_fn'](first_ts)),
                datetime.fromtimestamp(self.t.intervals[last_step]['bucket_fn'](last_ts) - 0.00001))

    def _get_interval(self, interval, ts=None, provider_id=None):
        """
        Возвращает значения в интервале в следующем виде:
        {item_id: Counter({'in':10, 'out':5, 'gen':0.5, 'show':40, 'click':10, 'ctr':0.25})}
        """
        ts = time.time() if ts is None else ts
        result = defaultdict(lambda: Counter())
        names = self._get_names(provider_id=provider_id)
        for x in names:
            for name in names[x]:
                data = self.t.get_histogram(
                    interval, name, key_fn=self._safe_int, ts=ts
                )
                for i, n in data.items():
                    result[i][name] = n

        self._normalise_result(result, provider_id=provider_id)

        return result

    def get_total_for_id(self, item_id, ts=None, provider_id=None):
        ts = time.time() if ts is None else ts
        r = {}
        names = self._get_names(provider_id=provider_id)
        for x in names:
            for name in names[x]:
                r[name] = self.t.get_histogram_value(
                    'total', name, item_id, ts=ts
                )

        self._normalise_result({item_id: r}, provider_id=provider_id)
        return r

    def get_total(self, **kwargs):
        return self._get_interval('total', **kwargs)

    def get_for_interval_range(self, intervals):
        result = StatsResult(lambda: Counter(), *self._intervals_to_ranges(intervals))
        for step, ts in intervals:
            result += self._get_interval(step, ts)

        self._normalise_result(result)
        return result

    def get_for_last_n_minutes(self, n, ts=None, provider_id=None):
        """
        Рассчитывает суммарную статистику за несколько последних минут.
        Работает только час, потом данные вымываются.
        """
        ts = time.time() if ts is None else ts
        result = defaultdict(lambda: Counter())
        for i in range(0, n):
            d = self._get_interval('min', ts - i * 60, provider_id=provider_id)
            for k, v in d.items():
                result[k] += v

        self._normalise_result(result, provider_id=provider_id)
        return result

    def get_for_5min(self, **kwargs):
        return self.get_for_last_n_minutes(5, **kwargs)

    def get_for_15min(self, **kwargs):
        return self.get_for_last_n_minutes(15, **kwargs)

    def get_for_30min(self, **kwargs):
        return self.get_for_last_n_minutes(30, **kwargs)

    def get_for_60min(self, **kwargs):
        return self.get_for_last_n_minutes(60, **kwargs)

    def _get_for_n_min_for_id(self, n, item_id, ts, provider_id=None):
        result = defaultdict(lambda: 0)
        for ts in range(int(ts), int(ts) + n * 60, 60):
            names = self._get_names(provider_id=provider_id)
            for x in names:
                for name in names[x]:
                    result[name] += self.t.get_histogram_value('min', name, item_id, ts=ts)
        self._normalise_result_for_item(result, provider_id=provider_id)
        return result

    def get_for_each_5min_in_60min(self, item_id, ts=None, provider_id=None):
        """
        Returns stats for each 5 minute interval of 60 minutes.

        :param ts: Start time of the interval, current time by default.
        """
        ts = int(time.time() if ts is None else ts)
        return [
            (
                datetime.fromtimestamp(start),
                self._get_for_n_min_for_id(
                    5, item_id, start, provider_id=provider_id
                )
            ) for start in range(ts, ts + 60 * 60, 5 * 60)
        ]

    def get_for_today(self, ts=None, provider_id=None):
        ts = time.time() if ts is None else ts
        return self._get_interval('day', ts, provider_id=provider_id)

    def get_for_yesterday(self, ts=None, provider_id=None):
        ts = time.time() if ts is None else ts
        yesterday_ts = ts - 86400
        return self._get_interval('day', yesterday_ts, provider_id=provider_id)

    def get_for_month(self, ts=None, provider_id=None):
        ts = time.time() if ts is None else ts
        return self._get_interval('month', ts, provider_id=provider_id)

    def get_for_range(self, start: datetime, end: datetime, provider_id=None):
        """
        Returns stats for range from `start` to `end`.
        """
        def _get_intervals():
            for offset in range((end - start).days):
                delta = timedelta(days=offset)
                day = (delta + start).timestamp()
                yield self._get_interval('day', day, provider_id=provider_id)

        def _merge_results(memo, interval):
            memo += interval
            return memo

        result = reduce(_merge_results, _get_intervals(), defaultdict(Counter))
        self._normalise_result(result, provider_id=provider_id)

        return result

    get_for_all_time = get_total


class BannerStats(_ItemStats):
    redis_key_name = 'banners'

    def get_average_ctrs(self, min_show=1000, ts=None):
        """
        Рассчитывает средний ctr для всех засвеченных баннеров на основе взвешенной статистики за вчера и сегодня.
        Данные за сегодня имеют коэффициент x2.
        """
        ts = time.time() if ts is None else ts
        result = defaultdict(lambda: defaultdict(lambda: 0))
        for name in ['show', 'click']:
            #yesterday = self.t.get_histogram('day', name, key_fn=int, ts=ts-86400)  # yesterday
            today = self.t.get_histogram('day', name, key_fn=int, ts=ts)

            #for i, n in yesterday.items():
                #result[i][name] = n

            for i, n in today.items():
                result[i][name] = n

        self._normalise_result(result)

        return {i: d['ctr'] for i, d in result.items()}


class LineStats(_ItemStats):
    redis_key_name = 'lines'


class ArticleStats(_ItemStats):
    redis_key_name = 'articles'


class GenerationStats(object):
    """
    Статистика заходов/уходов (генерации).
    """

    def __init__(self, redis_db, statsd):
        self.t = RedisTimeseries(redis_db, 'gen', {
            '5min': {
                # todo довольно накладно и неразнумно хранить пятиминутки и часы в редисе вечно,
                # todo нужно либо экспайрить либо архивировать данные в mysql, а здесь сделать прозрачный fallback
                # todo или не здесь, а в классе RedisTimeseries
                'step': '5m',
                'expire': '14d'
            },
            'hour': {
                'step': '1h'
            },
            'day': {
                'step': 'daily'
            }
        })
        self.statsd = statsd

    def register_visit(self, **kwargs):
        """
        Добавить заход на сайт.
        """
        self.t.update_histogram('site', 'in', **kwargs)
        self.statsd.incr('generation_in', kwargs.get('amount', 1))

    def register_pre_visit(self, **kwargs):
        """
        Добавить заход на сайт без окончания отрисовки.
        """
        self.t.update_histogram('site', 'pre_in', **kwargs)
        self.statsd.incr('generation_pre_in', kwargs.get('amount', 1))

    def register_ext_visit(self, **kwargs):
        """
        Добавить заход на сайт с внешним реферером. 
        """
        provider_id = kwargs.pop('provider_id', None)
        self.t.update_histogram('site', 'ext_in', **kwargs)
        if provider_id:
            self.t.update_histogram('site', 'ext_in_%s' % provider_id, **kwargs)
        self.statsd.incr('generation_ext_in', kwargs.get('amount', 1))


    def register_overall_daily_price(self, current_price, **kwargs):
        """
        Добавит общейсайтовую цену 
        """
        day_values = self.get_for_day() 

        if 'overall_price' in day_values:
           prev_price = day_values['overall_price']
        else:
           prev_price = 0

        print("CURRENT %s" % current_price)
        delta = float(current_price)-prev_price
        print("DELTA %s" % delta)
        self.t.update_float_histogram('site', 'overall_price', amount=delta, **kwargs)

    def register_exit(self, **kwargs):
        """
        Добавить уход с сайта наружу.
        """
        self.t.update_histogram('site', 'out', **kwargs)
        self.statsd.incr('generation_out', kwargs.get('amount', 1))

    def register_li_visit(self, **kwargs):
        """
        Добавить заход на сайт по версии лиру.
        """
        self.t.update_histogram('site', 'li_in', **kwargs)

    def register_li_exit(self, **kwargs):
        """
        Добавить уход с сайта наружу по версии лиру.
        """
        self.t.update_histogram('site', 'li_out', **kwargs)

    def fetch_and_register_liveinternet_stats(self):
        """
        Забрать статистику лирушечки и записать в нашу статистику генерации.
        Для наилучшего эффекта вызывать раз в пять минут.
        """
        li_in = LiveinternetStats.get_current_total_in()
        li_out = LiveinternetStats.get_current_total_out()

        # Текущие показатели это показатель с лиру минус предыдущий показатель за сегодня.
        so_far = self.get_for_day()
        new_in = li_in - so_far['li_in']
        new_out = li_out - so_far['li_out']

        self.register_li_visit(amount=new_in)
        self.register_li_exit(amount=new_out)

    @staticmethod
    def _result(d, provider_id=None):
        """
        Посчитать показатель генерации, учитывая значения по умолчанию.
        :type d: dict[str, int]
        :rtype: dict[str, int]
        """
        _in = d.get('in', 0)
        _overall_price = d.get('overall_price', 0)
        _ext_in_per_provider = d.get('ext_in_%s' % provider_id, 0)
        _pre_in = d.get('pre_in', 0)
        _ext_in = d.get('ext_in', 0)
        _out = d.get('out', 0)
        _gen = _out / _in if _in != 0 else 0

        _li_in = d.get('li_in', 0)
        _li_out = d.get('li_out', 0)
        _li_gen = _li_out / _li_in if _li_in != 0 else 0

        return {
            'in': _in,
            'ext_in_per_provider': _ext_in_per_provider,
            'pre_in': _pre_in,
            'ext_in': _ext_in,
            'out': _out,
            'gen': _gen,
            'li_in': _li_in,
            'li_out': _li_out,
            'li_gen': _li_gen,
            'overall_price': _overall_price
        }

    def get_for_interval(self, interval, **kwargs):
        """
        Данные для отрезка interval.
        """
        provider_id = kwargs.pop('provider_id', None)
        return self._result(
            self.t.get_histogram(interval, 'site', **kwargs),
            provider_id=provider_id
        )

    def get_for_5min(self, **kwargs):
        """
        Данные для отрезка в пять минут, пяааать минууууут.
        """
        return self.get_for_interval('5min', **kwargs)

    def get_for_hour(self, **kwargs):
        """
        Данные для календарного часа.
        """
        return self.get_for_interval('hour', **kwargs)

    def get_for_day(self, **kwargs):
        """
        Данные для календарного дня.
        """
        return self.get_for_interval('day', **kwargs)

    def get_for_all_days(self, **kwargs):
        """
        Данные за все дни, в которые собиралась статистика.
        :rtype: list[(int, dict[str, int)]
        """
        provider_id = kwargs.pop('provider_id', None)
        ts_list = self.t.get_all_buckets('day', 'site', **kwargs)
        return [
            (
                ts, self._result(
                    self.t.get_histogram('day', 'site', ts=ts),
                    provider_id=provider_id
                )
            )
            for ts in sorted(ts_list)
        ]


class ActiveItemsStats(object):
    """Special stats for tracking adding/removing active banners
    and sources.

    """

    def __init__(self, redis_db):
        self.t = RedisTimeseries(redis_db, 'active_items',
                                 {'5min': {'step': '5m'}})

    def _register_items(self, item_type, ids, ts, **kwargs):
        self.t.bulk_update_histogram(item_type, [int(x) for x in ids],
                                     ts=ts, **kwargs)

    def register_banners(self, ids, **kwargs):
        """Registers active banners ids, added active banners ids and
        removed active banners ids.
        
        """
        self._register_items('banners', ids, **kwargs)

    def register_sources(self, ids, **kwargs):
        """Registers active sources ids, added active sources ids and
        removed active sources ids.
        
        """
        self._register_items('sources', ids, **kwargs)

    def _only_latest(self, hist):
        if not hist:
            return []

        latest_value = max(hist.values())
        return [id for id, value in hist.items()
                if value == latest_value]

    def _get_items_for_day(self, item_type, start_ts):
        # Starts with latest five minutes of the previous day
        # because we need to calculate diffs:
        start_ts = int(start_ts - 300)
        end_ts = start_ts + 24 * 60 * 60 + 300
        for ts in range(start_ts, end_ts, 300):
            items = set(self._only_latest(
                self.t.get_histogram('5min', item_type, ts=ts)))
            # Don't yields for the previous day:
            if not start_ts == ts:
                yield {'ts': ts,
                       'added': items - previous,
                       'removed': previous - items,
                       'count': len(items)}
            previous = set(items)

    def get_banners_for_day(self, start_ts):
        """Returns {ts, added, removed} for active banners for each 5 minutes
        for day.

        """
        return list(self._get_items_for_day('banners', start_ts))

    def get_sources_for_day(self, start_ts):
        """Returns {ts, added, removed} for active sources for each 5 minutes
        for day.

        """
        return list(self._get_items_for_day('sources', start_ts))
