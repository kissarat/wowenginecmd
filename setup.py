import sys
from setuptools import setup
from setuptools.command.test import test as test_command


class PyTest(test_command):
    user_options = [('pytest-args=', 'a', "Arguments to pass to py.test")]

    def initialize_options(self):
        test_command.initialize_options(self)
        self.pytest_args = ['tests']

    def finalize_options(self):
        test_command.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        #import here, cause outside the eggs aren't loaded
        import pytest
        errno = pytest.main(self.pytest_args)
        sys.exit(errno)


setup(
    name='wowengine',
    version='0.1',
    packages=['wowengine'],
    url='http://wow-impulse.ru',
    install_requires=[
        # nicer REPL
        'IPython',
        'pgmagick',
        # parse php data from legacy db
        'phpserialize',
        'PyMySQL',
        'redis',
        'hiredis',
        'Flask-Cache',
        # commands for manage.py
        'Flask-Script',
        # ORM
        'Flask-SQLAlchemy',
        # needed for flask-restless, knows how to serialise decimal
        'simplejson',
        # make REST API automatically
        'Flask-Restless',
        'Flask-Migrate',
        'jsmin',
        'cssmin',
        # bundle js and css into one file
        'Flask-Assets',
        'Flask-Login',
        'celery[redis]',
        'flask-debugtoolbar',
    ],
    tests_require=[
        'Flask-Testing',
        'pytest',
        'pytest-dbfixtures'
    ],
    cmdclass={
        'test': PyTest
    },
    author='sgt',
    author_email='sgt.sgt@gmail.com'
)
