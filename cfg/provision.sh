#! /usr/bin/env bash

MYSQL_PASS=root
FILE='2015-01-14.dump'

echo -e "\n--- Okay, upgrading and installing now libs, python, virtualenv, redis, node... ---\n"

sudo apt-get -qq update
sudo apt-get upgrade -y

sudo apt-get install -y git sed build-essential python-software-properties curl language-pack-ru
sudo apt-get install -y libgraphicsmagick++1-dev libgraphicsmagick++1-dev nodejs python3-dev libboost-python-dev redis-server npm python-virtualenv python3-pip

sudo locale-gen
sudo dpkg-reconfigure locales
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

echo -e "\n--- MySQL server with client ---\n"

echo "mysql-server mysql-server/root_password password $MYSQL_PASS" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $MYSQL_PASS" | debconf-set-selections
sudo apt-get install -y mysql-server mysql-client

echo -e "\n--- Stop mysql ---\n"

sudo service mysql stop

#-------------------------------------------------------------------------------------------------------------------------------
echo -e "\n--- Let's start modify configs! ---\n"

#MySQL
echo -e "\n--- MySQL modifying configs ---\n"
if [ $(cat /etc/mysql/my.cnf | grep key_buffer_size | wc -c) = 0 ]; then
        sudo sed -i 's/key_buffer/key_buffer_size/g' /etc/mysql/my.cnf
fi

if [ $(cat /etc/mysql/my.cnf | grep myisam-recover-options | wc -c) = 0 ]; then
        sudo sed -i 's/myisam-recover/myisam-recover-options/g' /etc/mysql/my.cnf
fi

sudo sed -i '/default-character-set/d' /etc/mysql/my.cnf
sudo sed -i '/skip-character-set-client-handshake/d' /etc/mysql/my.cnf
sudo sed -i '/collation-server/d' /etc/mysql/my.cnf
sudo sed -i '/init-connect/d' /etc/mysql/my.cnf
sudo sed -i '/character-set-server/d' /etc/mysql/my.cnf

sudo sed -i 's/\[client\]/\[client\]\ndefault-character-set=utf8/g' /etc/mysql/my.cnf

sudo sed -i 's/\[mysql\]/\[mysql\]\ndefault-character-set=utf8/g' /etc/mysql/my.cnf

sudo sed -i 's/\[mysqld\]/\[mysqld\]\nskip-character-set-client-handshake/g' /etc/mysql/my.cnf
sudo sed -i 's/\[mysqld\]/\[mysqld\]\ncollation-server = utf8_general_ci/g' /etc/mysql/my.cnf
sudo sed -i "s/\[mysqld\]/\[mysqld\]\ninit-connect='SET NAMES utf8'/g" /etc/mysql/my.cnf
sudo sed -i 's/\[mysqld\]/\[mysqld\]\ncharacter-set-server = utf8/g' /etc/mysql/my.cnf

echo -e "\n--- Redis modifying configs ---\n"

sudo echo "unixsocket /tmp/redis.sock" >> /etc/redis/redis.conf
sudo echo "unixsocketperm 777" >> /etc/redis/redis.conf
sudo sed -i 's/#\?\s*bind.*/# bind 127.0.0.1/g' /etc/redis/redis.conf
sudo sed -i 's/#\?\s*port.*/# port 6379/g' /etc/redis/redis.conf

echo -e "\n--- Start services mysql and redis ---\n"

sudo service redis-server restart
sudo service mysql start

echo -e "\n--- Download end extract old DB ---\n"

#wget -P /tmp/ http://wow-impulse.ru/static/data.sql.gz > /dev/null 2>&1

mysql -u root -p"$MYSQL_PASS" -e 'create database wow CHARACTER SET utf8 COLLATE utf8_general_ci'
mysql -u root -p"$MYSQL_PASS" -e 'create database wowimpulse CHARACTER SET utf8 COLLATE utf8_general_ci'
mysql -u root -p"$MYSQL_PASS" -e 'create database wow_test CHARACTER SET utf8 COLLATE utf8_general_ci'

#zcat /tmp/data.sql.gz | mysql -u root -p"$MYSQL_PASS" wowimpulse

echo -e "\n--- Set Python3 as default and return nodejs to node ---\n"

alias python=python3
sudo ln -s /usr/bin/nodejs /usr/bin/node

echo -e "\n--- Python virtualenv ---\n"

sudo mkdir /var/py3
sudo chown -R vagrant /var/py3
virtualenv -p /usr/bin/python3.4 --no-site-packages /var/py3
source /var/py3/bin/activate
sudo npm install -g gulp > /dev/null 2>&1
cd /vagrant
pip install -r requirements.txt > /dev/null 2>&1
npm install
gulp > /dev/null 2>&1

echo -e "\n--- Create configs ---\n"

cp /vagrant/cfg/profile_local.py /vagrant/wowengine/config/
ln -sf profile_local.py /vagrant/wowengine/config/profile.py

echo -e "\n--- Migrate and convert DB ---\n"

python manage.py db upgrade
#python manage.py import_legacy_db

#echo -e "\n--- Start server! ---\n"
#python manage.py runserver -h 0.0.0.0

# После перезагрузки виртуальной машины выполнить команды:
# cd /vagrant
#source ../var/py3/bin/activate
#python manage.py runserver -h 0.0.0.0

echo -e "\n--- ALL DONE !!! ---\n"
