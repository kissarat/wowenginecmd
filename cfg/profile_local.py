from .base import BaseConfig

class Config(BaseConfig):
    """
    Конфигурация для разработки.
    """
    DEBUG = True
    ASSETS_DEBUG = True
    CSRF_ENABLED = False

    # если не найден файл с картинкой, редиректить на главный сервер
    # нужно только на копии разработчика, чтобы не было битых картинок на локальном сайте
    FALLBACK_TO_PRODUCTION_IMAGES = True
    PRODUCTION_HOSTNAME = 'turbo.wow-impulse.ru'
    SERVER_NAME = 'localhost:8080'

    ADMIN_USERS = BaseConfig.ADMIN_USERS + [('foo', 'bar')]

    DEBUG_TB_INTERCEPT_REDIRECTS = False

    JSONIFY_PRETTYPRINT_REGULAR = False

    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:,judtkbrbdctvjueo@localhost/wow?charset=utf8'
    SQLALCHEMY_LEGACY_DATABASE_URI = 'mysql+pymysql://root:,judtkbrbdctvjueo@localhost/wowimpulse?charset=utf8'
    
    REDIS_UNIX_SOCKET = '/tmp/redis.sock'

    S_P = True  # Config flag for Special Project (Used in templates)

