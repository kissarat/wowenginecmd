#!/usr/bin/env python3
from manager_commands import create_manager
from wowengine.config import config

if __name__ == '__main__':
    create_manager(cfg=config).run()
