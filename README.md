# An engine for SJ's websites

## Компоненты

* Веб-приложение написано на языке Python на основе фреймворка Flask.
* Для редакторского интерфейса используется Backbone + Marionette.
* Контент (материалы, информация об источниках и rss-лентах) хранится в БД Mysql, в более упрощенном виде,
  чем на старом вауимпульсе.
* Статистика и другая оперативная информация (обучающиеся настройки показа / аукционы) хранятся в Redis.
* Периодические задачи запускаются изнутри приложения через Celery (а не через cron, его не надо использовать).

## Установка

### Первичная установка в режиме разработки

Что должно быть на машине:

    $ python -V
    Python 3.4.0
    $ npm --version
    1.4.28
    $ celery worker --version
    3.1.13 (Cipater)
    $ redis-server --version
    Redis server v=2.8.15

Для Ubuntu необходимо выполнить:

    $ add-apt-repository ppa:chris-lea/node.js
    $ apt-get update
    $ apt-get install libgraphicsmagick++1-dev libgraphicsmagick++1-dev nodejs python3-dev libboost-python-dev
    
 Для запуска нужно выполнить следующие команды (установка зависимостей требует не менее 2Гб ОЗУ):

    $ virtualenv --no-site-packages /path/to/py3env
    $ source /path/to/py3env/bin/activate
    $ npm install -g gulp
    $ cd /path/to/wowengine
    $ pip install -r requirements.txt
    $ npm install
    $ gulp
    $ python manage_dev.py runserver

При необходимости - переключиться на конфигурацию для разработки:
    $ ln -sf profile_dev.py wowengine/config/profile.py

Загрузить данные из старой базы в новую (настройки подключений - в `config.py`)

    $ python manage.py import_legacy_db

Создание root-пользователя и всех ролей происходит посредством:

    $ python manage.py create_users_and_roles

Так как большая часть клиентского кода написана на CoffeeScript, для разработки лучше использовать
средства, которые умеют автоматически компилировать .coffee -> .js (PyCharm умеет). Для сервера
и вообще командной строки можно компилировать командой `gulp`.

### Обновление на сервере

Продакшен-версия бежит под uwsgi-демоном, который (для динамических данных) проксируется через nginx.
Каждый раз, когда код обновляется на сервере (например, через `git pull`), нужно проделывать следующее:

Если были изменения в схеме БД, прогнать майгрейшен

    $ python manage.py db upgrade

Скомпилировать coffeescript в javascript

    $ gulp

Перезапустить uwsgi

    $ service uwsgi restart

### Прогнать юнит-тесты

    python setup.py test

## Архитектура

### Работа с БД

Работа с MySQL ведется через ORM-библиотеку SQLAlchemy, а точнее - через ее плагин Flask-SQLAlchemy. Классы моделей для
работы с БД-объектами собраны в `models.py`.

Работа с Redis ведется через классы, собранные в `stats.py`. Многие из классов используют библиотеку
`redis_timeseries.py`, которая позволяет хранить временную статистику по интервалам.

### Кэширование

Результаты выполнения некоторых функций кэшируются с помощью модуля Flask-Cache. Кэш ведется в Redis в базе номер 2:

    $ redis-cli -n 2 keys "*"
    
В основном кэшируются результаты выполнения запросов к MySQL, данные в которых обновляются не так часто.
Если операции по изменению данных производятся через REST API (например, через админку), то соответствующие записи
в кэше перегружаются автоматически. Про кэш полезно знать только если данные в MySQL обновляются вручную. В таком
случае можно стереть существующий кэш одним из следующих способов:

    $ redis-cli -n 2 flushdb
    
или

    $ python manage.py cache_reset

### Cron

Синхронизировать директорию картинок:

    */2 * * * * /usr/bin/rsync -rLptvh --exclude "*-*" www@wow-impulse:~/htdocs/wowengine/wowengine/static/images /www/wowimpulse/wowengine/wowengine/static >/dev/null 2>/dev/null

    */5 * * * * /www/wowimpulse/py3env/bin/python /www/wowimpulse/wowengine/manage.py fetch_and_register_liveinternet_stats >/dev/null 2>/dev/null

### Файлы конфигурации

В директории wowengine/config хранится базовая конфигурация в файле base.py, и настройки, специфичные для различных профилей использования, на данный момент - продакшн и разработчиские профили.

Профиль по умолчанию - продакшн. Чтобы переключиться на другой профиль, необходимо создать симлинк profile.py, указывающий на соответствующий профил (файл profile_*.py)

Если существующие профили вам не подходят - не рекомендуется их менять, вместо этого лучше сделать отдельный профиль, например profile_local.py, в котором перечислить нужные настройки. Например:

```
from .profile_dev import Config as DevConfig

class Config(DevConfig):
    SQLALCHEMY_DATABASE_URI = '.....'

```
    $ ln -sf profile_local.py wowengine/config/profile.py

