gulp = require 'gulp'
gutil = require 'gulp-util'

sourcemaps = require 'gulp-sourcemaps'
coffeelint = require 'gulp-coffeelint'
coffee = require 'gulp-coffee'
uglify = require 'gulp-uglify'
rename = require "gulp-rename"
rimraf = require 'gulp-rimraf'
jasminePhantomJs = require('gulp-jasmine2-phantomjs')

sources =
  coffee: ['wowengine/frontend/static/js/**/*.coffee', 'wowengine/admin/static/js/**/*.coffee']
  compiled_dirs: 'wowengine/**/compiled'

tests = 'tests/**/*.coffee'

gulp.task 'lint', ->
  gulp.src(sources.coffee)
  .pipe(coffeelint({max_line_length: {value: 120}}))
  .pipe(coffeelint.reporter())

gulp.task 'coffee', ->
  gulp.src(sources.coffee, {base: 'wowengine/'})
  .pipe(sourcemaps.init())
  .pipe(coffee({bare: true}).on 'error', ->
    gutil.log.apply(gutil, arguments)
    @end()
  )
  #.pipe(concat('app.js'))
  .pipe(uglify())
  .pipe(sourcemaps.write({sourceRoot: '..'}))
  .pipe(rename (path) ->
      path.dirname += '/compiled'
      return null)
  .pipe(gulp.dest('wowengine'))

gulp.task 'clean', ->
  gulp.src(sources.compiled_dirs)
  .pipe(rimraf())

gulp.task 'test', ->
    gulp.src(tests)
    .pipe(jasminePhantomJs())

gulp.task 'watch', ->
  gulp.watch(sources.coffee, ['coffee'])

gulp.task 'default', ['coffee']
